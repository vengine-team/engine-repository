// D3D12HeapTest.cpp : Defines the entry point for the console application.
//

#include <engineEntry.h>
#include <string>
#include <iostream>
#include <array>
#include <windowsUtil.h>
#include <eng_D3D12_Device.h>

#define STRINGIFY2(X) #X
#define STRINGIFY(X) STRINGIFY2(X)

std::chrono::steady_clock::time_point t0 = std::chrono::steady_clock::now();

void reset_clock()
{
	t0 = std::chrono::steady_clock::now();
}
void report_duration(const std::string& message)
{
	std::chrono::steady_clock::time_point t1 = std::chrono::steady_clock::now();

	std::cout << message << std::chrono::duration_cast<std::chrono::milliseconds>(t1 - t0).count() << " ms" << std::endl;
}
#define TEST_COUNT 10000

constexpr UINT64 c_buffer_size_in_bytes = 12;

D3D12_RESOURCE_DESC resourceDesc = CD3DX12_RESOURCE_DESC::Buffer(c_buffer_size_in_bytes);
void test_default_commited()
{

	std::cout << "Default Committed Resources:" << std::endl;



	std::array<Microsoft::WRL::ComPtr<ID3D12Resource>, TEST_COUNT> committedResources;

	reset_clock();
	for (size_t i = 0; i < TEST_COUNT; i++)
	{
		AssertIfFailed
		(
			eng::D3D12::instance().getDevice()->CreateCommittedResource
			(
				&CD3DX12_HEAP_PROPERTIES(D3D12_HEAP_TYPE_DEFAULT),
				D3D12_HEAP_FLAG_NONE,
				&resourceDesc,
				D3D12_RESOURCE_STATE_GENERIC_READ,
				nullptr,
				IID_PPV_ARGS(&committedResources[i])
			),
			"Create Placed Resource Failed"
		);
	}
	report_duration("creating committed Resources: ");


	reset_clock();
	for (size_t i = 0; i < TEST_COUNT; i++)
	{
		committedResources[i].Reset();
	}
	report_duration("destroying committed Resources: ");

}

void test_upload_commited()
{

	std::cout << "Upload Commited Resources:" << std::endl;



	std::array<Microsoft::WRL::ComPtr<ID3D12Resource>, TEST_COUNT> committedResources;

	reset_clock();
	for (size_t i = 0; i < TEST_COUNT; i++)
	{
		AssertIfFailed
		(
			eng::D3D12::instance().getDevice()->CreateCommittedResource
			(
				&CD3DX12_HEAP_PROPERTIES(D3D12_HEAP_TYPE_UPLOAD),
				D3D12_HEAP_FLAG_NONE,
				&resourceDesc,
				D3D12_RESOURCE_STATE_GENERIC_READ,
				nullptr,
				IID_PPV_ARGS(&committedResources[i])
			),
			"Create Placed Resource Failed"
		);
	}
	report_duration("creating commited Resources: ");


	reset_clock();
	for (size_t i = 0; i < TEST_COUNT; i++)
	{
		D3D12_RANGE range = { 0,0 };
		void* pMappedMemory;
		committedResources[i]->Map(0, &range, &pMappedMemory);
	}
	report_duration("mapping commited Resources: ");



	reset_clock();
	for (size_t i = 0; i < TEST_COUNT; i++)
	{
		committedResources[i].Reset();
	}
	report_duration("destroying commited Resources: ");

}

void test_placed()
{

	std::cout << "Placed Resources:" << std::endl;

	D3D12_HEAP_DESC heapDesc = {};
	heapDesc.Alignment = D3D12_DEFAULT_RESOURCE_PLACEMENT_ALIGNMENT;
	heapDesc.Properties = CD3DX12_HEAP_PROPERTIES(D3D12_HEAP_TYPE_UPLOAD);
	heapDesc.Flags = D3D12_HEAP_FLAG_DENY_RT_DS_TEXTURES | D3D12_HEAP_FLAG_DENY_NON_RT_DS_TEXTURES;
	heapDesc.SizeInBytes = c_buffer_size_in_bytes;


	std::array<Microsoft::WRL::ComPtr<ID3D12Heap>, TEST_COUNT> heaps;

	reset_clock();
	for (size_t i = 0; i < TEST_COUNT; i++)
	{
		AssertIfFailed
		(
			eng::D3D12::instance().getDevice()->CreateHeap
			(
				&heapDesc,
				IID_PPV_ARGS(&heaps[i])
			),
			"Create Heap Failed"
		);
	}
	report_duration("Creating Heaps: ");



	std::array<Microsoft::WRL::ComPtr<ID3D12Resource>, TEST_COUNT> placedResources;

	reset_clock();
	for (size_t i = 0; i < TEST_COUNT; i++)
	{
		AssertIfFailed
		(
			eng::D3D12::instance().getDevice()->CreatePlacedResource
			(
				heaps[i].Get(),
				0,
				&resourceDesc,
				D3D12_RESOURCE_STATE_GENERIC_READ,
				nullptr,
				IID_PPV_ARGS(&placedResources[i])
			),
			"Create Placed Resource Failed"
		);
	}
	report_duration("placing Resources: ");



	reset_clock();
	for (size_t i = 0; i < TEST_COUNT; i++)
	{
		D3D12_RANGE range = { 0,0 };
		void* pMappedMemory;
		placedResources[i]->Map(0, &range, &pMappedMemory);
	}
	report_duration("mapping Resources Heaps: ");



	reset_clock();
	for (size_t i = 0; i < TEST_COUNT; i++)
	{
		heaps[i].Reset();
	}
	report_duration("destroying Heaps: ");

}

void test_allocator()
{
	std::cout << "Placed Logarithmic Dynamic Pool Allocator Resources:" << std::endl;



	std::array<typename eng::D3D12::ConstantBufferAllocator::ptr, TEST_COUNT> heaps;

	reset_clock();
	for (size_t i = 0; i < TEST_COUNT; i++)
	{
		heaps[i] = eng::D3D12::instance().constantBufferAllocator().allocate(256);
	}
	report_duration("Allocating Heap: ");



	reset_clock();
	for (size_t i = 0; i < TEST_COUNT; i++)
	{
		eng::D3D12::instance().constantBufferAllocator().deallocate(heaps[i]);
	}
	report_duration("Deallocating Heap: ");
}

void test_upload_heap_placed_resource_allocator()
{
	std::array<typename eng::D3D12::BufferAllocator::ptr, TEST_COUNT> heaps;

	reset_clock();
	for (size_t i = 0; i < TEST_COUNT; i++)
	{
		heaps[i] = eng::D3D12::instance().uploadBufferAllocator().allocate(256);
	}
	report_duration("Allocating Heap: ");




	std::array<Microsoft::WRL::ComPtr<ID3D12Resource>, TEST_COUNT> placedResources;

	reset_clock();
	for (size_t i = 0; i < TEST_COUNT; i++)
	{
		AssertIfFailed
		(
			eng::D3D12::instance().getDevice()->CreatePlacedResource
			(
				heaps[i].base_ptr.base_ptr.pHeap,
				heaps[i].base_ptr.base_ptr.offset,
				&resourceDesc,
				D3D12_RESOURCE_STATE_GENERIC_READ,
				nullptr,
				IID_PPV_ARGS(&placedResources[i])
			),
			"Create Placed Resource Failed"
		);
	}
	report_duration("placing Resources: ");


	reset_clock();
	for (size_t i = 0; i < TEST_COUNT; i++)
	{
		placedResources[i].Reset();
	}
	report_duration("releasing Resources: ");




	reset_clock();
	for (size_t i = 0; i < TEST_COUNT; i++)
	{
		eng::D3D12::instance().uploadBufferAllocator().deallocate(heaps[i]);
	}
	report_duration("Deallocating Heap: ");
}

void test_default_placed_resource_allocator()
{
	std::array<typename eng::D3D12::BufferAllocator::ptr, TEST_COUNT> heaps;

	reset_clock();
	for (size_t i = 0; i < TEST_COUNT; i++)
	{
		heaps[i] = eng::D3D12::instance().defaultBufferAllocator().allocate(256);
	}
	report_duration("Allocating Heap: ");




	std::array<Microsoft::WRL::ComPtr<ID3D12Resource>, TEST_COUNT> placedResources;

	reset_clock();
	for (size_t i = 0; i < TEST_COUNT; i++)
	{
		AssertIfFailed
		(
			eng::D3D12::instance().getDevice()->CreatePlacedResource
			(
				heaps[i].base_ptr.base_ptr.pHeap,
				heaps[i].base_ptr.base_ptr.offset,
				&resourceDesc,
				D3D12_RESOURCE_STATE_GENERIC_READ,
				nullptr,
				IID_PPV_ARGS(&placedResources[i])
			),
			"Create Placed Resource Failed"
		);
	}
	report_duration("placing Resources: ");


	reset_clock();
	for (size_t i = 0; i < TEST_COUNT; i++)
	{
		placedResources[i].Reset();
	}
	report_duration("releasing Resources: ");




	reset_clock();
	for (size_t i = 0; i < TEST_COUNT; i++)
	{
		eng::D3D12::instance().defaultBufferAllocator().deallocate(heaps[i]);
	}
	report_duration("Deallocating Heap: ");
}

void test_command_lists_creation_speed(D3D12_COMMAND_LIST_TYPE type)
{
	reset_clock();

	Microsoft::WRL::ComPtr<ID3D12CommandAllocator> pAllocator;
	eng::D3D12::instance().getDevice()->CreateCommandAllocator(type, IID_PPV_ARGS(&pAllocator));

	std::vector<Microsoft::WRL::ComPtr<ID3D12GraphicsCommandList>> lists;

	for (size_t i = 0; i < 100; i++)
	{
		lists.push_back({});
		eng::D3D12::instance().getDevice()->CreateCommandList(0, type, pAllocator.Get(), nullptr, IID_PPV_ARGS(&lists.back()));
		lists.back()->Close();
	}
	pAllocator.Reset();

	report_duration("Creating Command Lists: ");
}

int engMain()
{
	D3D12_RESOURCE_ALLOCATION_INFO info = eng::D3D12::instance().getDevice()->GetResourceAllocationInfo(0, 1, &resourceDesc);
	test_default_commited();
	std::cout << std::endl;
	test_upload_commited();
	std::cout << std::endl;
	test_placed();
	std::cout << std::endl;
	test_allocator();
	std::cout << std::endl;
	test_upload_heap_placed_resource_allocator();
	std::cout << std::endl;
	test_default_placed_resource_allocator();
	std::cout << std::endl;

	/*std::cout << std::endl;
	test_command_lists_creation_speed(D3D12_COMMAND_LIST_TYPE_DIRECT);
	std::cout << std::endl;
	test_command_lists_creation_speed(D3D12_COMMAND_LIST_TYPE_BUNDLE);
	std::cout << std::endl;*/

	reset_clock();

	while (true)
	{

	}

    return 0;
}

