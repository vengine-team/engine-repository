#include "ExampleCube.h"

ExampleCube::ExampleCube(const eng::Material3f* pMaterial)
{
	set_material(pMaterial);
	set_mesh(eng::resources::flatCubeMesh());
}
