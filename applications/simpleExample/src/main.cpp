#include <eng/engineEntry.h>

#include <eng/Window.h>

#include <CameraNode3D.h>
#include <ExampleCube.h>

double rotation = 0;
std::unique_ptr<ExampleCube> pExampleCube = nullptr;
std::unique_ptr<eng::tsg::Scene> pMainScene;

void onUpdate()
{
	rotation += 0.3 * (1 / pMainScene->getTargetFrameRate());

	pExampleCube->set_local_transform({ gtm::transform(gtm::angle_axisd{ rotation, gtm::vector3d{ 0, 1, 0 } }, gtm::identity_matrix<double, 3, 3>()), gtm::vector3d{0,0,0} });
}
ext::event_handler<ext::functor<onUpdate>> updateEventHandler = { {} };

//engMain is a platform independent entry point
int engMain()
{
	//loading resources before doing anything else
	eng::resources::load_resources();

	eng::Window mainWindow(L"Simple Example", { 20,20 }, { 300,300 }, eng::WindowStyle::SystemDefaultMenu, true);

	//Scene3D Object is a cartesian coordinate system that is updateable, Node3D Objects can be added to the coordinate system
	pMainScene = std::make_unique<eng::tsg::Scene>();

	std::unique_ptr<CameraNode3D> pMainCameraNode3D = std::make_unique<CameraNode3D>();
	pMainCameraNode3D->set_scene(pMainScene.get());
	pMainCameraNode3D->set_local_transform({ gtm::transform(gtm::angle_axisd{ atan(-1.0 / 2.0), gtm::vector3d{-1,0,0} }, gtm::identity_matrix<double, 3,3>()), gtm::vector3d{0,1,-2} });
	pMainCameraNode3D->set_renderTarget(&mainWindow.renderTarget());

	pMainScene->updateEvent() += updateEventHandler;

	pExampleCube = std::make_unique<ExampleCube>(eng::resources::sampleTextureMaterial());
	pExampleCube->set_scene(pMainScene.get());
	pExampleCube->set_local_transform({gtm::identity_matrix<double, 3,3>(), gtm::vector3d::zero() });

	mainWindow.open();

	while (true)
	{
		mainWindow.fetch();

		//updating at 60 frames per second
		std::chrono::steady_clock::time_point currentFrameBegin = std::chrono::steady_clock::now();
		std::chrono::steady_clock::time_point nextFrameBegin = currentFrameBegin + std::chrono::milliseconds(1000 / 60);

		//the scene is given a time point when it should return, the scene will try its best to return before that time point
		pMainScene->updateFrame(nextFrameBegin);

		std::this_thread::sleep_until(nextFrameBegin);
	}

	return 0;
}