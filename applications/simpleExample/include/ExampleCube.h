#pragma once
#include <eng/tsg/Node3.h>
#include <eng/tsg/MeshRenderer3.h>
#include <eng/resources/default.h>

class ExampleCube : public eng::tsg::add_components<eng::tsg::Node3d, eng::tsg::MeshRenderer3>
{
public:
	ExampleCube(const eng::Material3f* pMaterial);
};