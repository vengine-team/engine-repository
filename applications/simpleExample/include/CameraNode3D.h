#pragma once
#include <eng/tsg/Node3.h>
#include <eng/tsg/Camera3.h>

class CameraNode3D : public eng::tsg::add_components<eng::tsg::Node3d, eng::tsg::Camera3>
{
public:
	CameraNode3D();
};