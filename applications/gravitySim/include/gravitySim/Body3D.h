#pragma once
#include <eng/tsg/Node3.h>
#include <gravitySim/Physics.h>
#include <gravitySim/Mass.h>

namespace gravitySim
{
	class Body3d : public Mass, public eng::tsg::Node3d
	{
		gtm::vector3d m_momentum = { 0.0, 0.0, 0.0 };

		ext::ticket m_moduleTicket = {};

	public:

		///
		/// staged force will be added to momentum,
		/// Node3D will be repositioned by momentum
		/// staged force will be set to (0,0,0)
		///
		void advance(double seconds);

		void set_scene(eng::tsg::Scene* pScene) override;

		const gtm::vector3d& momentum();

		void add_momentum(const gtm::vector3d& force);
	};
};