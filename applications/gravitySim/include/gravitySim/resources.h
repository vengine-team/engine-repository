#pragma once
#include <eng/Material.h>
#include <eng/Font.h>
#include <eng/defaultDraw.h>


namespace gravitySim
{
	namespace resources
	{
		void load_resources();

		const eng::Font*			font();

		const eng::DeviceTexture2*	starSprite();

		const eng::Material2F*		fontMaterial();
		const eng::Material2F*		textureMaterial();

		const eng::Material3f*		billboardMaterial();
		const eng::Material2F*		backgroundMaterial();
		const eng::Material2F*		outlineMaterial();
		const eng::Material2F*		highlightedMaterial();
		const eng::Material2F*		pressedMaterial();
	}
}