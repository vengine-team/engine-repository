#pragma once
#include <eng/gpl/Forward_Node.h>
#include <eng/gpl/MenuBar.h>
#include <eng/gpl/Control_ioFrame.h>
#include <eng/gpl/SplitBox.h>
#include <eng/control/default_draw.h>

#include <gravitySim/UpdateThreadView.h>

#include <gravitySim/Scene3D.h>
#include <gravitySim/StellarObject3D.h>

#include <par/UpdateThread.h>


namespace gravitySim
{
	class MainControl : public eng::gpl::ControlParent<eng::gpl::Control>
	{
		eng::gpl::MenuBar*									m_pTestMenu = nullptr;

		eng::gpl::SplitBox_System							m_splitBoxSystem;
		eng::gpl::SplitBox_Tree								m_splitBoxTree = { m_splitBoxSystem };

		struct updateThread_pair
		{
			UpdateThreadView updateThreadView;
			eng::gpl::SplitBox_Element splitBox_element = { updateThreadView };

			ext::event_handler<ext::function_ptr<void(MainControl::*)(UpdateThreadView&)>> m_on_request_UpdateThreadView_removed;
			ext::event_handler<ext::function_ptr<void(MainControl::*)(UpdateThreadView&, SceneView&, size_t)>> m_on_request_UpdateThreadView_scene_move;

			updateThread_pair(ext::function_ptr<void(MainControl::*)(UpdateThreadView&)>& pF, ext::function_ptr<void(MainControl::*)(UpdateThreadView&, SceneView&, size_t)>& pF2);
		};

		std::vector<std::unique_ptr<updateThread_pair>>		m_updateThreadViews;

		void erase_updateThread(UpdateThreadView* pToBeErased);

		void set_move_targets();
		void rename_update_threads();

		void on_add_UpdateThreadView(eng::IButton&);
		ext::event_handler<ext::function_ptr<void(MainControl::*)(eng::IButton&)>> m_on_add_UpdateThreadView = { { &MainControl::on_add_UpdateThreadView, this } };

		UpdateThreadView* m_pTo_be_erased = nullptr;
		void on_request_UpdateThreadView_removed(UpdateThreadView& view);


		SceneView* m_pToBeMoved = nullptr;
		UpdateThreadView* m_pToBeMoved_from = nullptr;
		size_t m_pToBeMoved_where = 0;

		void on_UpdateThreadView_request_move(UpdateThreadView&, SceneView&, size_t);

	public:

		virtual bool onInput(const eng::input_state_change& change, gtm::vector2i offset, bool obstructed);

		virtual void setTextureMaterial(const eng::Material2F* pMaterial) override;

		virtual void localRender(eng::control::LocalGraphics& localGraphics) const override;

		virtual void set_Font(const eng::Font* pFont) override;

		virtual void setScale(gtm::vector2u scale) override;

		MainControl();
		virtual ~MainControl();
	};
}