#pragma once
#include <eng/tsg/Scene.h>

#include <chrono>


namespace gravitySim
{
	class Body3d;

	class IPhysicsAdvancer
	{
	public:
		virtual void advance(double seconds, eng::tsg::Scene * pScene) = 0;
	};

	class DefaultAdvancer : public IPhysicsAdvancer
	{
	public:
		virtual void advance(double seconds, eng::tsg::Scene * pScene) override;
	};

	class PhysicsModule : public eng::tsg::ISceneModule<eng::tsg::Scene>
	{
		std::unique_ptr<IPhysicsAdvancer> m_pAdvancer;

		friend class Body3d;

		ext::ticket_table<Body3d*> m_bodies;

	public:

		const ext::ticket_table<Body3d*>& bodies() const;

		std::unique_ptr<IPhysicsAdvancer>& advancer();
		const IPhysicsAdvancer* advancer() const;

		PhysicsModule();

		virtual void on_late_update(eng::tsg::Scene* pScene) override;
	};

	PhysicsModule * get_PhysicsModule(eng::tsg::Scene* pScene);
	const PhysicsModule* get_PhysicsModule(const eng::tsg::Scene* pScene);

};

#include "Body3D.h"