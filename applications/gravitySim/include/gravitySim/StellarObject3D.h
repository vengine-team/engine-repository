#pragma once
#include <eng/tsg/SpriteRenderer3.h>

#include "Body3D.h"

namespace gravitySim
{

	class StellarObjekt3D : public eng::tsg::add_components<Body3d, eng::tsg::SpriteRenderer3>
	{
	public:

		StellarObjekt3D();
		virtual ~StellarObjekt3D();
	};
}