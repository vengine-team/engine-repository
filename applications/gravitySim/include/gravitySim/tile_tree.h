#pragma once
#include <Windows.h>

#include <type_traits>
#include <array>
#include <memory>
#include <assert.h>
#include <bitset>
#include <optional>

#include <gtm/math.h>

#include <ext/indexed_iterator.h>
#include <ext/range.h>


namespace gravitySim
{
	template<size_t max_value, typename ...candidate_types>
	struct first_with_max_value;
	template<size_t max_value, typename candidate_t>
	struct first_with_max_value<max_value, candidate_t>
	{
		static_assert(max_value <= (std::numeric_limits<candidate_t>::max)());
		using type = candidate_t;
	};
	template<size_t max_value, typename candidate_t, typename ...other_candidate_types>
	struct first_with_max_value<max_value, candidate_t, other_candidate_types...>
	{
		using type = typename std::conditional<(max_value <= (std::numeric_limits<candidate_t>::max)()), candidate_t, typename first_with_max_value<max_value, other_candidate_types...>::type>::type;
	};



	template<typename numeric_t, gtm::dim_t dimension, typename value_t>
	struct position_value_pair
	{
		gtm::vector<numeric_t, dimension> position;
		value_t value;
	};



	template<typename numeric_t, gtm::dim_t dimension, typename leaf_value_t, typename node_value_t, typename index_t>
	struct tile_tree_child_data
	{
		using sector_index_t = typename first_with_max_value<dimension, unsigned char, unsigned short, unsigned int, unsigned long int, unsigned long, unsigned long long>::type;
		sector_index_t m_sector;
		index_t m_parent_node_index;
		using depth_t = typename first_with_max_value<sizeof(numeric_t) * CHAR_BIT, unsigned char, unsigned short, unsigned int, unsigned long int, unsigned long, unsigned long long>::type;
		depth_t m_depth;
		gtm::vector<numeric_t, dimension> m_position;
	};

	template<typename numeric_t, gtm::dim_t dimension, typename leaf_value_t, typename node_value_t, typename index_t>
	struct alignas(8) tile_tree_leaf_element_data : public tile_tree_child_data<numeric_t, dimension, leaf_value_t, node_value_t, index_t>
	{
		index_t m_next;
		leaf_value_t m_value;

		tile_tree_leaf_element_data(const position_value_pair<numeric_t, dimension, leaf_value_t>& pos_val_pair) :
			m_next(0),
			m_value(pos_val_pair.value)
		{
			m_position = pos_val_pair.position;
		}
	};

	template<typename numeric_t, gtm::dim_t dimension, typename leaf_value_t, typename node_value_t, typename index_t>
	struct alignas(8) tile_tree_node_data : public tile_tree_child_data<numeric_t, dimension, leaf_value_t, node_value_t, index_t>
	{
		std::array<index_t, gtm::pow(2, dimension)> m_sector_indices;
		std::bitset<gtm::pow(2, dimension)> m_sector_is_node_flags;
		node_value_t m_value;
		numeric_t m_scale;

		tile_tree_node_data(const node_value_t& value) : m_value(value)
		{
			for (auto& i : m_sector_indices)
			{
				i = 0;
			}
		}
	};

	template<typename numeric_t, gtm::dim_t dimension, typename leaf_value_t, typename node_value_t, typename index_t>
	class tile_tree;

	template<typename numeric_t, gtm::dim_t dimension, typename leaf_value_t, typename node_value_t, typename index_t>
	class tile_tree_node;
	
	template<typename numeric_t, gtm::dim_t dimension, typename leaf_value_t, typename node_value_t, typename index_t>
	class tile_tree_leaf;


	template<typename numeric_t, gtm::dim_t dimension, typename leaf_value_t, typename node_value_t, typename index_t>
	class tile_tree_child
	{
		using tile_tree_t = tile_tree<numeric_t, dimension, leaf_value_t, node_value_t, index_t>;
		using node_data_t = tile_tree_node_data<numeric_t, dimension, leaf_value_t, node_value_t, index_t>;
		using leaf_element_data_t = tile_tree_leaf_element_data<numeric_t, dimension, leaf_value_t, node_value_t, index_t>;
		using child_data_t = tile_tree_child_data<numeric_t, dimension, leaf_value_t, node_value_t, index_t>;

		using tile_tree_node_t = tile_tree_node<numeric_t, dimension, leaf_value_t, node_value_t, index_t>;
		using tile_tree_leaf_t = tile_tree_leaf<numeric_t, dimension, leaf_value_t, node_value_t, index_t>;
		using tile_tree_child_t = tile_tree_child<numeric_t, dimension, leaf_value_t, node_value_t, index_t>;

	protected:
	public:
		leaf_element_data_t* m_leaf_element_data_begin;
		node_data_t* m_node_data_begin;

		child_data_t* m_managed;
		bool m_is_node;

	public:
		tile_tree_child(leaf_element_data_t* leaf_element_data_begin, node_data_t* node_data_begin, child_data_t* managed, bool is_node) :
			m_leaf_element_data_begin(leaf_element_data_begin),
			m_node_data_begin(node_data_begin),
			m_managed(managed),
			m_is_node(is_node)
		{

		}
		tile_tree_child()
		{

		}

		const gtm::vector<numeric_t, dimension>& position()
		{
			return m_managed->m_position;
		}

		bool is_node() const
		{
			return m_is_node;
		}

		typename child_data_t::depth_t depth() const
		{
			return m_managed->m_depth;
		}

		tile_tree_node_t as_node();

		tile_tree_leaf_t as_leaf();

		auto sector() const
		{
			return m_managed->m_sector;
		}
		tile_tree_node_t parent();
	};

	template<typename numeric_t, gtm::dim_t dimension, typename leaf_value_t, typename node_value_t, typename index_t>
	class tile_tree_node : public tile_tree_child<numeric_t, dimension, leaf_value_t, node_value_t, index_t>
	{
		using tile_tree_t = tile_tree<numeric_t, dimension, leaf_value_t, node_value_t, index_t>;
		using node_data_t = tile_tree_node_data<numeric_t, dimension, leaf_value_t, node_value_t, index_t>;
		using leaf_element_data_t = tile_tree_leaf_element_data<numeric_t, dimension, leaf_value_t, node_value_t, index_t>;
		using child_data_t = tile_tree_child_data<numeric_t, dimension, leaf_value_t, node_value_t, index_t>;

		using tile_tree_node_t = tile_tree_node<numeric_t, dimension, leaf_value_t, node_value_t, index_t>;
		using tile_tree_leaf_t = tile_tree_leaf<numeric_t, dimension, leaf_value_t, node_value_t, index_t>;
		using tile_tree_child_t = tile_tree_child<numeric_t, dimension, leaf_value_t, node_value_t, index_t>;

	public:
		tile_tree_node(leaf_element_data_t* leaf_element_data_begin, node_data_t* node_data_begin, node_data_t* managed) : 
			tile_tree_child_t(leaf_element_data_begin, node_data_begin, managed, true)
		{

		}
		tile_tree_node()
		{

		}

		tile_tree_child<numeric_t, dimension, leaf_value_t, node_value_t, index_t> at_sector(gtm::vector<size_t, dimension> sector_coordinates) const
		{
			size_t index = 0;
			for (size_t i = 0; i < dimension; i++)
			{
				index += sector_coordinates[i] * eng::pow(2, i);
			}
			return m_children[index];
		}

		auto sectors() const
		{
			struct index_to_child_functor
			{
				tile_tree_child_t capture;

				std::optional<tile_tree_child_t> operator()(size_t i) const
				{
					//OutputDebugString(L"Node[");
					//OutputDebugString(std::to_wstring(m_managed->m_sector).c_str());
					//OutputDebugString(L"](");
					//OutputDebugString(std::to_wstring(static_cast<node_data_t*>(m_managed) - m_node_data_begin).c_str());
					//OutputDebugString(L"), ");
					//OutputDebugString(std::to_wstring(depth()).c_str());
					//OutputDebugString(L" -> [");

					//OutputDebugString(std::to_wstring(i).c_str());
					//OutputDebugString(L"]\n");
					node_data_t* managed = static_cast<node_data_t*>(capture.m_managed);

					if (managed->m_sector_indices[i] == 0)
					{
						return {};
					}

					child_data_t* child_data;
					if (managed->m_sector_is_node_flags[i])
					{
						child_data = capture.m_node_data_begin + managed->m_sector_indices[i];
					}
					else
					{
						child_data = capture.m_leaf_element_data_begin + managed->m_sector_indices[i];
					}

					return tile_tree_child_t
					{
						capture.m_leaf_element_data_begin,
						capture.m_node_data_begin,
						child_data,
						managed->m_sector_is_node_flags[i]
					};
				}
			};

			index_to_child_functor indexed_functor{ *this };
			
			auto begin = ext::make_indexed_iterator(0, indexed_functor);
			auto end = ext::make_indexed_iterator(gtm::pow(2, dimension), indexed_functor);

			return ext::range(begin, end);
		}

		node_value_t& value() const
		{
			return static_cast<node_data_t*>(m_managed)->m_value;
		}
	};

	template<typename numeric_t, gtm::dim_t dimension, typename leaf_value_t, typename node_value_t, typename index_t>
	inline tile_tree_node<numeric_t, dimension, leaf_value_t, node_value_t, index_t> tile_tree_child<numeric_t, dimension, leaf_value_t, node_value_t, index_t>::as_node()
	{
		assert(this->m_is_node);
		return tile_tree_node<numeric_t, dimension, leaf_value_t, node_value_t, index_t>(m_leaf_element_data_begin, m_node_data_begin, static_cast<node_data_t*>(m_managed));
	}

	template<typename numeric_t, gtm::dim_t dimension, typename leaf_value_t, typename node_value_t, typename index_t>
	inline tile_tree_node<numeric_t, dimension, leaf_value_t, node_value_t, index_t> gravitySim::tile_tree_child<numeric_t, dimension, leaf_value_t, node_value_t, index_t>::parent()
	{
		return node_data_t(m_leaf_element_data_begin, m_node_data_begin, static_cast<node_data_t*>(m_node_data_begin[m_managed->m_parent_node_index]));
	}

	template<typename numeric_t, gtm::dim_t dimension, typename leaf_value_t, typename node_value_t, typename index_t>
	class tile_tree_leaf : public tile_tree_child<numeric_t, dimension, leaf_value_t, node_value_t, index_t>
	{
		using tile_tree_t = tile_tree<numeric_t, dimension, leaf_value_t, node_value_t, index_t>;
		using node_data_t = tile_tree_node_data<numeric_t, dimension, leaf_value_t, node_value_t, index_t>;
		using leaf_element_data_t = tile_tree_leaf_element_data<numeric_t, dimension, leaf_value_t, node_value_t, index_t>;
		using child_data_t = tile_tree_child_data<numeric_t, dimension, leaf_value_t, node_value_t, index_t>;

		using tile_tree_node_t = tile_tree_node<numeric_t, dimension, leaf_value_t, node_value_t, index_t>;
		using tile_tree_leaf_t = tile_tree_leaf<numeric_t, dimension, leaf_value_t, node_value_t, index_t>;
		using tile_tree_child_t = tile_tree_child<numeric_t, dimension, leaf_value_t, node_value_t, index_t>;

	public:
		tile_tree_leaf(leaf_element_data_t* leaf_element_data_begin, node_data_t* node_data_begin, leaf_element_data_t* managed) :
			tile_tree_child_t(leaf_element_data_begin, node_data_begin, managed, false)
		{

		}
		tile_tree_leaf()
		{
			
		}

		std::optional<tile_tree_leaf> next() const
		{
			if (static_cast<leaf_element_data_t*>(m_managed)->m_next == 0)
			{
				return std::nullopt;
			}
			return tile_tree_leaf_t
			{ 
				m_leaf_element_data_begin, 
				m_node_data_begin,
				&m_leaf_element_data_begin[static_cast<leaf_element_data_t*>(m_managed)->m_next]
			};
		}

		leaf_value_t& value() const
		{
			return static_cast<leaf_element_data_t*>(m_managed)->m_value;
		}
	};

	template<typename numeric_t, gtm::dim_t dimension, typename leaf_value_t, typename node_value_t, typename index_t>
	inline tile_tree_leaf<numeric_t, dimension, leaf_value_t, node_value_t, index_t> gravitySim::tile_tree_child<numeric_t, dimension, leaf_value_t, node_value_t, index_t>::as_leaf()
	{
		assert(!this->m_is_node);
		return tile_tree_leaf<numeric_t, dimension, leaf_value_t, node_value_t, index_t>(m_leaf_element_data_begin, m_node_data_begin, static_cast<leaf_element_data_t*>(this->m_managed));
	}

	template<typename numeric_t, gtm::dim_t dimension, typename leaf_value_t, typename node_value_t, typename index_t>
	class tile_tree
	{
		using tile_tree_t = tile_tree<numeric_t, dimension, leaf_value_t, node_value_t, index_t>;
		using node_data_t = tile_tree_node_data<numeric_t, dimension, leaf_value_t, node_value_t, index_t>;
		using leaf_element_data_t = tile_tree_leaf_element_data<numeric_t, dimension, leaf_value_t, node_value_t, index_t>;
		using child_data_t = tile_tree_child_data<numeric_t, dimension, leaf_value_t, node_value_t, index_t>;

		using tile_tree_node_t = tile_tree_node<numeric_t, dimension, leaf_value_t, node_value_t, index_t>;
		using tile_tree_leaf_t = tile_tree_leaf<numeric_t, dimension, leaf_value_t, node_value_t, index_t>;
		using tile_tree_child_t = tile_tree_child<numeric_t, dimension, leaf_value_t, node_value_t, index_t>;

		static constexpr typename child_data_t::depth_t c_depth_limit = sizeof(numeric_t) * CHAR_BIT;

		//different constructors will initialize these differently for performance optimization
		node_data_t* m_node_data_begin;
		index_t m_initialized_node_data_size = 1;

		leaf_element_data_t* m_leaf_element_data_begin;
		index_t m_initialized_leaf_element_data_size = 1;

		//always initialized by internal_prepare_size_and_scale function
		numeric_t m_scale;
		size_t m_size;

		//will be used exclusivly by the internal_build_tree function
		index_t m_root_index = 0;
		bool m_root_is_tile;

		typename child_data_t::sector_index_t internal_calculate_sector_coordinates(const gtm::vector<numeric_t, dimension>& center, const gtm::vector<numeric_t, dimension>& pos)
		{
			typename child_data_t::sector_index_t out = 0;

			for (size_t i = 0; i < dimension; i++)
			{
				if (pos[i] > center[i])
				{
					out |= (typename child_data_t::sector_index_t(1) << i);
				}
			}

			return out;
		}

		inline index_t internal_alloc_next_node_data()
		{
			m_node_data_begin[m_initialized_node_data_size] = node_data_t({});
			index_t out = m_initialized_node_data_size;
			m_initialized_node_data_size++;
			return out;
		}

		inline void internal_set_as_root(leaf_element_data_t& leaf_element, index_t index)
		{
			m_root_index = index;
			m_root_is_tile = false;

			leaf_element.m_depth = 0;
			leaf_element.m_parent_node_index = 0;
		}
		inline void internal_set_as_root(node_data_t& node, index_t index)
		{
			m_root_index = index;
			m_root_is_tile = true;

			node.m_depth = 0;
			node.m_parent_node_index = 0;
			node.m_position = gtm::vector<numeric_t, dimension>::zero();
			node.m_scale = m_scale;
		}

		inline void internal_set_child(leaf_element_data_t& leaf_element, index_t leaf_element_index, typename child_data_t::sector_index_t sector, node_data_t& parent, index_t parent_index)
		{
			leaf_element.m_parent_node_index = parent_index;
			leaf_element.m_sector = sector;
			leaf_element.m_depth = parent.m_depth + 1;
			
			parent.m_sector_indices[sector] = leaf_element_index;
			parent.m_sector_is_node_flags[sector] = false;
		}
		inline gtm::vector<numeric_t, dimension> internal_calculate_sector_center(gtm::vector<numeric_t, dimension>& center, numeric_t sector_scale, typename child_data_t::sector_index_t sector_index)
		{
			gtm::vector<numeric_t, dimension> out = center;

			for (size_t i = 0; i < dimension; i++)
			{
				if (sector_index & (1 << i))
				{
					center[i] += sector_scale;
				}
				else
				{
					center[i] -= sector_scale;
				}
			}
			return out;
		}
		inline void internal_set_child(node_data_t& child_node, index_t child_node_index, typename child_data_t::sector_index_t sector, node_data_t& parent, index_t parent_index)
		{
			child_node.m_parent_node_index = parent_index;
			child_node.m_sector = sector;
			child_node.m_depth = parent.m_depth + 1;
			
			child_node.m_scale = parent.m_scale / 2;
			child_node.m_position = internal_calculate_sector_center(parent.m_position, child_node.m_scale, sector);

			parent.m_sector_indices[sector] = child_node_index;
		}

		inline void internal_append_leaf_element(leaf_element_data_t& before_leaf, index_t before_index, leaf_element_data_t& to_append, index_t to_append_index)
		{
			index_t prev_next = before_leaf.m_next;
			before_leaf.m_next = to_append_index;
			to_append.m_next = prev_next;
			
			to_append.m_parent_node_index = before_leaf.m_parent_node_index;
			to_append.m_sector = before_leaf.m_sector;
			to_append.m_depth = before_leaf.m_depth;
		}
	public:
		index_t internal_count_leaf_elements(std::optional<gravitySim::tile_tree_child<numeric_t, 3, leaf_value_t, node_value_t, index_t>> child, int n)
		{
			if (!child)
			{
				return 0;
			}

			for (size_t i = 0; i < n; i++)
			{
				OutputDebugString(L"  ");
			}

			if (child->is_node())
			{
				OutputDebugString(L"Node[");
				OutputDebugString(std::to_wstring(child->m_managed->m_sector).c_str());
				OutputDebugString(L"](");
				OutputDebugString(std::to_wstring(static_cast<node_data_t*>(child->m_managed) - child->m_node_data_begin).c_str());
				OutputDebugString(L"), ");
				OutputDebugString(std::to_wstring(child->depth()).c_str());
				OutputDebugString(L"\n");

				index_t count = 0;

				for (auto i : child->as_node().sectors())
				{
					count += internal_count_leaf_elements(i, n + 1);
				}

				return count;
			}
			else
			{
				gravitySim::tile_tree_leaf<double, 3, double, center_mass, index_t> leaf = child->as_leaf();
				index_t count = 1;
				
				while (leaf.next())
				{
					count++;
					leaf = *leaf.next();
				}
				OutputDebugString(L"Leaf[");
				OutputDebugString(std::to_wstring(child->m_managed->m_sector).c_str());
				OutputDebugString(L"](");
				OutputDebugString(std::to_wstring(static_cast<leaf_element_data_t*>(child->m_managed) - child->m_leaf_element_data_begin).c_str());
				OutputDebugString(L") ");
				OutputDebugString(std::to_wstring(child->depth()).c_str());
				OutputDebugString(L", ");
				OutputDebugString(std::to_wstring(count).c_str());
				OutputDebugString(L" elements\n");

				return count;
			}
		}
	private:
		void internal_insert_begin(leaf_element_data_t& leaf_element, index_t leaf_element_index)
		{
			if (m_root_index == 0) // special case A
			{
				internal_set_as_root(leaf_element, leaf_element_index);
			}
			else 
			{
				if(!m_root_is_tile) // special case B
				{
					index_t leaf_to_be_re_inserted_index = m_root_index;
					leaf_element_data_t& leaf_to_be_re_inserted = m_leaf_element_data_begin[leaf_to_be_re_inserted_index];

					index_t replacement_root_index = internal_alloc_next_node_data();
					node_data_t& replacement_root = m_node_data_begin[m_root_index];
					internal_set_as_root(replacement_root, replacement_root_index);

					auto sector_index = internal_calculate_sector_coordinates(replacement_root.m_position, leaf_to_be_re_inserted.m_position);
					internal_set_child(leaf_to_be_re_inserted, leaf_to_be_re_inserted_index, sector_index, replacement_root, replacement_root_index);
				}

				

				index_t current_node_index = m_root_index;
				node_data_t* p_current_node = &m_node_data_begin[current_node_index];

				while (true)
				{
					auto sector_index = internal_calculate_sector_coordinates(p_current_node->m_position, leaf_element.m_position);

					if (p_current_node->m_depth + 1 >= c_depth_limit)
					{
						index_t hit_leaf_index = p_current_node->m_sector_indices[sector_index];
						leaf_element_data_t& hit_leaf = m_leaf_element_data_begin[hit_leaf_index];
						internal_append_leaf_element(hit_leaf, hit_leaf_index, leaf_element, leaf_element_index);
						return;
					}
					else if (p_current_node->m_sector_indices[sector_index] == 0)
					{
						internal_set_child(leaf_element, leaf_element_index, sector_index, *p_current_node, current_node_index);
						return;
					}
					else
					{
						if (p_current_node->m_sector_is_node_flags[sector_index])
						{
							current_node_index = p_current_node->m_sector_indices[sector_index];;
							p_current_node = &m_node_data_begin[current_node_index];
						}
						else
						{
							node_data_t& root_data_ref = m_node_data_begin[m_root_index];

							index_t leaf_to_be_re_inserted_index = p_current_node->m_sector_indices[sector_index];
							leaf_element_data_t& leaf_to_be_re_inserted = m_leaf_element_data_begin[leaf_to_be_re_inserted_index];

							{
								index_t replacement_node_index = internal_alloc_next_node_data();
								node_data_t& replacement_node = m_node_data_begin[replacement_node_index];
								internal_set_child(replacement_node, replacement_node_index, sector_index, *p_current_node, current_node_index);

								current_node_index = replacement_node_index;
								p_current_node = &replacement_node;
							}

							auto sector_index_in_replacement = internal_calculate_sector_coordinates(p_current_node->m_position, leaf_to_be_re_inserted.m_position);
							internal_set_child(leaf_to_be_re_inserted, leaf_to_be_re_inserted_index, sector_index_in_replacement, *p_current_node, current_node_index);

							//OutputDebugString(L"+++++++++++++++++++++++++++++++++\n++++++++++++++++++++++++++++++++\nExpected count: ");
							//OutputDebugString(std::to_wstring(m_initialized_leaf_element_data_size).c_str());
							//OutputDebugString(L"\nStructure:\n");
							//
							//index_t count = internal_count_leaf_elements(root(), 0);

							//OutputDebugString(L"\nActual: ");
							//OutputDebugString(std::to_wstring(count).c_str());
							//OutputDebugString(L"\n");
						}
					}
				}
			}
		}


		template<typename it_t>
		void internal_prepare_size_and_scale(it_t begin, it_t end)
		{
			m_scale = 0;
			m_size = 0;
			for (auto i = begin; i != end; i++)
			{
				position_value_pair<numeric_t, dimension, leaf_value_t>& i_ref = *i;
				for (size_t dim = 0; dim < i_ref.position.size(); dim++)
				{
					if (i_ref.position[dim] > m_scale)
					{
						m_scale = i_ref.position[dim];
					}
				}
				m_size++;
			}

			//m_size is now std::distance(begin, end)
			//m_scale is now largest value found in all positions
		}

		template<typename it_t>
		void internal_build_tree(it_t begin, it_t end)
		{
			for (auto i = begin; i != end; i++)
			{
				m_leaf_element_data_begin[m_initialized_leaf_element_data_size] = leaf_element_data_t(*i);
				internal_insert_begin(m_leaf_element_data_begin[m_initialized_leaf_element_data_size], m_initialized_leaf_element_data_size);

				//OutputDebugString(L"================================\n================================\nExpected count: ");
				//OutputDebugString(std::to_wstring(m_initialized_leaf_element_data_size).c_str());
				//OutputDebugString(L"\nStructure:\n");
				//index_t count = internal_count_leaf_elements(root(), 0);
				//if (count == 9)
				//{
				//	bool b = false;
				//}

				//OutputDebugString(L"\nActual: ");
				//OutputDebugString(std::to_wstring(count).c_str());
				//OutputDebugString(L"\n");

				m_initialized_leaf_element_data_size++;
			}
		}

	public:

		auto& nodes()
		{

		}
		const auto& nodes() const
		{

		}

		std::optional<tile_tree_child_t> root()
		{
			if (m_root_index == 0)
			{
				return std::nullopt;
			}
			else if (m_root_is_tile)
			{
				return tile_tree_child_t(this->m_leaf_element_data_begin, this->m_node_data_begin, &this->m_node_data_begin[this->m_root_index], true);
			}
			else
			{
				return tile_tree_child_t(this->m_leaf_element_data_begin, this->m_node_data_begin, &this->m_leaf_element_data_begin[this->m_root_index], false);
			}

		}

		static constexpr size_t get_max_required_memory_size(size_t size)
		{
			size_t leaf_element_size_in_bytes = sizeof(leaf_element_data_t) * (size + 1);
			size_t node_element_size_in_bytes = sizeof(node_data_t) * (size * c_depth_limit + 1);

			return leaf_element_size_in_bytes + node_element_size_in_bytes;
		}

		template<typename it_t>
		tile_tree(it_t begin, it_t end, char* pMemory)
		{
			internal_prepare_size_and_scale(begin, end);

			this->m_leaf_element_data_begin = reinterpret_cast<leaf_element_data_t*>(pMemory);
			this->m_node_data_begin = reinterpret_cast<node_data_t*>(pMemory + sizeof(leaf_element_data_t)  * (m_size + 1));

			internal_build_tree(begin, end);
		}
	};
};