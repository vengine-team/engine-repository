#pragma once
#include <gravitySim/Spectator3D.h>
#include <gravitySim/StellarObject3D.h>

#include <eng/tsg/Scene.h>


namespace gravitySim
{
	struct stellarObjectDesc
	{
		double mass;
		gtm::vector3d position;
		gtm::vector3d momentum;
	};

	class Scene3D : public eng::tsg::Scene
	{
		std::vector<std::unique_ptr<StellarObjekt3D>> m_stellarObjects;
	public:

		size_t get_stellarObjects_count() const;

		gtm::vector3d calculate_center_of_mass() const;

		void remove_stellar_objects(size_t count);

		void add_stellar_objects(const std::vector<stellarObjectDesc>& objects);

		Scene3D();
		virtual ~Scene3D();
	};
};