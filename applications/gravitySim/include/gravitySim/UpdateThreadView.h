#pragma once
#include <par/UpdateThread.h>
#include <eng/gpl/MenuBar.h>
#include <eng/control/TilePanel.h>

#include <gravitySim/SceneView.h>
#include <gravitySim/TopBarMenu.h>

namespace gravitySim
{
	class UpdateThreadViewPanel : protected eng::control::TilePanel<eng::gpl::ControlParent<eng::gpl::Control>>, public virtual eng::gpl::Control
	{
		std::unique_ptr<eng::gpl::MenuBar> m_pMenu = nullptr;

		struct gravitySimSceneView_pair
		{
			std::unique_ptr<SceneView> pGravitySimSceneView;
			ext::event_handler<ext::function_ptr<void(UpdateThreadViewPanel::*)(SceneView&)>> on_sceneView_remove_request;
			ext::event_handler<ext::function_ptr<void(UpdateThreadViewPanel::*)(SceneView&, size_t)>> on_sceneView_move_request;
		};

		std::vector<std::unique_ptr<gravitySimSceneView_pair>> m_pViews;

		void on_request_sceneView_move(SceneView& view, size_t index);
		ext::event<SceneView&, size_t> m_request_sceneView_move_event;

		void on_request_sceneView_removal(SceneView& view);
		ext::event<SceneView*> m_request_sceneView_removal_event;

		std::vector<std::wstring> m_move_targets;

	public:

		void setMoveTargets(const std::vector<std::wstring> s);

		const std::vector<std::unique_ptr<gravitySimSceneView_pair>>& get_elements() const;

		ext::event_subscribe_access<SceneView*> request_sceneView_removal_event();

		ext::event_subscribe_access<SceneView&, size_t> request_sceneView_move_event();

		void add_view(std::unique_ptr<SceneView>&& pScene);
		std::unique_ptr<SceneView> release_view(SceneView* pScene);

		UpdateThreadViewPanel();
		virtual ~UpdateThreadViewPanel();
	};

	class UpdateThreadView : public virtual eng::gpl::Control, protected eng::gpl::ControlParent<eng::gpl::Control>
	{
		TopBarMenu m_topBarMenu;

		std::unique_ptr<eng::gpl::MenuBar> m_pMenuBar = nullptr;

		eng::UpdateThread m_updateThread;

		UpdateThreadViewPanel m_panel;

		SceneView* m_pTo_be_removed = nullptr;

		virtual void update() override;

		void add_view();
		void erase_view(SceneView* pScene);

		void on_add_SimSceneView(eng::IButton&);
		ext::event_handler<ext::function_ptr<void(UpdateThreadView::*)(eng::IButton&)>> m_on_add_SimSceneView = { { &UpdateThreadView::on_add_SimSceneView, this } };


		void on_request_SimSceneView_removal(SceneView*);
		ext::event_handler< ext::function_ptr<void(UpdateThreadView::*)(SceneView*)>> m_on_request_SimSceneView_removal = { { &UpdateThreadView::on_request_SimSceneView_removal, this } };


		ext::event<UpdateThreadView&, SceneView&, size_t> m_request_move_event;
		void on_request_SimSceneView_move(SceneView&, size_t);
		ext::event_handler<ext::function_ptr<void(UpdateThreadView::*)(SceneView&, size_t)>> m_on_request_SimSceneView_move = { { &UpdateThreadView::on_request_SimSceneView_move, this } };

		ext::event<UpdateThreadView&> m_request_removal_event;
		void on_request_removal(TopBarMenu&);
		ext::event_handler< ext::function_ptr<void(UpdateThreadView::*)(TopBarMenu&)>> m_on_request_removal_event = { { &UpdateThreadView::on_request_removal, this } };


	public:

		void set_title(const std::wstring& s);

		std::unique_ptr<SceneView> release_scene(SceneView& pScene);
		void insert_scene(std::unique_ptr<SceneView>&& pScene);

		void setMoveTargets(const std::vector<std::wstring> s);

		ext::event_subscribe_access<UpdateThreadView&, SceneView&, size_t> request_scene_view_move();

		ext::event_subscribe_access<UpdateThreadView&> request_removal();

		virtual void setScale(gtm::vector2u scale) override;

		UpdateThreadView();
		virtual ~UpdateThreadView();
	};
};