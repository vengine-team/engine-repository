#pragma once

namespace gravitySim
{
	class Mass
	{
	private:

		double m_mass = 0;

	public:

		virtual void setMass(double mass);
		double getMass() const;

		virtual ~Mass();
	};
}