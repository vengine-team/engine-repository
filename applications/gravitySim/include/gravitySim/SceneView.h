#pragma once
#include <gravitySim/Scene3D.h>
#include <gravitySim/SceneView.h>
#include <gravitySim/TopBarMenu.h>

#include <eng/gpl/Control_ioFrame.h>
#include <eng/gpl/MenuBar.h>

#include <ext/event.h>

namespace gravitySim
{
	class SceneView : public virtual eng::gpl::Control, protected eng::gpl::ControlParent<eng::gpl::Control>
	{
	private:

		TopBarMenu m_topBarMenu;
		void on_request_close(TopBarMenu&);
		ext::event_handler<ext::function_ptr<decltype(&SceneView::on_request_close)>> m_on_request_close = { { &SceneView::on_request_close, this } };

		ext::event<SceneView&> m_request_close_event;

		std::unique_ptr<eng::gpl::MenuBar> m_pMenuBar = nullptr;

		eng::gpl::Control_ioFrame m_ioFrame;

		Scene3D * m_pScene = nullptr;

		Spectator3D m_scene3DView;

		void on_minus_minus_button(eng::IButton&);
		void on_minus_button(eng::IButton&);

		void on_plus_button(eng::IButton&);
		void on_plus_plus_button(eng::IButton&);

		ext::event_handler<ext::function_ptr<void(SceneView::*)(eng::IButton&)>> m_on_minus_minus_button = { { &SceneView::on_minus_minus_button, this } };
		ext::event_handler<ext::function_ptr<void(SceneView::*)(eng::IButton&)>> m_on_minus_button = { { &SceneView::on_minus_button, this } };
		ext::event_handler<ext::function_ptr<void(SceneView::*)(eng::IButton&)>> m_on_plus_button = { { &SceneView::on_plus_button, this } };
		ext::event_handler<ext::function_ptr<void(SceneView::*)(eng::IButton&)>> m_on_plus_plus_button = { { &SceneView::on_plus_plus_button, this } };


		ext::event<SceneView&, size_t> m_request_move_event;
		void on_request_move(eng::IButton& button);
		std::vector<std::unique_ptr<ext::event_handler<ext::function_ptr<void(SceneView::*)(eng::IButton&)>>>> m_move_eventHandlers;

	public:

		ext::event_subscribe_access<SceneView&> request_close_event();

		ext::event_subscribe_access<SceneView&, size_t> request_move_event();

		virtual void setMoveTargets(const std::vector<std::wstring> s);

		virtual void setTextureMaterial(const eng::Material2F* pMaterial) override;

		virtual void setScale(gtm::vector2u scale) override;

		virtual void set_sim_scene(Scene3D* pScene);
		Scene3D* get_sim_scene();
		const Scene3D* get_sim_scene() const;

		SceneView(Scene3D* pData);
		virtual ~SceneView();
	};
};