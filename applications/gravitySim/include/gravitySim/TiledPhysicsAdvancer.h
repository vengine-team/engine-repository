#pragma once
#include <gravitySim/Physics.h>
#include <gravitySim/tile_tree.h>

namespace gravitySim 
{
	class TiledPhysicsAdvancer : public IPhysicsAdvancer
	{
		size_t m_log_of_size = 2;
		std::unique_ptr<char[]> m_cached_memory;
	public:
		virtual void advance(double seconds, eng::tsg::Scene * pScene) override;
	};
};