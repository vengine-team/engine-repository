#pragma once
#include <eng/gpl/Label.h>
#include <eng/gpl/Button.h>

#include <ext/event.h>

namespace gravitySim
{
	class TopBarMenu : public virtual eng::gpl::Control, protected eng::gpl::ControlParent<eng::gpl::Control>
	{
		eng::gpl::Label m_label;

		eng::gpl::Button m_close_button;

		void on_button_pressed(eng::IButton&);
		ext::event_handler<ext::function_ptr<decltype(&TopBarMenu::on_button_pressed)>> m_on_button_pressed = { { &TopBarMenu::on_button_pressed, this } };

		ext::event<TopBarMenu&> m_request_close_event;

	public:

		ext::event_subscribe_access<TopBarMenu&> request_close_event();

		virtual void set_text(const std::wstring& s);
		const std::wstring& get_text();

		virtual unsigned int calculateMinimumHeight() const override;

		virtual void setScale(gtm::vector2u scale) override;

		TopBarMenu();
		virtual ~TopBarMenu();
	};
};