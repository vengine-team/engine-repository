cbuffer ModelConstantBuffer : register(b0)
{
    float4x4 model;
};

cbuffer ViewProjectionConstantBuffer : register(b1)
{
    float4x4 view;
    float4x4 projection;
};

struct VertexShaderInput		
{
    float3 pos : POSITION;
    float3 normal : NORMAL;
    float3 extrant : EXTRANT;
    float2 uv0 : TEXCOORD0;
    float2 uv1 : TEXCOORD1;
    float2 uv2 : TEXCOORD2;
    float2 uv3 : TEXCOORD3;
};
struct FragmentShaderInput
{
    float4 pos : SV_POSITION;
    float2 uv0 : TEXCOORD0;
};

FragmentShaderInput main(VertexShaderInput input)
{
    FragmentShaderInput output;
    float4 pos = float4(0.0, 0.0, 0.0, 1.0f);
    pos = mul(pos, model);
    pos = mul(pos, view);
    input.pos.x *= model[0][0];
    input.pos.y *= model[1][1];
    input.pos.z *= model[2][2];
    pos.xyz += input.pos;
    pos = mul(pos, projection);

    output.pos = pos;
    output.uv0 = input.uv0;
    return output;
}