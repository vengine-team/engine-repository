// Per-fragment color data passed through the pixel shader.
Texture2D<float4> tex1 : register(t2);

SamplerState sampler1 : register(s3);

cbuffer colorBuffer : register(b4)
{
    float4 color;
};

struct FragmentShaderInput
{
    float4 pos : SV_POSITION;
    float2 uv0 : TEXCOORD0;
};

float4 main(FragmentShaderInput input) : SV_TARGET
{
    float4 sampled = tex1.Sample(sampler1, input.uv0);
    return sampled;
}