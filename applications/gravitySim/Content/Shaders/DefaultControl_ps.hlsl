// Per-fragment color data passed through the pixel shader.
cbuffer colorBuffer : register(b2)
{
    float4 color;
};

struct FragmentShaderInput
{
    float4 pos : SV_POSITION;
    float2 uv0 : TEXCOORD0;
};

// A pass-through function for the (interpolated) color data.
float4 main(FragmentShaderInput input) : SV_TARGET
{
    return color;
}
