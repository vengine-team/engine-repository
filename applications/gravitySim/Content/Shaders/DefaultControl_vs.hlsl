// A constant buffer that stores the three basic column-major matrices for composing geometry.

cbuffer ParentTransformConstBuffer : register(b0)
{
    matrix parentTransform;
};

cbuffer TransformConstantBuffer : register(b1)
{
	matrix transform;
};

// Per - vertex data used as input to the vertex shader.
struct VertexShaderInput
{
    float2 position : POSITION;
    float2 uv0 : TEXCOORD0;
    float2 uv1 : TEXCOORD1;
    float2 uv2 : TEXCOORD2;
    float2 uv3 : TEXCOORD3;
};

// Per-fragment color data passed through the pixel shader.
struct FragmentShaderInput
{
    float4 pos : SV_POSITION;
    float2 uv0 : TEXCOORD0;
};

// Simple shader to do vertex processing on the GPU.
FragmentShaderInput main(VertexShaderInput input)
{
	FragmentShaderInput output;
    
    output.uv0 = input.uv0;


    float4 position = float4(input.position, 0.0f, 1.0f);
    position = mul(position, transform);
    position = mul(position, parentTransform);
    output.pos = position;

	return output;
}