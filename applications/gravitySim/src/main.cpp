#pragma once
#include <eng/Window.h>
#include <eng/engineEntry.h>

#include <par/UpdateThread.h>
#include <eng/engUtil.h>
#include <eng/engineWindows.h>
#include <eng/control/Root.h>

#include <gravitySim/MainControl.h>
#include <gravitySim/resources.h>

eng::UpdateThread* pMainUpdateThread;

void on_window_request_close(eng::Window& sender)
{
	pMainUpdateThread->concurrentTerminate();
}

///
/// Entry point for the program
///
int engMain()
{		
	//Resource loading
	//engDefault::resources::loadResources();
	gravitySim::resources::load_resources();

	eng::Window mainWindow = { L"Gravity Sim Demo", { 100, 100 }, { 400, 400 }, eng::WindowStyle::SystemDefaultMenu, true };

	std::unique_ptr<gravitySim::MainControl> pTestRoot(new gravitySim::MainControl());

	pTestRoot->set_Font(gravitySim::resources::font());
	pTestRoot->setFontMaterial(gravitySim::resources::fontMaterial());
	pTestRoot->setTextureMaterial(gravitySim::resources::textureMaterial());

	pTestRoot->setBackgroundMaterial(gravitySim::resources::backgroundMaterial());
	pTestRoot->setHighlightedMaterial(gravitySim::resources::highlightedMaterial());
	pTestRoot->setPressedMaterial(gravitySim::resources::pressedMaterial());
	pTestRoot->setOutlineMaterial(gravitySim::resources::outlineMaterial());

	eng::control::Root<gravitySim::MainControl> mainControlRoot;
	mainControlRoot.setControl(std::move(pTestRoot));
	mainControlRoot.setIioFrame(&mainWindow);


	mainWindow.open();

	eng::UpdateThread mainUpdateThread;
	pMainUpdateThread = &mainUpdateThread;


	mainUpdateThread.concurrentRegisterUpdateable(&mainControlRoot);

	ext::event_handler<ext::functor<&on_window_request_close>> on_request_application_close = { ext::functor<&on_window_request_close>{ } };

	on_request_application_close.setEvent(mainWindow.request_close_event());

	mainUpdateThread.join();

	on_request_application_close.unsetEvent(mainWindow.request_close_event());

	mainControlRoot.setIioFrame(nullptr);

    return 0;
}
