#include <gravitySim/MainControl.h>

gravitySim::MainControl::updateThread_pair::updateThread_pair(ext::function_ptr<void(gravitySim::MainControl::*)(UpdateThreadView&)>& pF, ext::function_ptr<void(gravitySim::MainControl::*)(UpdateThreadView&, SceneView&, size_t)>& pF2) :
	m_on_request_UpdateThreadView_removed(pF),
	m_on_request_UpdateThreadView_scene_move(pF2)
{

}


void gravitySim::MainControl::setScale(gtm::vector2u scale)
{
	eng::gpl::ControlParent<eng::gpl::Control>::setScale(scale);
	m_pTestMenu->setScale({ scale[0], m_pTestMenu->calculateHeight(scale[0]) });

	m_splitBoxTree.setLocalPosition({ 10, static_cast<int>(m_pTestMenu->getScale()[1]) + 10 });
	m_splitBoxTree.setScale({ getScale()[0] - 20 , getScale()[1] - m_splitBoxTree.getLocalPosition()[1] - 10 });
}

void gravitySim::MainControl::erase_updateThread(UpdateThreadView * pToBeErased)
{
	for (auto i = std::begin(m_updateThreadViews); i < std::end(m_updateThreadViews); i++)
	{
		if (&(*i)->updateThreadView == pToBeErased)
		{
			m_splitBoxTree.remove((*i)->splitBox_element);

			(*i)->updateThreadView.request_removal() -= (*i)->m_on_request_UpdateThreadView_removed;
			(*i)->updateThreadView.request_scene_view_move() -= (*i)->m_on_request_UpdateThreadView_scene_move;

			m_updateThreadViews.erase(i);

			set_move_targets();
			rename_update_threads();
			return;
		}
	}

	assert(false);
}

void gravitySim::MainControl::set_move_targets()
{
	for (size_t i = 0; i < m_updateThreadViews.size(); i++)
	{
		std::vector<std::wstring> moveTargets;
		for (size_t j = 0; j < m_updateThreadViews.size(); j++)
		{
			if (j != i)
			{
				moveTargets.push_back(L"Update Thread ");
				moveTargets.back().append(std::to_wstring(j));
			}
		}

		m_updateThreadViews[i]->updateThreadView.setMoveTargets(moveTargets);
	}
}

void gravitySim::MainControl::rename_update_threads()
{
	for (size_t i = 0; i < m_updateThreadViews.size(); i++)
	{
		std::wstring new_title = L"Update Thread ";
		new_title.append(std::to_wstring(i));

		m_updateThreadViews[i]->updateThreadView.set_title(new_title);
	}
}

void gravitySim::MainControl::on_add_UpdateThreadView(eng::IButton&)
{
	m_updateThreadViews.push_back
	(
		std::make_unique<updateThread_pair>
		(
			ext::function_ptr<void(gravitySim::MainControl::*)(UpdateThreadView&)>{&gravitySim::MainControl::on_request_UpdateThreadView_removed, this},
			ext::function_ptr<void(gravitySim::MainControl::*)(UpdateThreadView&, SceneView&, size_t)>{&gravitySim::MainControl::on_UpdateThreadView_request_move, this}
		)
	);
	m_updateThreadViews.back()->updateThreadView.setOutlineEnabled(true);
	m_updateThreadViews.back()->updateThreadView.request_removal() += m_updateThreadViews.back()->m_on_request_UpdateThreadView_removed;
	m_updateThreadViews.back()->updateThreadView.request_scene_view_move() += m_updateThreadViews.back()->m_on_request_UpdateThreadView_scene_move;
	m_splitBoxTree.insert(m_updateThreadViews.back()->splitBox_element);

	set_move_targets();
	rename_update_threads();
}

void gravitySim::MainControl::on_request_UpdateThreadView_removed(UpdateThreadView& view)
{
	m_pTo_be_erased = &view;
}

void gravitySim::MainControl::on_UpdateThreadView_request_move(UpdateThreadView & updateThreadView, SceneView& scene, size_t index)
{
	for (size_t i = 0; i <= index; i++)
	{
		if (&m_updateThreadViews[i]->updateThreadView == &updateThreadView)
		{
			index++;
		}
	}

	m_pToBeMoved = &scene;
	m_pToBeMoved_from = &updateThreadView;
	m_pToBeMoved_where = index;
}

bool gravitySim::MainControl::onInput(const eng::input_state_change & change, gtm::vector2i offset, bool obstructed)
{
	bool out = eng::gpl::ControlParent<eng::gpl::Control>::onInput(change, offset, obstructed);

	if (m_pToBeMoved != nullptr)
	{
		std::unique_ptr<SceneView> pTo_be_moved = m_pToBeMoved_from->release_scene(*m_pToBeMoved);

		m_updateThreadViews[m_pToBeMoved_where]->updateThreadView.insert_scene(std::move(pTo_be_moved));

		m_pToBeMoved = nullptr;
	}

	if (m_pTo_be_erased)
	{
		erase_updateThread(m_pTo_be_erased);
		m_pTo_be_erased = nullptr;
	}
	return out;
}

void gravitySim::MainControl::setTextureMaterial(const eng::Material2F * pMaterial)
{
	eng::gpl::ControlParent<eng::gpl::Control>::setTextureMaterial(pMaterial);
}

void gravitySim::MainControl::localRender(eng::control::LocalGraphics & localGraphics) const
{
	eng::control::draw_quad(localGraphics, { { 0.0, 0.0 }, static_cast<gtm::vector2d>(getScale()) }, getPressedMaterial());
}

void gravitySim::MainControl::set_Font(const eng::Font * pFont)
{
	eng::gpl::ControlParent<eng::gpl::Control>::set_Font(pFont);
	m_pTestMenu->setScale({ getScale()[0], m_pTestMenu->calculateHeight(getScale()[1]) });
}

gravitySim::MainControl::MainControl()
{
	//setup main controls
	children().push_back(&m_splitBoxTree);

	eng::gpl::Menu_Element_Desc mainMenu_add_desc[1];
	mainMenu_add_desc[0].text = L"UpdateThread";

	eng::gpl::Menu_Element_Desc mainMenu_desc[1];
	mainMenu_desc[0].text = L"Add";
	mainMenu_desc[0].numSubmenuElements = _countof(mainMenu_add_desc);
	mainMenu_desc[0].pSubmenuElements = mainMenu_add_desc;

	m_pTestMenu = new eng::gpl::MenuBar(_countof(mainMenu_desc), mainMenu_desc);
	children().push_back(m_pTestMenu);
	m_pTestMenu->setLocalPosition({ 0, 0 });

	m_pTestMenu->get_submenu_element(0).get_submenu_element(0).upEvent() += m_on_add_UpdateThreadView;
}
gravitySim::MainControl::~MainControl()
{
	while (!m_updateThreadViews.empty())
	{
		erase_updateThread(&m_updateThreadViews.back()->updateThreadView);
	}

	m_pTestMenu->get_submenu_element(0).get_submenu_element(0).upEvent() -= m_on_add_UpdateThreadView;

	children().erase(std::find(children().begin(), children().end(), m_pTestMenu));

	children().erase(std::find(children().begin(), children().end(), &m_splitBoxTree));
}
