#include <gravitySim/UpdateThreadView.h>
#include <eng/control/layout.h>

const unsigned int c_outline_width = 1;

void gravitySim::UpdateThreadViewPanel::on_request_sceneView_move(SceneView & view, size_t index)
{
	m_request_sceneView_move_event(view, index);
}

void gravitySim::UpdateThreadViewPanel::on_request_sceneView_removal(SceneView & view)
{
	m_request_sceneView_removal_event(&view);
}

void gravitySim::UpdateThreadViewPanel::setMoveTargets(const std::vector<std::wstring> s)
{
	m_move_targets = s;

	for (auto i = std::begin(m_pViews); i != std::end(m_pViews); i++)
	{
		(*i)->pGravitySimSceneView->setMoveTargets(s);
	}
}

const std::vector<std::unique_ptr<gravitySim::UpdateThreadViewPanel::gravitySimSceneView_pair>>& gravitySim::UpdateThreadViewPanel::get_elements() const
{
	return m_pViews;
}

ext::event_subscribe_access<gravitySim::SceneView*> gravitySim::UpdateThreadViewPanel::request_sceneView_removal_event()
{
	return { m_request_sceneView_removal_event };
}

ext::event_subscribe_access<gravitySim::SceneView&, size_t> gravitySim::UpdateThreadViewPanel::request_sceneView_move_event()
{
	return { m_request_sceneView_move_event };
}

void gravitySim::UpdateThreadViewPanel::add_view(std::unique_ptr<gravitySim::SceneView>&& pScene)
{
	gravitySimSceneView_pair* pPair = new gravitySimSceneView_pair
	{
		std::move(pScene),
	{ { &gravitySim::UpdateThreadViewPanel::on_request_sceneView_removal, this } },
	{ { &gravitySim::UpdateThreadViewPanel::on_request_sceneView_move, this } }
	};
	m_pViews.push_back(std::unique_ptr<gravitySimSceneView_pair>{ pPair });
	children().push_back(m_pViews.back()->pGravitySimSceneView.get());

	m_pViews.back()->pGravitySimSceneView->request_close_event() += m_pViews.back()->on_sceneView_remove_request;
	m_pViews.back()->pGravitySimSceneView->request_move_event() += m_pViews.back()->on_sceneView_move_request;

	m_pViews.back()->pGravitySimSceneView->setOutlineEnabled(true);

	m_pViews.back()->pGravitySimSceneView->setMoveTargets(m_move_targets);

	setScale(getScale());
}

std::unique_ptr<gravitySim::SceneView> gravitySim::UpdateThreadViewPanel::release_view(gravitySim::SceneView* pScene)
{
	for (auto i = m_pViews.begin(); i != m_pViews.end(); i++)
	{
		if ((*i)->pGravitySimSceneView.get() == pScene)
		{
			(*i)->pGravitySimSceneView->request_close_event() -= (*i)->on_sceneView_remove_request;
			(*i)->pGravitySimSceneView->request_move_event() -= (*i)->on_sceneView_move_request;

			children().erase(std::find(std::begin(children()), std::end(children()), (*i)->pGravitySimSceneView.get()));

			std::unique_ptr<gravitySim::SceneView> out = std::move((*i)->pGravitySimSceneView);

			m_pViews.erase(i);
			setScale(getScale());

			return out;
		}
	}
	assert(false);
	return nullptr;
}

gravitySim::UpdateThreadViewPanel::UpdateThreadViewPanel()
{
	setElementPadding({ 2,2 });
	setElementGap({ 4,4 });
}
gravitySim::UpdateThreadViewPanel::~UpdateThreadViewPanel()
{

}

void gravitySim::UpdateThreadView::update()
{
	eng::gpl::ControlParent<eng::gpl::Control>::update();

	if (m_pTo_be_removed)
	{
		m_updateThread.concurrentUnregisterUpdateable(m_pTo_be_removed->get_sim_scene());

		std::unique_ptr<SceneView> view = m_panel.release_view(m_pTo_be_removed);

		gravitySim::Scene3D* pScene = view->get_sim_scene();

		view->set_sim_scene(nullptr);

		delete pScene;

		m_pTo_be_removed = nullptr;
	}
}

void gravitySim::UpdateThreadView::add_view()
{
	std::unique_ptr<SceneView> pSceneView = std::make_unique<SceneView>(new gravitySim::Scene3D());
	gravitySim::Scene3D* pScene = pSceneView->get_sim_scene();
	m_panel.add_view(std::move(pSceneView));

	m_updateThread.concurrentRegisterUpdateable(pScene);
}
void gravitySim::UpdateThreadView::erase_view(gravitySim::SceneView* pScene)
{
	m_pTo_be_removed = pScene;
}


void gravitySim::UpdateThreadView::on_add_SimSceneView(eng::IButton&)
{
	add_view();
}

void gravitySim::UpdateThreadView::on_request_SimSceneView_removal(gravitySim::SceneView* pScene)
{
	erase_view(pScene);
}

void gravitySim::UpdateThreadView::on_request_SimSceneView_move(SceneView& scene, size_t index)
{
	m_request_move_event(*this, scene, index);
}

void gravitySim::UpdateThreadView::on_request_removal(gravitySim::TopBarMenu &)
{
	m_request_removal_event(*this);
}


void gravitySim::UpdateThreadView::set_title(const std::wstring & s)
{
	m_topBarMenu.set_text(s);
}

std::unique_ptr<gravitySim::SceneView> gravitySim::UpdateThreadView::release_scene(SceneView & pScene)
{
	m_updateThread.concurrentUnregisterUpdateable(pScene.get_sim_scene());

	return m_panel.release_view(&pScene);
}

void gravitySim::UpdateThreadView::insert_scene(std::unique_ptr<SceneView>&& pScene)
{
	gravitySim::Scene3D* _pScene = pScene->get_sim_scene();
	m_panel.add_view(std::move(pScene));

	m_updateThread.concurrentRegisterUpdateable(_pScene);
}

void gravitySim::UpdateThreadView::setMoveTargets(const std::vector<std::wstring> s)
{
	m_panel.setMoveTargets(s);
}

ext::event_subscribe_access<gravitySim::UpdateThreadView&, gravitySim::SceneView&, size_t> gravitySim::UpdateThreadView::request_scene_view_move()
{
	return { m_request_move_event };
}

ext::event_subscribe_access<gravitySim::UpdateThreadView&> gravitySim::UpdateThreadView::request_removal()
{
	return { m_request_removal_event };
}

void gravitySim::UpdateThreadView::setScale(gtm::vector2u scale)
{
	eng::gpl::ControlParent<eng::gpl::Control>::setScale(scale);

	gtm::rectangle<int> rect = eng::control::padded_rectangle(scale, { c_outline_width , c_outline_width });

	int pen_y = c_outline_width;
	eng::control::append_vertical(pen_y, rect, m_topBarMenu);
	eng::control::append_vertical(pen_y, rect, *m_pMenuBar);
	eng::control::append_vertical_fill(pen_y, rect, m_panel);
}

gravitySim::UpdateThreadView::UpdateThreadView()
{
	m_topBarMenu.set_text(L"Update Thread");
	children().push_back(&m_topBarMenu);
	m_topBarMenu.request_close_event() += m_on_request_removal_event;

	children().push_back(&m_panel);
	m_panel.setLocalPosition({ 0,0 });
	m_panel.request_sceneView_removal_event() += m_on_request_SimSceneView_removal;
	m_panel.request_sceneView_move_event() += m_on_request_SimSceneView_move;

	std::array<eng::gpl::Menu_Element_Desc, 1> menu_add_element_desc = {};
	menu_add_element_desc[0].text = L"Gravity Simulation";

	std::array<eng::gpl::Menu_Element_Desc, 1> menu_element_desc = {};
	menu_element_desc[0].text = L"Add";
	menu_element_desc[0].numSubmenuElements = menu_add_element_desc.size();
	menu_element_desc[0].pSubmenuElements = menu_add_element_desc.data();

	m_pMenuBar.reset(new eng::gpl::MenuBar(menu_element_desc.size(), menu_element_desc.data()));
	children().push_back(m_pMenuBar.get());
	m_pMenuBar->setLocalPosition({ c_outline_width, c_outline_width });

	m_pMenuBar->get_submenu_element(0).get_submenu_element(0).upEvent() += m_on_add_SimSceneView;
}
gravitySim::UpdateThreadView::~UpdateThreadView()
{
	m_pMenuBar->get_submenu_element(0).get_submenu_element(0).upEvent() -= m_on_add_SimSceneView;
	children().erase(std::find(std::begin(children()), std::end(children()), m_pMenuBar.get()));

	m_panel.request_sceneView_move_event() -= m_on_request_SimSceneView_move;
	m_panel.request_sceneView_removal_event() -= m_on_request_SimSceneView_removal;
	children().erase(std::find(std::begin(children()), std::end(children()), &m_panel));

	m_topBarMenu.request_close_event() -= m_on_request_removal_event;
	children().erase(std::find(std::begin(children()), std::end(children()), &m_topBarMenu));

	while (!m_panel.get_elements().empty())
	{
		SceneView& ref = *m_panel.get_elements().front()->pGravitySimSceneView;

		m_updateThread.concurrentUnregisterUpdateable(ref.get_sim_scene());

		std::unique_ptr<SceneView> view = m_panel.release_view(&ref);

		gravitySim::Scene3D* pScene = view->get_sim_scene();

		view->set_sim_scene(nullptr);

		delete pScene;
	}

	m_updateThread.concurrentTerminate();
	m_updateThread.join();
}
