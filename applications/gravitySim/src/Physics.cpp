#include <gravitySim/Physics.h>

#define GRAVITATIONAL_CONSTANT 0.000000000066742

void gravitySim::DefaultAdvancer::advance(double seconds, eng::tsg::Scene * pScene)
{
	seconds = seconds * 10;

	PhysicsModule* pModule = get_PhysicsModule(pScene);

	double total_mass = 0;
	gtm::vector3d center_of_mass = gtm::vector3d::zero();

	if (pModule->bodies().size() > 1)
	{
		int n = 0;
	}

	for (auto i = pModule->bodies().begin(); i != pModule->bodies().end(); i++)
	{
		gtm::vector3d i_pos = (*i)->transform().translation();

		gtm::vector3d momentum = gtm::vector3d::zero();

		for (auto j = pModule->bodies().begin(); j != pModule->bodies().end(); j++)
		{
			if (j != i)
			{

				gtm::vector3d j_pos = (*j)->transform().translation();

				double distance = gtm::magnitude(i_pos - j_pos);

				if (distance != 0.0)
				{
					double acceleration = (GRAVITATIONAL_CONSTANT * (*j)->getMass()) / (distance * distance);

					momentum += (j_pos - i_pos) * acceleration;
				}
			}
		}

		(*i)->add_momentum(momentum * seconds);
		(*i)->advance(seconds);

		center_of_mass += i_pos;
		total_mass += (*i)->getMass();
	}

	size_t t = pModule->bodies().size();
	center_of_mass /= (double)pModule->bodies().size();

	for (auto i = pModule->bodies().begin(); i != pModule->bodies().end(); i++)
	{
		(*i)->set_local_transform({ (*i)->local_transform().linear(), (*i)->local_transform().translation() -center_of_mass });
	}
}

const ext::ticket_table<gravitySim::Body3d*>& gravitySim::PhysicsModule::bodies() const
{
	return m_bodies;
}

std::unique_ptr<gravitySim::IPhysicsAdvancer>& gravitySim::PhysicsModule::advancer()
{
	return m_pAdvancer;
}

const gravitySim::IPhysicsAdvancer * gravitySim::PhysicsModule::advancer() const
{
	return m_pAdvancer.get();
}

gravitySim::PhysicsModule::PhysicsModule()
{
	m_pAdvancer.reset(new gravitySim::DefaultAdvancer());
}

void gravitySim::PhysicsModule::on_late_update(eng::tsg::Scene* pScene)
{
	m_pAdvancer->advance(1.0 / pScene->getTargetFrameRate(), pScene);
}


eng::module_key<gravitySim::PhysicsModule> physics_moduleKey;
eng::module_guard<eng::tsg::Scene, gravitySim::PhysicsModule> physics_moduleGuard = { physics_moduleKey };


gravitySim::PhysicsModule * gravitySim::get_PhysicsModule(eng::tsg::Scene * pScene)
{
	return pScene->getModule(physics_moduleKey);
}
const gravitySim::PhysicsModule * gravitySim::get_PhysicsModule(const eng::tsg::Scene * pScene)
{
	return pScene->getModule(physics_moduleKey);
}
