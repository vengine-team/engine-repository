#include <gravitySim/resources.h>
#include <memory>


std::shared_ptr<eng::DeviceBuffer> pBlackConstantBuffer;
std::shared_ptr<eng::DeviceBuffer> pStarColorConstantBuffer;
std::shared_ptr<eng::DeviceBuffer> pWhiteConstantBuffer;

std::shared_ptr<eng::DeviceBuffer> pLightGreyConstantBuffer;
std::shared_ptr<eng::DeviceBuffer> pDarkGreyConstantBuffer;
std::shared_ptr<eng::DeviceBuffer> pLightBlueConstantBuffer;
std::shared_ptr<eng::DeviceBuffer> pBlueConstantBuffer;


std::shared_ptr<eng::Font>	pFont;

std::shared_ptr<eng::DeviceTexture2> pStarSprite;

std::shared_ptr<eng::D3D12::Shader> pDefaultControl_vs;
std::shared_ptr<eng::D3D12::Shader> pDefaultControl_ps;
void loadDefautlControlShaders()
{
	pDefaultControl_vs.reset(new eng::D3D12::Shader(std::filesystem::current_path().append(L"Content\\Shaders\\DefaultControl_vs.hlsl"), "main", "vs_4_0"));
	pDefaultControl_ps.reset(new eng::D3D12::Shader(std::filesystem::current_path().append(L"Content\\Shaders\\DefaultControl_ps.hlsl"), "main", "ps_4_0"));
}

std::shared_ptr<eng::D3D12::Shader> pTexture_ps;
void loadTexture_ps_Shader()
{
	pTexture_ps.reset(new eng::D3D12::Shader(std::filesystem::current_path().append(L"Content\\Shaders\\Texture_ps.hlsl"), "main", "ps_4_0"));
}

std::shared_ptr<eng::D3D12::Shader> pBillboard_vs;
std::shared_ptr<eng::D3D12::Shader> pBillboard_ps;
void loadBillboardShaders()
{
	pBillboard_vs.reset(new eng::D3D12::Shader(std::filesystem::current_path().append(L"Content\\Shaders\\Billboard_vs.hlsl"), "main", "vs_4_0"));
	pBillboard_ps.reset(new eng::D3D12::Shader(std::filesystem::current_path().append(L"Content\\Shaders\\Billboard_ps.hlsl"), "main", "ps_4_0"));
}


std::shared_ptr<eng::RenderPass3f> pBillboardRenderPass;
void loadBillboardRenderPass()
{
	CD3DX12_ROOT_PARAMETER rootParameters[5];

	CD3DX12_DESCRIPTOR_RANGE range0;
	range0.Init(D3D12_DESCRIPTOR_RANGE_TYPE_CBV, 1, 0);
	rootParameters[0].InitAsDescriptorTable(1, &range0);

	CD3DX12_DESCRIPTOR_RANGE range1;
	range1.Init(D3D12_DESCRIPTOR_RANGE_TYPE_CBV, 1, 1);
	rootParameters[1].InitAsDescriptorTable(1, &range1);

	CD3DX12_DESCRIPTOR_RANGE range2;
	range2.Init(D3D12_DESCRIPTOR_RANGE_TYPE_SRV, 1, 2);
	rootParameters[2].InitAsDescriptorTable(1, &range2);

	CD3DX12_DESCRIPTOR_RANGE range3;
	range3.Init(D3D12_DESCRIPTOR_RANGE_TYPE_SAMPLER, 1, 3);
	rootParameters[3].InitAsDescriptorTable(1, &range3);

	CD3DX12_DESCRIPTOR_RANGE range4;
	range4.Init(D3D12_DESCRIPTOR_RANGE_TYPE_CBV, 1, 4);
	rootParameters[4].InitAsDescriptorTable(1, &range4);

	D3D12_ROOT_SIGNATURE_FLAGS rootSignatureFlags =
		D3D12_ROOT_SIGNATURE_FLAG_ALLOW_INPUT_ASSEMBLER_INPUT_LAYOUT | // Only the input assembler stage needs access to the constant buffer.
		D3D12_ROOT_SIGNATURE_FLAG_DENY_DOMAIN_SHADER_ROOT_ACCESS |
		D3D12_ROOT_SIGNATURE_FLAG_DENY_GEOMETRY_SHADER_ROOT_ACCESS |
		D3D12_ROOT_SIGNATURE_FLAG_DENY_HULL_SHADER_ROOT_ACCESS;

	CD3DX12_ROOT_SIGNATURE_DESC sample_root_signature_desc;
	sample_root_signature_desc.Init
	(
		_countof(rootParameters),
		rootParameters,
		0,
		nullptr,
		rootSignatureFlags
	);


	eng::render_pass_parameter dx12_parameters[4];
	dx12_parameters[0].type = eng::shader_resource::device_buffer;
	dx12_parameters[0].constantBufferParameter.constantBufferDescriptor_rootParamIndex = 0;
	dx12_parameters[1].type = eng::shader_resource::device_buffer;
	dx12_parameters[1].constantBufferParameter.constantBufferDescriptor_rootParamIndex = 1;
	dx12_parameters[2].type = eng::shader_resource::device_texture;
	dx12_parameters[2].textureParameter.shaderResourceView_rootParamIndex = 2;
	dx12_parameters[2].textureParameter.sampler_rootParamIndex = 3;
	dx12_parameters[3].type = eng::shader_resource::device_buffer;
	dx12_parameters[3].constantBufferParameter.constantBufferDescriptor_rootParamIndex = 4;

	eng::D3D12::RenderPass_Desc renderPassDesc = {};
	renderPassDesc.depth_stencil_Desc.DepthEnable = FALSE;
	renderPassDesc.vertexShader = pBillboard_vs.get();
	renderPassDesc.pixelShader = pBillboard_ps.get();
	renderPassDesc.numParameters = _countof(dx12_parameters);
	renderPassDesc.pParameters = dx12_parameters;
	renderPassDesc.root_signature_desc = sample_root_signature_desc;
	renderPassDesc.cullMode = eng::D3D12::cull_mode::none;
	{
		renderPassDesc.blend_Desc.BlendEnable = TRUE;

		renderPassDesc.blend_Desc.SrcBlend = D3D12_BLEND_SRC_ALPHA;
		renderPassDesc.blend_Desc.DestBlend = D3D12_BLEND_ONE;
		renderPassDesc.blend_Desc.BlendOp = D3D12_BLEND_OP_ADD;
		renderPassDesc.blend_Desc.SrcBlendAlpha = D3D12_BLEND_ONE;
		renderPassDesc.blend_Desc.DestBlendAlpha = D3D12_BLEND_ONE;
		renderPassDesc.blend_Desc.BlendOpAlpha = D3D12_BLEND_OP_ADD;
	};

	pBillboardRenderPass.reset(new eng::RenderPass3f(renderPassDesc));
}

std::unique_ptr<eng::MaterialSignature3f> pBillboardMaterialSignature;
void loadBillboardMaterialSignature()
{
	std::array<eng::material_parameter, 4> parameters
	{
		eng::material_parameter{ eng::shader_resource::device_buffer, 0 },
		eng::material_parameter{ eng::shader_resource::device_buffer, 1 },
		eng::material_parameter{ eng::shader_resource::device_texture, (std::numeric_limits<size_t>::max)() },
		eng::material_parameter{ eng::shader_resource::device_buffer, (std::numeric_limits<size_t>::max)() },
	};


	std::array<eng::render_pass_binding<eng::vertex3f>, 1> bindings
	{
		eng::render_pass_binding<eng::vertex3f>{ pBillboardRenderPass, { 0, 1, 2 } }
	};

	pBillboardMaterialSignature = std::make_unique<eng::MaterialSignature3f>(parameters.begin(), parameters.end(), bindings.begin(), bindings.end(), 100);
}

std::shared_ptr<eng::RenderPass2F> pDefaultControlRenderPass;
std::shared_ptr<eng::MaterialSignature2F> pDefaultControlMaterialSignature;

std::shared_ptr<eng::RenderPass2F> pTextureControlRenderPass;
std::shared_ptr < eng::MaterialSignature2F> pTextureControlMaterialSignature;

std::shared_ptr<eng::Material3f> pBillboardMaterial;
std::shared_ptr<eng::Material2F> pBackgroundMaterial;
std::shared_ptr<eng::Material2F> pOutlineMaterial;
std::shared_ptr<eng::Material2F> pHighlightMaterial;
std::shared_ptr<eng::Material2F> pPressedControlMaterial;

std::shared_ptr<eng::Material2F> pFontMaterial;
std::shared_ptr<eng::Material2F> pTextureMaterial;

void gravitySim::resources::load_resources()
{
	pStarSprite.reset(new eng::DeviceTexture2(eng::load_png(std::filesystem::current_path().append(L"Content\\Textures\\StarSprite.png"))));

	pFont.reset(new eng::Font(std::filesystem::current_path().append(L"Content\\Fonts\\arial.ttf"), 12));
	
	loadDefautlControlShaders();
	loadTexture_ps_Shader();
	loadBillboardShaders();

	loadBillboardRenderPass();
	loadBillboardMaterialSignature();

	//creating default render Pass
	{
		CD3DX12_ROOT_PARAMETER rootSignatureParams[3];
		CD3DX12_DESCRIPTOR_RANGE range0;
		range0.Init(D3D12_DESCRIPTOR_RANGE_TYPE_CBV, 1, 0);
		rootSignatureParams[0].InitAsDescriptorTable(1, &range0);
		CD3DX12_DESCRIPTOR_RANGE range1;
		range1.Init(D3D12_DESCRIPTOR_RANGE_TYPE_CBV, 1, 1);
		rootSignatureParams[1].InitAsDescriptorTable(1, &range1);
		CD3DX12_DESCRIPTOR_RANGE range2;
		range2.Init(D3D12_DESCRIPTOR_RANGE_TYPE_CBV, 1, 2);
		rootSignatureParams[2].InitAsDescriptorTable(1, &range2);



		D3D12_ROOT_SIGNATURE_FLAGS rootSignatureFlags =
			D3D12_ROOT_SIGNATURE_FLAG_ALLOW_INPUT_ASSEMBLER_INPUT_LAYOUT | // the input assembler stage needs access to the constant buffer.
			D3D12_ROOT_SIGNATURE_FLAG_DENY_DOMAIN_SHADER_ROOT_ACCESS |
			D3D12_ROOT_SIGNATURE_FLAG_DENY_GEOMETRY_SHADER_ROOT_ACCESS |
			D3D12_ROOT_SIGNATURE_FLAG_DENY_HULL_SHADER_ROOT_ACCESS;
		//The pixel shader also needs access to the root signature

		CD3DX12_ROOT_SIGNATURE_DESC dx12_rootSignatureDesc;
		dx12_rootSignatureDesc.Init(_countof(rootSignatureParams), rootSignatureParams, 0, nullptr, rootSignatureFlags);

		eng::render_pass_parameter dx12_parameters[3];
		dx12_parameters[0] = { eng::shader_resource::device_buffer };
		dx12_parameters[0].constantBufferParameter.constantBufferDescriptor_rootParamIndex = 0;
		dx12_parameters[1] = { eng::shader_resource::device_buffer };
		dx12_parameters[1].constantBufferParameter.constantBufferDescriptor_rootParamIndex = 1;
		dx12_parameters[2] = { eng::shader_resource::device_buffer };
		dx12_parameters[2].constantBufferParameter.constantBufferDescriptor_rootParamIndex = 2;

		eng::D3D12::RenderPass_Desc renderPassDesc = {};
		{
			renderPassDesc.vertexShader = pDefaultControl_vs.get();
			renderPassDesc.pixelShader = pDefaultControl_ps.get();

			renderPassDesc.numParameters = _countof(dx12_parameters);
			renderPassDesc.pParameters = dx12_parameters;

			renderPassDesc.root_signature_desc = dx12_rootSignatureDesc;

			renderPassDesc.cullMode = eng::D3D12::cull_mode::none;

			renderPassDesc.depth_stencil_Desc.DepthEnable = FALSE;
		}

		pDefaultControlRenderPass.reset(new eng::RenderPass2F(renderPassDesc));
	}
	
	//texture render pass
	{
		CD3DX12_ROOT_PARAMETER rootSignatureParams[5];
		CD3DX12_DESCRIPTOR_RANGE range0;
		range0.Init(D3D12_DESCRIPTOR_RANGE_TYPE_CBV, 1, 0);
		rootSignatureParams[0].InitAsDescriptorTable(1, &range0);
		CD3DX12_DESCRIPTOR_RANGE range1;
		range1.Init(D3D12_DESCRIPTOR_RANGE_TYPE_CBV, 1, 1);
		rootSignatureParams[1].InitAsDescriptorTable(1, &range1);
		CD3DX12_DESCRIPTOR_RANGE range2;
		range2.Init(D3D12_DESCRIPTOR_RANGE_TYPE_SRV, 1, 2);
		rootSignatureParams[2].InitAsDescriptorTable(1, &range2);
		CD3DX12_DESCRIPTOR_RANGE range3;
		range3.Init(D3D12_DESCRIPTOR_RANGE_TYPE_SAMPLER, 1, 3);
		rootSignatureParams[3].InitAsDescriptorTable(1, &range3);
		CD3DX12_DESCRIPTOR_RANGE range4;
		range4.Init(D3D12_DESCRIPTOR_RANGE_TYPE_CBV, 1, 4);
		rootSignatureParams[4].InitAsDescriptorTable(1, &range4);



		D3D12_ROOT_SIGNATURE_FLAGS rootSignatureFlags =
			D3D12_ROOT_SIGNATURE_FLAG_ALLOW_INPUT_ASSEMBLER_INPUT_LAYOUT | // the input assembler stage needs access to the constant buffer.
			D3D12_ROOT_SIGNATURE_FLAG_DENY_DOMAIN_SHADER_ROOT_ACCESS |
			D3D12_ROOT_SIGNATURE_FLAG_DENY_GEOMETRY_SHADER_ROOT_ACCESS |
			D3D12_ROOT_SIGNATURE_FLAG_DENY_HULL_SHADER_ROOT_ACCESS;
		//The pixel shader also needs access to the root signature

		CD3DX12_ROOT_SIGNATURE_DESC dx12_rootSignatureDesc;
		dx12_rootSignatureDesc.Init(_countof(rootSignatureParams), rootSignatureParams, 0, nullptr, rootSignatureFlags);

		eng::render_pass_parameter dx12_parameters[4];
		dx12_parameters[0] = { eng::shader_resource::device_buffer };
		dx12_parameters[0].constantBufferParameter.constantBufferDescriptor_rootParamIndex = 0;
		dx12_parameters[1] = { eng::shader_resource::device_buffer };
		dx12_parameters[1].constantBufferParameter.constantBufferDescriptor_rootParamIndex = 1;
		dx12_parameters[2] = { eng::shader_resource::device_texture };
		dx12_parameters[2].textureParameter.shaderResourceView_rootParamIndex = 2;
		dx12_parameters[2].textureParameter.sampler_rootParamIndex = 3;
		dx12_parameters[3] = { eng::shader_resource::device_buffer };
		dx12_parameters[3].constantBufferParameter.constantBufferDescriptor_rootParamIndex = 4;


		eng::D3D12::RenderPass_Desc renderPassDesc = {};
		{
			renderPassDesc.vertexShader = pDefaultControl_vs.get();
			renderPassDesc.pixelShader = pTexture_ps.get();

			renderPassDesc.numParameters = _countof(dx12_parameters);
			renderPassDesc.pParameters = dx12_parameters;

			renderPassDesc.root_signature_desc = dx12_rootSignatureDesc;

			renderPassDesc.cullMode = eng::D3D12::cull_mode::none;

			renderPassDesc.depth_stencil_Desc.DepthEnable = FALSE;

			renderPassDesc.blend_Desc.BlendEnable = TRUE;
			renderPassDesc.blend_Desc.SrcBlend = D3D12_BLEND_SRC_ALPHA;
			renderPassDesc.blend_Desc.DestBlend = D3D12_BLEND_INV_SRC_ALPHA;
			renderPassDesc.blend_Desc.BlendOp = D3D12_BLEND_OP_ADD;
			renderPassDesc.blend_Desc.SrcBlendAlpha = D3D12_BLEND_SRC_ALPHA;
			renderPassDesc.blend_Desc.DestBlendAlpha = D3D12_BLEND_INV_SRC_ALPHA;
			renderPassDesc.blend_Desc.BlendOpAlpha = D3D12_BLEND_OP_ADD;
		}

		pTextureControlRenderPass.reset(new eng::RenderPass2F(renderPassDesc));
	}


	//default control material signature
	{
		std::array<eng::material_parameter, 3> parameters
		{
			eng::material_parameter{ eng::shader_resource::device_buffer, 0 },
			eng::material_parameter{ eng::shader_resource::device_buffer, 1 },
			eng::material_parameter{ eng::shader_resource::device_buffer, (std::numeric_limits<size_t>::max)() }
		};


		std::array<eng::render_pass_binding<eng::vertex2f>, 1> bindings
		{
			eng::render_pass_binding<eng::vertex2f>{ pDefaultControlRenderPass, { 0, 1, 2 } }
		};

		pDefaultControlMaterialSignature = std::make_shared<eng::MaterialSignature2F>(parameters.begin(), parameters.end(), bindings.begin(), bindings.end(), 0);
	}

	// texture material signature
	{
		std::array<eng::material_parameter, 4> parameters =
		{
			eng::material_parameter{ eng::shader_resource::device_buffer, 0 },
			eng::material_parameter{ eng::shader_resource::device_buffer, 1 },
			eng::material_parameter{ eng::shader_resource::device_texture, 2 },
			eng::material_parameter{ eng::shader_resource::device_buffer, (std::numeric_limits<size_t>::max)() }
		};

		std::array<eng::render_pass_binding<eng::vertex2f>, 1> bindings
		{
			eng::render_pass_binding<eng::vertex2f>{ pTextureControlRenderPass, { 0, 1, 2, 3 } }
		};

		pTextureControlMaterialSignature = std::make_unique<eng::MaterialSignature2F>(parameters.begin(), parameters.end(), bindings.begin(), bindings.end(), 0);
	}

	pBillboardMaterial = std::make_shared<eng::Material3f>(*pBillboardMaterialSignature);
	pBillboardMaterial->setTextureShaderResource(2, pStarSprite.get());
	float starColor[] =
	{
		1.0,
		1.0,
		1.0,
		1.000000000f
	};
	pStarColorConstantBuffer = std::make_unique<eng::DeviceBuffer>(sizeof(starColor));
	pStarColorConstantBuffer->store(starColor, sizeof(starColor));

	pBillboardMaterial->setConstBufferShaderResource(3, pStarColorConstantBuffer.get());


	pFontMaterial = std::make_shared<eng::Material2F>(*pTextureControlMaterialSignature);
	float black[] =
	{
		0.0f / 255.0f,
		0.0f / 255.0f,
		0.0f / 255.0f,
		1.000000000f
	};
	pBlackConstantBuffer = std::make_unique<eng::DeviceBuffer>(sizeof(black));
	pBlackConstantBuffer->store(black, sizeof(black));

	pFontMaterial->setConstBufferShaderResource(3, pBlackConstantBuffer.get());

	pTextureMaterial = std::make_shared<eng::Material2F>(*pTextureControlMaterialSignature);
	float white[] =
	{
		1.0f,
		1.0f,
		1.0f,
		1.0f
	};	
	pWhiteConstantBuffer = std::make_unique<eng::DeviceBuffer>(sizeof(white));
	pWhiteConstantBuffer->store(white, sizeof(white));

	pTextureMaterial->setConstBufferShaderResource(3, pWhiteConstantBuffer.get());

	pBackgroundMaterial = std::make_shared<eng::Material2F>(*pDefaultControlMaterialSignature);
	pOutlineMaterial = std::make_shared<eng::Material2F>(*pDefaultControlMaterialSignature);
	pHighlightMaterial = std::make_shared<eng::Material2F>(*pDefaultControlMaterialSignature);
	pPressedControlMaterial = std::make_shared<eng::Material2F>(*pDefaultControlMaterialSignature);


	float lightGrey[] = 
	{ 
		238.0f / 255.0f, 
		238.0f / 255.0f, 
		242.0f / 255.0f, 
		1.000000000f 
	};
	pLightGreyConstantBuffer = std::make_unique<eng::DeviceBuffer>(sizeof(lightGrey));
	pLightGreyConstantBuffer->store(lightGrey, sizeof(lightGrey));

	float darkGrey[] =  
	{ 
		155.0f / 255.0f,
		159.0f / 255.0f, 
		185.0f / 255.0f, 
		1.000000000f 
	};
	pDarkGreyConstantBuffer = std::make_unique<eng::DeviceBuffer>(sizeof(darkGrey));
	pDarkGreyConstantBuffer->store(darkGrey, sizeof(darkGrey));

	float lightBlue[] = 
	{ 
		201.0f / 255.0f, 
		222.0f / 255.0f, 
		245.0f / 255.0f, 
		1.000000000f 
	};
	pLightBlueConstantBuffer = std::make_unique<eng::DeviceBuffer>(sizeof(lightBlue));
	pLightBlueConstantBuffer->store(lightBlue, sizeof(lightBlue));

	float blue[] = 
	{ 
		000.0f / 255.0f, 
		122.0f / 255.0f, 
		204.0f / 255.0f, 
		1.000000000f 
	};
	pBlueConstantBuffer = std::make_unique<eng::DeviceBuffer>(sizeof(blue));
	pBlueConstantBuffer->store(blue, sizeof(blue));


	pBackgroundMaterial->setConstBufferShaderResource(2, pLightGreyConstantBuffer.get());
	pOutlineMaterial->setConstBufferShaderResource(2, pDarkGreyConstantBuffer.get());
	pHighlightMaterial->setConstBufferShaderResource(2, pLightBlueConstantBuffer.get());
	pPressedControlMaterial->setConstBufferShaderResource(2, pBlueConstantBuffer.get());
}

const eng::Font * gravitySim::resources::font()
{
	return pFont.get();
}

const eng::DeviceTexture2 * gravitySim::resources::starSprite()
{
	return pStarSprite.get();
}


const eng::Material2F * gravitySim::resources::fontMaterial()
{
	return pFontMaterial.get();
}

const eng::Material2F * gravitySim::resources::textureMaterial()
{
	return pTextureMaterial.get();
}



const eng::Material3f * gravitySim::resources::billboardMaterial()
{
	return pBillboardMaterial.get();
}

const eng::Material2F * gravitySim::resources::backgroundMaterial()
{
	return pBackgroundMaterial.get();
}

const eng::Material2F * gravitySim::resources::outlineMaterial()
{
	return pOutlineMaterial.get();
}

const eng::Material2F * gravitySim::resources::highlightedMaterial()
{
	return pHighlightMaterial.get();
}

const eng::Material2F * gravitySim::resources::pressedMaterial()
{
	return pPressedControlMaterial.get();
}
