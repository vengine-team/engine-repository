#include <gravitySim/Mass.h>

void gravitySim::Mass::setMass(double mass)
{
	m_mass = mass;
}

double gravitySim::Mass::getMass() const
{
	return m_mass;
}

gravitySim::Mass::~Mass()
{

}
