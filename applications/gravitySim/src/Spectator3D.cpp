#include <gravitySim/Spectator3D.h>

void gravitySim::Spectator3D::onInputChange(const eng::input_state_change& change)
{
	if (m_drag)
	{
		if (change.before.mouse.left_button && !change.current.mouse.left_button)
		{
			m_drag = false;
		}

		if (change.type == eng::input_type::Mouse && change.mouse_property == eng::mouse_property::Position)
		{
			gtm::vector2i pressed_mouse_delta = change.current.mouse.position - change.before.mouse.position;

			gtm::vector2d mouse_relative_delta = static_cast<gtm::vector2d>(pressed_mouse_delta);

			mouse_relative_delta[0] /= static_cast<double>(get_ioFrame()->getClientScale()[0]) * 0.5;
			mouse_relative_delta[1] /= static_cast<double>(get_ioFrame()->getClientScale()[1]) * 0.5;

			transform_t t = local_transform();
			t.linear() = gtm::transform(t.linear(), gtm::to_matrix(gtm::angle_axisd{ mouse_relative_delta[1], gtm::vector3d{-1, 0, 0 } }));
			t.linear() = gtm::transform(t.linear(), gtm::to_matrix(gtm::angle_axisd{ mouse_relative_delta[0], gtm::vector3d{ 0, 1, 0 } }));
			set_local_transform(t);
		}
	}
	else
	{
		if (!change.before.mouse.left_button && change.current.mouse.left_button)
		{
			if (gtm::contains({ { 0,0 }, (gtm::vector2i)m_pioFrame->getClientScale() }, change.current.mouse.position))
			{
				m_drag = true;
			}
		}
	}

	if (!m_captured_mouse)
	{
		if ((!change.before.mouse.left_button && change.current.mouse.left_button ||
			!change.before.mouse.right_button && change.current.mouse.right_button ||
			!change.before.mouse.middle_button && change.current.mouse.middle_button))
		{
			if (gtm::contains({ { 0,0 }, (gtm::vector2i)m_pioFrame->getClientScale() }, change.current.mouse.position))
			{
				m_captured_mouse = true;
			}
		}
	}
	else
	{
		if ((!change.before.mouse.left_button && change.current.mouse.left_button ||
			!change.before.mouse.right_button && change.current.mouse.right_button ||
			!change.before.mouse.middle_button && change.current.mouse.middle_button))
		{
			if (!gtm::contains({ { 0,0 }, (gtm::vector2i)m_pioFrame->getClientScale() }, change.current.mouse.position))
			{
				m_captured_mouse = false;
			}
		};

	}
}

void gravitySim::Spectator3D::onEarlyUpdate()
{
	if (get_ioFrame())
	{
		get_ioFrame()->fetch();

		for (auto i = get_ioFrame()->input().states_begin(); i != get_ioFrame()->input().states_end(); i++)
		{
			onInputChange(*i);
		}


		if (m_captured_mouse)
		{
			gtm::vector3d localMovement = gtm::vector3d::zero();

			if (get_ioFrame()->input().latest_state().keyboard.keyStates['A'])
			{
				localMovement += gtm::vector3d{ 1,0,0 };
			}
			if (get_ioFrame()->input().latest_state().keyboard.keyStates['D'])
			{
				localMovement += gtm::vector3d{ -1,0,0 };
			}
			if (get_ioFrame()->input().latest_state().keyboard.keyStates[VK_SPACE])
			{
				localMovement += gtm::vector3d{ 0, 1, 0 };
			}
			if (get_ioFrame()->input().latest_state().keyboard.keyStates['C'])
			{
				localMovement += gtm::vector3d{ 0,-1, 0 };
			}
			if (get_ioFrame()->input().latest_state().keyboard.keyStates['W'])
			{
				localMovement += gtm::vector3d{ 0, 0, 1 };
			}
			if (get_ioFrame()->input().latest_state().keyboard.keyStates['S'])
			{
				localMovement += gtm::vector3d{ 0, 0,-1 };
			}

			localMovement = localMovement * 10 / scene()->getTargetFrameRate();


			transform_t t = local_transform();

			t.translation() += gtm::transform(t.linear(), localMovement);

			if (get_ioFrame()->input().latest_state().keyboard.keyStates['Q'])
			{
				t.linear() = gtm::transform(t.linear(), gtm::to_matrix(gtm::angle_axisd{ -0.025, gtm::vector3d{ 0, 0, 1 } }));
			}
			if (get_ioFrame()->input().latest_state().keyboard.keyStates['E'])
			{
				t.linear() = gtm::transform(t.linear(), gtm::to_matrix(gtm::angle_axisd{ 0.025, gtm::vector3d{ 0, 0, 1 } }));
			}

			set_local_transform(t);
		}
	}
}

void gravitySim::Spectator3D::set_scene(eng::tsg::Scene * pScene)
{
	if (scene())
	{
		scene()->earlyUpdateEvent() -= m_onEarlyUpdate;
	}

	next::set_scene(pScene);

	if (scene())
	{
		scene()->earlyUpdateEvent() += m_onEarlyUpdate;
	}

}

void gravitySim::Spectator3D::set_ioFrame(eng::IioFrame * pioFrame)
{
	m_pioFrame = pioFrame;
	set_renderTarget(&m_pioFrame->renderTarget());
}

eng::IioFrame * gravitySim::Spectator3D::get_ioFrame()
{
	return m_pioFrame;
}

const eng::IioFrame * gravitySim::Spectator3D::get_ioFrame() const
{
	return m_pioFrame;
}

gravitySim::Spectator3D::Spectator3D()
{

}

gravitySim::Spectator3D::~Spectator3D()
{
	if (scene())
	{
		scene()->earlyUpdateEvent() -= m_onEarlyUpdate;
	}
}
