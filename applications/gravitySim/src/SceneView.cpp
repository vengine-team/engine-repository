#include <gravitySim/SceneView.h>
#include <eng/control/layout.h>

const unsigned int outline_width = 1;

std::unique_ptr<eng::gpl::MenuBar> make_menuBar(const std::vector<std::wstring> s)
{
	std::vector<eng::gpl::Menu_Element_Desc> move_submenu_desc;
	move_submenu_desc.resize(s.size());
	for (size_t i = 0; i < s.size(); i++)
	{
		move_submenu_desc[i].text = s[i];
	}

	eng::gpl::Menu_Element_Desc menu_desc[5] = {};
	menu_desc[0].text = L"Move";
	menu_desc[0].numSubmenuElements = move_submenu_desc.size();
	menu_desc[0].pSubmenuElements = move_submenu_desc.data();
	menu_desc[1].text = L"- -";
	menu_desc[2].text = L" - ";
	menu_desc[3].text = L" + ";
	menu_desc[4].text = L"+ +";

	return std::move(std::make_unique<eng::gpl::MenuBar>(_countof(menu_desc), menu_desc));
}


void gravitySim::SceneView::on_request_close(gravitySim::TopBarMenu &)
{
	m_request_close_event(*this);
}

void gravitySim::SceneView::on_minus_minus_button(eng::IButton &)
{
	std::condition_variable condition;
	std::mutex mutex;
	std::unique_lock<std::mutex> unique_lock(mutex);

	const double relative_amount = 0.5;

	m_pScene->immediate_task_queue().concurrent_push
	(
		[&]()
	{
		std::scoped_lock<std::mutex> lock(mutex);
		m_pScene->remove_stellar_objects((size_t)((double)m_pScene->get_stellarObjects_count() * relative_amount + 0.5));
		condition.notify_one();
	}
	);

	condition.wait(unique_lock);
}
void gravitySim::SceneView::on_minus_button(eng::IButton &)
{
	std::condition_variable condition;
	std::mutex mutex;
	std::unique_lock<std::mutex> unique_lock(mutex);

	const double relative_amount = 0.1;

	m_pScene->immediate_task_queue().concurrent_push
	(
		[&]()
	{
		std::scoped_lock<std::mutex> lock(mutex);
		m_pScene->remove_stellar_objects((size_t)((double)m_pScene->get_stellarObjects_count() * relative_amount + 0.5));
		condition.notify_one();
	}
	);

	condition.wait(unique_lock);
}


void add_scene_stellar_objects(gravitySim::Scene3D* scene, double relative_amount)
{
	size_t amount_to_add = (size_t)((double)scene->get_stellarObjects_count() * relative_amount + 1.0);

	const double speed = 0.05 * std::cbrt(amount_to_add);

	const size_t stellar_x = (size_t)std::sqrt(amount_to_add);
	const size_t stellar_y = 2;
	const size_t stellar_z = (size_t)std::sqrt(amount_to_add);

	std::vector<gravitySim::stellarObjectDesc> objectDescs;
	objectDescs.resize(stellar_x * stellar_y * stellar_z);

	for (size_t x = 0; x < stellar_x; x++)
	{
		for (size_t y = 0; y < stellar_y; y++)
		{

			for (size_t z = 0; z < stellar_z; z++)
			{
				size_t index = x * (stellar_y * stellar_z) + y * stellar_z + z;

				objectDescs[index].mass = 10000000;

				if (x < stellar_x / 2)
				{
					objectDescs[index].momentum = gtm::vector3d{ 0, 0, 1 } *speed;
				}
				else
				{
					objectDescs[index].momentum = gtm::vector3d{ 0, 0, -1 } * speed;
				}

				if (z < stellar_z / 2)
				{
					objectDescs[index].momentum += gtm::vector3d{ -1, 0, 0 } *speed;
				}
				else
				{
					objectDescs[index].momentum += gtm::vector3d{ 1, 0, 0 } *speed;
				}

				objectDescs[index].position = { (double)x * 2, (double)y * 2, (double)z * 2 };
			}
		}
	}

	scene->add_stellar_objects(objectDescs);
}

void gravitySim::SceneView::on_plus_button(eng::IButton &)
{
	std::condition_variable condition;
	std::mutex mutex;
	std::unique_lock<std::mutex> unique_lock(mutex);

	const double relative_amount = 0.2;

	m_pScene->immediate_task_queue().concurrent_push
	(
		[&]()
	{
		std::scoped_lock<std::mutex> lock(mutex);
		add_scene_stellar_objects(m_pScene, relative_amount);
		condition.notify_one();
	}
	);

	condition.wait(unique_lock);
}
void gravitySim::SceneView::on_plus_plus_button(eng::IButton &)
{
	std::condition_variable condition;
	std::mutex mutex;
	std::unique_lock<std::mutex> unique_lock(mutex);

	const double relative_amount = 2.0;

	m_pScene->immediate_task_queue().concurrent_push
	(
		[&]()
	{
		std::scoped_lock<std::mutex> lock(mutex);
		add_scene_stellar_objects(m_pScene, relative_amount);
		condition.notify_one();
	}
	);

	condition.wait(unique_lock);
}

void gravitySim::SceneView::on_request_move(eng::IButton & button)
{
	for (size_t i = 0; i < m_pMenuBar->get_submenu_element(0).get_submenu_element_count(); i++)
	{
		if (&button == &m_pMenuBar->get_submenu_element(0).get_submenu_element(i))
		{
			m_request_move_event(*this, i);
			return;
		}
	}
	assert(false);
}

ext::event_subscribe_access<gravitySim::SceneView&> gravitySim::SceneView::request_close_event()
{
	return { m_request_close_event };
}

ext::event_subscribe_access<gravitySim::SceneView&, size_t> gravitySim::SceneView::request_move_event()
{
	return { m_request_move_event };
}

void gravitySim::SceneView::setMoveTargets(const std::vector<std::wstring> s)
{
	for (size_t i = 0; i < m_move_eventHandlers.size(); i++)
	{
		m_pMenuBar->get_submenu_element(0).get_submenu_element(i).upEvent() -= *m_move_eventHandlers[i];
	}


	m_pMenuBar->get_submenu_element(1).upEvent() -= m_on_minus_minus_button;
	m_pMenuBar->get_submenu_element(2).upEvent() -= m_on_minus_button;
	m_pMenuBar->get_submenu_element(3).upEvent() -= m_on_plus_button;
	m_pMenuBar->get_submenu_element(4).upEvent() -= m_on_plus_plus_button;


	children().erase(std::find(std::begin(children()), std::end(children()), m_pMenuBar.get()));
	m_pMenuBar = make_menuBar(s);
	children().push_back(m_pMenuBar.get());

	m_pMenuBar->get_submenu_element(1).upEvent() += m_on_minus_minus_button;
	m_pMenuBar->get_submenu_element(2).upEvent() += m_on_minus_button;
	m_pMenuBar->get_submenu_element(3).upEvent() += m_on_plus_button;
	m_pMenuBar->get_submenu_element(4).upEvent() += m_on_plus_plus_button;

	if (m_move_eventHandlers.size() < s.size())
	{
		for (size_t i = m_move_eventHandlers.size(); i < s.size(); i++)
		{
			ext::function_ptr<void(SceneView::*)(eng::IButton&)> pFunction = { &gravitySim::SceneView::on_request_move, this };
			auto p = std::make_unique<ext::event_handler<ext::function_ptr<void(SceneView::*)(eng::IButton&)>>>(pFunction);
			m_move_eventHandlers.push_back(std::move(p));
		}
	}
	else
	{
		m_move_eventHandlers.resize(s.size());
	}

	for (size_t i = 0; i < m_move_eventHandlers.size(); i++)
	{
		m_pMenuBar->get_submenu_element(0).get_submenu_element(i).upEvent() += *m_move_eventHandlers[i];
	}

	setScale(getScale());
}

void gravitySim::SceneView::setTextureMaterial(const eng::Material2F * pMaterial)
{
	eng::gpl::ControlParent<eng::gpl::Control>::setTextureMaterial(pMaterial);
	m_ioFrame.setRenderTargetMaterial(pMaterial);
}

void gravitySim::SceneView::setScale(gtm::vector2u scale)
{
	eng::gpl::ControlParent<eng::gpl::Control>::setScale(scale);

	gtm::rectangle<int> rect = eng::control::padded_rectangle(scale, { outline_width, outline_width });

	int pen_y = outline_width;
	eng::control::append_vertical(pen_y, rect, m_topBarMenu);
	eng::control::append_vertical(pen_y, rect, *m_pMenuBar);
	eng::control::append_vertical_fill(pen_y, rect, m_ioFrame);
}

void gravitySim::SceneView::set_sim_scene(gravitySim::Scene3D * pScene)
{
	m_pScene = pScene;
	m_scene3DView.set_scene(pScene);

	m_scene3DView.set_local_transform({ gtm::identity_matrix<double, 3, 3>(), gtm::vector3d{ 0, 0, -3 } });
	m_scene3DView.set_ioFrame(&m_ioFrame);
}

gravitySim::Scene3D* gravitySim::SceneView::get_sim_scene()
{
	return m_pScene;
}

const gravitySim::Scene3D* gravitySim::SceneView::get_sim_scene() const
{
	return m_pScene;
}


gravitySim::SceneView::SceneView(gravitySim::Scene3D * pScene)
{
	children().push_back(&m_topBarMenu);
	m_topBarMenu.set_text(L"Scene");
	m_topBarMenu.request_close_event() += m_on_request_close;

	m_pScene = pScene;

	m_ioFrame.setLocalPosition({ outline_width, outline_width });
	children().push_back(&m_ioFrame);

	set_sim_scene(pScene);

	m_pMenuBar = make_menuBar({});
	children().push_back(m_pMenuBar.get());

	m_pMenuBar->get_submenu_element(1).upEvent() += m_on_minus_minus_button;
	m_pMenuBar->get_submenu_element(2).upEvent() += m_on_minus_button;
	m_pMenuBar->get_submenu_element(3).upEvent() += m_on_plus_button;
	m_pMenuBar->get_submenu_element(4).upEvent() += m_on_plus_plus_button;

	m_pMenuBar->setLocalPosition({ outline_width, outline_width });
}
gravitySim::SceneView::~SceneView()
{
	setMoveTargets({});

	m_pMenuBar->get_submenu_element(1).upEvent() -= m_on_minus_minus_button;
	m_pMenuBar->get_submenu_element(2).upEvent() -= m_on_minus_button;
	m_pMenuBar->get_submenu_element(3).upEvent() -= m_on_plus_button;
	m_pMenuBar->get_submenu_element(4).upEvent() -= m_on_plus_plus_button;

	children().erase(std::find(std::begin(children()), std::end(children()), &m_ioFrame));

	children().erase(std::find(std::begin(children()), std::end(children()), m_pMenuBar.get()));

	m_topBarMenu.request_close_event() -= m_on_request_close;
	children().erase(std::find(std::begin(children()), std::end(children()), &m_topBarMenu));
}
