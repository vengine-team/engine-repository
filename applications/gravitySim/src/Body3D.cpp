#include <gravitySim/Body3D.h>

void gravitySim::Body3d::advance(double seconds)
{
	transform_t t = local_transform();
	t.translation() += m_momentum * seconds;
	set_local_transform(t);
}

void gravitySim::Body3d::set_scene(eng::tsg::Scene* pScene)
{
	if(scene())
	{
		gravitySim::get_PhysicsModule(scene())->m_bodies.erase(m_moduleTicket);
	}

	eng::tsg::Node3d::set_scene(pScene);

	if (scene())
	{
		gravitySim::get_PhysicsModule(scene())->m_bodies.insert({ this, m_moduleTicket });
	}
}

const gtm::vector3d & gravitySim::Body3d::momentum()
{
	return m_momentum;
}

void gravitySim::Body3d::add_momentum(const gtm::vector3d& force)
{
	m_momentum += force;
}
