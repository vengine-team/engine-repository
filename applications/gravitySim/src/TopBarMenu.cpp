#include <gravitySim/TopBarMenu.h>
#include <eng/control/layout.h>

void gravitySim::TopBarMenu::on_button_pressed(eng::IButton &)
{
	m_request_close_event(*this);
}

ext::event_subscribe_access<gravitySim::TopBarMenu&> gravitySim::TopBarMenu::request_close_event()
{
	return { m_request_close_event };
}

void gravitySim::TopBarMenu::set_text(const std::wstring & s)
{
	m_label.set_text(s);
}

const std::wstring & gravitySim::TopBarMenu::get_text()
{
	return m_label.get_text();
}

unsigned int gravitySim::TopBarMenu::calculateMinimumHeight() const
{
	return (std::max)(m_label.calculateMinimumHeight(), m_close_button.calculateMinimumHeight());
}

void gravitySim::TopBarMenu::setScale(gtm::vector2u scale)
{
	eng::gpl::ControlParent<eng::gpl::Control>::setScale(scale);

	m_close_button.setScale(eng::control::calculate_widest_scale(m_close_button));
	m_close_button.setLocalPosition((gtm::vector2i)gtm::vector2u{ getScale()[0] - m_close_button.getScale()[0], 0 });

	m_label.setScale(eng::control::calculate_widest_scale(m_label));
	m_label.setLocalPosition({ 0,0 });
}

gravitySim::TopBarMenu::TopBarMenu()
{
	children().push_back(&m_label);
	m_label.set_text_padding({ 10,5 });
	m_close_button.set_text(L"X");
	m_close_button.upEvent() += m_on_button_pressed;
	children().push_back(&m_close_button);
}

gravitySim::TopBarMenu::~TopBarMenu()
{
	m_close_button.upEvent() -= m_on_button_pressed;

	children().erase(std::find(std::begin(children()), std::end(children()), &m_label));
	children().erase(std::find(std::begin(children()), std::end(children()), &m_close_button));
}
