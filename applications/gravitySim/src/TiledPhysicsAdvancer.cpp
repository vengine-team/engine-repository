#include <gravitySim/TiledPhysicsAdvancer.h>
#include <ext/translation_iterator.h>
#include <Windows.h>

#define GRAVITATIONAL_CONSTANT 0.000000000066742

struct center_mass
{
	gtm::vector<double, 3> center;
	double mass;
};
template<typename index_t>
center_mass recursive_accumulate_mass_center(std::optional<gravitySim::tile_tree_child<double, 3, double, center_mass, index_t>> child)
{
	if (!child)
	{
		return { gtm::vector<double, 3>::zero(), 0 };
	}

	if (child->is_node())
	{
		center_mass sum;
		sum.center = gtm::vector<double, 3>::zero();
		sum.mass = 0;
		
		for (auto i : child->as_node().sectors())
		{
			center_mass cm = recursive_accumulate_mass_center<index_t>(i);
			sum.center += cm.center;
			sum.mass += cm.mass;
		}

		child->as_node().value() = sum;

		return sum;
	}
	else
	{
		gravitySim::tile_tree_leaf<double, 3, double, center_mass, index_t> leaf = child->as_leaf();
		center_mass sum = { leaf.position(), leaf.value() };

		size_t count = 1;

		//while (leaf.next())
		//{
		//	count++;
		//	leaf = *leaf.next();
		//	sum.center += leaf.position();
		//	sum.mass += leaf.value();
		//}

		//OutputDebugString(L"Leaf[");
		//OutputDebugString(std::to_wstring(child->m_managed->m_sector).c_str());
		//OutputDebugString(L"](");
		//OutputDebugString(std::to_wstring(static_cast<gravitySim::tile_tree_leaf_element_data<double, 3, double, center_mass, index_t>*>(child->m_managed) - child->m_leaf_element_data_begin).c_str());
		//OutputDebugString(L") ");
		//OutputDebugString(std::to_wstring(child->depth()).c_str());
		//OutputDebugString(L", ");
		//OutputDebugString(std::to_wstring(count).c_str());
		//OutputDebugString(L" elements\n");

		return sum;
	}
}

void gravitySim::TiledPhysicsAdvancer::advance(double seconds, eng::tsg::Scene * pScene)
{
	seconds = seconds * 10;

	using index_t = size_t;

	PhysicsModule* pModule = get_PhysicsModule(pScene);
	
	size_t required_memory_size_in_bytes = tile_tree<double, 3, double, center_mass, index_t>::get_max_required_memory_size(pModule->bodies().size());
	size_t log_of_size = (size_t)std::log2(required_memory_size_in_bytes + 1);

	if (m_log_of_size != log_of_size)
	{
		m_log_of_size = log_of_size;
		m_cached_memory = std::make_unique<char[]>(gtm::pow(2, log_of_size));

		assert(reinterpret_cast<std::uintptr_t>(m_cached_memory.get()) % sizeof(std::max_align_t) == 0);
	}

	auto translator_functor = [](gravitySim::Body3d const * p) 
	{
		return gravitySim::position_value_pair<double, 3, double>{ p->local_transform().translation(), p->getMass() };
	};

	auto begin = ext::make_translation_iterator(pModule->bodies().begin(), translator_functor);
	auto end = ext::make_translation_iterator(pModule->bodies().end(), translator_functor);

	tile_tree<double, 3, double, center_mass, index_t> tile_tree = { begin, end, reinterpret_cast<char*>(m_cached_memory.get()) };
	//OutputDebugString(L"--------------------------------\n--------------------------------\nExpected count: ");
	//OutputDebugString(std::to_wstring(pModule->bodies().size()).c_str());
	//OutputDebugString(L"\nStructure:\n");
	//index_t count = tile_tree.internal_count_leaf_elements(tile_tree.root(), 0);

	//OutputDebugString(L"\nActual: ");
	//OutputDebugString(std::to_wstring(count).c_str());
	//OutputDebugString(L"\n");

	recursive_accumulate_mass_center(tile_tree.root());

	//if (tile_tree.root())
	//{
	//	if (tile_tree.root()->is_node())
	//	{
	//		center_mass total_mass = tile_tree.root()->as_node().value();
	//		int n = 0;
	//	}
	//}


	constexpr size_t node_data_size = sizeof(tile_tree_node_data<double, 3, double, center_mass, index_t>);
	constexpr size_t leaf_element_data_size = sizeof(tile_tree_leaf_element_data<double, 3, double, center_mass, index_t>);
}
