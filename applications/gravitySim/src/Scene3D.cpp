#include <gravitySim/Scene3D.h>
#include <gravitySim/TiledPhysicsAdvancer.h>

size_t gravitySim::Scene3D::get_stellarObjects_count() const
{
	return m_stellarObjects.size();
}

gtm::vector3d gravitySim::Scene3D::calculate_center_of_mass() const
{
	double total_mass = 0;
	gtm::vector3d out = gtm::vector3d::zero();

	for (auto i = std::begin(m_stellarObjects); i != std::end(m_stellarObjects); i++)
	{
		out += (*i)->transform().translation() * (*i)->getMass();
		total_mass += (*i)->getMass();
	}

	out /= total_mass;

	return out;
}

void gravitySim::Scene3D::remove_stellar_objects(size_t count)
{
	if (count > m_stellarObjects.size())
	{
		throw std::invalid_argument("count was larget than get_stellarObjects_count()!");
	}
	else
	{
		if (count != 0)
		{
			size_t diff = m_stellarObjects.size() / count;

			size_t i = 0;

			for (auto pos = std::begin(m_stellarObjects); i < count;)
			{
				(*pos)->set_scene(nullptr);

				pos = m_stellarObjects.erase(pos);

				pos += (diff - 1);
				i++;
			}
		}
	}
}

void gravitySim::Scene3D::add_stellar_objects(const std::vector<stellarObjectDesc>& objects)
{

	size_t j = m_stellarObjects.size();
	m_stellarObjects.resize(m_stellarObjects.size() + objects.size());

	for (auto i = std::cbegin(objects); i != std::cend(objects); i++, j++)
	{
		m_stellarObjects[j] = std::make_unique<StellarObjekt3D>();
		m_stellarObjects[j]->set_scene(this);

		m_stellarObjects[j]->set_local_transform({ gtm::identity_matrix<double, 3, 3>(), i->position});

		m_stellarObjects[j]->setMass(i->mass);
		m_stellarObjects[j]->add_momentum(i->momentum);
	}

	OutputDebugString(std::wstring(L"Total Object count: ").append(std::to_wstring(m_stellarObjects.size())).append(L"\n").c_str());
}

gravitySim::Scene3D::Scene3D()
{
	//get_PhysicsModule(this)->advancer() = std::make_unique<TiledPhysicsAdvancer>();
}

gravitySim::Scene3D::~Scene3D()
{
	for (auto i = std::begin(m_stellarObjects); i < std::end(m_stellarObjects); i++)
	{
		(*i)->set_scene(nullptr);
	}
}