#pragma once
#include "NodeTest.h"
#include <Event_Node.h>

class BlueNode
{
	int m_blueNumber;
public:
	virtual void setBlueNumber(int blueNumber)
	{
		m_blueNumber = blueNumber;
	}
	int getBlueNumber()
	{
		return m_blueNumber;
	}
};

class BlueNodeChild;

class BlueNodeParent : public virtual BlueNode, public eng::Basic_EventParent<BlueNodeChild>
{
	virtual void setBlueNumber(int blueNumber) override;
};
class BlueNodeChild : public virtual BlueNode, public eng::Basic_EventChild<BlueNodeParent>
{

};

void BlueNodeParent::setBlueNumber(int blueNumber)
{
	BlueNode::setBlueNumber(blueNumber);

	for (auto i = getChildren().begin(); i != getChildren().end(); i++)
	{
		(*i)->setBlueNumber(blueNumber);
	}
}




class RedBlueNode : public virtual BlueNode
{
	int m_blackNumber;
public:
	virtual void setRedNumber(int blueNumber)
	{
		m_blackNumber = blueNumber;
	}
	int getRedNumber()
	{
		return m_blackNumber;
	}
};

class RedBlueNodeChild;

class RedBlueNodeParent : public virtual RedBlueNode, public eng::Basic_EventParent<RedBlueNodeChild>
{
	virtual void setRedNumber(int blueNumber) override;
};

class RedBlueNodeChild : public virtual RedBlueNode, public eng::Basic_EventChild<RedBlueNodeParent>
{

};




void RedBlueNodeParent::setRedNumber(int blackNumber)
{
	RedBlueNode::setRedNumber(blackNumber);

	for (auto i = getChildren().begin(); i != getChildren().end(); i++)
	{
		(*i)->setRedNumber(blackNumber);
	}
}

class RedBlue_Blue_InterfaceNode : public BlueNodeParent, public RedBlueNodeParent, public BlueNodeChild
{

};

int nodeTest()
{
	BlueNodeParent* pRoot = new BlueNodeParent();

	RedBlue_Blue_InterfaceNode* pSpecialNode = new RedBlue_Blue_InterfaceNode();
	pRoot->push_back_child(pSpecialNode);

	BlueNodeChild* pBlueChild = new BlueNodeChild();
	pSpecialNode->BlueNodeParent::push_back_child(pBlueChild);

	RedBlueNodeChild* pRedBlueChild = new RedBlueNodeChild();
	pSpecialNode->RedBlueNodeParent::push_back_child(pRedBlueChild);

	delete pRoot;

	return 0;
}

