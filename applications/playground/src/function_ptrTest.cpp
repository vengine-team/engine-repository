#include "function_ptrTest.h"
#include <iostream>

int add(int a, int b)
{
	return a + b;
}

class bar
{
	int add(int a, int b)
	{
		return a + b;
	}
public:
	eng::functor<&bar::add> m_functor = { this };
};


int function_ptrTest()
{
	bar b;

	return b.m_functor(2, 3) + b.m_functor(2, 3);
}
