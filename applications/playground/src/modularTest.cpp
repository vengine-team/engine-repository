#include "ModularTest.h"
#include <Modular.h>
#include <future>
#include <string>

class ModularTest;

class IModularTestModule : public eng::module_base<ModularTest>
{
public:
	virtual ~IModularTestModule()
	{

	}
};

class ModularTest : public eng::Modular<ModularTest, IModularTestModule>
{
	int m_number = 0;
};

class IModularTestStringModule : public IModularTestModule
{
	std::string m_string;
};

class IModularTestFloatModule : public IModularTestModule
{
	float m_floatNumber;
};


int modularTest()
{
	ModularTest modular0;


	eng::module_key<IModularTestFloatModule> floatVectorModuleKey;
	ModularTest::addModule(floatVectorModuleKey);

	eng::module_key<IModularTestStringModule> stringVectorModuleKey;
	ModularTest::addModule(stringVectorModuleKey);

	IModularTestFloatModule* pModular0_IntVectorModule = modular0.getModule(floatVectorModuleKey);

	ModularTest modular1;

	IModularTestStringModule* pModular1_IntVectorModule = modular1.getModule(stringVectorModuleKey);

	ModularTest::removeModule(stringVectorModuleKey);


	ModularTest::removeModule(floatVectorModuleKey);


	return 0;
}
