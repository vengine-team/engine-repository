#include "materialRootSignature.h"
#include <iostream>
#include <MaterialSignature.h>
#include <Material.h>

int materialRootSignatureTest()
{
	eng::resource_Binding_Desc first_types[3] = 
	{ 
		{ 0, 0 }, 
		{ 1, 0 }, 
		{ 2, 0 } 
	};
	eng::resource_Binding_Desc second_types[3] =
	{
		{ 0, 1 }, 
		{ 1, 1 }, 
		{ 2, 1 } 
	};

	eng::RootParameter_Desc parameters[2] = 
	{
		{ eng::ShaderResource::DeviceBuffer, 3, first_types }, 
		{ eng::ShaderResource::DeviceTexture2, 3, second_types } 
	};

	eng::RootSignature_Desc rootSignatureDesc = { 2,parameters };

	eng::RootSignature materialRootSignatureTest(rootSignatureDesc);

	eng::RootSignature materialRootSignatureTestCopy = materialRootSignatureTest;

	//std::cout << std::to_string(materialRootSignatureTest) << std::endl;

	return 0;
}
