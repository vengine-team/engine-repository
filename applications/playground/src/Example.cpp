#include <UpdateThread.h>
#include <string>
#include <event.h>

class ModelScene : public eng::IUpdateable
{
public:
	ModelScene(std::wstring modelPath)
	{
		//initialize model
	}
	virtual ~ModelScene()
	{
		//free model
	}
	virtual void updateThreadRegistered(eng::UpdateThread* pUpdateThread)
	{

	}
	virtual void updateThreadUnregistered(eng::UpdateThread* pUpdateThread)
	{

	}
	virtual eng::clock_t::time_point updateFrame(eng::clock_t::time_point end)
	{
		eng::clock_t::time_point begin = eng::clock_t::now();
		// render 3 dimensional model to render Target
		return begin + std::chrono::milliseconds(1000 / 60);
	}
};
class UIScene : public eng::IUpdateable
{
	ModelScene* m_pModelScene;
	eng::event<> m_closePressedEvent;
public:
	eng::event<>& closePressedEvent()
	{
		return m_closePressedEvent;
	}
	UIScene(ModelScene* pModelScene)
	{
		m_pModelScene = pModelScene;
		//create window
	}
	virtual ~UIScene()
	{
		//close window
	}
	virtual void updateThreadRegistered(eng::UpdateThread* pUpdateThread)
	{

	}
	virtual void updateThreadUnregistered(eng::UpdateThread* pUpdateThread)
	{

	}
	virtual eng::clock_t::time_point updateFrame(eng::clock_t::time_point end)
	{
		eng::clock_t::time_point begin = eng::clock_t::now();
		// render UI and the model render Target into this render Target
		return begin + std::chrono::milliseconds(1000 / 60);
	}
};

eng::UpdateThread* pModelUpdateThread;
eng::UpdateThread* pUIUpdateThread;

void onClose()
{
	pModelUpdateThread->concurrentTerminate();
	pUIUpdateThread->concurrentTerminate();
}

int _main()
{
	eng::UpdateThread modelUpdateThread;
	pModelUpdateThread = &modelUpdateThread;
	eng::UpdateThread uiUpdateThread;
	pUIUpdateThread = &uiUpdateThread;


	ModelScene modelScene(L"C:\\Users\\Martin Newell\\Models\\utah_teapot.obj");

	UIScene uiScene(&modelScene);
	eng::event_handler<eng::functor<&onClose>> onCloseHandler = { {} };
	uiScene.closePressedEvent() += onCloseHandler;


	uiUpdateThread.concurrentRegisterUpdateable(&uiScene);
	modelUpdateThread.concurrentRegisterUpdateable(&modelScene);

	uiUpdateThread.join();
	modelUpdateThread.join();

	uiScene.closePressedEvent() -= onCloseHandler;

	return 0;
}