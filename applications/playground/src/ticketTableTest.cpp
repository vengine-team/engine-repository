#include "ticketTableTest.h"

int ticketTableTest()
{
	ext::ticket_table<int> test_group;

	ext::ticket keys[10];


	for (size_t i = 0; i < 10; i++)
	{
		test_group.insert({ static_cast<int>(i), keys[i] });
	}


	for (size_t i = 0; i < 10; i++)
	{
		test_group.erase(keys[i]);
	}

	return 0;
}