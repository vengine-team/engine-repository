#include "eventHandlerTest.h"
#include <event.h>
#include <eventHandler.h>


int eventHandlerTest_number = 0;

struct bar
{
	void addInt(int number)
	{
		eventHandlerTest_number += number;
	}
};


int eventHandlerTest()
{
	bar b;

	static const size_t testCount = 100;

	eventHandlerTest_number = 0;

	eng::event<int> testEvent;

	std::unique_ptr<eng::event_handler<eng::functor<&bar::addInt>>> event_handler[testCount];

	for (size_t i = 0; i < testCount; i++)
	{
		event_handler[i].reset(new eng::event_handler<eng::functor<&bar::addInt>>({ &b }));
		event_handler[i]->setEvent(testEvent);
	}

	testEvent(4);

	for (size_t i = 0; i < testCount; i++)
	{
		event_handler[i]->unsetEvent(testEvent);
	}

	return eventHandlerTest_number;
}