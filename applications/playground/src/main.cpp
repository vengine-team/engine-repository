#include <iostream>

template<auto F, typename R, typename T, typename ...P>

struct member_functor
{

	T* pInstance;

	R operator()(P... args)
	{

		return (*this->pInstance.*F)(args...);

	}
};

class base { };

class bar : virtual base
{
	void print_result(int a, int b)
	{

		std::cout << a + b << std::endl;

	}

	member_functor<&bar::print_result, void, bar, int, int> functor = { this };

public: 

};

int main()
{

	//bar b; b.functor(40, 2); return 0;
}


