#pragma once
#include <string>
#include <iostream>
#include <ctime>

using testFunction = int(*)();

void EvaluateTest(testFunction function, std::string testName)
{
	std::cout << testName + ": " << std::endl << std::endl;

	long t1 = std::clock();

	int result = function();

	long t2 = std::clock();

	std::cout << "result: " << result << "  time: " << t2 - t1 << " milliseconds" << std::endl;
}