#pragma once
#include <eng/engineEntry.h>
#include <eng/Window.h>
#include <par/UpdateThread.h>

#include <celestials/System.h>
#include <celestials/Spectator3d.h>

#include <eng/resources/default.h>

///
/// Entry point for the program
///
int engMain()
{
	gtm::vector<double, 3> rgbPart = { 1.0,1.0,1.0, };
	gtm::vector<double, 4> color = concat(rgbPart, 0.0);

	eng::resources::load_resources();

	eng::UpdateThread updateThread;

	celestials::System system;
	
	eng::Window mainWindow = { L"Celestials Test", { 0,0 }, { 300, 300 }, eng::WindowStyle::SystemDefault, true };
	mainWindow.open();

	celestials::Spectator3D spectator;
	spectator.set_local_transform({ gtm::to_matrix(gtm::angle_axisd{ -M_PI_2, gtm::vector3d{ -1,0,0 } }), gtm::vector3d{ 0,10,0 } });
	spectator.set_scene(&system);
	spectator.set_ioFrame(&mainWindow);

	updateThread.concurrentRegisterUpdateable(&system);

	while (true)
	{
		std::this_thread::sleep_for(std::chrono::seconds(1));
	}

	updateThread.concurrentTerminate();
	updateThread.join();

	return 0;
}
