#include <celestials/resources.h>

eng::Mesh3f celestials::resources::make_smooth_tetrahedron()
{
	/*    0


	1           3
	      2
	*/

	gtm::vector3f p0 = { 0, 1, 0 };
	gtm::vector3f p1 = { (float)-sqrt(2.0 / 9.0),  -1.0f / 3.0f, (float)sqrt(2.0 / 3.0) };
	gtm::vector3f p2 = { (float)-sqrt(2.0 / 9.0),  -1.0f / 3.0f, (float)-sqrt(2.0 / 3.0) };
	gtm::vector3f p3 = { (float)sqrt(8.0 / 9.0), -1.0f / 3.0f , 0 };

	float p0m = magnitude(p0);
	float p1m = magnitude(p1);
	float p2m = magnitude(p2);
	float p3m = magnitude(p3);

	std::vector<eng::vertex3f> vertices
	{
		{ p0, p0, p0 },
		{ p1, p1, p1 },
		{ p2, p2, p2 },
		{ p3, p3, p3 }
	};

	std::vector<eng::triangle_indices> triangles =
	{
		{0, 1, 2},
		{0, 3, 1},
		{3, 0, 2},
		{1, 3, 2}
	};

	eng::Mesh3f out;
	out.vertices() = vertices;
	out.triangles() = triangles;

	return out;
}

eng::Mesh3f celestials::resources::make_smooth_icosahedron()
{
	const float radius = 1.0f;
	const float radius_2 = radius / 2.0f;
	const float edge_length = radius / sin((2.0f * (float)M_PI) / 5.0f);
	const float edge_length_2 = edge_length / 2.0f;
	const float edge_center_radius = sqrt(radius * radius - edge_length_2 * edge_length_2);

	std::vector<eng::vertex3f> vertices = 
	{
		//violet plane
		{ gtm::vector3f{ 0, edge_center_radius, edge_length_2} }, // +Y / Up
		{ gtm::vector3f{ 0, edge_center_radius,-edge_length_2} },
		{ gtm::vector3f{ 0,-edge_center_radius, edge_length_2} }, // -Y / Down
		{ gtm::vector3f{ 0,-edge_center_radius,-edge_length_2} },

		// green plane
		{ gtm::vector3f{ edge_center_radius, edge_length_2, 0} }, // +X / right
		{ gtm::vector3f{ edge_center_radius,-edge_length_2, 0} },
		{ gtm::vector3f{-edge_center_radius, edge_length_2, 0} }, // -X / left
		{ gtm::vector3f{-edge_center_radius,-edge_length_2, 0} },

		// light green plane
		{ gtm::vector3f{ edge_length_2, 0, edge_center_radius} }, // +Z / right
		{ gtm::vector3f{-edge_length_2, 0, edge_center_radius} },
		{ gtm::vector3f{ edge_length_2, 0,-edge_center_radius} }, // -Z / right
		{ gtm::vector3f{-edge_length_2, 0,-edge_center_radius} }
	};

	std::vector<eng::triangle_indices> triangles;

	for (eng::index_type rect_i = 0; rect_i < 3; rect_i++)
	{
		const eng::index_type rect_start_i = rect_i * 4;

		const eng::index_type connected_plane_start_i = ((rect_i + 1) % 3) * 4;

		triangles.push_back({ rect_start_i, rect_start_i + 1, connected_plane_start_i });
		triangles.push_back({ rect_start_i, connected_plane_start_i + 2, rect_start_i + 1 });

		triangles.push_back({ rect_start_i + 2, connected_plane_start_i + 1, rect_start_i + 3 });
		triangles.push_back({ rect_start_i + 2, rect_start_i + 3, connected_plane_start_i + 3 });
	}

	triangles.push_back({ 0, 4, 8 });
	triangles.push_back({ 0, 9, 6 });

	triangles.push_back({ 1, 6, 11 });
	triangles.push_back({ 1, 10, 4 });


	triangles.push_back({ 2, 8, 5 });
	triangles.push_back({ 2, 7, 9 });

	triangles.push_back({ 3, 11, 7 });
	triangles.push_back({ 3, 5, 10 });

	eng::Mesh3f out;
	out.triangles() = triangles;
	out.vertices() = vertices;
	return out;
}