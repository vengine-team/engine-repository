#include <celestials/Celestial.h>

gtm::vector3d celestials::orbit3d::calculate_position(eng::tsg::general_purpose_clock::time_point t) const
{
	std::chrono::duration<double> time_since_epoch = t.time_since_epoch();
	double completed_orbits = time_since_epoch / std::chrono::duration<double>(period);
	time_since_epoch = std::chrono::duration<double>(period) * (completed_orbits - std::floor(completed_orbits));
	double mean_velocity = (2.0 * M_PI) / std::chrono::duration<double>(period).count();
	double mean_anomaly = mean_velocity * (time_since_epoch.count() - 0);

	double eccentric_anomaly = chached_eccentric_anomaly;

	//eccentric_anomaly = f(x) = x - eccentricity * sin(x) - mean_anomaly
	//f'(x) = 1 - cos(x)

	for (size_t i = 0; i < 128; i++)
	{
		double f_at_out = eccentric_anomaly - eccentricity * sin(eccentric_anomaly) - mean_anomaly;
		eccentric_anomaly = eccentric_anomaly - (f_at_out / (1 - eccentricity * cos(eccentric_anomaly)));
	}

	chached_eccentric_anomaly = eccentric_anomaly;

	//general ellipse properties
	double center_x = (periapsis * eccentricity);
	double semi_major_axis = periapsis + center_x;

	//calculate position
	double x = semi_major_axis * (cos(eccentric_anomaly) - eccentricity);
	double z = semi_major_axis * sin(eccentric_anomaly) * sqrt(1 - eccentricity * eccentricity);

	return { x, 0, z };
}


void celestials::Celestial::update()
{
	eng::tsg::Node3d::set_local_transform({ gtm::identity_matrix<double, 3, 3>() ,orbit().calculate_position(scene()->clock().now()) });
}

void celestials::Celestial::set_scene(scene_t * pScene)
{
	if (scene())
	{
		scene()->updateEvent() -= m_update_event_handler;
	}

	eng::tsg::Node3d::set_scene(pScene);

	if (scene())
	{
		scene()->updateEvent() += m_update_event_handler;
	}
}

void celestials::Celestial::set_local_transform(const transform_t& t)
{
	assert(false);
}

celestials::Celestial * celestials::Celestial::get_central_body()
{
	return m_pCentral_body;
}

const celestials::Celestial * celestials::Celestial::get_central_body() const
{
	return m_pCentral_body;
}

void celestials::Celestial::set_central_body(Celestial * pCentral_body)
{
	if (m_pCentral_body == nullptr)
	{
		m_pCentral_body->m_satellites.erase(m_parent_satellites_ticket);
	}

	m_pCentral_body = pCentral_body;

	if (pCentral_body != nullptr)
	{
		pCentral_body->m_satellites.insert({ this, m_parent_satellites_ticket });
	}
}

const celestials::orbit3d & celestials::Celestial::orbit() const
{
	return m_orbit;
}

const ext::ticket_table<celestials::Celestial*>& celestials::Celestial::satellites() const
{
	return m_satellites;
}

celestials::Celestial::Celestial()
{
}

celestials::Celestial::~Celestial()
{
}
