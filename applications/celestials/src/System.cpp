#include <celestials/System.h>

celestials::Celestial * celestials::System::central_body()
{
	return m_pCentral_body.get();
}

celestials::System::System()
{
	std::unique_ptr<celestials::Terrestrial> pTestBody = std::make_unique<celestials::Terrestrial>();
	pTestBody->set_scene(this);

	m_pCentral_body = std::move(pTestBody);
}

celestials::System::~System()
{

}
