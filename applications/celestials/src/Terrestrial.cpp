#include <celestials/Terrestrial.h>
#include <celestials/resources.h>
#include <celestials/tessellation.h>

#include <eng/resources/default.h>

#include <gtm/noise.h>


celestials::Terrestrial::Terrestrial()
{
	eng::Mesh3f mesh = resources::make_smooth_icosahedron();


	for (size_t i = 0; i < 4; i++)
	{
		mesh = celestials::subdivide_triangles(mesh, [](const eng::vertex3f& a, const eng::vertex3f& b) {return eng::lerp(a, b, 0.5f); });
	}

	for (auto& i : mesh.vertices())
	{
		i.position = normalize(i.position);

		float frequency = 3.0;
		float weight = 1.0;
		float perlin_result = 0;
		for (size_t octave_i = 0; octave_i < 4; octave_i++)
		{
			perlin_result += gtm::perlin(gtm::vector3f{ i.position[0] * frequency, i.position[1] * frequency, i.position[2] * frequency }) * weight;
			frequency *= 2.0f;
			weight *= 0.5f;
		}

		i.position *= 1 + (perlin_result * 0.05f);
	}

	mesh = celestials::disconnect_triangles(mesh);
	eng::recalculateNormals(mesh);

	m_deviceMesh = eng::DeviceMesh3f(mesh);

	set_mesh(&m_deviceMesh);
	set_material({ eng::resources::simpleLightMaterial() });
}

celestials::Terrestrial::~Terrestrial()
{

}
