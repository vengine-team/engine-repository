#pragma once
#include "celestials_Celestial.h"

namespace celestials
{
	class Star : public Celestial
	{
	private:

	public:
		virtual ISurface* surface();
		virtual const ISurface* surface() const;

		virtual IAtmosphere* atmosphere();
		virtual const IAtmosphere* atmosphere() const;

		virtual ~Star() = default;
	};
}