#pragma once
#include <celestials/Terrestrial.h>

#include <eng/tsg/Scene.h>


namespace celestials
{
	class System : public eng::tsg::Scene
	{
		std::unique_ptr<celestials::Celestial> m_pCentral_body = nullptr;
	public:
		celestials::Celestial* central_body();

		System();
		virtual ~System();
	};
}