#pragma once
#include <celestials/Terrestrial.h>

namespace celestials
{
	std::unique_ptr<Celestial> generate_star_system(int seed);
}