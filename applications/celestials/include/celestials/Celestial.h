#pragma once
#include <ext/ticket_table.h>
#include <memory>
#include <eng/tsg/Node3.h>
#include <gtm/math.h>

namespace celestials
{
	class IAtmosphere
	{
	public:
		virtual ~IAtmosphere() = default;
	};

	class ISurface
	{
	public:
		virtual ~ISurface() = default;
	};

	struct orbit3d
	{
		double eccentricity = 0.7;
		double periapsis = 2;
		mutable double chached_eccentric_anomaly;
		eng::tsg::general_purpose_clock::duration period = std::chrono::seconds(10);//in seconds

		gtm::vector3d calculate_position(eng::tsg::general_purpose_clock::time_point t) const;
	};

	class Celestial : public eng::tsg::Node3d
	{
	private:
		ext::ticket m_parent_satellites_ticket;
		Celestial * m_pCentral_body = nullptr;
		ext::ticket_table<Celestial*> m_satellites;
		orbit3d m_orbit;
	protected:
		virtual void update();
		ext::event_handler<ext::function_ptr<decltype(&Celestial::update)>> m_update_event_handler = { { &Celestial::update, this } };

		virtual void set_scene(eng::tsg::Scene* pScene) override;
	public:

		virtual void set_local_transform(const transform_t& t) override;

		Celestial* get_central_body();
		const Celestial* get_central_body() const;
		void set_central_body(Celestial* pCentralBody);

		const orbit3d& orbit() const;

		const ext::ticket_table<Celestial*>& satellites() const;

		//virtual ISurface* surface() = 0;
		//virtual const ISurface* surface() const = 0;

		//virtual IAtmosphere* atmosphere() = 0;
		//virtual const IAtmosphere* atmosphere() const = 0;

		Celestial();
		virtual ~Celestial();
	};
}
