#pragma once
#include <eng/Mesh.h>
#include <array>

namespace celestials
{

	template<typename vertex_t, typename F>
	eng::index_type _subdivide_line_or_match(std::vector<vertex_t>& vertices, size_t initial_vertex_count, std::vector<eng::line_indices>& lines, eng::line_indices line, F functor)
	{
		eng::index_type out;

		auto match = std::find(lines.begin(), lines.end(), line);
		if (match == lines.end())
		{
			out = static_cast<eng::index_type>(vertices.size());

			const vertex_t& a = vertices[line[0]];
			const vertex_t& b = vertices[line[1]];

			vertices.push_back(functor(a, b));
			lines.push_back(line);
		}
		else
		{
			out = static_cast<eng::index_type>(initial_vertex_count+ std::distance(lines.begin(), match));
		}
		return out;
	}

	template<typename vertex_t, typename F>
	eng::Mesh<vertex_t> subdivide_triangles(const eng::Mesh<vertex_t>& mesh, F functor, size_t count)
	{
		std::vector<vertex_t> vertices = mesh.vertices();
		std::vector<eng::triangle_indices> triangles = mesh.triangles();
		std::vector<eng::line_indices> lines;

		for (size_t i = 0; i < mesh.triangles().size(); i++)
		{
			eng::line_indices c = { triangles[i][0], triangles[i][1] };
			if (c[0] > c[1])
			{
				std::swap(c[0], c[1]);
			}
			eng::line_indices a = { triangles[i][1], triangles[i][2] };
			if (a[0] > a[1])
			{
				std::swap(a[0], a[1]);
			}
			eng::line_indices b = { triangles[i][2], triangles[i][0] };
			if (b[0] > b[1])
			{
				std::swap(b[0], b[1]);
			}

			eng::index_type c_vertex_index = _subdivide_line_or_match(vertices, mesh.vertices().size(), lines, c, functor);
			eng::index_type a_vertex_index = _subdivide_line_or_match(vertices, mesh.vertices().size(), lines, a, functor);
			eng::index_type b_vertex_index = _subdivide_line_or_match(vertices, mesh.vertices().size(), lines, b, functor);

			vertex_t& _a = vertices[a_vertex_index];
			vertex_t& _b = vertices[b_vertex_index];
			vertex_t& _c = vertices[c_vertex_index];

			triangles.push_back({ triangles[i][0],c_vertex_index, b_vertex_index });
			triangles.push_back({ triangles[i][1],a_vertex_index, c_vertex_index });
			triangles.push_back({ triangles[i][2],b_vertex_index, a_vertex_index });
			triangles[i] = { c_vertex_index, a_vertex_index, b_vertex_index };
		}
		eng::Mesh<vertex_t> out;
		out.triangles() = triangles;
		out.vertices() = vertices;
		return out;
	}

	template<typename vertex_t, typename F>
	eng::Mesh<vertex_t> subdivide_triangles(const eng::Mesh<vertex_t>& mesh, F functor)
	{
		std::vector<vertex_t> vertices = mesh.vertices();
		std::vector<eng::triangle_indices> triangles = mesh.triangles();
		std::vector<eng::line_indices> lines;

		for (size_t i = 0; i < mesh.triangles().size(); i++)
		{
			eng::line_indices c = { triangles[i][0], triangles[i][1] };
			if (c[0] > c[1])
			{
				std::swap(c[0], c[1]);
			}
			eng::line_indices a = { triangles[i][1], triangles[i][2] };
			if (a[0] > a[1])
			{
				std::swap(a[0], a[1]);
			}
			eng::line_indices b = { triangles[i][2], triangles[i][0] };
			if (b[0] > b[1])
			{
				std::swap(b[0], b[1]);
			}

			eng::index_type c_vertex_index = _subdivide_line_or_match(vertices, mesh.vertices().size(), lines, c, functor);
			eng::index_type a_vertex_index = _subdivide_line_or_match(vertices, mesh.vertices().size(), lines, a, functor);
			eng::index_type b_vertex_index = _subdivide_line_or_match(vertices, mesh.vertices().size(), lines, b, functor);

			vertex_t& _a = vertices[a_vertex_index];
			vertex_t& _b = vertices[b_vertex_index];
			vertex_t& _c = vertices[c_vertex_index];

			triangles.push_back({ triangles[i][0],c_vertex_index, b_vertex_index });
			triangles.push_back({ triangles[i][1],a_vertex_index, c_vertex_index });
			triangles.push_back({ triangles[i][2],b_vertex_index, a_vertex_index });
			triangles[i] = { c_vertex_index, a_vertex_index, b_vertex_index };
		}
		eng::Mesh<vertex_t> out;
		out.triangles() = triangles;
		out.vertices() = vertices;
		return out;
	}

	template<typename vertex_t>
	eng::Mesh<vertex_t> disconnect_triangles(const eng::Mesh<vertex_t>& mesh)
	{
		std::vector<vertex_t> vertices;
		std::vector<eng::triangle_indices> triangles;

		for (size_t i = 0; i < mesh.triangles().size(); i++)
		{
			eng::index_type vertices_size = static_cast<eng::index_type>(vertices.size());

			triangles.push_back({ vertices_size, vertices_size + 1, vertices_size + 2 });
			vertices.push_back(mesh.vertices()[mesh.triangles()[i][0]]);
			vertices.push_back(mesh.vertices()[mesh.triangles()[i][1]]);
			vertices.push_back(mesh.vertices()[mesh.triangles()[i][2]]);
		}

		eng::Mesh<vertex_t> out;
		out.vertices() = vertices;
		out.triangles() = triangles;
		return out;
	}
};
