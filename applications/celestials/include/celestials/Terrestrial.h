#pragma once
#include <celestials/Celestial.h>

#include <eng/tsg/Node3.h>
#include <eng/tsg/MeshRenderer3.h>
#include <eng/tsg/ModelMatrix.h>


namespace celestials
{
	class Terrestrial : public eng::tsg::add_components<Celestial, eng::tsg::MeshRenderer3>
	{
		eng::DeviceMesh3f m_deviceMesh = { {} };

	public:

		//virtual Surface* surface() override;
		//virtual const Surface* surface() const override;

		//virtual Atmosphere* atmosphere() override;
		//virtual const Atmosphere* atmosphere() const override;

		Terrestrial();
		virtual ~Terrestrial();
	};
};