#pragma once
#include <eng/tsg/Node3.h>
#include <eng/tsg/Camera3.h>

#include <eng/IioFrame.h>

namespace celestials
{
	class Spectator3D : public eng::tsg::add_components<eng::tsg::Node3d, eng::tsg::Camera3>
	{
		eng::IioFrame* m_pioFrame = nullptr;

		bool m_captured_mouse = false;
		bool m_drag = false;

		void onInputChange(const eng::input_state_change& change);

		void onEarlyUpdate();
		ext::event_handler<ext::functor<&Spectator3D::onEarlyUpdate>> m_onEarlyUpdate = { { this } };

	public:

		virtual void set_scene(eng::tsg::Scene* pScene) override;

		void set_ioFrame(eng::IioFrame* pioFrame);

		eng::IioFrame* get_ioFrame();
		const eng::IioFrame* get_ioFrame() const;

		Spectator3D();
		virtual ~Spectator3D();
	};
};