#pragma once
#include <eng/defaultDraw.h>

namespace celestials
{
	namespace resources
	{
		eng::Mesh3f make_smooth_tetrahedron();
		eng::Mesh3f make_smooth_icosahedron();
	};
};