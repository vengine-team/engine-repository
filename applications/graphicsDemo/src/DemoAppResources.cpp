#include <filesystem>

#include <graphicsDemo/DemoAppResources.h>
#include <eng/graphicsUtil.h>

namespace graphicsDemo
{
	namespace resources
	{
		std::shared_ptr<eng::DeviceBuffer const> pOutlineProperties;

		std::shared_ptr<eng::D3D12::Shader const> pSampleVertexShader;
		std::shared_ptr<eng::D3D12::Shader const> pSampleFragmentShader;
		std::shared_ptr<eng::D3D12::Shader const> pSampleTextureFragmentShader;

		std::shared_ptr<eng::D3D12::Shader const> pSimpleLightVertexShader;
		std::shared_ptr<eng::D3D12::Shader const> pSimpleLightFragmentShader;


		std::shared_ptr<eng::Font const> pFont;


		std::shared_ptr<eng::DeviceTexture2 const> pTexture2;

		std::shared_ptr<eng::D3D12::Shader const> pOutline_vs;
		std::shared_ptr<eng::D3D12::Shader const> pOutline_ps;
		std::shared_ptr<eng::RenderPass3f const> pOutlineRenderPass;
		std::shared_ptr<eng::MaterialSignature3f const> pOutlineMaterialSignature;
		std::shared_ptr<eng::Material3f const> pOutlineMaterial;

		void loadOutlineRenderPass()
		{
			pOutline_vs.reset(new eng::D3D12::Shader(std::filesystem::current_path().append(L"Content\\Shaders\\Outline_vs.hlsl"), "main", "vs_5_1"));
			pOutline_ps.reset(new eng::D3D12::Shader(std::filesystem::current_path().append(L"Content\\Shaders\\Outline_ps.hlsl"), "main", "ps_5_1"));

			{
				CD3DX12_ROOT_PARAMETER rootParameters[3];

				CD3DX12_DESCRIPTOR_RANGE range0;
				range0.Init(D3D12_DESCRIPTOR_RANGE_TYPE_CBV, 1, 0);
				rootParameters[0].InitAsDescriptorTable(1, &range0);

				CD3DX12_DESCRIPTOR_RANGE range1;
				range1.Init(D3D12_DESCRIPTOR_RANGE_TYPE_CBV, 1, 1);
				rootParameters[1].InitAsDescriptorTable(1, &range1);

				CD3DX12_DESCRIPTOR_RANGE range2;
				range2.Init(D3D12_DESCRIPTOR_RANGE_TYPE_CBV, 1, 2);
				rootParameters[2].InitAsDescriptorTable(1, &range2);

				D3D12_ROOT_SIGNATURE_FLAGS rootSignatureFlags =
					D3D12_ROOT_SIGNATURE_FLAG_ALLOW_INPUT_ASSEMBLER_INPUT_LAYOUT | // Only the input assembler stage needs access to the constant buffer.
					D3D12_ROOT_SIGNATURE_FLAG_DENY_DOMAIN_SHADER_ROOT_ACCESS |
					D3D12_ROOT_SIGNATURE_FLAG_DENY_GEOMETRY_SHADER_ROOT_ACCESS |
					D3D12_ROOT_SIGNATURE_FLAG_DENY_HULL_SHADER_ROOT_ACCESS;

				CD3DX12_ROOT_SIGNATURE_DESC sample_root_signature_desc;
				sample_root_signature_desc.Init
				(
					_countof(rootParameters),
					rootParameters,
					0,
					nullptr,
					rootSignatureFlags
				);


				eng::render_pass_parameter dx12_parameters[3];
				dx12_parameters[0].type = eng::shader_resource::device_buffer;
				dx12_parameters[0].constantBufferParameter.constantBufferDescriptor_rootParamIndex = 0;
				dx12_parameters[1].type = eng::shader_resource::device_buffer;
				dx12_parameters[1].constantBufferParameter.constantBufferDescriptor_rootParamIndex = 1;
				dx12_parameters[2].type = eng::shader_resource::device_buffer;
				dx12_parameters[2].constantBufferParameter.constantBufferDescriptor_rootParamIndex = 2;

				eng::D3D12::RenderPass_Desc renderPassDesc = {};
				renderPassDesc.vertexShader = pOutline_vs.get();
				renderPassDesc.pixelShader = pOutline_ps.get();
				renderPassDesc.numParameters = _countof(dx12_parameters);
				renderPassDesc.pParameters = dx12_parameters;
				renderPassDesc.root_signature_desc = sample_root_signature_desc;
				renderPassDesc.cullMode = eng::D3D12::cull_mode::none;
				renderPassDesc.depth_stencil_Desc.DepthWriteMask = D3D12_DEPTH_WRITE_MASK_ZERO;

				pOutlineRenderPass.reset(new eng::RenderPass<eng::vertex3f>(renderPassDesc));
			}

			{
				std::array<eng::material_parameter, 3> params
				{
					eng::material_parameter{ eng::shader_resource::device_buffer, 0 },
					eng::material_parameter{ eng::shader_resource::device_buffer, 1 },
					eng::material_parameter{ eng::shader_resource::device_buffer, (std::numeric_limits<size_t>::max)() }
				};

				std::array<eng::render_pass_binding<eng::vertex3f>, 1> bindings
				{
					eng::render_pass_binding<eng::vertex3f>{ pOutlineRenderPass, { 1, 2, 3 } }
				};

				pOutlineMaterialSignature = std::make_shared<eng::MaterialSignature3f>(params.begin(), params.end(), bindings.begin(), bindings.end(), 100);
			}

			auto temp_material = std::make_unique<eng::Material3f>(*pOutlineMaterialSignature);


			std::array<float, 5> values = { 1.0f, 0.0f, 0.0f, 1.0f, 0.05f };
			auto temp = std::make_unique<eng::DeviceBuffer>(sizeof(values));
			temp->store(values.data(), sizeof(values));
			pOutlineProperties = std::move(temp);


			temp_material->setConstBufferShaderResource(2, pOutlineProperties.get());

			pOutlineMaterial = std::move(temp_material);
		}

		std::shared_ptr<eng::RenderPass3f const> pSampleRenderPass;
		std::shared_ptr<eng::MaterialSignature3f const> pSampleMaterialSignature;
		std::shared_ptr<eng::Material3f const> pSampleMaterial;

		std::shared_ptr<eng::RenderPass3f const> pTextureRenderPass;
		std::shared_ptr<eng::MaterialSignature3f const> pTextureMaterialSignature;
		std::shared_ptr<eng::Material3f const> pTextureMaterial;

		std::shared_ptr<eng::RenderPass3f const> pSimpleLightRenderPass;
		std::shared_ptr<eng::MaterialSignature3f const> pSimpleLightMaterialSignature;
		std::shared_ptr<eng::Material3f const> pSimpleLightMaterial;
	}
}

void graphicsDemo::resources::load_resources()
{
	//Font
	{
		pFont.reset(new eng::Font(std::filesystem::current_path().append(L"Content\\Fonts\\arial.ttf"), 64));
	}

	//texture2
	{
		eng::Bitmap2 loadedBitmap = eng::load_png(std::filesystem::current_path().append(L"Content\\Textures\\test.png"));
		//
		//eng::Bitmap2 testBitmap
		//(
		//	3, 
		//	{ 
		//		{ 255,  0,  0,255 }, { 255,255,255,255 }, { 255,255,255,255 }, { 255,255,255,255 }, { 255,255,255,255 }, { 255,  0,  0,255 },
		//		{ 255,255,255,255 }, {   0,  0,  0,255 }, {   0,  0,  0,255 }, {   0,  0,  0,255 }, {   0,  0,  0,255 }, { 255,255,255,255 },
		//		{ 255,  0,  0,255 }, { 255,255,255,255 }, { 255,255,255,255 }, { 255,255,255,255 }, { 255,255,255,255 }, { 255,  0,  0,255 }
		//	}
		//);

		//loadedBitmap.copy_to_Position({ 100,4 }, testBitmap);

		pTexture2.reset(new eng::DeviceTexture2(loadedBitmap));
	}

	//Materials
	{
		loadOutlineRenderPass();

		std::wstring sampleVertexShaderPath = std::filesystem::current_path().append(L"Content\\Shaders\\SampleVertexShader.hlsl");
		std::wstring sampleFragmentShaderPath = std::filesystem::current_path().append(L"Content\\Shaders\\SampleFragmentShader.hlsl");
		std::wstring textureFragmentShaderPath = std::filesystem::current_path().append(L"Content\\Shaders\\TextureFragmentShader.hlsl");

		std::wstring simpleLightVertexShaderPath = std::filesystem::current_path().append(L"Content\\Shaders\\SimpleLightVertexShader.hlsl");
		std::wstring simpleLightFragmentShaderPath = std::filesystem::current_path().append(L"Content\\Shaders\\SimpleLightFragmentShader.hlsl");


		//Compile Shaders
		pSampleVertexShader.reset(new eng::D3D12::Shader(sampleVertexShaderPath, "main", "vs_5_1"));
		pSampleFragmentShader.reset(new eng::D3D12::Shader(sampleFragmentShaderPath, "main", "ps_5_1"));
		pSampleTextureFragmentShader.reset(new eng::D3D12::Shader(textureFragmentShaderPath, "main", "ps_5_1"));

		pSimpleLightVertexShader.reset(new eng::D3D12::Shader(simpleLightVertexShaderPath, "main", "vs_5_1"));
		pSimpleLightFragmentShader.reset(new eng::D3D12::Shader(simpleLightFragmentShaderPath, "main", "ps_5_1"));

		//sampleRenderPass
		{
			CD3DX12_ROOT_PARAMETER rootParameters[2];

			CD3DX12_DESCRIPTOR_RANGE range0;
			range0.Init(D3D12_DESCRIPTOR_RANGE_TYPE_CBV, 1, 0);
			rootParameters[0].InitAsDescriptorTable(1, &range0);

			CD3DX12_DESCRIPTOR_RANGE range1;
			range1.Init(D3D12_DESCRIPTOR_RANGE_TYPE_CBV, 1, 1);
			rootParameters[1].InitAsDescriptorTable(1, &range1);

			D3D12_ROOT_SIGNATURE_FLAGS rootSignatureFlags =
				D3D12_ROOT_SIGNATURE_FLAG_ALLOW_INPUT_ASSEMBLER_INPUT_LAYOUT | // Only the input assembler stage needs access to the constant buffer.
				D3D12_ROOT_SIGNATURE_FLAG_DENY_DOMAIN_SHADER_ROOT_ACCESS |
				D3D12_ROOT_SIGNATURE_FLAG_DENY_GEOMETRY_SHADER_ROOT_ACCESS |
				D3D12_ROOT_SIGNATURE_FLAG_DENY_HULL_SHADER_ROOT_ACCESS |
				D3D12_ROOT_SIGNATURE_FLAG_DENY_PIXEL_SHADER_ROOT_ACCESS;

			CD3DX12_ROOT_SIGNATURE_DESC sample_root_signature_desc;
			sample_root_signature_desc.Init
			(
				_countof(rootParameters),
				rootParameters,
				0,
				nullptr,
				rootSignatureFlags
			);


			eng::render_pass_parameter dx12_parameters[2];
			dx12_parameters[0].type = eng::shader_resource::device_buffer;
			dx12_parameters[0].constantBufferParameter.constantBufferDescriptor_rootParamIndex = 0;
			dx12_parameters[1].type = eng::shader_resource::device_buffer;
			dx12_parameters[1].constantBufferParameter.constantBufferDescriptor_rootParamIndex = 1;

			eng::D3D12::RenderPass_Desc renderPassDesc = {};
			renderPassDesc.vertexShader = pSampleVertexShader.get();
			renderPassDesc.pixelShader = pSampleFragmentShader.get();
			renderPassDesc.numParameters = _countof(dx12_parameters);
			renderPassDesc.pParameters = dx12_parameters;
			renderPassDesc.root_signature_desc = sample_root_signature_desc;

			pSampleRenderPass.reset(new eng::RenderPass<eng::vertex3f>(renderPassDesc));
		}

		//TextureRenderPass
		{
			CD3DX12_ROOT_PARAMETER rootParameters[4];

			CD3DX12_DESCRIPTOR_RANGE range0;
			range0.Init(D3D12_DESCRIPTOR_RANGE_TYPE_CBV, 1, 0);
			rootParameters[0].InitAsDescriptorTable(1, &range0);

			CD3DX12_DESCRIPTOR_RANGE range1;
			range1.Init(D3D12_DESCRIPTOR_RANGE_TYPE_CBV, 1, 1);
			rootParameters[1].InitAsDescriptorTable(1, &range1);

			CD3DX12_DESCRIPTOR_RANGE range2;
			range2.Init(D3D12_DESCRIPTOR_RANGE_TYPE_SRV, 1, 2);
			rootParameters[2].InitAsDescriptorTable(1, &range2);

			CD3DX12_DESCRIPTOR_RANGE range3;
			range3.Init(D3D12_DESCRIPTOR_RANGE_TYPE_SAMPLER, 1, 3);
			rootParameters[3].InitAsDescriptorTable(1, &range3);

			D3D12_ROOT_SIGNATURE_FLAGS rootSignatureFlags =
				D3D12_ROOT_SIGNATURE_FLAG_ALLOW_INPUT_ASSEMBLER_INPUT_LAYOUT | // Only the input assembler and the fragment shader stage needs access to the root signature
				D3D12_ROOT_SIGNATURE_FLAG_DENY_DOMAIN_SHADER_ROOT_ACCESS |
				D3D12_ROOT_SIGNATURE_FLAG_DENY_GEOMETRY_SHADER_ROOT_ACCESS |
				D3D12_ROOT_SIGNATURE_FLAG_DENY_HULL_SHADER_ROOT_ACCESS;

			CD3DX12_ROOT_SIGNATURE_DESC texture_root_signature_desc;
			texture_root_signature_desc.Init
			(
				_countof(rootParameters),
				rootParameters,
				0,
				nullptr,
				rootSignatureFlags
			);




			eng::render_pass_parameter dx12_parameters[3];

			dx12_parameters[0].type = eng::shader_resource::device_buffer;
			dx12_parameters[0].constantBufferParameter.constantBufferDescriptor_rootParamIndex = 0;

			dx12_parameters[1].type = eng::shader_resource::device_buffer;
			dx12_parameters[1].constantBufferParameter.constantBufferDescriptor_rootParamIndex = 1;

			dx12_parameters[2].type = eng::shader_resource::device_texture;
			dx12_parameters[2].textureParameter.shaderResourceView_rootParamIndex = 2;
			dx12_parameters[2].textureParameter.sampler_rootParamIndex = 3;


			eng::D3D12::RenderPass_Desc renderPassDesc = {};
			renderPassDesc.vertexShader = pSampleVertexShader.get();
			renderPassDesc.pixelShader = pSampleTextureFragmentShader.get();
			renderPassDesc.numParameters = _countof(dx12_parameters);
			renderPassDesc.pParameters = dx12_parameters;
			renderPassDesc.root_signature_desc = texture_root_signature_desc;

			pTextureRenderPass.reset(new eng::RenderPass<eng::vertex3f>(renderPassDesc));
		}

		//LightRenderPass
		{
			CD3DX12_ROOT_PARAMETER rootParameters[2];

			CD3DX12_DESCRIPTOR_RANGE range0;
			range0.Init(D3D12_DESCRIPTOR_RANGE_TYPE_CBV, 1, 0);
			rootParameters[0].InitAsDescriptorTable(1, &range0);

			CD3DX12_DESCRIPTOR_RANGE range1;
			range1.Init(D3D12_DESCRIPTOR_RANGE_TYPE_CBV, 1, 1);
			rootParameters[1].InitAsDescriptorTable(1, &range1);

			D3D12_ROOT_SIGNATURE_FLAGS rootSignatureFlags =
				D3D12_ROOT_SIGNATURE_FLAG_ALLOW_INPUT_ASSEMBLER_INPUT_LAYOUT | // Only the input assembler stage needs access to the constant buffer.
				D3D12_ROOT_SIGNATURE_FLAG_DENY_DOMAIN_SHADER_ROOT_ACCESS |
				D3D12_ROOT_SIGNATURE_FLAG_DENY_GEOMETRY_SHADER_ROOT_ACCESS |
				D3D12_ROOT_SIGNATURE_FLAG_DENY_HULL_SHADER_ROOT_ACCESS |
				D3D12_ROOT_SIGNATURE_FLAG_DENY_PIXEL_SHADER_ROOT_ACCESS;

			CD3DX12_ROOT_SIGNATURE_DESC sample_root_signature_desc;
			sample_root_signature_desc.Init
			(
				_countof(rootParameters),
				rootParameters,
				0,
				nullptr,
				rootSignatureFlags
			);


			eng::render_pass_parameter dx12_parameters[2];
			dx12_parameters[0].type = eng::shader_resource::device_buffer;
			dx12_parameters[0].constantBufferParameter.constantBufferDescriptor_rootParamIndex = 0;
			dx12_parameters[1].type = eng::shader_resource::device_buffer;
			dx12_parameters[1].constantBufferParameter.constantBufferDescriptor_rootParamIndex = 1;

			eng::D3D12::RenderPass_Desc renderPassDesc = {};
			renderPassDesc.vertexShader = pSimpleLightVertexShader.get();
			renderPassDesc.pixelShader = pSimpleLightFragmentShader.get();
			renderPassDesc.numParameters = _countof(dx12_parameters);
			renderPassDesc.pParameters = dx12_parameters;
			renderPassDesc.root_signature_desc = sample_root_signature_desc;

			pSimpleLightRenderPass.reset(new eng::RenderPass<eng::vertex3f>(renderPassDesc));
		}

		//sampleMaterialSignature
		{
			std::array<eng::material_parameter, 2> params
			{
				eng::material_parameter{ eng::shader_resource::device_buffer, 0 },
				eng::material_parameter{ eng::shader_resource::device_buffer, 1 }
			};

			std::array<eng::render_pass_binding<eng::vertex3f>, 1> bindings
			{
				eng::render_pass_binding<eng::vertex3f>{ pSampleRenderPass, { 0, 1 } }
			};
			
			pSampleMaterialSignature = std::make_shared<eng::MaterialSignature3f>(params.begin(), params.end(), bindings.begin(), bindings.end(), 100);
		}

		//textureMaterialSignature
		{
			std::array<eng::material_parameter, 3> params
			{
				eng::material_parameter{ eng::shader_resource::device_buffer, 0 },
				eng::material_parameter{ eng::shader_resource::device_buffer, 1 },
				eng::material_parameter{ eng::shader_resource::device_buffer, 2 }
			};

			std::array<eng::render_pass_binding<eng::vertex3f>, 1> bindings
			{
				eng::render_pass_binding<eng::vertex3f>{ pTextureRenderPass, { 0, 1, 2 } }
			};

			pTextureMaterialSignature = std::make_shared<eng::MaterialSignature3f>(params.begin(), params.end(), bindings.begin(), bindings.end(), 100);
		}


		//simpleLightmaterialSignature
		{
			std::array<eng::material_parameter, 2> params
			{
				eng::material_parameter{ eng::shader_resource::device_buffer, 0 },
				eng::material_parameter{ eng::shader_resource::device_buffer, 1 }
			};

			std::array<eng::render_pass_binding<eng::vertex3f>, 1> bindings
			{
				eng::render_pass_binding<eng::vertex3f>{ pSimpleLightRenderPass, { 0, 1 } }
			};

			pSimpleLightMaterialSignature = std::make_unique<eng::MaterialSignature3f>(params.begin(), params.end(), bindings.begin(), bindings.end(), 100);
		}


		//sampleMaterial
		{
			pSampleMaterial = std::make_shared<eng::Material3f>(*pSampleMaterialSignature);
		}

		//textureMaterial
		{
			auto tmp = std::make_unique<eng::Material3f>(*pTextureMaterialSignature);
			tmp->setTextureShaderResource(2, pTexture2.get());

			pTextureMaterial = std::move(tmp);
		}

		//simpleLightMaterial
		{
			pSimpleLightMaterial = std::make_shared<eng::Material3f>(*pSimpleLightMaterialSignature);
		}
	}
}

eng::Font const * graphicsDemo::resources::sampleFont()
{
	return pFont.get();
}

std::shared_ptr<eng::DeviceTexture2 const> const& graphicsDemo::resources::sampleTexture2()
{
	return pTexture2;
}

std::shared_ptr<eng::RenderPass3f const> const& graphicsDemo::resources::sampleRenderPass()
{
	return pSampleRenderPass;
}

std::shared_ptr<eng::RenderPass3f const> const& graphicsDemo::resources::outlineRenderPass()
{
	return pOutlineRenderPass;
}

std::shared_ptr<eng::Material3f const> const& graphicsDemo::resources::outlineMaterial()
{
	return pOutlineMaterial;
}

std::shared_ptr<eng::Material3f const> const& graphicsDemo::resources::sampleMaterial()
{
	return pSampleMaterial;
}

std::shared_ptr<eng::Material3f const> const& graphicsDemo::resources::textureMaterial()
{
	return pTextureMaterial;
}

std::shared_ptr<eng::Material3f const> const& graphicsDemo::resources::simpleLightMaterial()
{
	return pSimpleLightMaterial;
}
