#include <graphicsDemo/Model.h>


graphicsDemo::Model::Model(const eng::DeviceMesh3f * pMesh, std::vector<const eng::Material3f*> pMaterials)
{
	set_mesh(pMesh);
	set_material(pMaterials[0]);
}

graphicsDemo::Model::~Model()
{

}
