#include <graphicsDemo/ModelViewNode.h>
#include <string>

void graphicsDemo::ModelViewNode::set_scene(eng::tsg::Scene* pScene)
{
	if (scene() != nullptr)
	{
		scene()->updateEvent() -= m_onUpdateHandler;
	}

	next::set_scene(pScene);

	if (scene() != nullptr)
	{
		scene()->updateEvent() += m_onUpdateHandler;
	}
}

void graphicsDemo::ModelViewNode::onSceneUpdate()
{
	m_window.fetch();
}

const eng::IioFrame * graphicsDemo::ModelViewNode::getIioFrame() const
{
	return &m_window;
}

graphicsDemo::ModelViewNode::ModelViewNode() : 
	m_window(L"not set", { 0,0 }, { 300,300 }, eng::WindowStyle::SystemDefaultMenu, true)
{
	m_window.open();

	set_local_transform({ gtm::transform(gtm::angle_axisd{ atan(1.0 / -3.0), gtm::vector3d{ -1, 0, 0 } }, gtm::identity_matrix<double, 3, 3>()), gtm::vector3d{ 0, 1, -1 } });

	set_renderTarget(&m_window.renderTarget());
}

graphicsDemo::ModelViewNode::~ModelViewNode()
{
	if (scene() != nullptr)
	{
		m_onUpdateHandler.unsetEvent(scene()->updateEvent());
	}
}
