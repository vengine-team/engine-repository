
#include <graphicsDemo/RuntimeMaterialPresentation.h>



void graphicsDemo::RuntimeMaterialPresentation::onSceneUpdate()
{
	m_modelRotationInRad += 1 * (0.2 / getTargetFrameRate());

	if (m_pModelViewNode)
	{
		bool dirty_flag = false;

		for (auto i = m_pModelViewNode->getIioFrame()->input().states_begin(); i != m_pModelViewNode->getIioFrame()->input().states_end(); i++)
		{
			if (!i->before.mouse.left_button && i->current.mouse.left_button)
			{
				float r = static_cast<float>(rand()) / static_cast<float>(RAND_MAX);
				float g = static_cast<float>(rand()) / static_cast<float>(RAND_MAX);
				float b = static_cast<float>(rand()) / static_cast<float>(RAND_MAX);

				m_runtimeMaterialOutlineColors.push_back(outlineProperties{ {r, g, b, 1} , (m_runtimeMaterialOutlineColors.size() + 1 ) * 0.05f});
				m_runtimeMaterialOutlineColorConstantBuffers.push_back(eng::DeviceBuffer(sizeof(outlineProperties)));
				m_runtimeMaterialOutlineColorConstantBuffers.back().store(&m_runtimeMaterialOutlineColors.back(), sizeof(outlineProperties));

				dirty_flag = true;
			}
		}
		for (auto i = m_pModelViewNode->getIioFrame()->input().states_begin(); i != m_pModelViewNode->getIioFrame()->input().states_end(); i++)
		{
			if (!i->before.mouse.right_button && i->current.mouse.right_button)
			{
				if (!m_runtimeMaterialOutlineColors.empty())
				{
					m_runtimeMaterialOutlineColors.pop_back();
				}

				dirty_flag = true;
			}
		}

		if (dirty_flag)
		{
			createMaterial(graphicsDemo::resources::simpleLightMaterial()->signature(), m_runtimeMaterialOutlineColors.size());

			m_pModel = std::make_unique<graphicsDemo::Model>(eng::resources::flatCubeMesh(), std::vector<eng::Material3f const *>{ m_pRuntimeMaterial.get() });
			m_pModel->set_scene(this);
			m_pModel->set_local_transform({ gtm::transform(gtm::angle_axisd{atan(1.0 / 3.0), gtm::vector3d{ 0,1,0 } }, gtm::identity_matrix<double, 3, 3>()) , gtm::vector3d{ 1,0,0 } });

			m_pOutlinedModel = std::make_unique<graphicsDemo::Model>(eng::resources::flatCubeMesh(), std::vector<eng::Material3f const*>{ m_pRuntimeMaterial.get() });
			m_pOutlinedModel->set_scene(this);
			m_pOutlinedModel->set_local_transform({ gtm::transform(gtm::angle_axisd{ atan(1.0 / 3.0), gtm::vector3d{ 0,1,0 } }, gtm::identity_matrix<double, 3, 3>()) , gtm::vector3d{ -1,0,0 } });
		}
	}

	if (m_pModel != nullptr)
	{
		m_pModel->set_local_transform({ gtm::transform(gtm::angle_axisd{ m_modelRotationInRad, gtm::vector3d{ 0,1,0 } }, gtm::identity_matrix<double, 3, 3>()) , gtm::vector3d{ 1,0,0 } });
	} 
}


void graphicsDemo::RuntimeMaterialPresentation::createMaterial(const eng::MaterialSignature3f& base, size_t numOutlines)
{
	std::vector<eng::material_parameter> params;
	params.insert(params.end(), base.parameters().begin(), base.parameters().end());
	for (size_t i = 0; i < numOutlines; i++)
	{
		params.push_back({ eng::shader_resource::device_buffer, (std::numeric_limits<size_t>::max)() });
	}


	std::vector<eng::render_pass_binding<eng::vertex3f>> bindings;
	bindings.resize(numOutlines);
	for (size_t i = 0; i < numOutlines; i++)
	{
		bindings[i].render_pass = graphicsDemo::resources::outlineRenderPass();
		bindings[i].parameter_bindings = { 0, 1, base.parameters().size() + i };
	}
	bindings.insert(bindings.end(), base.render_pass_bindings().begin(), base.render_pass_bindings().end());

	m_pRuntimeMaterialSignature = std::make_shared<eng::MaterialSignature3f>(params.begin(), params.end(), bindings.begin(), bindings.end(), 10);

	m_pRuntimeMaterial = std::make_shared<eng::Material3f>(*m_pRuntimeMaterialSignature);

	size_t constantBuffer_i = numOutlines - 1;
	for (size_t i = base.parameters().size(); i < base.parameters().size() + numOutlines; i++)
	{
		m_pRuntimeMaterial->setConstBufferShaderResource(i, &m_runtimeMaterialOutlineColorConstantBuffers[constantBuffer_i]);

		constantBuffer_i--;
	}
}

graphicsDemo::RuntimeMaterialPresentation::RuntimeMaterialPresentation()
{
	updateEvent() += m_onUpdate;

	//outline only material

	std::array<std::array<float, 4>, 0> colors;

	createMaterial(graphicsDemo::resources::simpleLightMaterial()->signature(), colors.size());

	m_pModel = std::make_unique<graphicsDemo::Model>(eng::resources::flatCubeMesh(), std::vector<eng::Material3f const*>{ m_pRuntimeMaterial.get() });
	m_pModel->set_scene(this);
	m_pModel->set_local_transform
	(
		{ 
			gtm::transform
			(
				gtm::angle_axisd{ m_modelRotationInRad, gtm::vector3d{ 0,1,0 }}, 
				gtm::identity_matrix<double, 3, 3>()
			),
			gtm::vector3d{ 1, 0, 0 }
		}
	);


	m_pOutlinedModel = std::make_unique<graphicsDemo::Model>(eng::resources::flatCubeMesh(), std::vector<eng::Material3f const*>{ m_pRuntimeMaterial.get() });
	m_pOutlinedModel->set_scene(this);
	m_pOutlinedModel->set_local_transform({ gtm::transform(gtm::angle_axisd{ m_modelRotationInRad, gtm::vector3d{ 0,1,0 } }, gtm::identity_matrix<double, 3, 3>()), gtm::vector3d{ -1, 0, 0 } });

	m_pModelViewNode = std::make_unique<graphicsDemo::ModelViewNode>();
	m_pModelViewNode->set_scene(this);
	m_pModelViewNode->set_local_transform({ gtm::transform(gtm::angle_axisd{ -atan(1.0 / 3.0), gtm::vector3d{ -1,0,0 } }, gtm::identity_matrix<double, 3, 3>()), gtm::vector3d{ 0, 1, -3 } });
}

graphicsDemo::RuntimeMaterialPresentation::~RuntimeMaterialPresentation()
{

}