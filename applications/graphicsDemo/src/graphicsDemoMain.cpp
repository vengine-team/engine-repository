#pragma once

#include <eng/engineEntry.h>

#include <par/UpdateThread.h>

#include <graphicsDemo/RuntimeMaterialPresentation.h>
#include <graphicsDemo/ModelViewNode.h>

#include <graphicsDemo/DemoAppResources.h>

int engMain()
{
	eng::resources::load_resources();
	graphicsDemo::resources::load_resources();

	eng::UpdateThread updateThread;

	graphicsDemo::RuntimeMaterialPresentation presentation;
	updateThread.concurrentRegisterUpdateable(&presentation);

	while (true)
	{
		std::this_thread::sleep_for(std::chrono::milliseconds(10));
	}

	updateThread.concurrentTerminate();

	updateThread.join();

	return 0;
}
