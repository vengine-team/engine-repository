#pragma once
#include <eng/DeviceMesh.h>
#include <eng/Material.h>
#include <eng/Font.h>

#include <eng/defaultDraw.h>

namespace graphicsDemo
{ 
	namespace resources
	{
		void load_resources();

		eng::Font const* sampleFont();

		std::shared_ptr<eng::DeviceTexture2 const> const& sampleTexture2();

		std::shared_ptr<eng::RenderPass3f const> const& sampleRenderPass();
		std::shared_ptr<eng::RenderPass3f const> const& outlineRenderPass();
		std::shared_ptr<eng::Material3f const> const& outlineMaterial();

		std::shared_ptr<eng::Material3f const> const& sampleMaterial();
		std::shared_ptr<eng::Material3f const> const& textureMaterial();
		std::shared_ptr<eng::Material3f const> const& simpleLightMaterial();
	}
}