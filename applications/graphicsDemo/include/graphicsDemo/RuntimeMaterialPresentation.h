#pragma once
#include <eng/tsg/Node3.h>
#include <graphicsDemo/Model.h>
#include <graphicsDemo/DemoAppResources.h>
#include <graphicsDemo/ModelViewNode.h>
#include <eng/resources/default.h>
#include <ext/event.h>

namespace graphicsDemo
{
	class RuntimeMaterialPresentation : public eng::tsg::Scene
	{
		struct outlineProperties
		{
			std::array<float, 4> color;
			float thickness;
		};

		std::shared_ptr<eng::MaterialSignature3f> m_pRuntimeMaterialSignature = nullptr;
		std::shared_ptr<eng::Material3f> m_pRuntimeMaterial = nullptr;
		void createMaterial(const eng::MaterialSignature3f& pBase, size_t numOutlines);
		std::vector<eng::DeviceBuffer> m_runtimeMaterialOutlineColorConstantBuffers;
		std::vector<outlineProperties> m_runtimeMaterialOutlineColors;

		std::unique_ptr<eng::tsg::Node3d> m_pModel;
		std::unique_ptr<eng::tsg::Node3d> m_pOutlinedModel;

		std::unique_ptr<ModelViewNode> m_pModelViewNode;

		void onSceneUpdate();
		ext::event_handler<ext::functor<&RuntimeMaterialPresentation::onSceneUpdate>> m_onUpdate = { { this } };

		double m_modelRotationInRad = 0;

	public:
		RuntimeMaterialPresentation();
		virtual ~RuntimeMaterialPresentation();
	};
};