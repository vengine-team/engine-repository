#pragma once
#include <eng/tsg/Node3.h>
#include <eng/tsg/MeshRenderer3.h>

namespace graphicsDemo
{
	class Model : public eng::tsg::add_components<eng::tsg::Node3d, eng::tsg::MeshRenderer3>
	{
	public:

		Model(const eng::DeviceMesh3f * pMesh, std::vector<const eng::Material3f*> pMaterials);
		virtual ~Model();
	};
};