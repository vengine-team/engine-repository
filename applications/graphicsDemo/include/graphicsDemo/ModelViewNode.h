#pragma once
#include <eng/tsg/Camera3.h>
#include <eng/tsg/Node3.h>

#include <eng/Window.h>
#include <ext/event.h>

#include <memory>


namespace graphicsDemo
{
	class ModelViewNode : public eng::tsg::add_components<eng::tsg::Node3d, eng::tsg::Camera3>
	{
	private:

		eng::Window m_window;

		void onSceneUpdate();
		ext::event_handler<ext::functor<&ModelViewNode::onSceneUpdate>> m_onUpdateHandler = { { this } };

	public:

		void set_scene(eng::tsg::Scene* pScene) override;

		const eng::IioFrame* getIioFrame() const;

		ModelViewNode();
		virtual ~ModelViewNode();
	};
}