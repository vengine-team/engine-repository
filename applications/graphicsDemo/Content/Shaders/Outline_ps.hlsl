cbuffer outlineProperties : register(b2)
{
    float4 outlineColor;
    float outlineThickness;
};

// Per-fragment color data passed through the pixel shader.
struct FragmentShaderInput
{
    float4 pos : SV_POSITION;
    float3 color : NORMAL;
    float2 uv0 : TEXCOORD0;
};

// A pass-through function for the (interpolated) color data.
float4 main(FragmentShaderInput input) : SV_TARGET
{
    return float4(outlineColor.rgb, 1);
}
