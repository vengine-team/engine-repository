// Two constant buffers that store the three basic column-major matrices for composing geometry.

cbuffer ModelConstantBuffer : register(b0)
{
	matrix model;
};

cbuffer ViewProjectionConstantBuffer : register(b1)
{
	matrix view;
	matrix projection;
};

// Per-vertex data used as input to the vertex shader.
struct VertexShaderInput
{
	float3 pos : POSITION;
	float3 normal : NORMAL;
	float2 uv0 : TEXCOORD0;
	float2 uv1 : TEXCOORD1;
	float2 uv2 : TEXCOORD2;
	float2 uv3 : TEXCOORD3;
};

// Per-fragment color data passed through the pixel shader.
struct FragmentShaderInput
{
	float4 pos : SV_POSITION;
	float3 normal : NORMAL;
	float3 light : LIGHT;
	float2 uv0 : TEXCOORD0;
};

// Simple shader to do vertex processing on the GPU.
FragmentShaderInput main(VertexShaderInput input)
{
	float3 lightVector = normalize(float3(0.2, -1.0, 1.0));

	FragmentShaderInput output;


	float4 pos = float4(input.pos, 1.0f);
    float4 normal = float4(input.normal, 0.0f);

	// Transform vectors into global space.
	pos = mul(pos, model);
    normal = mul(normal, model);
    
    float angle_light_normal = acos(dot(normal.xyz, -lightVector) / (length(normal.xyz) * length(-lightVector)));

    // Transform vectors into camera space 
	pos = mul(pos, view);
    normal = mul(normal, view);

    // Transform vectors into screen space
    pos = mul(pos, projection);
	normal = mul(normal, projection);

    
    float angle_derived_intensity = cos(abs(angle_light_normal));
    if (angle_derived_intensity < 0)
    {
        angle_derived_intensity = 0;
    }
    output.light = float3(0.2, 0.2, 0.2) + float3(0.8, 0.8, 0.8) * angle_derived_intensity;
	output.pos = pos;
	output.normal = normal.xyz;
	output.uv0 = input.uv0;

	return output;
}
