
struct FragmentShaderInput
{
	float4 pos : SV_POSITION;
	float3 color : NORMAL;
	float3 light : LIGHT;
	float2 uv0 : TEXCOORD0;
};

float4 main(FragmentShaderInput input) : SV_TARGET
{
	return float4(1.0 * input.light.x, 1.0 * input.light.y, 1.0 * input.light.z, 1.0);
}