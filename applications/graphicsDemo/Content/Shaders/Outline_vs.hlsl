// Two constant buffers that store the three basic column-major matrices for composing geometry.

cbuffer ModelConstantBuffer : register(b0)
{
	matrix model;
};

cbuffer ViewProjectionConstantBuffer : register(b1)
{
	matrix view;
	matrix projection;
};

cbuffer outlineProperties : register(b2)
{
    float4 outlineColor;
    float outlineThickness;
};

// Per-vertex data used as input to the vertex shader.
struct VertexShaderInput
{
	float3 pos : POSITION;
	float3 normal : NORMAL;
    float3 extrant : EXTRANT;
    float2 uv0 : TEXCOORD0;
    float2 uv1 : TEXCOORD1;
    float2 uv2 : TEXCOORD2;
    float2 uv3 : TEXCOORD3;
};

// Per-fragment color data passed through the pixel shader.
struct FragmentShaderInput
{
	float4 pos : SV_POSITION;
	float3 color : NORMAL;
    float2 uv0 : TEXCOORD0;
};

// Simple shader to do vertex processing on the GPU.
FragmentShaderInput main(VertexShaderInput input)
{
	FragmentShaderInput output;
    float4 pos = float4(input.pos + input.extrant * outlineThickness, 1.0f);

	// Transform the vertex position into projected space.
    pos = mul(pos, model);
    pos = mul(pos, view);
    pos = mul(pos, projection);
	
	output.pos = pos;
    output.color = input.normal;
    output.uv0 = input.uv0;

	return output;
}
