
Texture2D<float4> tex1 : register(t2);

SamplerState sampler1 : register(s3);

struct FragmentShaderInput
{ 
    float4 pos : SV_POSITION;
    float3 color : NORMAL;
    float2 uv0 : TEXCOORD0;
};

float4 main(FragmentShaderInput input) : SV_TARGET
{
    return tex1.Sample(sampler1, input.uv0);
}