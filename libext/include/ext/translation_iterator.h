#pragma once
#include <iterator>

namespace ext
{
	template<typename value_t>
	struct p_to_dereferenced
	{
		value_t m_value;

		p_to_dereferenced(value_t value) : m_value(value)
		{

		}

		const value_t& operator*() const
		{
			return m_value;
		}

		const value_t* operator->() const
		{
			return &m_value;
		}
	};

	template<typename target_t, typename underlying_t, typename iterator_category>
	struct translation_iterator_imp;


	template<typename target_t, typename underlying_t>
	struct translation_iterator_imp<target_t, underlying_t, std::input_iterator_tag>
	{
		underlying_t m_underlying;

		bool operator==(const target_t& other) const
		{
			return this->m_underlying == other.m_underlying;
		}

		bool operator!=(const target_t& other) const
		{
			return this->m_underlying != other.m_underlying;
		}

		target_t operator++(int)
		{
			target_t out = *static_cast<target_t*>(this);
			this->m_underlying++;
			return out;
		}

		target_t& operator++()
		{
			this->m_underlying++;
			return *static_cast<target_t*>(this);
		}

		translation_iterator_imp()
		{

		}
		translation_iterator_imp(underlying_t underlying) : m_underlying(underlying)
		{

		}
	};

	template<typename target_t, typename underlying_t>
	struct translation_iterator_imp<target_t, underlying_t, std::forward_iterator_tag> : public translation_iterator_imp<target_t, underlying_t, std::input_iterator_tag>
	{
		translation_iterator_imp()
		{

		}
		translation_iterator_imp(underlying_t underlying) : translation_iterator_imp<target_t, underlying_t, std::input_iterator_tag>(underlying)
		{

		}
	};

	template<typename target_t, typename underlying_t>
	struct translation_iterator_imp<target_t, underlying_t, std::bidirectional_iterator_tag> : public translation_iterator_imp<target_t, underlying_t, std::forward_iterator_tag>
	{
		target_t operator--(int)
		{
			target_t out = *static_cast<target_t*>(this);
			this->m_underlying--;
			return out;
		}

		target_t& operator--()
		{
			this->m_underlying--;
			return *static_cast<target_t*>(this);
		}

		translation_iterator_imp()
		{

		}
		translation_iterator_imp(underlying_t underlying) : translation_iterator_imp<target_t, underlying_t, std::forward_iterator_tag>(underlying)
		{

		}
	};

	template<typename target_t, typename underlying_t>
	struct translation_iterator_imp<target_t, underlying_t, std::random_access_iterator_tag> : public translation_iterator_imp<target_t, underlying_t, std::bidirectional_iterator_tag>
	{
		target_t operator+(const typename underlying_t::difference_type& difference) const
		{
			target_t out = *static_cast<const target_t*>(this);
			out.m_underlying += difference;
			return out;
		}
		target_t operator-(const typename underlying_t::difference_type& difference) const
		{
			target_t out = *static_cast<const target_t*>(this);
			out.m_underlying -= difference;
			return out;
		}
		typename underlying_t::difference_type operator-(const target_t& other) const
		{
			return this->m_underlying - other.m_underlying;
		}


		bool operator<(const target_t& other) const
		{
			return this->m_underlying < other.m_underlying;
		}
		bool operator>(const target_t& other) const
		{
			return this->m_underlying > other.m_underlying;
		}

		bool operator<=(const target_t& other) const
		{
			return this->m_underlying <= other.m_underlying;
		}
		bool operator>=(const target_t& other) const
		{
			return this->m_underlying >= other.m_underlying;
		}

		auto operator[](typename underlying_t::difference_type difference) const
		{
			target_t out = *static_cast<const target_t*>(this);
			out.m_underlying += difference;
			return *out;
		}

		target_t operator+=(const typename underlying_t::difference_type& difference)
		{
			this->m_underlying += difference;
			return *static_cast<const target_t*>(this);
		}
		target_t operator-=(const typename underlying_t::difference_type& difference)
		{
			this->m_underlying -= difference;
			return *static_cast<const target_t*>(this);
		}

		translation_iterator_imp()
		{

		}
		translation_iterator_imp(underlying_t underlying) : translation_iterator_imp<target_t, underlying_t, std::bidirectional_iterator_tag>(underlying)
		{

		}
	};

	template<typename underlying_t, typename translation_functor>
	struct translation_iterator : public translation_iterator_imp<translation_iterator<underlying_t, translation_functor>, underlying_t, typename underlying_t::iterator_category>, public std::iterator_traits<underlying_t>
	{
	private:
		translation_functor m_translation_functor;
	public:

		auto operator*() const
		{
			return m_translation_functor(*(this->m_underlying));
		}

		auto operator->() const
		{
			return p_to_dereferenced(**this);
		}

		translation_iterator()
		{

		}
		translation_iterator(underlying_t underlying, const translation_functor& functor) : 
			translation_iterator_imp<translation_iterator<underlying_t, translation_functor>, underlying_t, typename underlying_t::iterator_category>(underlying), 
			m_translation_functor(functor)
		{

		}
	};

	template<typename underlying_t, typename translation_functor>
	auto make_translation_iterator(underlying_t underlying, const translation_functor& functor)
	{
		return translation_iterator<underlying_t, translation_functor>(underlying, functor);
	}
}
