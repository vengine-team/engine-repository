#pragma once
#include <ext/translation_iterator.h>

#include <iterator>


namespace ext
{
	template<typename index_t, typename difference_t, typename functor_t>
	struct indexed_iterator
	{
		index_t m_position;
		functor_t m_functor;
	public:
		auto operator*() const
		{
			return m_functor(this->m_position);
		}

		auto operator->() const
		{
			return p_to_dereferenced(**this);
		}

		indexed_iterator(const index_t& position, const functor_t& functor) : 
			m_position(position),
			m_functor(functor)
		{

		}

		using difference_type = difference_t;
		using iterator_category = std::random_access_iterator_tag;

		bool operator==(const indexed_iterator<index_t, difference_t, functor_t>& other) const
		{
			return this->m_position == other.m_position;
		}
		bool operator!=(const indexed_iterator<index_t, difference_t, functor_t>& other) const
		{
			return this->m_position != other.m_position;
		}


		indexed_iterator<index_t, difference_t, functor_t>& operator++()
		{
			this->m_position++;
			return *this;
		}
		indexed_iterator<index_t, difference_t, functor_t> operator++(int)
		{
			indexed_iterator<index_t, difference_t, functor_t> out = *this;
			this->m_position++;
			return out;
		}


		indexed_iterator<index_t, difference_t, functor_t>& operator--()
		{
			this->m_position--;
			return *this;
		}
		indexed_iterator<index_t, difference_t, functor_t> operator--(int)
		{
			indexed_iterator<index_t, difference_t, functor_t> out = *static_cast<indexed_iterator<index_t, difference_t, functor_t>*>(this);
			this->m_position--;
			return out;
		}


		indexed_iterator<index_t, difference_t, functor_t> operator+(const difference_type& difference) const
		{
			indexed_iterator<index_t, difference_t, functor_t> out = *this;
			out.m_position += difference;
			return out;
		}
		indexed_iterator<index_t, difference_t, functor_t> operator-(const difference_type& difference) const
		{
			indexed_iterator<index_t, difference_t, functor_t> out = *static_cast<const indexed_iterator<index_t, difference_t, functor_t>*>(this);
			out.m_position -= difference;
			return out;
		}
		difference_t operator-(const indexed_iterator<index_t, difference_t, functor_t>& other) const
		{
			return this->m_position - other.m_position;
		}


		bool operator<(const indexed_iterator<index_t, difference_t, functor_t>& other) const
		{
			return this->m_position < other.m_position;
		}
		bool operator>(const indexed_iterator<index_t, difference_t, functor_t>& other) const
		{
			return this->m_position > other.m_position;
		}

		bool operator<=(const indexed_iterator<index_t, difference_t, functor_t>& other) const
		{
			return this->m_position <= other.m_position;
		}
		bool operator>=(const indexed_iterator<index_t, difference_t, functor_t>& other) const
		{
			return this->m_position >= other.m_position;
		}


		indexed_iterator<index_t, difference_t, functor_t>& operator+=(const difference_type& difference)
		{
			this->m_position += difference;
			return *this;
		}
		indexed_iterator<index_t, difference_t, functor_t>& operator-=(const difference_type& difference)
		{
			this->m_position -= difference;
			return *this;
		}
	};

	template<typename functor_t>
	auto make_indexed_iterator(size_t position, functor_t functor)
	{
		return indexed_iterator<size_t, ptrdiff_t, functor_t>(position, functor);
	}

	template<typename target_t, typename index_t, typename difference_t>
	struct indexed_iterator_base
	{
		index_t m_position;

		indexed_iterator_base()
		{

		}
		indexed_iterator_base(const index_t& position) : m_position(position)
		{

		}

		using difference_type = difference_t;
		using iterator_category = std::random_access_iterator_tag;

		bool operator==(const target_t& other) const
		{
			return this->m_position == other.m_position;
		}
		bool operator!=(const target_t& other) const
		{
			return this->m_position != other.m_position;
		}


		target_t& operator++()
		{
			this->m_position++;
			return *static_cast<target_t*>(this);
		}
		target_t operator++(int)
		{
			target_t out = *static_cast<target_t*>(this);
			this->m_position++;
			return out;
		}


		target_t& operator--()
		{
			this->m_position--;
			return *static_cast<target_t*>(this);
		}
		target_t operator--(int)
		{
			target_t out = *static_cast<target_t*>(this);
			this->m_position--;
			return out;
		}


		target_t operator+(const difference_type& difference) const
		{
			target_t out = *static_cast<const target_t*>(this);
			out.m_position += difference;
			return out;
		}
		target_t operator-(const difference_type& difference) const
		{
			target_t out = *static_cast<const target_t*>(this);
			out.m_position -= difference;
			return out;
		}
		difference_t operator-(const target_t& other) const
		{
			return this->m_position - other.m_position;
		}


		bool operator<(const target_t& other) const
		{
			return this->m_position < other.m_position;
		}
		bool operator>(const target_t& other) const
		{
			return this->m_position > other.m_position;
		}

		bool operator<=(const target_t& other) const
		{
			return this->m_position <= other.m_position;
		}
		bool operator>=(const target_t& other) const
		{
			return this->m_position >= other.m_position;
		}


		target_t& operator+=(const difference_type& difference)
		{
			this->m_position += difference;
			return *static_cast<target_t*>(this);
		}
		target_t& operator-=(const difference_type& difference)
		{
			this->m_position -= difference;
			return *static_cast<target_t*>(this);
		}
	};
}