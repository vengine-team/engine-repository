#pragma once

namespace ext
{
	template<typename F>
	class function_ptr;

	template<typename R, typename...P>
	class function_ptr<R(P...)>
	{
		R(*m_pFunction)(P...);
	public:
		R operator()(P... args)
		{
			return (*m_pFunction)(args...);
		}


	};
	template<typename R, typename T, typename...P>
	class function_ptr<R(T::*)(P...)>
	{
		R(T::* m_pFunction)(P...);
		T* m_pInstance;
	public:
		R operator()(P... args)
		{
			return (*m_pInstance.*m_pFunction)(args...);
		}

		function_ptr(R(T::* pFunction)(P...), T* pInstance) : m_pFunction(pFunction), m_pInstance(pInstance)
		{

		}

		function_ptr(const function_ptr<R(T::*)(P...)>& source) : m_pFunction(source.m_pFunction), m_pInstance(source.m_pInstance)
		{

		}
		function_ptr(function_ptr<R(T::*)(P...)>&& source) : m_pFunction(source.m_pFunction), m_pInstance(source.m_pInstance)
		{

		}

		function_ptr<R(T::*)(P...)>& operator=(const function_ptr<R(T::*)(P...)>& source)
		{
			m_pInstance = source.m_pInstance;
			m_pFunction = source.m_pFunction;
			return *this;
		}
		function_ptr<R(T::*)(P...)>& operator==(function_ptr<R(T::*)(P...)>&& source)
		{
			m_pInstance = source.m_pInstance;
			m_pFunction = source.m_pFunction;
			return *this;
		}

	};




	template<auto F, typename pF>
	struct _functor_impl;

	template<auto F, typename R, typename ...P>
	struct _functor_impl<F, R(*)(P...)>
	{
		R operator()(P... args)
		{
			return F(args...);
		}
	};

	template<auto F, typename R, typename T, typename ...P>
	struct _functor_impl<F, R(T::*)(P...)>
	{
	private:
		T* m_pInstance;
	public:

		T * instance() const
		{
			return m_pInstance;
		}

		R operator()(P... args)
		{
			auto f = F;
			return ((*m_pInstance).*F)(args...);
		}

		_functor_impl(T* pInstance) : m_pInstance(pInstance)
		{

		}
	};

	template<auto F>
	struct functor : _functor_impl<F, decltype(F)>
	{

	};
}