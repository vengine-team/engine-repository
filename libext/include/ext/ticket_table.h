#pragma once
#include <vector>
#include <limits>
#include <assert.h>
#include <memory>

#include <ext/translation_iterator.h>

namespace ext
{
	struct ticket final
	{
	public:
		using ticket_number_t = size_t;
		ticket_number_t m_number;


		ticket()
		{
#ifndef NDEBUG
			m_number = (std::numeric_limits<ticket_number_t>::max)();
#endif // !NDEBUG
		}
		ticket& operator=(const ticket& rValue) = delete;
		ticket& operator=(ticket && rValue) = delete;
		ticket(const ticket& rValue) = delete;
		ticket(ticket && rValue) = delete;
		~ticket()
		{
#ifndef NDEBUG
			assert(m_number == (std::numeric_limits<ticket_number_t>::max)());
#endif // !NDEBUG
		}
	};


	template<typename T, typename allocator_t = std::allocator<std::pair<T, std::reference_wrapper<ticket>>>>
	class ticket_table
	{
	private:

		using container_t = std::vector<std::pair<T, std::reference_wrapper<ticket>>, allocator_t>;
		container_t m_values;

	public:

		struct translator
		{
			auto& operator()(typename container_t::value_type& ref) const
			{
				return ref.first;
			}
		};
		using iterator = ext::translation_iterator<typename container_t::iterator, translator>;
		using reverse_iterator = ext::translation_iterator<typename container_t::reverse_iterator, translator>;

		struct const_translator
		{
			auto& operator()(const typename container_t::value_type& ref) const
			{
				return ref.first;
			}
		};
		using const_iterator = ext::translation_iterator<typename container_t::const_iterator, const_translator>;
		using const_reverse_iterator = ext::translation_iterator<typename container_t::const_reverse_iterator, const_translator>;

		ticket_table()
		{

		}
		ticket_table<T>& operator=(const ticket_table<T>& rValue) = delete;
		ticket_table<T>& operator=(ticket_table<T>&& rValue)
		{
			m_values = std::move(rValue.m_values);
			return *this;
		}
		ticket_table(const ticket_table<T>& rValue) = delete;
		ticket_table(ticket_table<T>&& rValue)
		{
			m_values = std::move(rValue.m_values);
		}

		virtual ~ticket_table()
		{
#ifdef NDEBUG
			for (size_t i = 0; i < m_values.size(); i++)
			{
				m_values[i].second.get().m_number = (std::numeric_limits<ticket::ticket_number_t>::max)();
			}
#endif // !NDEBUG
		}



		///
		/// access
		///
		const_iterator begin() const
		{
			return const_iterator{ this->m_values.begin(), {} };
		}
		const_iterator end() const
		{
			return const_iterator{ this->m_values.end(),{} };
		}
		const_iterator cbegin() const
		{
			return const_iterator{ this->m_values.cbegin(),{} };
		}
		const_iterator cend() const
		{
			return const_iterator{ this->m_values.cend(),{} };
		}

		const_reverse_iterator rbegin() const
		{
			return const_reverse_iterator{ this->m_values.rbegin(),{} };
		}
		const_reverse_iterator rend() const
		{
			return const_reverse_iterator{ this->m_values.rend(),{} };
		}
		const_reverse_iterator crbegin() const
		{
			return const_reverse_iterator{ this->m_values.crbegin(),{} };
		}
		const_reverse_iterator crend() const
		{
			return const_reverse_iterator{ this->m_values.crend(),{} };
		}

		const T& operator[](const ext::ticket& ticket) const
		{
			return this->m_values[ticket.m_number].first;
		}

		///
		/// size of the container
		///
		size_t size() const
		{
			return m_values.size();
		}

		bool empty() const
		{
			return m_values.empty();
		}

		///
		/// add a value to the container
		/// the number reference must be preserved else where
		/// as long as this value is stored in the container
		///
		void insert(const std::pair<T, std::reference_wrapper<ticket>> pair)
		{
#ifndef NDEBUG
			//break if ticket is already associated with any container
			assert(pair.second.get().m_number == (std::numeric_limits<ticket::ticket_number_t>::max)());
#endif // !NDEBUG

			pair.second.get().m_number = m_values.size();
			m_values.push_back(pair);
		}
		void erase(ticket& _ticket)
		{
#ifndef NDEBUG
			//break if ticket is not associated with any container
			assert(_ticket.m_number != (std::numeric_limits<ticket::ticket_number_t>::max)());
#endif // !NDEBUG



			m_values[_ticket.m_number] = m_values.back();
			m_values[_ticket.m_number].second.get().m_number = _ticket.m_number;
			m_values.pop_back();

#ifndef NDEBUG
			//indicate that the ticket is not associated with any container
			_ticket.m_number = (std::numeric_limits<ticket::ticket_number_t>::max)();
#endif // !NDEBUG
		}
	};
};