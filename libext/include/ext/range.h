#pragma once

namespace ext
{
	template<typename iterator_t>
	class range
	{
		iterator_t m_begin;
		iterator_t m_end;
	public:
		range()
		{

		}
		range(iterator_t begin, iterator_t end) : m_begin(begin), m_end(end)
		{

		}

		iterator_t begin() const
		{
			return m_begin;
		}

		iterator_t end() const
		{
			return m_end;
		}
	};
}