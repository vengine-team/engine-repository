#pragma once
#include <queue>
#include <memory>

namespace eng
{
	template<typename base_type>
	class any_queue
	{
		std::queue<std::unique_ptr<base_type>> m_values;

	public:

		template<typename value_t>
		void push(const value_t& value)
		{
			m_values.push(std::unique_ptr<base_type>(new value_t(value)));
		}

		template<typename value_t>
		void push(value_t&& value)
		{
			m_values.push(std::unique_ptr<base_type>(new value_t(std::move(value))));
		}

		base_type* front() const
		{
			return m_values.front().get();
		}

		bool empty() const
		{
			return m_values.empty();
		}

		size_t size() const
		{
			return m_values.size();
		}

		void pop()
		{
			m_values.pop();
		}
	};
}