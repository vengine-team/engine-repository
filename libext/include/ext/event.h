#pragma once
#include <ext/ticket_table.h>
#include <ext/functor.h>

namespace ext
{
	template<typename ...P>
	class event_subscribe_access;

	//concept event
	//{
	//public
	//	add(eng::callable<void, P...>* pHandler, ticket<eng::callable<void, P...>*>& ticket);
	//  remove(ticket<eng::callable<void, P...>*>& ticket);
	//}

	template<typename ...P>
	class event;

	template<typename ...P>
	class _event_handler_impl_base
	{
	public:
		void(*_pInvoke)(_event_handler_impl_base<P...>*, P...);
	};

	template<typename ...P>
	class event
	{
	private:
		ext::ticket_table<_event_handler_impl_base<P...>*> m_assignedHandlers;
	public:

		void operator()(P... parameters)
		{
			try
			{
				for (auto i = m_assignedHandlers.begin(); i != m_assignedHandlers.end(); i++)
				{
					(*(*i)->_pInvoke)(*i, parameters...);
				}
			}
			catch (const std::exception& e)
			{
				std::string s = e.what();
				assert(0);
			}
		}

		void add(_event_handler_impl_base<P...>* pHandler, ext::ticket& _ticket)
		{
			m_assignedHandlers.insert({ pHandler, _ticket });
		}
		void remove(ext::ticket& _ticket)
		{
			m_assignedHandlers.erase(_ticket);
		}

		event()
		{

		}
		event(const event<P...>& other) = delete;
		event(event<P...>&& other) = delete;
	};

	template<typename ...P>
	class event_subscribe_access
	{
	private:
		event<P...>& m_refEvent;
	public:
		event_subscribe_access(event<P...>& event) : m_refEvent(event)
		{

		}

		void add(_event_handler_impl_base<P...>* pHandler, ext::ticket& _ticket)
		{
			m_refEvent.add(pHandler, _ticket);
		}
		void remove(ext::ticket& ticket)
		{
			m_refEvent.remove(ticket);
		}
	};



	template<typename F, typename R, typename ...P>
	static void _event_handler_invoke(_event_handler_impl_base<P...>* p, P... args);

	template<typename F>
	class _event_handler_impl;

	template<typename F, typename R, typename ...P>
	class _event_handler_impl<R(F::*)(P...)> : private _event_handler_impl_base<P...>
	{
		friend void _event_handler_invoke<F, R, P...>(_event_handler_impl_base<P...>*, P...);

		F m_functor;

		ext::ticket m_ticket;
	public:

		template<typename E>
		void setEvent(E& event)
		{
			event.add(this, this->m_ticket);
		}
		template<typename E>
		void unsetEvent(E& event)
		{
			event.remove(this->m_ticket);
		}

		_event_handler_impl(const F& functor) : m_functor(functor)
		{
			this->_pInvoke = &_event_handler_invoke<F, R, P...>;
		}
		_event_handler_impl(F&& functor) : m_functor(std::move(functor))
		{
			this->_pInvoke = &_event_handler_invoke<F, R, P...>;
		}
	};

	//member function template specialization
	template<typename F>
	class event_handler : public _event_handler_impl<decltype(&F::operator())>
	{
	public:
		event_handler(const F& functor) : _event_handler_impl<decltype(&F::operator())>(functor)
		{

		}
		event_handler(F&& functor) : _event_handler_impl<decltype(&F::operator())>(std::move(functor))
		{

		}

		event_handler(const event_handler<F>& source) = delete;
		event_handler(event_handler<F>&& source) = delete;
	};
	
	template<typename F, typename R, typename ...P>
	static void _event_handler_invoke(_event_handler_impl_base<P...>* p, P... args)
	{
		static_cast<_event_handler_impl<R(F::*)(P...)>*>(p)->m_functor(args...);
	}
};


template<typename E, typename F>
E& operator+=(E& event, ext::event_handler<F>& refHandler)
{
	refHandler.setEvent<E>(event);
	return event;
}


template<typename E, typename F>
E& operator-=(E& event, ext::event_handler<F>& refHandler)
{
	refHandler.unsetEvent<E>(event);
	return event;
}