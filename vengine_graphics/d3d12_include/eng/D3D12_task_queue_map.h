#pragma once
#include <ext/any_queue.h>

#include <map>
#include <queue>

namespace eng
{
	namespace D3D12
	{
		template<typename key_t>
		class task_queue_map
		{
		public:

			class functor_wrapper_base
			{
			public:

				virtual void invoke() = 0;

				virtual ~functor_wrapper_base() = default;
			};

			template<typename functor_t>
			class functor_wrapper final : public functor_wrapper_base
			{
				functor_t m_functor;
			public:
				virtual void invoke() override
				{
					m_functor();
				}
				functor_wrapper(functor_t&& functor) : m_functor(std::move(functor))
				{

				}
			};
		private:

			std::map<key_t, eng::any_queue<functor_wrapper_base>> m_TaskQueueMap;

		public:

			using iterator = typename std::map<key_t, eng::any_queue<functor_wrapper_base>>::iterator;

			iterator find(key_t key)
			{
				return m_TaskQueueMap.find(key);
			}

			iterator begin()
			{
				return m_TaskQueueMap.begin();
			}
			iterator end()
			{
				return m_TaskQueueMap.end();
			}


			using const_iterator = typename std::map<key_t, eng::any_queue<functor_wrapper_base>>::const_iterator;
			const_iterator begin() const
			{
				return m_TaskQueueMap.begin();
			}
			const_iterator end() const
			{
				return m_TaskQueueMap.end();
			}


			template<typename functor_t>
			void insert(key_t key, const functor_t& functor)
			{
				functor_wrapper<functor_t> f_wrapper = functor_wrapper<functor_t>(functor);

				auto i = m_TaskQueueMap.find(key);

				if (i == m_TaskQueueMap.end())
				{
					eng::any_queue<functor_wrapper_base> queue;

					queue.push<functor_wrapper<functor_t>>(f_wrapper);
					m_TaskQueueMap.insert({ key, std::move(queue) });
				}
				else
				{
					i->second.push<functor_wrapper<functor_t>>(f_wrapper);
				}
			}

			template<typename functor_t>
			void insert(key_t key, functor_t&& functor)
			{
				functor_wrapper<functor_t> f_wrapper = functor_wrapper<functor_t>(std::move(functor));

				auto i = m_TaskQueueMap.find(key);

				if (i == m_TaskQueueMap.end())
				{
					eng::any_queue<functor_wrapper_base> queue;
					queue.push<functor_wrapper<functor_t>>(std::move(f_wrapper));
					m_TaskQueueMap.insert({ key, std::move(queue) });
				}
				else
				{
					i->second.push<functor_wrapper<functor_t>>(std::move(f_wrapper));
				}
			}

			bool empty() const
			{
				return m_TaskQueueMap.empty();
			}

			template<typename mutex_t>
			void process(iterator pos, std::unique_lock<mutex_t>& mutex)
			{
				eng::any_queue<functor_wrapper_base>& queue = pos->second;

				while (!queue.empty())
				{
					functor_wrapper_base* pNext = queue.front();

					mutex.unlock();

					pNext->invoke();

					mutex.lock();

					queue.pop();
				}

				m_TaskQueueMap.erase(pos);
			}
		};
	};
};