#pragma once
#include "D3D12_memory.h"
#include "D3D12_LogarithmicDynamicPoolAllocator.h"


namespace eng
{
	namespace D3D12
	{
		class BufferPoolAllocator final
		{
			size_t m_block_count;

			UINT64 m_64KB_aligned_block_size_in_bytes;

			size_t m_used_blocks_count = 0;

			size_t m_head_index = 0;

			std::unique_ptr<size_t[]> m_pIndexLinks;

			Microsoft::WRL::ComPtr<ID3D12Heap> m_pHeap;
		public:

			struct ptr final
			{
				ID3D12Heap * pHeap;
				UINT64 offset;
			};


			BufferPoolAllocator(size_t block_count, UINT64 blockSize_in_bytes, D3D12_HEAP_PROPERTIES heapProperties, ID3D12Device* pDevice);
			BufferPoolAllocator(const BufferPoolAllocator& source) = delete;
			BufferPoolAllocator& operator=(const BufferPoolAllocator& source) = delete;
			BufferPoolAllocator(BufferPoolAllocator&& source);
			BufferPoolAllocator& operator=(BufferPoolAllocator&& source);
			~BufferPoolAllocator();

			size_t block_count() const;

			bool empty() const;
			bool filled() const;

			ptr allocate();
			void deallocate(typename ptr ptr);
		};

		struct buffer_PoolAllocator_creator
		{
			ID3D12Device* pDevice;
			D3D12_HEAP_PROPERTIES heapProperties;

			BufferPoolAllocator operator()(size_t chunk_size, UINT64 block_size_in_bytes)
			{
				return { chunk_size, block_size_in_bytes, heapProperties, pDevice };
			}
		};
		struct buffer_DynamicAllocator_chunk_size_functor
		{
			UINT64 operator()(size_t index, UINT64 block_size_in_bytes)
			{
				return static_cast<UINT64>(pow(2, index));
			}
		};

		using BufferDynamicAllocator = DynamicPoolAllocator<BufferPoolAllocator, buffer_DynamicAllocator_chunk_size_functor, buffer_PoolAllocator_creator>;

		struct buffer_Dynamic_allocator_creator
		{
			ID3D12Device* pDevice;
			D3D12_HEAP_PROPERTIES heapProperties;

			BufferDynamicAllocator operator()(UINT64 block_size_in_bytes)
			{
				return { block_size_in_bytes,{},{ pDevice, heapProperties } };
			}
		};

		struct buffer_block_size_to_index_functor
		{
			size_t operator()(UINT64 size_in_bytes)
			{
				return static_cast<UINT64>(ceil(log2(eng::D3D12::aligned_size<UINT64>(size_in_bytes, D3D12_DEFAULT_RESOURCE_PLACEMENT_ALIGNMENT) / D3D12_DEFAULT_RESOURCE_PLACEMENT_ALIGNMENT)));
			}
		};

		struct buffer_index_to_aligned_block_size_functor
		{
			UINT64 operator()(size_t index)
			{
				return D3D12_DEFAULT_RESOURCE_PLACEMENT_ALIGNMENT * static_cast<UINT64>(pow(2, index));
			}
		};


		using BufferAllocator = LogarithmicDynamicPoolAllocator<BufferDynamicAllocator, buffer_Dynamic_allocator_creator, buffer_block_size_to_index_functor, buffer_index_to_aligned_block_size_functor>;
	}
}