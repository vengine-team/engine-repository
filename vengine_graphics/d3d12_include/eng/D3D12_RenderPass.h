#pragma once
#include <eng/D3D12_Shader.h>
#include <d3dx12.h>
#include <wrl.h>
#include <vector>
#include <eng/ShaderResource.h>

#include <eng/RenderPass.h>
#include <eng/D3D12_descriptors.h>
#include <eng/DeviceMesh.h>

namespace eng
{
	namespace D3D12
	{
		enum class cull_mode
		{
			none,
			front,
			back
		};

		//interface between platform dependent structures and platform independent structures
		struct device_texture
		{
			UINT shaderResourceView_rootParamIndex;
			UINT sampler_rootParamIndex;
		};
		struct device_buffer
		{
			UINT constantBufferDescriptor_rootParamIndex;
		};
	}

	struct render_pass_parameter
	{
		shader_resource type;

		union
		{
			D3D12::device_texture textureParameter;
			D3D12::device_buffer constantBufferParameter;
		};
	};

	namespace D3D12
	{
		struct RenderPass_Desc
		{
			Shader const * vertexShader = nullptr;
			Shader const * pixelShader = nullptr;

			D3D12_ROOT_SIGNATURE_DESC root_signature_desc = {};

			size_t numParameters = 0;
			render_pass_parameter* pParameters = nullptr;

			cull_mode cullMode = cull_mode::back;

			D3D12_DEPTH_STENCIL_DESC depth_stencil_Desc = CD3DX12_DEPTH_STENCIL_DESC(D3D12_DEFAULT);

			D3D12_RENDER_TARGET_BLEND_DESC blend_Desc =
			{
				FALSE,FALSE,
				D3D12_BLEND_ONE, D3D12_BLEND_ZERO, D3D12_BLEND_OP_ADD,
				D3D12_BLEND_ONE, D3D12_BLEND_ZERO, D3D12_BLEND_OP_ADD,
				D3D12_LOGIC_OP_NOOP,
				D3D12_COLOR_WRITE_ENABLE_ALL,
			};
		};

		template<typename vertex_type>
		class RenderPass : public eng::D3D12::DeviceResource
		{
			mutable Microsoft::WRL::ComPtr<ID3D12RootSignature> m_pRootSignature;
			mutable Microsoft::WRL::ComPtr<ID3D12PipelineState> m_pPipelineState;
		public:
			ID3D12RootSignature* _getRootSignature() const
			{
				return m_pRootSignature.Get();
			}
			ID3D12PipelineState* _getPipelineState() const
			{
				return m_pPipelineState.Get();
			}


			RenderPass(const eng::D3D12::RenderPass_Desc& desc)
			{
				if (!desc.vertexShader)
				{
					throw std::invalid_argument("Vertex Shader was nullptr!");
				}
				if (!desc.pixelShader)
				{
					throw std::invalid_argument("Fragment Shader was nullptr!");
				}

				//Create root signature
				{
					Microsoft::WRL::ComPtr<ID3D10Blob> pSignature;
					Microsoft::WRL::ComPtr<ID3D10Blob> pError;

					D3D12_VERSIONED_ROOT_SIGNATURE_DESC versioned_desc = {};
					versioned_desc.Version = D3D_ROOT_SIGNATURE_VERSION::D3D_ROOT_SIGNATURE_VERSION_1_0;
					versioned_desc.Desc_1_0 = desc.root_signature_desc;

					AssertIfFailed(D3DX12SerializeVersionedRootSignature(&versioned_desc, D3D_ROOT_SIGNATURE_VERSION_1_1, &pSignature, &pError), "Serialize Root Signature Failed");
					AssertIfFailed(eng::D3D12::instance().getDevice()->CreateRootSignature(0, pSignature->GetBufferPointer(), pSignature->GetBufferSize(), IID_PPV_ARGS(&m_pRootSignature)), "CreateRootSignature Failed");
				}

				D3D12_RASTERIZER_DESC rasterizerDesc = CD3DX12_RASTERIZER_DESC(D3D12_DEFAULT);

				D3D12_GRAPHICS_PIPELINE_STATE_DESC state = {};
				{
					state.pRootSignature = m_pRootSignature.Get();

					state.VS = desc.vertexShader->_shaderByteCode();
					state.PS = desc.pixelShader->_shaderByteCode();
					state.DS = { nullptr, 0 };
					state.HS = { nullptr, 0 };
					state.GS = { nullptr, 0 };

					//state.StreamOutput;

					state.BlendState = CD3DX12_BLEND_DESC(D3D12_DEFAULT);
					state.BlendState.RenderTarget[0] = desc.blend_Desc;

					state.SampleMask = UINT_MAX;
					state.RasterizerState = rasterizerDesc;
					state.DepthStencilState = desc.depth_stencil_Desc;
					state.InputLayout = vertex_type::D3D12_inputLayoutDesc();
					state.IBStripCutValue = D3D12_INDEX_BUFFER_STRIP_CUT_VALUE::D3D12_INDEX_BUFFER_STRIP_CUT_VALUE_DISABLED;
					state.PrimitiveTopologyType = D3D12_PRIMITIVE_TOPOLOGY_TYPE_TRIANGLE;
					state.NumRenderTargets = 1;
					state.RTVFormats[0] = D3D12::c_textureFormat;
					state.DSVFormat = D3D12::c_depthBufferFormat;
					state.SampleDesc.Count = 1;
					//state.NodeMask;
					//state.CachedPSO;
					state.Flags = D3D12_PIPELINE_STATE_FLAG_NONE;
				}



				std::chrono::steady_clock::time_point t0 = std::chrono::steady_clock::now();

				AssertIfFailed(eng::D3D12::instance().getDevice()->CreateGraphicsPipelineState(&state, IID_PPV_ARGS(&m_pPipelineState)), "pDevice->CreateGraphicsPipelineState() failed");

				std::chrono::steady_clock::time_point t1 = std::chrono::steady_clock::now();

				std::chrono::milliseconds time = std::chrono::duration_cast<std::chrono::milliseconds>((t1 - t0) * 100);

				std::wstring outputWString = L"CreateGraphicsPipelineState time: ";
				outputWString.append(std::to_wstring(time.count()));
				outputWString.append(L"\n");
				OutputDebugString(outputWString.c_str());
			}
		};
	}
}