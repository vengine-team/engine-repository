#pragma once
#include "D3D12_SharedCommandQueue.h"

#include <d3d12.h>
#include <wrl.h>

#include <array>
#include <mutex>


namespace eng
{
	namespace D3D12
	{
		class GraphicsCommandList final
		{
			std::mutex m_mutex;

			static constexpr size_t s_allocator_count = 8;
			static constexpr size_t s_max_recording_sessions_pre_allocator = 128;

			Microsoft::WRL::ComPtr<ID3D12GraphicsCommandList> m_pGraphicsCommandList = nullptr;


			struct allocator
			{
				Microsoft::WRL::ComPtr<ID3D12CommandAllocator> instance;
				fence_t completion_fence = 0;
			};

			size_t m_current_allocator_index = 0;
			size_t m_current_recording_sessions = 0;
			std::array<typename GraphicsCommandList::allocator, s_allocator_count> m_allocators;

			eng::D3D12::SharedCommandQueue& m_command_queue;

			fence_t advance_session();

		public:

			template<typename F>
			fence_t concurrent_execute(F functor)
			{
				std::scoped_lock<std::mutex> lock(m_mutex);

				functor(m_pGraphicsCommandList.Get());

				return advance_session();
			}

			GraphicsCommandList(ID3D12Device* pDevice, eng::D3D12::SharedCommandQueue& command_queue);
			~GraphicsCommandList();
		};
	}
}