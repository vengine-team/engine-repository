#pragma once
#include <d3d12.h>

namespace eng
{
	namespace D3D12
	{
		template<typename numeric_t>
		numeric_t aligned_size(numeric_t size, numeric_t alignment)
		{
			return ((size + alignment - 1) / alignment) * alignment;
		}
	};
};