#pragma once
#include <d3d12.h>
#include <memory>
#include <vector>
#include <wrl.h>
#include <assert.h>
#include <mutex>

#include <gtm/math.h>

namespace eng
{
	namespace D3D12
	{
		template<typename T>
		struct default_pool_allocator_creator_t
		{
			T* operator()(size_t chunk_size, UINT64 bock_size_in_bytes)
			{
				return T(chunk_size, bock_size_in_bytes);
			}
		};

		template<typename pool_allocator_t, typename chunk_size_functor, typename pool_allocator_creator_t = default_pool_allocator_creator_t<pool_allocator_t>>
		class DynamicPoolAllocator final
		{
			UINT64 m_block_size_in_bytes;
			pool_allocator_creator_t m_creator;

			size_t m_head_index = 0;

			std::vector<pool_allocator_t> m_chunks;

			chunk_size_functor m_chunk_functor;

		public:

			struct ptr final
			{
				typename pool_allocator_t::ptr base_ptr;
				size_t pool_index;
			};

			DynamicPoolAllocator(UINT64 blockSize_in_bytes, chunk_size_functor&& chunk_size_functor, pool_allocator_creator_t&& creator) :
				m_block_size_in_bytes(blockSize_in_bytes), m_creator(std::move(creator)), m_chunk_functor(chunk_size_functor)
			{

			}

			DynamicPoolAllocator(UINT64 blockSize_in_bytes) :
				m_block_size_in_bytes(blockSize_in_bytes),
			{

			}
			DynamicPoolAllocator(const DynamicPoolAllocator& source) = delete;
			DynamicPoolAllocator(DynamicPoolAllocator&& source) noexcept
			{
				m_block_size_in_bytes = source.m_block_size_in_bytes;
				m_creator = std::move(source.m_creator);
				m_chunk_functor = std::move(source.m_chunk_functor);

				m_head_index = source.m_head_index;

				m_chunks = std::move(source.m_chunks);
			}
			DynamicPoolAllocator& operator=(const DynamicPoolAllocator& source) = delete;
			DynamicPoolAllocator& operator=(DynamicPoolAllocator&& source) noexcept
			{
				m_block_size_in_bytes = source.m_block_size_in_bytes;
				m_creator = std::move(source.m_creator);
				m_chunk_functor = std::move(source.m_chunk_functor);

				m_head_index = source.m_head_index;

				m_chunks = std::move(source.m_chunks);
				return *this;
			}
			~DynamicPoolAllocator()
			{

			}

			bool empty() const
			{
				return m_chunks.size() == 1 && m_chunks.front().empty();
			}


			ptr allocate()
			{
				for (UINT64 i = m_head_index; i < m_chunks.size(); i++)
				{
					if (!m_chunks[i].filled())
					{
						return { m_chunks[i].allocate(), i };
					}
				}
				m_head_index = m_chunks.size();
				m_chunks.push_back(m_creator(m_chunk_functor(m_head_index, (UINT)m_block_size_in_bytes), m_block_size_in_bytes));
				return { m_chunks.back().allocate(), m_head_index };
			}
			void deallocate(ptr ptr)
			{
				assert(m_chunks.size() > 0);

				m_chunks[ptr.pool_index].deallocate(ptr.base_ptr);

				m_head_index = ptr.pool_index;

				while (m_chunks.back().empty())
				{
					m_chunks.pop_back();
					if (m_chunks.empty())
					{
						break;
					}
				}
			}
		};

		template<typename T>
		struct default_dynamic_pool_allocator_creator
		{
			T* operator()(UINT64 bock_size_in_bytes)
			{
				return T(bock_size_in_bytes);
			}
		};

		struct default_block_size_to_index_functor
		{
			size_t operator()(UINT64 size_in_bytes)
			{
				return static_cast<UINT64>(ceil(log2(size_in_bytes)));
			}
		};

		struct default_index_to_aligned_block_size_functor
		{
			UINT64 operator()(size_t index)
			{
				return { (UINT64)gtm::pow(2, index) };
			}
		};

		template
		<
			typename dynamic_pool_allocator_t,
			typename dynamic_pool_allocator_creator_t = default_dynamic_pool_allocator_creator<dynamic_pool_allocator_t>,
			typename block_size_to_index_functor_t = default_block_size_to_index_functor,
			typename index_to_aligned_block_size_functor_t = default_index_to_aligned_block_size_functor
		>
		class LogarithmicDynamicPoolAllocator final
		{
			std::mutex m_mutex;

			dynamic_pool_allocator_creator_t m_creator;
			block_size_to_index_functor_t m_block_size_to_index_functor;
			index_to_aligned_block_size_functor_t m_index_to_aligned_block_size_functor;

			std::vector<dynamic_pool_allocator_t> m_allocators;
		public:

			struct ptr
			{
				typename dynamic_pool_allocator_t::ptr base_ptr;
				size_t dynamic_pool_index;
			};

			LogarithmicDynamicPoolAllocator()
			{

			}
			LogarithmicDynamicPoolAllocator(dynamic_pool_allocator_creator_t& creator, block_size_to_index_functor_t& size_to_index, index_to_aligned_block_size_functor_t& index_to_size) :
				m_creator(creator),
				m_block_size_to_index_functor(size_to_index),
				m_index_to_aligned_block_size_functor(index_to_size)
			{

			}

			LogarithmicDynamicPoolAllocator(const LogarithmicDynamicPoolAllocator& source) = delete;
			LogarithmicDynamicPoolAllocator(LogarithmicDynamicPoolAllocator&& source)
			{
				m_creator = std::move(source.m_creator);
				m_block_size_to_index_functor = std::move(source.m_block_size_to_index_functor);
				m_index_to_aligned_block_size_functor = std::move(source.m_index_to_aligned_block_size_functor);

				m_allocators = std::move(source.m_allocators);
			}
			LogarithmicDynamicPoolAllocator& operator=(const LogarithmicDynamicPoolAllocator& source) = delete;
			LogarithmicDynamicPoolAllocator& operator=(LogarithmicDynamicPoolAllocator&& source)
			{
				m_creator = std::move(source.m_creator);
				m_block_size_to_index_functor = std::move(source.m_block_size_to_index_functor);
				m_index_to_aligned_block_size_functor = std::move(source.m_index_to_aligned_block_size_functor);

				m_allocators = std::move(source.m_allocators);
				return *this;
			}
			~LogarithmicDynamicPoolAllocator()
			{

			}

			ptr allocate(UINT64 size_in_bytes)
			{
				std::scoped_lock<std::mutex> lock(m_mutex);

				size_t allocator_index = m_block_size_to_index_functor(size_in_bytes);

				while (m_allocators.size() < allocator_index + 1)
				{
					m_allocators.push_back(m_creator(m_index_to_aligned_block_size_functor(m_allocators.size())));
				}
				return { m_allocators[allocator_index].allocate(), allocator_index };
			}

			void deallocate(ptr ptr)
			{
				std::scoped_lock<std::mutex> lock(m_mutex);

				m_allocators[ptr.dynamic_pool_index].deallocate(ptr.base_ptr);

				if (ptr.dynamic_pool_index = m_allocators.size() - 1)
				{
					for (size_t i = m_allocators.size() - 1; i != 0; i--)
					{
						if (m_allocators[i].empty())
						{
							m_allocators.pop_back();
						}
						else
						{
							break;
						}
					}
				}
			}
		};
	};
};