#pragma once
#include "D3D12_descriptors.h"

#include "D3D12_SharedCommandQueue.h"

#include "D3D12_GraphicsCommandList.h"

#include "D3D12_ConstantBufferAllocator.h"
#include "D3D12_BufferAllocator.h"

namespace eng
{
	namespace D3D12
	{
		class Device final
		{
		private:
			Microsoft::WRL::ComPtr<IDXGIFactory4>			m_pDXGIFactory = nullptr;
			Microsoft::WRL::ComPtr<ID3D12Device>			m_pD3dDevice = nullptr;

			std::unique_ptr<eng::D3D12::SharedCommandQueue>	m_pCommandQueue;
			std::unique_ptr<eng::D3D12::AsyncTaskQueue>		m_pAsyncTaskQueue;
			std::mutex m_commandQueueMutex;

			std::unique_ptr<GraphicsCommandList>			m_pGraphicsCommandList;


			std::unique_ptr<ConstantBufferAllocator>		m_pConstantBufferAllocator;
			std::unique_ptr<BufferAllocator>				m_pDefaultBufferAllocator;
			std::unique_ptr<BufferAllocator>				m_pUploadHeapBufferAllocator;

		public:
			IDXGIFactory4*				getDXGIFactory();
			ID3D12Device*				getDevice();


			AsyncTaskQueue*				getAsyncTaskQueue();
			SharedCommandQueue*			getCommandQueue();
			std::mutex&					commandQueueMutex();
			
			GraphicsCommandList&		graphicsCommandList();

			ConstantBufferAllocator&	constantBufferAllocator();
			BufferAllocator&			defaultBufferAllocator();
			BufferAllocator&			uploadBufferAllocator();

			Device();
			~Device();
		};

		Device& instance();
	};
};

