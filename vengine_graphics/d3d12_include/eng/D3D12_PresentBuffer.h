#pragma once
#include "D3D12_Device.h"
#include "D3D12_DeviceResource.h"

#include <gtm/math.h>
#include <wrl.h>


namespace eng
{
	namespace D3D12
	{
		class PresentBuffer : public eng::D3D12::DeviceResource
		{
			Microsoft::WRL::ComPtr<ID3D12Resource>			m_pPresentBuffer = nullptr;
			Microsoft::WRL::ComPtr<ID3D12DescriptorHeap>	m_pPresentBufferDescriptorHeap;

			Microsoft::WRL::ComPtr<ID3D12DescriptorHeap>	m_pSamplerDescriptorHeap;


			void createPresentBuffer(ID3D12Device* pDevice, ID3D12Resource** ppBuffer, gtm::vector2u scale);
			void createDescriptorsAndViews(ID3D12Device* pDevice, ID3D12Resource* pResource, ID3D12DescriptorHeap** ppPresentBufferDescHeap, ID3D12DescriptorHeap** ppSamplerDescHeap);

		public:

			ID3D12DescriptorHeap*		D3D12_getPresentBufferDescriptorHeap();
			D3D12_GPU_DESCRIPTOR_HANDLE	D3D12_getPresentBufferGPUDescriptorHandle();
			ID3D12Resource*				D3D12_getPresentBuffer();

			ID3D12DescriptorHeap *		D3D12_getSamplerDescriptorHeap();
			D3D12_GPU_DESCRIPTOR_HANDLE	D3D12_getSamplerGPUDescriptorHandle();


			PresentBuffer(gtm::vector2u scale);
			virtual ~PresentBuffer();
		};
	};
};