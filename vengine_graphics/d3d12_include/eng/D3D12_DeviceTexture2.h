#pragma once
#include <eng/D3D12_Device.h>
#include <eng/D3D12_DeviceResource.h>
#include <wrl.h>
#include <eng/Bitmap2.h>

namespace eng
{
	namespace D3D12
	{
		class DeviceTexture2 final : public eng::D3D12::DeviceResource
		{
		private:

			Microsoft::WRL::ComPtr<ID3D12Resource> m_pTexture2Resource;
			Microsoft::WRL::ComPtr<ID3D12DescriptorHeap> m_pTexture2DescriptorHeap;

			Microsoft::WRL::ComPtr<ID3D12DescriptorHeap> m_pSamplerDescriptorHeap;

			void D3D12UploadTexture(const eng::Bitmap2& bitmap);
			void D3D12CreateDescriptorsAndViews(ID3D12Resource* pTexture);

		public:
			ID3D12DescriptorHeap* dx12_textureDescriptorHeap;
			D3D12_GPU_DESCRIPTOR_HANDLE dx12_textureGPUDescriptorHandle;

			ID3D12DescriptorHeap* dx12_samplerDescriptorHeap;
			D3D12_GPU_DESCRIPTOR_HANDLE dx12_samplerGPUDescriptorHandle;

			DeviceTexture2(const eng::Bitmap2& bitmap);
			~DeviceTexture2();
		};
	};
};