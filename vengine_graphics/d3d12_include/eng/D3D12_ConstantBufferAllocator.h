#pragma once
#include <eng/D3D12_LogarithmicDynamicPoolAllocator.h>
#include <eng/D3D12_memory.h>

namespace eng
{
	namespace D3D12
	{
		class ConstantBufferPoolAllocator final
		{
			UINT m_block_count;

			UINT m_256_aligned_block_size_in_bytes;

			UINT m_used_blocks_count = 0;

			UINT64 m_head_index = 0;

			std::unique_ptr<size_t[]> m_pIndexLinks;

			Microsoft::WRL::ComPtr<ID3D12Heap>				m_pHeap;
			Microsoft::WRL::ComPtr<ID3D12Resource>			m_pResource = nullptr;
			Microsoft::WRL::ComPtr<ID3D12DescriptorHeap>	m_pDescriptorHeap = nullptr;
			UINT											m_descriptorSize = 0;
			UINT8*											m_pMappedResource = nullptr;
			D3D12_GPU_DESCRIPTOR_HANDLE						m_gpuDescriptorHandle;
		public:

			struct ptr final
			{
				ID3D12DescriptorHeap * pDescriptorHeap;
				UINT8* pMappedMemory;
				D3D12_GPU_DESCRIPTOR_HANDLE gpuDescriptorHandle;
			};


			ConstantBufferPoolAllocator(UINT block_count, UINT blockSize_in_bytes, ID3D12Device* pDevice);
			ConstantBufferPoolAllocator(const ConstantBufferPoolAllocator& source) = delete;
			ConstantBufferPoolAllocator& operator=(const ConstantBufferPoolAllocator& source) = delete;
			ConstantBufferPoolAllocator(ConstantBufferPoolAllocator&& source);
			ConstantBufferPoolAllocator& operator=(ConstantBufferPoolAllocator&& source);
			~ConstantBufferPoolAllocator();

			UINT block_count() const;

			bool empty() const;
			bool filled() const;

			typename ConstantBufferPoolAllocator::ptr allocate();
			void deallocate(typename ConstantBufferPoolAllocator::ptr ptr);
		};


		struct constantBuffer_PoolAllocator_creator
		{
			ID3D12Device* pDevice;

			ConstantBufferPoolAllocator operator()(UINT chunk_size, UINT block_size_in_bytes)
			{
				return { chunk_size, block_size_in_bytes, pDevice };
			}
		};
		struct constantBuffer_DynamicAllocator_chunk_size_functor
		{
			UINT64 operator()(size_t index, UINT64 block_size_in_bytes)
			{
				return (D3D12_DEFAULT_RESOURCE_PLACEMENT_ALIGNMENT / block_size_in_bytes) * static_cast<UINT64>(pow(2, index));
			}
		};	

		using ConstantBufferDynamicAllocator = DynamicPoolAllocator<ConstantBufferPoolAllocator, constantBuffer_DynamicAllocator_chunk_size_functor, constantBuffer_PoolAllocator_creator>;
		
		struct constantBuffer_Dynamic_allocator_creator
		{
			ID3D12Device* pDevice;

			ConstantBufferDynamicAllocator operator()(UINT64 block_size_in_bytes)
			{
				return { block_size_in_bytes, {}, { pDevice } };
			}
		};

		struct constantBuffer_block_size_to_index_functor
		{
			size_t operator()(UINT64 size_in_bytes)
			{
				return static_cast<UINT64>((log2(eng::D3D12::aligned_size<UINT64>(size_in_bytes, 256) / 256)));
			}
		};

		struct constantBuffer_index_to_aligned_block_size_functor
		{
			UINT64 operator()(size_t index)
			{
				return (UINT64)(256 * pow(2, index));
			}
		};


		using ConstantBufferAllocator = LogarithmicDynamicPoolAllocator<ConstantBufferDynamicAllocator, constantBuffer_Dynamic_allocator_creator, constantBuffer_block_size_to_index_functor, constantBuffer_index_to_aligned_block_size_functor>;
	}
}