#pragma once
#include "par/task_queue.h"
#include "eng/D3D12_task_queue_map.h"

#include <d3d12.h>
#include <wrl.h>

#include <mutex>
#include <atomic>


namespace eng
{
	namespace D3D12
	{
		using fence_t = UINT64;

		class AsyncTaskQueue;

		template<typename T>
		class delete_task
		{
			std::unique_ptr<T> m_instance;

		public:

			void operator()()
			{
				m_instance.reset();
			}

			delete_task(std::unique_ptr<T>&& instance) : m_instance(std::move(instance))
			{

			}
			delete_task(delete_task&& other) : m_instance(std::move(other.m_instance))
			{

			}
		};

		class SharedCommandQueue
		{
		private:

			std::mutex m_fenceMutex;

			Microsoft::WRL::ComPtr<ID3D12CommandQueue> m_pCommandQueue;

			Microsoft::WRL::ComPtr<ID3D12Fence> m_pFence;
			fence_t m_latest_fence = 0;

		public:

			ID3D12Fence* getFence();

			fence_t concurrent_execute_and_signal(UINT numCommandLists, ID3D12CommandList* const * pCommandLists);

			ID3D12CommandQueue* queue();

			fence_t concurrentSignal();

			fence_t concurrentGetLatestFence();

			void concurrentWaitForCompletion(fence_t fenceValue);

			SharedCommandQueue(ID3D12Device* pDevice);
			virtual ~SharedCommandQueue();
		};


		class AsyncTaskQueue
		{
			ID3D12Fence* m_pFence;

			HANDLE m_fenceEventHandle;
			HANDLE m_taskInserted;

			std::mutex m_fenceTaskQueue_mutex;
			
			task_queue_map<fence_t> m_taskQueueMap;

			std::thread m_thread;
			bool m_terminateFlag = false;

			void thread_complete_first_task_queue();
			void thread_wait_for_insert();
			void thread_wait_for_insert_or_fence_signal(fence_t fence);
			void thread_wait_for_fence_signal(fence_t fence);
			void thread_function();
		public:

			template<typename functor_t>
			void concurrent_push(fence_t fence, const functor_t& functor)
			{
				std::scoped_lock<std::mutex> _lock(m_fenceTaskQueue_mutex);
				
				m_taskQueueMap.insert(fence, functor);

				SetEvent(m_taskInserted);
			}

			template<typename functor_t>
			void concurrent_push(fence_t fence, functor_t&& functor)
			{
				std::scoped_lock<std::mutex> _lock(m_fenceTaskQueue_mutex);
				
				m_taskQueueMap.insert(fence, std::move(functor));

				SetEvent(m_taskInserted);
			}

			AsyncTaskQueue(ID3D12Fence* pFence);
			virtual ~AsyncTaskQueue();
		};

	};
};