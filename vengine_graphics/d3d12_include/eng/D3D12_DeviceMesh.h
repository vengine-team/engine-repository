#pragma once
#include "D3D12_Device.h"
#include "eng/Mesh.h"
#include "D3D12_DeviceResource.h"
#include <mutex>
#include <iostream>

namespace eng
{
	namespace D3D12
	{
		template<typename vertex_t>
		class DeviceMesh : public eng::D3D12::DeviceResource
		{
		public:

			size_t						m_vertexCount;
			D3D12_VERTEX_BUFFER_VIEW	m_vertexBufferView;

			size_t						m_indexCount;
			D3D12_INDEX_BUFFER_VIEW		m_indexBufferView;
			D3D12_INDEX_BUFFER_VIEW		m_reverseIndexBufferView;

		private:

			std::tuple<typename eng::D3D12::BufferAllocator::ptr, Microsoft::WRL::ComPtr<ID3D12Resource>> createUploadBufferResource(const void* pData, size_t size_in_bytes)
			{
				CD3DX12_RESOURCE_DESC outBufferDesc = CD3DX12_RESOURCE_DESC::Buffer(size_in_bytes);

				typename eng::D3D12::BufferAllocator::ptr pOut = eng::D3D12::instance().uploadBufferAllocator().allocate(size_in_bytes);
				Microsoft::WRL::ComPtr<ID3D12Resource> pOutResource;
				AssertIfFailed
				(
					D3D12::instance().getDevice()->CreatePlacedResource
					(
						pOut.base_ptr.base_ptr.pHeap,
						pOut.base_ptr.base_ptr.offset,
						&outBufferDesc,
						D3D12_RESOURCE_STATE_GENERIC_READ,
						nullptr,
						IID_PPV_ARGS(&pOutResource)
					),
					"Create placed Upload Buffer failed"
				);					
				
				pOutResource->SetName(L"eng::DeviceMesh::createUploadBufferResource(void*, size_t) pOutResource");
				D3D12_RANGE readRange = { 0,0 };
				UINT8* m_pMappedBufferUploadSource;
				pOutResource->Map(0, &readRange, reinterpret_cast<void**>(&m_pMappedBufferUploadSource));
				memcpy(m_pMappedBufferUploadSource, pData, size_in_bytes);

				return { pOut, pOutResource };
			}
			std::tuple<typename eng::D3D12::BufferAllocator::ptr, Microsoft::WRL::ComPtr<ID3D12Resource>> createDefaultBufferResource(size_t size_in_bytes)
			{
				CD3DX12_RESOURCE_DESC outBufferDesc = CD3DX12_RESOURCE_DESC::Buffer(size_in_bytes);

				typename eng::D3D12::BufferAllocator::ptr pOut = eng::D3D12::instance().defaultBufferAllocator().allocate(size_in_bytes);
				Microsoft::WRL::ComPtr<ID3D12Resource> pOutResource;
				AssertIfFailed
				(
					D3D12::instance().getDevice()->CreatePlacedResource
					(
						pOut.base_ptr.base_ptr.pHeap,
						pOut.base_ptr.base_ptr.offset,
						&outBufferDesc,
						D3D12_RESOURCE_STATE_COPY_DEST,
						nullptr,
						IID_PPV_ARGS(&pOutResource)
					),
					"Create placed Upload Buffer failed"
				);
				return { pOut, pOutResource };
			}

			typename eng::D3D12::BufferAllocator::ptr	m_pVertexBuffer;
			Microsoft::WRL::ComPtr<ID3D12Resource>		m_pVertexBufferResource;


			typename eng::D3D12::BufferAllocator::ptr	m_pIndexBuffer;
			Microsoft::WRL::ComPtr<ID3D12Resource>		m_pIndexBufferResource;

			typename eng::D3D12::BufferAllocator::ptr	m_pReverseIndexBuffer;
			Microsoft::WRL::ComPtr<ID3D12Resource>		m_pReverseIndexBufferResource;

			void uploadMesh(const Mesh<vertex_t>& mesh)
			{
				std::chrono::steady_clock::time_point t0 = std::chrono::steady_clock::now();

				m_vertexCount = mesh.vertices().size();
				const UINT vertexBufferSize = static_cast<UINT>(mesh.vertices().size() * sizeof(mesh.vertices()[0]));

				typename eng::D3D12::BufferAllocator::ptr pVertexBufferUploadSource;
				Microsoft::WRL::ComPtr<ID3D12Resource> pVertexBufferUploadResource;

				if (mesh.vertices().size() != 0)
				{
					std::tie(pVertexBufferUploadSource, pVertexBufferUploadResource) = createUploadBufferResource(mesh.vertices().data(), vertexBufferSize);
					pVertexBufferUploadResource->SetName(L"eng::D3D12::DeviceMesh<vertex_t>::pVertexBufferUploadSourceResource");


					m_pVertexBufferResource.Reset();

					std::tie(m_pVertexBuffer, m_pVertexBufferResource) = createDefaultBufferResource(vertexBufferSize);
					m_pVertexBufferResource->SetName(L"eng::D3D12::DeviceMesh<vertex_t>::m_pVertexBufferResource ");
				}



				D3D12_SUBRESOURCE_DATA vertexData = {};
				vertexData.pData = reinterpret_cast<const BYTE*>(mesh.vertices().data());
				vertexData.RowPitch = vertexBufferSize;
				vertexData.SlicePitch = vertexData.RowPitch;


				m_indexCount = mesh.triangles().size() * 3;
				const UINT indexBufferSize = static_cast<UINT>(mesh.triangles().size() * sizeof(mesh.triangles()[0]));
				std::vector<triangle_indices> reverseIndices;
				reverseIndices.resize(mesh.triangles().size());
				for (size_t i = 0; i < mesh.triangles().size(); i++)
				{
					reverseIndices[i] = { mesh.triangles()[i][1], mesh.triangles()[i][0], mesh.triangles()[i][2] };
				}


				typename eng::D3D12::BufferAllocator::ptr pIndexBufferUpload;
				Microsoft::WRL::ComPtr<ID3D12Resource> pIndexBufferUploadResource;

				typename eng::D3D12::BufferAllocator::ptr pReverseIndexBufferUpload;
				Microsoft::WRL::ComPtr<ID3D12Resource> pReverseIndexBufferUploadResource;

				if (mesh.triangles().size() != 0)
				{
					//index buffer
					CD3DX12_RESOURCE_DESC indexBufferDesc = CD3DX12_RESOURCE_DESC::Buffer(indexBufferSize);
					{

						std::tie(pIndexBufferUpload, pIndexBufferUploadResource) = createUploadBufferResource(mesh.triangles().data(), indexBufferSize);
						pIndexBufferUploadResource->SetName(L"eng::D3D12::DeviceMesh<vertex_t>::pIndexBufferUploadSourceResource");


						m_pIndexBufferResource.Reset();

						std::tie(m_pIndexBuffer, m_pIndexBufferResource) = createDefaultBufferResource(indexBufferSize);
						m_pIndexBufferResource->SetName(L"eng::D3D12::DeviceMesh<vertex_t>::m_pIndexBufferResource ");
					}

					//reverse index buffer
					{

						std::tie(pReverseIndexBufferUpload, pReverseIndexBufferUploadResource) = createUploadBufferResource(reverseIndices.data(), indexBufferSize);
						pReverseIndexBufferUploadResource->SetName(L"eng::D3D12::DeviceMesh<vertex_t>::pReverseIndexBufferUploadSourceResource");


						m_pReverseIndexBufferResource.Reset();

						std::tie(m_pReverseIndexBuffer, m_pReverseIndexBufferResource) = createDefaultBufferResource(indexBufferSize);
						m_pReverseIndexBufferResource->SetName(L"eng::D3D12::DeviceMesh<vertex_t>::m_pReverseIndexBufferResource ");
					}
				}
				//D3D12_SUBRESOURCE_DATA indexData{};
				//indexData.pData = reinterpret_cast<const BYTE*>(mesh.indices().data());
				//indexData.RowPitch = indexBufferSize;
				//indexData.SlicePitch = indexData.RowPitch;


				//D3D12_SUBRESOURCE_DATA reverseIndexData{};
				//reverseIndexData.pData = reinterpret_cast<const BYTE*>(m_reverseIndices.data());
				//reverseIndexData.RowPitch = indexBufferSize;
				//reverseIndexData.SlicePitch = indexData.RowPitch;


				ID3D12Resource* pVertexBufferResourceDest = m_pVertexBufferResource.Get();
				ID3D12Resource* pIndexBufferResourceDest = m_pIndexBufferResource.Get();
				ID3D12Resource* pReverseIndexBufferResourceDest = m_pReverseIndexBufferResource.Get();

				//command list mutex must be locked, because other threads could access the resources
				fence_t completionValue = D3D12::instance().graphicsCommandList().concurrent_execute
				(
					[&](ID3D12GraphicsCommandList* pGraphicsCommandList) 
					{

						if (mesh.vertices().size() != 0)
						{
							//Record Vertex Buffer upload
							pGraphicsCommandList->CopyResource(pVertexBufferResourceDest, pVertexBufferUploadResource.Get());

							CD3DX12_RESOURCE_BARRIER vertexBufferResourceBarrier =
								CD3DX12_RESOURCE_BARRIER::Transition(pVertexBufferResourceDest, D3D12_RESOURCE_STATE_COPY_DEST, D3D12_RESOURCE_STATE_VERTEX_AND_CONSTANT_BUFFER);
							pGraphicsCommandList->ResourceBarrier(1, &vertexBufferResourceBarrier);
						}

						if (mesh.triangles().size() != 0)
						{
							//Record Index Buffer upload
							pGraphicsCommandList->CopyResource(pIndexBufferResourceDest, pIndexBufferUploadResource.Get());

							CD3DX12_RESOURCE_BARRIER indexBufferResourceBarrier =
								CD3DX12_RESOURCE_BARRIER::Transition(pIndexBufferResourceDest, D3D12_RESOURCE_STATE_COPY_DEST, D3D12_RESOURCE_STATE_INDEX_BUFFER);
							pGraphicsCommandList->ResourceBarrier(1, &indexBufferResourceBarrier);


							//Record reverse Index Buffer upload
							pGraphicsCommandList->CopyResource(pReverseIndexBufferResourceDest, pReverseIndexBufferUploadResource.Get());

							CD3DX12_RESOURCE_BARRIER reverseIndexBufferResourceBarrier =
								CD3DX12_RESOURCE_BARRIER::Transition(pReverseIndexBufferResourceDest, D3D12_RESOURCE_STATE_COPY_DEST, D3D12_RESOURCE_STATE_INDEX_BUFFER);
							pGraphicsCommandList->ResourceBarrier(1, &reverseIndexBufferResourceBarrier);
						}


					}
				);

				if (mesh.vertices().size() != 0)
				{
					eng::D3D12::instance().getAsyncTaskQueue()->concurrent_push
					(
						completionValue,
						[
							pVertexBufferUploadSource,
							pVertexBufferUploadResource{ std::move(pVertexBufferUploadResource) }
						]()
						{
							eng::D3D12::instance().uploadBufferAllocator().deallocate(pVertexBufferUploadSource);
						}
					);

					m_vertexBufferView.BufferLocation = m_pVertexBufferResource->GetGPUVirtualAddress();
					m_vertexBufferView.StrideInBytes = sizeof(vertex_t);
					m_vertexBufferView.SizeInBytes = vertexBufferSize;
				}

				if (mesh.triangles().size() != 0)
				{
					eng::D3D12::instance().getAsyncTaskQueue()->concurrent_push
					(
						completionValue,
						[
							pIndexBufferUpload,
							pIndexBufferUploadResource{ std::move(pIndexBufferUploadResource) },
							pReverseIndexBufferUpload,
							pReverseIndexBufferUploadResource{ std::move(pReverseIndexBufferUploadResource) }
						]()
						{
							eng::D3D12::instance().uploadBufferAllocator().deallocate(pIndexBufferUpload);
							eng::D3D12::instance().uploadBufferAllocator().deallocate(pReverseIndexBufferUpload);
						}
					);

					m_indexBufferView.BufferLocation = m_pIndexBufferResource->GetGPUVirtualAddress();
					m_indexBufferView.SizeInBytes = indexBufferSize;
					m_indexBufferView.Format = DXGI_FORMAT_R32_UINT;

					m_reverseIndexBufferView.BufferLocation = m_pReverseIndexBufferResource->GetGPUVirtualAddress();
					m_reverseIndexBufferView.SizeInBytes = indexBufferSize;
					m_reverseIndexBufferView.Format = DXGI_FORMAT_R32_UINT;
				}

				//std::chrono::steady_clock::time_point t1 = std::chrono::steady_clock::now();

				//std::chrono::steady_clock::duration d = t1 - t0;


				//std::wstring out = L"Mesh CPU dur: ";
				//out.append(std::to_wstring((double)d.count() / (double)1000000));
				//out.append(L"\n");
				//OutputDebugString(out.c_str());
			}

		public:

			DeviceMesh(const Mesh<vertex_t>& mesh)
			{
				uploadMesh(mesh);
			}
			virtual ~DeviceMesh()
			{

			}
		};
	};
};