#pragma once
#include <d3d12.h>
#include <dxgi1_6.h>

namespace eng
{
	namespace D3D12
	{
		struct resource_gpu_descriptor
		{
			ID3D12DescriptorHeap* pDescriptorHeap;
			D3D12_GPU_DESCRIPTOR_HANDLE gpu_descriptor_handle;
		};

		struct texture_gpu_descriptor
		{
			resource_gpu_descriptor texture;
			resource_gpu_descriptor sampler;
		};

		constexpr UINT			c_bufferCount = 2;

		constexpr DXGI_FORMAT	c_textureFormat = DXGI_FORMAT_R8G8B8A8_UNORM;
		constexpr DXGI_FORMAT	c_depthBufferFormat = DXGI_FORMAT_D32_FLOAT;
	};
};