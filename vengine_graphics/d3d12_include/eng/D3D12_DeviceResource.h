#pragma once
#include "D3D12_Device.h"
#include "D3D12_task_queue_map.h"

#include <ext/ticket_table.h>

#include <shared_mutex>
#include <atomic>
#include <vector>

namespace eng
{
	namespace D3D12
	{
		class DeviceResource
		{
		public:
			DeviceResource() = default;
			DeviceResource(const DeviceResource& source) = delete;
			DeviceResource(DeviceResource&& source) = delete;
			DeviceResource& operator=(const DeviceResource& source) = delete;
			DeviceResource& operator=(DeviceResource&& source) = delete;
			virtual ~DeviceResource() = default;
		};
	}
}