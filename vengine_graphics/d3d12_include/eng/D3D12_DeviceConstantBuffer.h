#pragma once
#include "D3D12_Device.h"
#include "D3D12_DeviceResource.h"

namespace eng
{
	namespace D3D12
	{
		class DeviceConstantBuffer final : public eng::D3D12::DeviceResource
		{
			typename D3D12::ConstantBufferAllocator::ptr m_ptr;
		public:

			void* mappedBuffer();

			eng::D3D12::resource_gpu_descriptor descriptor();

			DeviceConstantBuffer(size_t size_in_bytes);
			~DeviceConstantBuffer();
		};
	}
}