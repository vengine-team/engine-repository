#pragma once
#include <d3d12.h>
#include <string>
#include <wrl.h>
#include <d3dcompiler.h>
#include <eng/windowsUtil.h>
#include <filesystem>

namespace eng
{
	namespace D3D12
	{
		class Shader
		{
		private:

			Microsoft::WRL::ComPtr<ID3D10Blob> m_byteCodeBlob;

			D3D12_SHADER_BYTECODE m_byteCode;

		public:

			Shader(std::filesystem::path file, std::string entryPoint, std::string target);
			Shader(const void* pSrcData, size_t srcDataSize, std::string entryPoint, std::string target);

			D3D12_SHADER_BYTECODE const& _shaderByteCode() const;
		};
	}
};