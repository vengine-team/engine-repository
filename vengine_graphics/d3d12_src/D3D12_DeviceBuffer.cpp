#include <eng/D3D12_DeviceConstantBuffer.h>

void * eng::D3D12::DeviceConstantBuffer::mappedBuffer()
{
	return m_ptr.base_ptr.base_ptr.pMappedMemory;
}

eng::D3D12::resource_gpu_descriptor eng::D3D12::DeviceConstantBuffer::descriptor()
{
	return { m_ptr.base_ptr.base_ptr.pDescriptorHeap, m_ptr.base_ptr.base_ptr.gpuDescriptorHandle };
}

eng::D3D12::DeviceConstantBuffer::DeviceConstantBuffer(size_t size_in_bytes) : m_ptr(eng::D3D12::instance().constantBufferAllocator().allocate(size_in_bytes))
{

}

eng::D3D12::DeviceConstantBuffer::~DeviceConstantBuffer()
{
	eng::D3D12::instance().constantBufferAllocator().deallocate(m_ptr);
}
