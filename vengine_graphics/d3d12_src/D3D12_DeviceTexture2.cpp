#include <eng/D3D12_DeviceTexture2.h>
#include <eng/windowsUtil.h>
#include <eng/D3D12_descriptors.h>
#include <d3dx12.h>

void eng::D3D12::DeviceTexture2::D3D12UploadTexture(const eng::Bitmap2& bitmap)
{
	Microsoft::WRL::ComPtr<ID3D12Resource> pIntermediate;

	D3D12_RESOURCE_DESC texture_resource_desc = {};
	texture_resource_desc.MipLevels = 1;
	texture_resource_desc.Format = eng::D3D12::c_textureFormat;

	texture_resource_desc.Width = static_cast<UINT>(bitmap.scale()[0]);
	texture_resource_desc.Height = static_cast<UINT>(bitmap.scale()[1]);
	texture_resource_desc.DepthOrArraySize = 1;

	texture_resource_desc.Flags = D3D12_RESOURCE_FLAG_NONE;
	texture_resource_desc.SampleDesc.Count = 1;
	texture_resource_desc.SampleDesc.Quality = 0;
	texture_resource_desc.Dimension = D3D12_RESOURCE_DIMENSION_TEXTURE2D;

	//Create gpu resource
	{
		AssertIfFailed
		(
			D3D12::instance().getDevice()->CreateCommittedResource
			(
				&CD3DX12_HEAP_PROPERTIES(D3D12_HEAP_TYPE_DEFAULT),
				D3D12_HEAP_FLAG_NONE,
				&texture_resource_desc,
				D3D12_RESOURCE_STATE_COPY_DEST,
				nullptr,
				IID_PPV_ARGS(&m_pTexture2Resource)
			),
			"eng::Texture2D::D3D12UploadTexture() creating ID3D12Resource failed"
		);
		m_pTexture2Resource->SetName(L"eng::DeviceTexture2::m_pTexture2Resource");
	}

	//Create intermediate resource for upload
	{
		const UINT64 uploadBufferSize = GetRequiredIntermediateSize(m_pTexture2Resource.Get(), 0, 1);

		AssertIfFailed
		(
			D3D12::instance().getDevice()->CreateCommittedResource
			(
				&CD3DX12_HEAP_PROPERTIES(D3D12_HEAP_TYPE_UPLOAD),
				D3D12_HEAP_FLAG_NONE,
				&CD3DX12_RESOURCE_DESC::Buffer(uploadBufferSize),
				D3D12_RESOURCE_STATE_GENERIC_READ,
				nullptr,
				IID_PPV_ARGS(&pIntermediate)
			),
			"eng::Texture2D::D3D12UploadTexture() creating intermediate ID3D12Resource failed"
		);
		pIntermediate->SetName(L"eng::DeviceTexture2::pIntermediate");
	}

	color* pTexture = new color[4];
	pTexture[0] = { 255,  0,  0,255 };
	pTexture[1] = { 255,  0,  0,255 };
	pTexture[2] = { 0,255,  0,255 };
	pTexture[3] = { 255,  0,  0,255 };

	D3D12_SUBRESOURCE_DATA textureData = {};
	textureData.pData = bitmap.pixels().data();
	textureData.RowPitch = bitmap.scale()[0] * sizeof(bitmap.pixels()[0]);
	textureData.SlicePitch = textureData.RowPitch * bitmap.scale()[1];


	// upload Texture
	fence_t completed = D3D12::instance().graphicsCommandList().concurrent_execute
	(
		[&](ID3D12GraphicsCommandList* pGraphicsCommandList)
		{
			UpdateSubresources(pGraphicsCommandList, m_pTexture2Resource.Get(), pIntermediate.Get(), 0, 0, 1, &textureData);

			pGraphicsCommandList->ResourceBarrier(1, &CD3DX12_RESOURCE_BARRIER::Transition(m_pTexture2Resource.Get(), D3D12_RESOURCE_STATE_COPY_DEST, D3D12_RESOURCE_STATE_PIXEL_SHADER_RESOURCE));
		}
	);

	D3D12::instance().getAsyncTaskQueue()->concurrent_push(completed, [pIntermediate]() {});
}

void eng::D3D12::DeviceTexture2::D3D12CreateDescriptorsAndViews(ID3D12Resource * pTexture)
{
	{
		D3D12_DESCRIPTOR_HEAP_DESC samplerDescHeapDesc = {};
		samplerDescHeapDesc.Flags = D3D12_DESCRIPTOR_HEAP_FLAG_SHADER_VISIBLE;
		samplerDescHeapDesc.Type = D3D12_DESCRIPTOR_HEAP_TYPE_SAMPLER;
		samplerDescHeapDesc.NumDescriptors = 1;
		D3D12::instance().getDevice()->CreateDescriptorHeap(&samplerDescHeapDesc, IID_PPV_ARGS(&m_pSamplerDescriptorHeap));

		D3D12_SAMPLER_DESC sampler_desc = {};
		sampler_desc.Filter = D3D12_FILTER_MIN_MAG_MIP_POINT;
		sampler_desc.AddressU = D3D12_TEXTURE_ADDRESS_MODE_BORDER;
		sampler_desc.AddressV = D3D12_TEXTURE_ADDRESS_MODE_BORDER;
		sampler_desc.AddressW = D3D12_TEXTURE_ADDRESS_MODE_BORDER;
		sampler_desc.MipLODBias = 0;
		sampler_desc.MaxAnisotropy = 0;
		sampler_desc.ComparisonFunc = D3D12_COMPARISON_FUNC_NEVER;
		sampler_desc.BorderColor[0] = 0;
		sampler_desc.BorderColor[1] = 0;
		sampler_desc.BorderColor[2] = 0;
		sampler_desc.BorderColor[3] = 1;
		sampler_desc.MinLOD = 0.0f;
		sampler_desc.MaxLOD = D3D12_FLOAT32_MAX;

		D3D12::instance().getDevice()->CreateSampler(&sampler_desc, m_pSamplerDescriptorHeap->GetCPUDescriptorHandleForHeapStart());





		D3D12_DESCRIPTOR_HEAP_DESC textureDescHeapDesc = {};
		textureDescHeapDesc.Flags = D3D12_DESCRIPTOR_HEAP_FLAG_SHADER_VISIBLE;
		textureDescHeapDesc.Type = D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV;
		textureDescHeapDesc.NumDescriptors = 1;
		D3D12::instance().getDevice()->CreateDescriptorHeap(&textureDescHeapDesc, IID_PPV_ARGS(&m_pTexture2DescriptorHeap));




		D3D12_SHADER_RESOURCE_VIEW_DESC srvDesc = {};
		srvDesc.Shader4ComponentMapping = D3D12_DEFAULT_SHADER_4_COMPONENT_MAPPING;
		srvDesc.Format = eng::D3D12::c_textureFormat;
		srvDesc.ViewDimension = D3D12_SRV_DIMENSION_TEXTURE2D;
		srvDesc.Texture2D.MipLevels = 1;
		D3D12::instance().getDevice()->CreateShaderResourceView(pTexture, &srvDesc, m_pTexture2DescriptorHeap->GetCPUDescriptorHandleForHeapStart());



		dx12_textureDescriptorHeap = m_pTexture2DescriptorHeap.Get();
		dx12_textureGPUDescriptorHandle = m_pTexture2DescriptorHeap->GetGPUDescriptorHandleForHeapStart();

		dx12_samplerDescriptorHeap = m_pSamplerDescriptorHeap.Get();
		dx12_samplerGPUDescriptorHandle = m_pSamplerDescriptorHeap->GetGPUDescriptorHandleForHeapStart();
	}
}

eng::D3D12::DeviceTexture2::DeviceTexture2(const eng::Bitmap2& bitmap)
{
	if (bitmap.scale()[0] > 0 && bitmap.scale()[1] > 0)
	{
		D3D12UploadTexture(bitmap);
		D3D12CreateDescriptorsAndViews(m_pTexture2Resource.Get());
	}
	else
	{
		//Create null views
		D3D12CreateDescriptorsAndViews(nullptr);
	}
}
eng::D3D12::DeviceTexture2::~DeviceTexture2()
{

}
