#include <eng/D3D12_SharedCommandQueue.h>
#include <eng/D3D12_descriptors.h>
#include <eng/windowsUtil.h>
#include <assert.h>


ID3D12Fence * eng::D3D12::SharedCommandQueue::getFence()
{
	return m_pFence.Get();
}

eng::D3D12::fence_t eng::D3D12::SharedCommandQueue::concurrent_execute_and_signal(UINT numCommandLists, ID3D12CommandList* const * pCommandLists)
{
	std::scoped_lock<std::mutex> lock(m_fenceMutex);

	m_pCommandQueue->ExecuteCommandLists(numCommandLists, pCommandLists);

	m_latest_fence++;

	m_pCommandQueue->Signal(m_pFence.Get(), m_latest_fence);

	return m_latest_fence;
}
ID3D12CommandQueue * eng::D3D12::SharedCommandQueue::queue()
{
	return m_pCommandQueue.Get();
}

eng::D3D12::fence_t eng::D3D12::SharedCommandQueue::concurrentSignal()
{
	std::scoped_lock<std::mutex> lock(m_fenceMutex);

	m_latest_fence++;

	m_pCommandQueue->Signal(m_pFence.Get(), m_latest_fence);

	return m_latest_fence;
}

eng::D3D12::fence_t eng::D3D12::SharedCommandQueue::concurrentGetLatestFence()
{
	std::scoped_lock<std::mutex> lock(m_fenceMutex);
	return m_latest_fence;
}

void eng::D3D12::SharedCommandQueue::concurrentWaitForCompletion(eng::D3D12::fence_t fenceValue)
{
	eng::D3D12::fence_t completedValue = m_pFence->GetCompletedValue();

	if (completedValue < fenceValue)
	{
		HANDLE eventHandle = CreateEvent(nullptr, FALSE, FALSE, nullptr);

		std::chrono::steady_clock::time_point t1 = std::chrono::steady_clock::now();


		m_pFence->SetEventOnCompletion(fenceValue, eventHandle);
		WaitForSingleObjectEx(eventHandle, INFINITE, FALSE);

		std::chrono::steady_clock::time_point t2 = std::chrono::steady_clock::now();

		std::chrono::steady_clock::duration dur = t2 - t1;

		std::chrono::microseconds durInMicroS = std::chrono::duration_cast<std::chrono::microseconds>(dur);

		std::wstring msg = L"Wait fot GPU time ";
		msg.append(std::to_wstring(durInMicroS.count()));
		msg.append(L"\n");

		CloseHandle(eventHandle);
	}
}

eng::D3D12::SharedCommandQueue::SharedCommandQueue(ID3D12Device * pDevice)
{
	D3D12_COMMAND_QUEUE_DESC queueDesc = {};
	queueDesc.Flags = D3D12_COMMAND_QUEUE_FLAG_NONE;
	queueDesc.Type = D3D12_COMMAND_LIST_TYPE_DIRECT;
	
	AssertIfFailed(pDevice->CreateCommandQueue(&queueDesc, IID_PPV_ARGS(&m_pCommandQueue)), "eng::D3D12::createCommandQueue() pDevice->CreateCommandQueue() failed");

	AssertIfFailed
	(
		pDevice->CreateFence(0, D3D12_FENCE_FLAG_NONE, IID_PPV_ARGS(&m_pFence)),
		"application_pDevice->CreateFence() failed"
	);
}

eng::D3D12::SharedCommandQueue::~SharedCommandQueue()
{

}






void eng::D3D12::AsyncTaskQueue::thread_complete_first_task_queue()
{
	std::unique_lock<std::mutex> unique_lock(m_fenceTaskQueue_mutex);

	m_taskQueueMap.process(m_taskQueueMap.begin(), unique_lock);

	ResetEvent(m_taskInserted);
}

void eng::D3D12::AsyncTaskQueue::thread_wait_for_insert()
{
	WaitForSingleObjectEx(m_taskInserted, INFINITE, FALSE);
}

void eng::D3D12::AsyncTaskQueue::thread_wait_for_insert_or_fence_signal(fence_t fence)
{
	m_pFence->SetEventOnCompletion(fence, m_fenceEventHandle);

	HANDLE events[2] = 
	{ 
		m_fenceEventHandle, 
		m_taskInserted 
	};

	WaitForMultipleObjectsEx(2, events, FALSE, INFINITE, FALSE);
}

void eng::D3D12::AsyncTaskQueue::thread_wait_for_fence_signal(fence_t fence)
{
	m_pFence->SetEventOnCompletion(fence, m_fenceEventHandle);
	WaitForSingleObjectEx(m_fenceEventHandle, INFINITE, FALSE);
}

void eng::D3D12::AsyncTaskQueue::thread_function()
{
	while (!m_terminateFlag)
	{
		

		bool queue_empty;

		fence_t earliest_fence;
		{
			std::scoped_lock<std::mutex> lock(m_fenceTaskQueue_mutex);

			queue_empty = m_taskQueueMap.empty();
			if (!queue_empty)
			{
				earliest_fence = m_taskQueueMap.begin()->first;
			}
		}

		if (queue_empty)
		{
			thread_wait_for_insert();
		}
		else
		{
			thread_wait_for_insert_or_fence_signal(earliest_fence);
		}
		//there is definitly something in the task queue now

		{
			std::scoped_lock<std::mutex> lock(m_fenceTaskQueue_mutex);

			earliest_fence = m_taskQueueMap.begin()->first;
		}

		if (m_pFence->GetCompletedValue() >= earliest_fence)
		{
			thread_complete_first_task_queue();
		}
	}

	//end of life, no more task inserts
	while (!m_taskQueueMap.empty())
	{		
		//there is definitly something in the task queue now

		fence_t earliest_fence = m_taskQueueMap.begin()->first;

		thread_wait_for_fence_signal(earliest_fence);

		if (m_pFence->GetCompletedValue() >= earliest_fence)
		{
			thread_complete_first_task_queue();
		}
	}
}

eng::D3D12::AsyncTaskQueue::AsyncTaskQueue(ID3D12Fence* pFence) : m_pFence(pFence)
{
	m_fenceEventHandle = CreateEvent(nullptr, FALSE, FALSE, nullptr);
	m_taskInserted = CreateEvent(nullptr, FALSE, FALSE, nullptr);

	m_thread = std::thread(&eng::D3D12::AsyncTaskQueue::thread_function, this);
}

eng::D3D12::AsyncTaskQueue::~AsyncTaskQueue()
{
	struct terminate_task_functor
	{
		bool& target;
		void operator()()
		{
			target = true;
		}
	};

	concurrent_push(0, terminate_task_functor{ m_terminateFlag });

	m_thread.join();
}
