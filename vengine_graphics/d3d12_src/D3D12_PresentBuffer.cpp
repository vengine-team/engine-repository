#include <eng/D3D12_PresentBuffer.h>
#include <eng/windowsUtil.h>
#include <d3dx12.h>

void eng::D3D12::PresentBuffer::createPresentBuffer(ID3D12Device* pDevice, ID3D12Resource** ppBuffer, gtm::vector2u scale)
{
	D3D12_RESOURCE_DESC texture_resource_desc = {};
	texture_resource_desc.MipLevels = 1;
	texture_resource_desc.Format = eng::D3D12::c_textureFormat;

	texture_resource_desc.Width = (UINT64)scale[0];
	texture_resource_desc.Height = (UINT64)scale[1];
	texture_resource_desc.DepthOrArraySize = 1;

	texture_resource_desc.Flags = D3D12_RESOURCE_FLAG_NONE;
	texture_resource_desc.SampleDesc.Count = 1;
	texture_resource_desc.SampleDesc.Quality = 0;
	texture_resource_desc.Dimension = D3D12_RESOURCE_DIMENSION_TEXTURE2D;

	//Create gpu resource
	{
		AssertIfFailed
		(
			D3D12::instance().getDevice()->CreateCommittedResource
			(
				&CD3DX12_HEAP_PROPERTIES(D3D12_HEAP_TYPE_DEFAULT),
				D3D12_HEAP_FLAG_NONE,
				&texture_resource_desc,
				D3D12_RESOURCE_STATE_PIXEL_SHADER_RESOURCE,
				nullptr,
				IID_PPV_ARGS(ppBuffer)
			),
			"eng::D3D12::PresentBuffer::createPresentBuffer() eng::D3D12::PresentBuffer::m_pPresentBuffer"
		);
	}
}

void eng::D3D12::PresentBuffer::createDescriptorsAndViews(ID3D12Device* pDevice, ID3D12Resource* pResource, ID3D12DescriptorHeap** ppPresentBufferDescHeap, ID3D12DescriptorHeap** ppSamplerDescHeap)
{
	D3D12_DESCRIPTOR_HEAP_DESC samplerDescHeapDesc = {};
	samplerDescHeapDesc.Flags = D3D12_DESCRIPTOR_HEAP_FLAG_SHADER_VISIBLE;
	samplerDescHeapDesc.Type = D3D12_DESCRIPTOR_HEAP_TYPE_SAMPLER;
	samplerDescHeapDesc.NumDescriptors = 1;
	pDevice->CreateDescriptorHeap(&samplerDescHeapDesc, IID_PPV_ARGS(ppSamplerDescHeap));

	D3D12_SAMPLER_DESC sampler_desc = {};
	sampler_desc.Filter = D3D12_FILTER_MIN_MAG_MIP_POINT;
	sampler_desc.AddressU = D3D12_TEXTURE_ADDRESS_MODE_BORDER;
	sampler_desc.AddressV = D3D12_TEXTURE_ADDRESS_MODE_BORDER;
	sampler_desc.AddressW = D3D12_TEXTURE_ADDRESS_MODE_BORDER;
	sampler_desc.MipLODBias = 0;
	sampler_desc.MaxAnisotropy = 0;
	sampler_desc.ComparisonFunc = D3D12_COMPARISON_FUNC_NEVER;
	sampler_desc.BorderColor[0] = 0;
	sampler_desc.BorderColor[1] = 0;
	sampler_desc.BorderColor[2] = 0;
	sampler_desc.BorderColor[3] = 1;
	sampler_desc.MinLOD = 0.0f;
	sampler_desc.MaxLOD = D3D12_FLOAT32_MAX;

	pDevice->CreateSampler(&sampler_desc, (*ppSamplerDescHeap)->GetCPUDescriptorHandleForHeapStart());





	D3D12_DESCRIPTOR_HEAP_DESC textureDescHeapDesc = {};
	textureDescHeapDesc.Flags = D3D12_DESCRIPTOR_HEAP_FLAG_SHADER_VISIBLE;
	textureDescHeapDesc.Type = D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV;
	textureDescHeapDesc.NumDescriptors = 1;
	pDevice->CreateDescriptorHeap(&textureDescHeapDesc, IID_PPV_ARGS(ppPresentBufferDescHeap));




	D3D12_SHADER_RESOURCE_VIEW_DESC srvDesc = {};
	srvDesc.Shader4ComponentMapping = D3D12_DEFAULT_SHADER_4_COMPONENT_MAPPING;
	srvDesc.Format = eng::D3D12::c_textureFormat;
	srvDesc.ViewDimension = D3D12_SRV_DIMENSION_TEXTURE2D;
	srvDesc.Texture2D.MipLevels = 1;
	pDevice->CreateShaderResourceView(pResource, &srvDesc, (*ppPresentBufferDescHeap)->GetCPUDescriptorHandleForHeapStart());
}

ID3D12DescriptorHeap * eng::D3D12::PresentBuffer::D3D12_getPresentBufferDescriptorHeap()
{
	return m_pPresentBufferDescriptorHeap.Get();
}

ID3D12Resource * eng::D3D12::PresentBuffer::D3D12_getPresentBuffer()
{
	return m_pPresentBuffer.Get();
}

ID3D12DescriptorHeap * eng::D3D12::PresentBuffer::D3D12_getSamplerDescriptorHeap()
{
	return m_pSamplerDescriptorHeap.Get();
}

D3D12_GPU_DESCRIPTOR_HANDLE eng::D3D12::PresentBuffer::D3D12_getSamplerGPUDescriptorHandle()
{
	return m_pSamplerDescriptorHeap->GetGPUDescriptorHandleForHeapStart();
}

D3D12_GPU_DESCRIPTOR_HANDLE eng::D3D12::PresentBuffer::D3D12_getPresentBufferGPUDescriptorHandle()
{
	return m_pPresentBufferDescriptorHeap->GetGPUDescriptorHandleForHeapStart();
}

eng::D3D12::PresentBuffer::PresentBuffer(gtm::vector2u scale)
{
	createPresentBuffer(eng::D3D12::instance().getDevice(), &m_pPresentBuffer, scale);
	m_pPresentBuffer->SetName(L"eng::D3D12::PresentBuffer.m_pPresentBuffer");
	createDescriptorsAndViews
	(
		eng::D3D12::instance().getDevice(),
		m_pPresentBuffer.Get(),
		&m_pPresentBufferDescriptorHeap,
		&m_pSamplerDescriptorHeap
	);
	m_pPresentBufferDescriptorHeap->SetName(L"eng::D3D12::PresentBuffer.m_pPresentBufferDescriptorHeap");

	m_pSamplerDescriptorHeap->SetName(L"eng::D3D12::PresentBuffer.m_pSamplerDescriptorHeap");
}

eng::D3D12::PresentBuffer::~PresentBuffer()
{

}
