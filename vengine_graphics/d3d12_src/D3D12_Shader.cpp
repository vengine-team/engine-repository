#pragma once
#include <eng/D3D12_Shader.h>

using namespace Microsoft::WRL;

eng::D3D12::Shader::Shader(std::filesystem::path file, std::string entryPoint, std::string target)
{
	Microsoft::WRL::ComPtr<ID3D10Blob> errorBlob;

	UINT compileFlags = 0;

	HRESULT hr = D3DCompileFromFile(file.wstring().c_str(), nullptr, nullptr, entryPoint.c_str(), target.c_str(), compileFlags, 0, &m_byteCodeBlob, &errorBlob);
	if (!SUCCEEDED(hr))
	{
		char* pError = reinterpret_cast<char*>(errorBlob->GetBufferPointer());

		std::string msg = "Compilation of Shader failed: ";
		msg.append(pError);

		throw std::invalid_argument(msg);
	}
	m_byteCode = D3D12_SHADER_BYTECODE{ reinterpret_cast<UINT8*>(m_byteCodeBlob->GetBufferPointer()), m_byteCodeBlob->GetBufferSize() };
}

eng::D3D12::Shader::Shader(const void * pSrcData, size_t srcDataSize, std::string entryPoint, std::string target)
{
	Microsoft::WRL::ComPtr<ID3D10Blob> errorBlob;

	UINT compileFlags = 0;

	HRESULT hr = D3DCompile(pSrcData, static_cast<SIZE_T>(srcDataSize), nullptr, nullptr, nullptr, entryPoint.c_str(), target.c_str(), compileFlags, 0, &m_byteCodeBlob, &errorBlob);

	if (!SUCCEEDED(hr))
	{
		char* pError = reinterpret_cast<char*>(errorBlob->GetBufferPointer());

		std::string msg = "Compilation of Shader failed: ";
		msg.append(pError);

		throw std::invalid_argument(msg);
	}
	m_byteCode = D3D12_SHADER_BYTECODE{ reinterpret_cast<UINT8*>(m_byteCodeBlob->GetBufferPointer()), m_byteCodeBlob->GetBufferSize() };
}

D3D12_SHADER_BYTECODE const& eng::D3D12::Shader::_shaderByteCode() const
{
	return m_byteCode;
}
