#include <eng/D3D12_GraphicsCommandList.h>
#include <eng/windowsUtil.h>

eng::D3D12::fence_t eng::D3D12::GraphicsCommandList::advance_session()
{
	m_pGraphicsCommandList->Close();

	ID3D12CommandList* pCommandList[1] = { m_pGraphicsCommandList.Get() };
	eng::D3D12::fence_t out = m_command_queue.concurrent_execute_and_signal(1, pCommandList);
	m_allocators[m_current_allocator_index].completion_fence = out;

	m_current_recording_sessions++;

	if (m_current_recording_sessions == s_max_recording_sessions_pre_allocator)
	{
		m_current_recording_sessions = 0;

		m_current_allocator_index = (m_current_allocator_index + 1) % s_allocator_count;


		m_command_queue.concurrentWaitForCompletion(m_allocators[m_current_allocator_index].completion_fence);
		AssertIfFailed(m_allocators[m_current_allocator_index].instance->Reset(), "m_allocators[m_current_allocator_index].instance->Reset()");
	}

	m_pGraphicsCommandList->Reset(m_allocators[m_current_allocator_index].instance.Get(), nullptr);

	return out;
}

eng::D3D12::GraphicsCommandList::GraphicsCommandList(ID3D12Device * pDevice, eng::D3D12::SharedCommandQueue& command_queue) : m_command_queue(command_queue)
{
	for (size_t i = 0; i < s_allocator_count; i++)
	{
		AssertIfFailed
		(
			pDevice->CreateCommandAllocator
			(
				D3D12_COMMAND_LIST_TYPE_DIRECT,
				IID_PPV_ARGS(&m_allocators[i].instance)
			),
			"application_pDevice->CreateCommandAllocator() failed"
		);
		m_allocators[i].instance->SetName(std::wstring(L"application_pCommandAllocator[").append(std::to_wstring(i)).append(L"]").c_str());

	}

	AssertIfFailed
	(
		pDevice->CreateCommandList
		(
			0,
			D3D12_COMMAND_LIST_TYPE_DIRECT,
			m_allocators[m_current_allocator_index].instance.Get(),
			nullptr,
			IID_PPV_ARGS(&m_pGraphicsCommandList)
		),
		"application_pDevice->CreateCommandList()"
	);
	m_pGraphicsCommandList->SetName(L"application_pCommandList");
}

eng::D3D12::GraphicsCommandList::~GraphicsCommandList()
{

}
