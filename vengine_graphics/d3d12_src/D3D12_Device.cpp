#include <eng/D3D12_Device.h>
#include <eng/windowsUtil.h>
#include <d3dx12.h>

#include <streambuf>
#include <fstream>
#include <iostream>

void createD3dDevice(IDXGIFactory4* pFactory, ID3D12Device** ppD3dDevice)
{
	if (AllocConsole() == TRUE)
	{
		freopen("CONIN$", "r", stdin);
		freopen("CONOUT$", "w", stdout);
		freopen("CONOUT$", "w", stderr);
	}


	Microsoft::WRL::ComPtr<IDXGIAdapter1> pAdapter = nullptr;
	for (UINT i = 0; pFactory->EnumAdapters1(i, &pAdapter) != DXGI_ERROR_NOT_FOUND; i++)
	{
		DXGI_ADAPTER_DESC1 desc1;
		pAdapter->GetDesc1(&desc1);
		std::wcout << i << L". " << desc1.Description << L"\n";
	}

	while (true)
	{
		std::wcout << L"Select Adapter: ";
		std::wstring input;
		getline(std::wcin, input);
		try
		{
			int selection = std::stoi(input);
			if (selection < 0)
			{
				std::wcout << "invalid input!\n";
			}
			pFactory->EnumAdapters1(selection, &pAdapter);
			break;
		}
		catch (const std::exception&)
		{
			std::wcout << "invalid input!\n";
		}
	}

	HRESULT hr = D3D12CreateDevice(pAdapter.Get(), D3D_FEATURE_LEVEL_11_0, IID_PPV_ARGS(ppD3dDevice));
	AssertIfFailed(hr, "Device Creation failed");

	if (FreeConsole() != TRUE)
	{
		assert(false);
	}
}

#define NAME_D3D12_OBJECT(x) x.Get()->SetName(L#x)

using namespace Microsoft::WRL;

const UINT c_allocatorCount = 2;

IDXGIFactory4 * eng::D3D12::Device::getDXGIFactory()
{
	return m_pDXGIFactory.Get();
}
ID3D12Device * eng::D3D12::Device::getDevice()
{
	return m_pD3dDevice.Get();
}

eng::D3D12::AsyncTaskQueue * eng::D3D12::Device::getAsyncTaskQueue()
{
	return m_pAsyncTaskQueue.get();
}

eng::D3D12::SharedCommandQueue * eng::D3D12::Device::getCommandQueue()
{
	return m_pCommandQueue.get();
}

std::mutex & eng::D3D12::Device::commandQueueMutex()
{
	return m_commandQueueMutex;
}

eng::D3D12::GraphicsCommandList & eng::D3D12::Device::graphicsCommandList()
{
	return *m_pGraphicsCommandList;
}

eng::D3D12::ConstantBufferAllocator & eng::D3D12::Device::constantBufferAllocator()
{
	return *m_pConstantBufferAllocator;
}

eng::D3D12::BufferAllocator & eng::D3D12::Device::defaultBufferAllocator()
{
	return *m_pDefaultBufferAllocator;
}

eng::D3D12::BufferAllocator & eng::D3D12::Device::uploadBufferAllocator()
{
	return *m_pUploadHeapBufferAllocator;
}

eng::D3D12::Device::~Device()
{

}

eng::D3D12::Device::Device()
{
#ifndef NDEBUG

	{
		ComPtr<ID3D12Debug> pDebugController;

		if (SUCCEEDED(D3D12GetDebugInterface(IID_PPV_ARGS(&pDebugController))))
		{
			pDebugController->EnableDebugLayer();
		}
		else
		{
			OutputDebugString(L"GetDebug Interface was enabled but failed");
		}
	}

#endif
	AssertIfFailed(::CreateDXGIFactory1(IID_PPV_ARGS(&m_pDXGIFactory)), "RenderTarget::createDxgiFactory() failed");
	createD3dDevice(m_pDXGIFactory.Get(), &m_pD3dDevice);
	m_pD3dDevice->SetName(L"m_pD3dDevice");
	m_pCommandQueue.reset(new eng::D3D12::SharedCommandQueue(getDevice()));
	m_pAsyncTaskQueue.reset(new eng::D3D12::AsyncTaskQueue(m_pCommandQueue->getFence()));

	m_pGraphicsCommandList = std::make_unique<GraphicsCommandList>(getDevice(), *getCommandQueue());

	buffer_Dynamic_allocator_creator default_creator{ getDevice(), CD3DX12_HEAP_PROPERTIES(D3D12_HEAP_TYPE_DEFAULT) };
	buffer_Dynamic_allocator_creator upload_creator{ getDevice(), CD3DX12_HEAP_PROPERTIES(D3D12_HEAP_TYPE_UPLOAD) };
	m_pConstantBufferAllocator = std::make_unique<D3D12::ConstantBufferAllocator>(constantBuffer_Dynamic_allocator_creator{ getDevice() }, constantBuffer_block_size_to_index_functor{}, constantBuffer_index_to_aligned_block_size_functor{});
	m_pDefaultBufferAllocator = std::make_unique<D3D12::BufferAllocator>(default_creator, buffer_block_size_to_index_functor{}, buffer_index_to_aligned_block_size_functor{});
	m_pUploadHeapBufferAllocator = std::make_unique<D3D12::BufferAllocator>(upload_creator, buffer_block_size_to_index_functor{}, buffer_index_to_aligned_block_size_functor{});
}

eng::D3D12::Device & eng::D3D12::instance()
{
	static eng::D3D12::Device instance;

	return instance;
}