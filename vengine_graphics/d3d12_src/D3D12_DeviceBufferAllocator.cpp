#include <eng/D3D12_ConstantBufferAllocator.h>
#include <d3dx12.h>
#include <assert.h>
#include <eng/windowsUtil.h>
#include <eng/D3D12_memory.h>

eng::D3D12::ConstantBufferPoolAllocator::ConstantBufferPoolAllocator(UINT block_count, UINT blockSize_in_bytes, ID3D12Device* pDevice) :
	m_block_count(block_count),
	m_256_aligned_block_size_in_bytes(blockSize_in_bytes),
	m_pIndexLinks(std::make_unique<size_t[]>(block_count))
{
	assert(blockSize_in_bytes % 256 == 0);
	assert((block_count * blockSize_in_bytes) % D3D12_DEFAULT_RESOURCE_PLACEMENT_ALIGNMENT == 0);


	D3D12_HEAP_DESC heapDesc = {};
	heapDesc.Alignment = D3D12_DEFAULT_RESOURCE_PLACEMENT_ALIGNMENT;
	heapDesc.Properties = CD3DX12_HEAP_PROPERTIES(D3D12_HEAP_TYPE_UPLOAD);
	heapDesc.Flags = D3D12_HEAP_FLAG_DENY_RT_DS_TEXTURES | D3D12_HEAP_FLAG_DENY_NON_RT_DS_TEXTURES;
	heapDesc.SizeInBytes = m_256_aligned_block_size_in_bytes * block_count;

	AssertIfFailed(pDevice->CreateHeap(&heapDesc, IID_PPV_ARGS(&m_pHeap)), "CreateHeap failed");
	

	D3D12_RESOURCE_DESC resourceDesc = CD3DX12_RESOURCE_DESC::Buffer(heapDesc.SizeInBytes);
	AssertIfFailed(pDevice->CreatePlacedResource(m_pHeap.Get(), 0, &resourceDesc, D3D12_RESOURCE_STATE_GENERIC_READ, nullptr, IID_PPV_ARGS(&m_pResource)), "Create Resource failed");


	D3D12_DESCRIPTOR_HEAP_DESC descriptor_heapDesc = {};
	descriptor_heapDesc.NumDescriptors = block_count;
	descriptor_heapDesc.Type = D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV;
	descriptor_heapDesc.Flags = D3D12_DESCRIPTOR_HEAP_FLAG_SHADER_VISIBLE;
	descriptor_heapDesc.NodeMask = 0;
	AssertIfFailed(pDevice->CreateDescriptorHeap(&descriptor_heapDesc, IID_PPV_ARGS(&m_pDescriptorHeap)), "Create Descriptor Heap failed");

	D3D12_GPU_VIRTUAL_ADDRESS cbvGPUAdress = m_pResource->GetGPUVirtualAddress();
	CD3DX12_CPU_DESCRIPTOR_HANDLE cbvCpuHandle(m_pDescriptorHeap->GetCPUDescriptorHandleForHeapStart());
	m_descriptorSize = pDevice->GetDescriptorHandleIncrementSize(D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV);

	for (size_t i = 0; i < m_block_count; i++)
	{
		m_pIndexLinks[i] = i + 1;
	
		D3D12_CONSTANT_BUFFER_VIEW_DESC const_buffer_view_desc;
		const_buffer_view_desc.BufferLocation = cbvGPUAdress;
		const_buffer_view_desc.SizeInBytes = m_256_aligned_block_size_in_bytes;
		pDevice->CreateConstantBufferView(&const_buffer_view_desc, cbvCpuHandle);

		cbvGPUAdress += const_buffer_view_desc.SizeInBytes;
		cbvCpuHandle.Offset(m_descriptorSize);
	}

	D3D12_RANGE range = { 0,0 };

	m_pResource->Map(0, &range, reinterpret_cast<void**>(&m_pMappedResource));
	m_gpuDescriptorHandle = m_pDescriptorHeap->GetGPUDescriptorHandleForHeapStart();
}
eng::D3D12::ConstantBufferPoolAllocator::ConstantBufferPoolAllocator(ConstantBufferPoolAllocator && source)
{
	m_block_count = source.m_block_count;

	m_256_aligned_block_size_in_bytes = source.m_256_aligned_block_size_in_bytes;

	m_used_blocks_count = source.m_used_blocks_count;

	m_head_index = source.m_head_index;

	m_pIndexLinks = std::move(source.m_pIndexLinks);

	m_pHeap = std::move(source.m_pHeap);
	m_pResource = std::move(source.m_pResource);
	m_pDescriptorHeap = std::move(source.m_pDescriptorHeap);
	m_descriptorSize = source.m_descriptorSize;
	m_pMappedResource = source.m_pMappedResource;
	m_gpuDescriptorHandle = source.m_gpuDescriptorHandle;

	source.m_block_count = 0;
	source.m_used_blocks_count = 0;
}

eng::D3D12::ConstantBufferPoolAllocator & eng::D3D12::ConstantBufferPoolAllocator::operator=(ConstantBufferPoolAllocator && source)
{
	m_block_count = source.m_block_count;

	m_256_aligned_block_size_in_bytes = source.m_256_aligned_block_size_in_bytes;

	m_used_blocks_count = source.m_used_blocks_count;

	m_head_index = source.m_head_index;

	m_pIndexLinks = std::move(source.m_pIndexLinks);

	m_pHeap = std::move(source.m_pHeap);
	m_pResource = std::move(source.m_pResource);
	m_pDescriptorHeap = std::move(source.m_pDescriptorHeap);
	m_descriptorSize = source.m_descriptorSize;
	m_pMappedResource = source.m_pMappedResource;
	m_gpuDescriptorHandle = source.m_gpuDescriptorHandle;

	source.m_block_count = 0;
	source.m_used_blocks_count = 0;
	return *this;
}

eng::D3D12::ConstantBufferPoolAllocator::~ConstantBufferPoolAllocator()
{
	assert(m_used_blocks_count == 0);
}

UINT eng::D3D12::ConstantBufferPoolAllocator::block_count() const
{
	return m_block_count;
}

bool eng::D3D12::ConstantBufferPoolAllocator::empty() const
{
	return m_used_blocks_count == 0;
}

bool eng::D3D12::ConstantBufferPoolAllocator::filled() const
{
	return m_used_blocks_count == m_block_count;
}

typename eng::D3D12::ConstantBufferPoolAllocator::ptr eng::D3D12::ConstantBufferPoolAllocator::allocate()
{
	assert(m_used_blocks_count != m_block_count);

	m_used_blocks_count++;

	size_t index = m_head_index;

	m_head_index = m_pIndexLinks[m_head_index];

	return 
	{
		m_pDescriptorHeap.Get(),
		m_pMappedResource + index * m_256_aligned_block_size_in_bytes,
		{ m_gpuDescriptorHandle.ptr + index * m_descriptorSize }
	};
}

void eng::D3D12::ConstantBufferPoolAllocator::deallocate(typename eng::D3D12::ConstantBufferPoolAllocator::ptr ptr)
{
	assert(m_used_blocks_count != 0);
	assert(ptr.pDescriptorHeap == m_pDescriptorHeap.Get());

	m_used_blocks_count--;

	size_t formerHead = m_head_index;
	m_head_index = (ptr.gpuDescriptorHandle.ptr - m_gpuDescriptorHandle.ptr) / m_descriptorSize;

	m_pIndexLinks[m_head_index] = formerHead;
}
