#include <eng/D3D12_BufferAllocator.h>
#include <eng/D3D12_memory.h>
#include <d3dx12.h>

#include <eng/windowsUtil.h>

#include <assert.h>


eng::D3D12::BufferPoolAllocator::BufferPoolAllocator(size_t block_count, UINT64 blockSize_in_bytes, D3D12_HEAP_PROPERTIES heapProperties, ID3D12Device * pDevice) :
	m_block_count(block_count),
	m_64KB_aligned_block_size_in_bytes(blockSize_in_bytes),
	m_pIndexLinks(std::make_unique<size_t[]>(block_count))
{
	assert(blockSize_in_bytes % (1024 * 64) == 0);

	D3D12_HEAP_DESC heapDesc = {};
	heapDesc.Alignment = D3D12_DEFAULT_RESOURCE_PLACEMENT_ALIGNMENT;
	heapDesc.Properties = heapProperties;
	heapDesc.Flags = D3D12_HEAP_FLAG_DENY_RT_DS_TEXTURES | D3D12_HEAP_FLAG_DENY_NON_RT_DS_TEXTURES;
	heapDesc.SizeInBytes = m_64KB_aligned_block_size_in_bytes * block_count;

	AssertIfFailed(pDevice->CreateHeap(&heapDesc, IID_PPV_ARGS(&m_pHeap)), "CreateHeap failed");

	for (size_t i = 0; i < m_block_count; i++)
	{
		m_pIndexLinks[i] = i + 1;
	}
}

eng::D3D12::BufferPoolAllocator::BufferPoolAllocator(BufferPoolAllocator && source)
{
	m_block_count = source.m_block_count;

	m_64KB_aligned_block_size_in_bytes = source.m_64KB_aligned_block_size_in_bytes;

	m_used_blocks_count = source.m_used_blocks_count;

	m_head_index = source.m_head_index;

	m_pIndexLinks = std::move(source.m_pIndexLinks);

	m_pHeap = std::move(source.m_pHeap);

	source.m_block_count = 0;
	source.m_used_blocks_count = 0;
}

eng::D3D12::BufferPoolAllocator & eng::D3D12::BufferPoolAllocator::operator=(BufferPoolAllocator && source)
{
	m_block_count = source.m_block_count;

	m_64KB_aligned_block_size_in_bytes = source.m_64KB_aligned_block_size_in_bytes;

	m_used_blocks_count = source.m_used_blocks_count;

	m_head_index = source.m_head_index;

	m_pIndexLinks = std::move(source.m_pIndexLinks);

	m_pHeap = std::move(source.m_pHeap);

	source.m_block_count = 0;
	source.m_used_blocks_count = 0;

	return *this;
}

eng::D3D12::BufferPoolAllocator::~BufferPoolAllocator()
{
	assert(m_used_blocks_count == 0);
}

size_t eng::D3D12::BufferPoolAllocator::block_count() const
{
	return m_block_count;
}

bool eng::D3D12::BufferPoolAllocator::empty() const
{
	return m_used_blocks_count == 0;
}

bool eng::D3D12::BufferPoolAllocator::filled() const
{
	return m_used_blocks_count == m_block_count;
}

eng::D3D12::BufferPoolAllocator::ptr eng::D3D12::BufferPoolAllocator::allocate()
{
	assert(m_used_blocks_count != m_block_count);

	m_used_blocks_count++;

	size_t index = m_head_index;

	m_head_index = m_pIndexLinks[m_head_index];

	return
	{
		m_pHeap.Get(),
		index * m_64KB_aligned_block_size_in_bytes
	};
}

void eng::D3D12::BufferPoolAllocator::deallocate(typename ptr ptr)
{
	assert(m_used_blocks_count != 0);
	assert(ptr.pHeap == m_pHeap.Get());

	m_used_blocks_count--;

	size_t formerHead = m_head_index;
	m_head_index = ptr.offset / m_64KB_aligned_block_size_in_bytes;

	m_pIndexLinks[m_head_index] = formerHead;
}
