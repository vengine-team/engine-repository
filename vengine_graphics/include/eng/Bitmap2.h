#pragma once
#include <vector>
#include <gtm/math.h>
#include "color.h"

namespace eng
{

	class Bitmap2
	{
	private:
		std::vector<color> m_pixels;
		gtm::vector<size_t, 2> m_scale;
	public:

		gtm::vector<size_t, 2> scale() const;

		const color& pixel(const gtm::vector<size_t, 2>& pos) const;
		color& pixel(const gtm::vector<size_t, 2>& pos);

		const std::vector<color>& pixels() const;

		//helper functions
		void copy_to_Position(gtm::vector<signed long long, 2> pos, const Bitmap2&);

		//initialization constructors
		Bitmap2(size_t height, const std::vector<color>& pixels);
		Bitmap2(size_t height, std::vector<color>&& pixels);
		Bitmap2(gtm::vector<size_t, 2> scale, color _color);
		Bitmap2(gtm::vector<size_t, 2> scale);


		//syntax sugar
		Bitmap2& operator=(const Bitmap2& source);
		Bitmap2& operator=(Bitmap2&& source);


		Bitmap2();
		Bitmap2(const Bitmap2& source);
		Bitmap2(Bitmap2&& source);

		virtual ~Bitmap2() noexcept;
	};

	eng::Bitmap2 load_png(std::wstring path);
};