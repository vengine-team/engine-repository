#pragma once
#include <Windows.h>
#include <string>

namespace eng
{
	namespace windows
	{
		void engineFromWinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow);

		HINSTANCE getApplicationHInstance();

		std::wstring getApplicationPath();
		std::wstring getApplicationDirectory();
	}
}
