#pragma once

#include <wrl.h>
#include <d3dx12.h>
#include <eng/windowsUtil.h>
#include <DirectXMath.h>

#include <eng/Mesh.h>
#include <eng/D3D12_Device.h>
#include <eng/D3D12_DeviceMesh.h>

namespace eng
{
	template<typename vertex_type>
	class DeviceMesh
	{
	private:
		Mesh<vertex_type> m_deviceMesh;
		
		mutable std::shared_ptr<eng::D3D12::DeviceMesh<vertex_type>> m_pD3D12_DeviceMesh = nullptr;

	public:
#pragma region DirectX

		std::shared_ptr<eng::D3D12::DeviceMesh<vertex_type>> D3D12_getMesh() const
		{
			return m_pD3D12_DeviceMesh;
		}

#pragma endregion
		explicit DeviceMesh(const Mesh<vertex_type>& source)
		{
			set(source);
		}
		DeviceMesh(Mesh<vertex_type>&& source)
		{
			set(std::move(source));
		}
		DeviceMesh(DeviceMesh<vertex_type>&& other)
		{
			m_deviceMesh = std::move(other.m_deviceMesh);
			m_pD3D12_DeviceMesh = std::move(other.m_pD3D12_DeviceMesh);
		}
		DeviceMesh<vertex_type>& operator=(DeviceMesh<vertex_type>&& other)
		{
			m_deviceMesh = std::move(other.m_deviceMesh);
			m_pD3D12_DeviceMesh = std::move(other.m_pD3D12_DeviceMesh);
			return *this;
		}
		virtual ~DeviceMesh()
		{

		}

		void set(const Mesh<vertex_type>& mesh)
		{
			m_deviceMesh = mesh;
			m_pD3D12_DeviceMesh = std::make_shared<eng::D3D12::DeviceMesh<vertex_type>>(mesh);
			eng::D3D12::fence_t latestFence = eng::D3D12::instance().getCommandQueue()->concurrentGetLatestFence();
			eng::D3D12::instance().getAsyncTaskQueue()->concurrent_push(latestFence, [ptr{ m_pD3D12_DeviceMesh }](){});
		}
		void set(Mesh<vertex_type>&& mesh)
		{
			m_deviceMesh = mesh;
			m_pD3D12_DeviceMesh = std::make_shared<eng::D3D12::DeviceMesh<vertex_type>>(mesh);
			eng::D3D12::fence_t latestFence = eng::D3D12::instance().getCommandQueue()->concurrentGetLatestFence();
			eng::D3D12::instance().getAsyncTaskQueue()->concurrent_push(latestFence, [ptr{ m_pD3D12_DeviceMesh }](){});
		}

		const Mesh<vertex_type>& getMesh() const
		{
			return m_deviceMesh;
		}
	};
};