#pragma once
#include <eng/D3D12_descriptors.h>
#include <eng/D3D12_DeviceResource.h>

#include <eng/ShaderResource.h>

#include <gtm/math.h>
#include <memory>

namespace eng
{
	class IDeviceTexture2 : public ShaderResource
	{
	public:
#ifdef _DIRECTX

		virtual std::tuple<D3D12::texture_gpu_descriptor, std::shared_ptr<eng::D3D12::DeviceResource>> D3D12_get_SRV_and_Sampler() const = 0;
#endif // _DIRECTX

		virtual gtm::vector2u get_scale() const = 0;

		virtual ~IDeviceTexture2() = default;
	};
};