#pragma once
#include <algorithm>
#include <memory>

namespace eng
{
	template<typename translated_t, typename translator, typename underlying_It_t>
	struct const_translated_container_iterator
	{
	private:
		underlying_It_t m_underlying_It;
	public:

		using difference_type = ptrdiff_t;
		using value_type = translated_t;
		using pointer = const translated_t*;
		using reference = const translated_t&;
		using iterator_category = std::bidirectional_iterator_tag;

		const_translated_container_iterator(underlying_It_t underlying) : m_underlying_It(underlying)
		{

		}

		reference operator*() const
		{	// return designated object

			translator translator;
			return translator(*m_underlying_It);
		}
		typename std::pointer_traits<pointer>::pointer operator->() const
		{	// return pointer to class object
			return (typename std::pointer_traits<pointer>::pointer>::pointer_to(**this));
		}

		bool operator==(const const_translated_container_iterator& other) const
		{
			return m_underlying_It == other.m_underlying_It;
		}

		bool operator!=(const const_translated_container_iterator& other) const
		{
			return m_underlying_It != other.m_underlying_It;
		}

		const_translated_container_iterator & operator++()
		{	// preincrement
			this->m_underlying_It++;
			return (*this);
		}
		const_translated_container_iterator operator++(int)
		{	// postincrement
			sequence_view_iterator _Tmp = *this;
			++*this;
			return (_Tmp);
		}

		const_translated_container_iterator& operator--()
		{	// predecrement
			this->m_underlying_It--;
			return (*this);
		}
		const_translated_container_iterator operator--(int)
		{	// postdecrement
			sequence_view_iterator _Tmp = *this;
			--*this;
			return (_Tmp);
		}
	};

	template<typename translated_t, typename container_t, typename read_translator, typename write_translator>
	class translated_container_view
	{
	private:

		container_t& m_container;

	public:

		translated_container_view(container_t& container_ref) : m_container(container_ref), manager_ref(manager_ref)
		{
			
		}

		using const_iterator = const_translated_container_iterator<translated_t, read_translator, container_t::const_iterator>;
		const_iterator begin() const
		{
			return { m_container.begin() };
		}
		const_iterator end() const
		{
			return { m_container.end() };
		}

		const_iterator cbegin() const
		{
			return { m_container.cbegin() };
		}
		const_iterator cend() const
		{
			return { m_container.cend() };
		}

		using const_reverse_iterator = const_translated_container_iterator<translated_t, read_translator, container_t::const_reverse_iterator>;
		const_reverse_iterator rbegin() const
		{
			return { m_container.rbegin() };
		}
		const_reverse_iterator rend() const
		{
			return { m_container.rend() };
		}

		const_reverse_iterator crbegin() const
		{
			return { m_container.crbegin() };
		}
		const_reverse_iterator crend() const
		{
			return { m_container.crend() };
		}

		size_t size() const
		{
			return m_container.size();
		}

		void push_back(typename const translated_t& value)
		{
			write_translator translator;
			m_container.push_back(translator(value));
		}
		void push_back(typename translated_t&& value)
		{
			write_translator translator;
			m_container.push_back(translator(std::move(value)));
		}

		const_iterator erase(const_iterator& It)
		{
			return { m_container.erase(It) };
		}

		void pop_back()
		{
			m_manager.pop_back();
		}
	};
};