#pragma once
#include "Node.h"
#include "event.h"

namespace eng
{
	template<typename _Child_t>
	class Basic_EventParent : public Basic_Parent<_Child_t>
	{
	private:

	public:

		virtual ~Basic_EventParent()
		{

		}
	};

	template<typename _Parent_t>
	class Basic_EventChild : public Basic_Child<_Parent_t>
	{
	private:

		eng::event<const _Parent_t*>	m_parent_set_event;
		eng::event<const _Parent_t*>	m_parent_removed_event;

	public:

		eng::event_subscribe_access<const _Parent_t*> parent_set_event()
		{
			return { m_parent_set_event };
		}
		eng::event_subscribe_access<const _Parent_t*> parent_removed_event()
		{
			return { m_parent_removed_event };
		}

		virtual void parent_set(const Parent_t* pCurrentParent)
		{
			Basic_Child<_Parent_t>::parent_set(pCurrentParent);
			m_parent_set_event(pCurrentParent);
		}

		virtual void parent_removed(const Parent_t* pPreviousParent)
		{
			Basic_Child<_Parent_t>::parent_removed(pPreviousParent);
			m_parent_removed_event(pPreviousParent);
		}

		Basic_EventChild()
		{

		}
		virtual ~Basic_EventChild()
		{

		}
	};
};