#pragma once
#include <gtm/math.h>

#include <string>
#include <vector>
#include <array>

namespace eng
{
	enum class input_type
	{
		Mouse,
		Keyboard
	};
	
	enum class mouse_property
	{
		LeftButton,
		RightButton,
		MiddleButton,
		Position
	};
	
	struct mouse_input_state final
	{
		bool left_button;
		bool right_button;
		bool middle_button;

		gtm::vector2i position;
	};
	struct keyboard_input_state final
	{
		std::array<bool, 256> keyStates = {};
	};
	
	struct input_state final
	{
		mouse_input_state mouse;
		keyboard_input_state keyboard;
	};
	
	struct input_state_change final
	{
		input_type type;
		union
		{
			mouse_property mouse_property;
			size_t changed_keycode;
		};

		input_state before;
		input_state current;
	};
	
	struct FrameInput_Desc
	{
	public:
		std::wstring compositionString = L"";
#pragma warning("/you know what to do")
		input_state initialState;
#pragma warning("you know what to do/")
		std::vector<input_state_change> mouseStateChanges;
	};
	
	const input_state& getLatestMouseState(const FrameInput_Desc& desc);

	class Input
	{
	private:
		void countMouseChanges();

		std::wstring m_composition_string = L"";

		input_state m_initial_state;
		std::vector<input_state_change> m_mouseStateChanges;

		unsigned int m_mouseLeftDownCount = 0;
		unsigned int m_mouseLeftUpCount = 0;
	public:

		using const_iterator = std::vector<input_state_change>::const_iterator;
		using const_reverse_iterator = std::vector<input_state_change>::const_reverse_iterator;

		const std::wstring& composition_string() const;

		const input_state& initial_state() const;
		const input_state& latest_state() const;

		const_iterator states_begin() const;
		const_iterator states_end() const;

		const_reverse_iterator states_rbegin() const;
		const_reverse_iterator states_rend() const;

		size_t mouse_states_count() const;

		unsigned int mouseLeftDownCount() const;
		unsigned int mouseLeftUpCount() const;

		Input(const Input& source);
		Input(Input&& source);
		Input& operator=(const Input& source);
		Input& operator=(Input&& source);
		explicit Input(const FrameInput_Desc& desc);
		explicit Input(FrameInput_Desc&& desc);


		virtual ~Input();
	};
};
