#pragma once
#include <eng/D3D12_Device.h>
#include <eng/D3D12_DeviceResource.h>

#include <eng/D3D12_DeviceConstantBuffer.h>

#include <array>
#include <memory>

namespace eng
{
	class DeviceBuffer
	{
		static constexpr size_t s_reserve_buffers = eng::D3D12::c_bufferCount;

		mutable std::array<std::shared_ptr<eng::D3D12::DeviceConstantBuffer>, s_reserve_buffers> m_d3d12ConstantBuffers;
		size_t m_current_bufferIndex = 0;

		size_t m_size_in_bytes;

	public:

		std::tuple<eng::D3D12::resource_gpu_descriptor, std::shared_ptr<eng::D3D12::DeviceResource>> D3D12_ConstantBuffer_transaction() const;

		void store(void* pData, size_t size_in_bytes);

		//size may be zero
		DeviceBuffer(size_t size_in_bytes);
		DeviceBuffer(const DeviceBuffer& source) = delete;
		DeviceBuffer(DeviceBuffer&& other);
		DeviceBuffer& operator=(const DeviceBuffer& source) = delete;
		DeviceBuffer& operator=(DeviceBuffer&& other);
		~DeviceBuffer();
	};
}