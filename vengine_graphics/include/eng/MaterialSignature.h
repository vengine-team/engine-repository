#pragma once
#include "RenderPass.h"
#include <vector>
#include "RenderLayer.h"
#include <string>

namespace eng
{
	struct resource_Binding_Desc
	{
		size_t render_pass_index;
		size_t resource_index;
	};

	struct RootParameter_Desc
	{
		eng::shader_resource shaderResourceType;

		size_t num_shaderResourcesBindings;
		const resource_Binding_Desc* pShaderResourcesBindings;
	};
	struct RootSignature_Desc
	{
		size_t numParameters;
		const RootParameter_Desc* pParameters;
	};


	class RootSignature
	{
	private:
		std::shared_ptr<char> m_pScene;
		struct std_free_deleter
		{
			void operator()(char* pCharArray);
		};


		// for faster random access at the cost of copy and assignment speed
		resource_Binding_Desc* m_pResourceIndices_begin;
		size_t m_numParameters;

	public:

		size_t parameterCount() const;
		const RootParameter_Desc& getRootParameter(size_t parameterIndex) const;

		///
		/// Creates a root signature form the description, the data from the description is copied, 
		/// a RootSignature Object does not depend on the RootSignature_Desc that was used to create it
		///
		RootSignature(const RootSignature_Desc& desc);

		RootSignature(const RootSignature& source);
		RootSignature(RootSignature&& source) noexcept;
	};

	struct material_parameter
	{
		eng::shader_resource type;
		size_t tag;
	};

	template<typename vertex_type>
	struct render_pass_binding
	{
		std::shared_ptr<RenderPass<vertex_type> const> render_pass;
		std::vector<size_t> parameter_bindings;
	};

	template<typename vertex_type>
	class MaterialSignature
	{
	private:

		std::vector<render_pass_binding<vertex_type>>	m_renderPass_bindings;
		std::vector<material_parameter>					m_parameters;
		unsigned int									m_queueStep;

	public:

		const std::vector<render_pass_binding<vertex_type>>& render_pass_bindings() const
		{
			return m_renderPass_bindings;
		}

		const std::vector<material_parameter>& parameters() const
		{
			return m_parameters;
		}

		const unsigned int queueStep() const
		{
			return m_queueStep;
		}

		template<typename parameters_it_t, typename render_pass_it_t>
		MaterialSignature(parameters_it_t parameters_begin, parameters_it_t parameters_end, render_pass_it_t render_pass_bindings_begin, render_pass_it_t render_pass_bindings_end, unsigned int queueStep)
		{
			m_renderPass_bindings = std::vector<render_pass_binding<vertex_type>>(render_pass_bindings_begin, render_pass_bindings_end);
			m_parameters = std::vector<material_parameter>(parameters_begin, parameters_end);
			m_queueStep = queueStep;
		}
		
		MaterialSignature<vertex_type>& operator=(const MaterialSignature<vertex_type>& other)
		{
			m_renderPass_bindings = other.m_renderPass_bindings;
			m_parameters = other.m_parameters;
			m_queueStep = other.m_queueStep;

			return *this;
		}
		MaterialSignature<vertex_type>& operator=(MaterialSignature<vertex_type>&& other)
		{
			m_renderPass_bindings = std::move(other.m_renderPass_bindings);
			m_parameters = std::move(other.m_parameters);
			m_queueStep = std::move(other.m_queueStep);

			return *this;
		}

		MaterialSignature(const MaterialSignature<vertex_type>& other)
		{
			m_renderPass_bindings = other.m_renderPass_bindings;
			m_parameters = other.m_parameters;
			m_queueStep = other.m_queueStep;
		}
		MaterialSignature(MaterialSignature<vertex_type>&& other)
		{
			m_renderPass_bindings = std::move(other.m_renderPass_bindings);
			m_parameters = std::move(other.m_parameters);
			m_queueStep = std::move(other.m_queueStep);
		}
	};
};


namespace std
{
	std::string to_string(const eng::RootSignature& value);
	std::wstring to_wstring(const eng::RootSignature& value);
}
