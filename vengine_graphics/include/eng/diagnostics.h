#pragma once
#include <par/IUpdateable.h>

#include <string>
#include <map>
#include <vector>
#include <shared_mutex>
#include <assert.h>

namespace eng 
{
	namespace diagnostics
	{
		class TimeRecorder
		{
			std::shared_mutex m_mutex;

			size_t m_currentIndex = 0;

			size_t m_maxSteps = 600;

			struct recorder_pair
			{
				std::chrono::steady_clock::time_point begin;
				std::chrono::steady_clock::duration duration = std::chrono::steady_clock::duration::zero();
			};

			std::map<std::string, std::pair<bool, std::vector<recorder_pair>>> m_recorded;
			using const_iterator = std::vector<recorder_pair>::const_iterator;

		public:

			void beginRead();

			const_iterator durations_begin(std::string s);

			const_iterator durations_end(std::string s);

			void endRead();


			void beginStep();


			void endStep();


			void reportBegin(std::string key);


			std::chrono::steady_clock::duration reportEnd(std::string key);

		};

		class InformationWindow
		{
		public:
			InformationWindow();
		};
	}
};