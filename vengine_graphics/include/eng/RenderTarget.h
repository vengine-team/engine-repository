#pragma once
#include "eng/D3D12_PresentBuffer.h"
#include "eng/D3D12_DeviceResource.h"


#include "eng/IDeviceTexture2.h"
#include "eng/DeviceMesh.h"
#include "eng/Material.h"

#include <shared_mutex>
#include <memory>

#include <ext/indexed_iterator.h>
#include <ext/range.h>

namespace eng
{
	template<typename vertex_t, typename overrides_iterator_t>
	struct mesh_material_arguments_iterator : public ext::indexed_iterator_base<mesh_material_arguments_iterator<vertex_t, overrides_iterator_t>, size_t, ptrdiff_t>
	{
		const eng::Material<vertex_t>* m_pMaterial;
		size_t m_render_pass_index;

		overrides_iterator_t m_nullOverrides_begin;
		size_t m_num_nullOverrideResources;

		const void* m_pShader_resource;

		const void* operator*() const
		{
			const eng::render_pass_binding<vertex_t>& currentPass_binding = m_pMaterial->signature().render_pass_bindings()[m_render_pass_index];

			size_t parameter_slot = currentPass_binding.parameter_bindings[this->m_position];
			const void* pShaderResource;
			if (m_pMaterial->signature().parameters()[this->m_position].tag < m_num_nullOverrideResources)
			{
				pShaderResource = *(m_nullOverrides_begin + parameter_slot);
			}
			else
			{
				pShaderResource = m_pMaterial->bound_resources()[parameter_slot];
			}

			assert(pShaderResource && "One or more Parameters are nullptr");

			return pShaderResource;
		}
		const void** const operator->() const
		{
			m_pShader_resource = **this;
			return &m_pShader_resource;
		}
	};

	template<typename vertex_t, typename overrides_iterator_t>
	struct mesh_material_arguments
	{
		mesh_material_arguments_iterator<vertex_t, overrides_iterator_t> m_begin;
		mesh_material_arguments_iterator<vertex_t, overrides_iterator_t> m_end;

		const mesh_material_arguments_iterator<vertex_t, overrides_iterator_t>& begin() const
		{
			return m_begin;
		}
		const mesh_material_arguments_iterator<vertex_t, overrides_iterator_t>& end() const
		{
			return m_end;
		}
	};

	template<typename vertex_t, typename overrides_iterator_t>
	struct mesh_material_iterator : public ext::indexed_iterator_base<mesh_material_iterator<vertex_t, overrides_iterator_t>, size_t, ptrdiff_t>
	{
		const eng::DeviceMesh<vertex_t>* m_pMesh;
		const eng::Material<vertex_t>* m_pMaterial;

		overrides_iterator_t m_nullOverrides_begin;
		size_t m_num_nullOverrideResources;

		struct dereferenced
		{
			friend struct mesh_material_iterator<vertex_t, overrides_iterator_t>;
			const mesh_material_iterator<vertex_t, overrides_iterator_t>& m_ref;
			std::array<size_t, 0> m_variable_argument_indices;

			dereferenced(const mesh_material_iterator<vertex_t, overrides_iterator_t>* p) : m_ref(*p)
			{

			}
		public:

			const eng::DeviceMesh<vertex_t>* mesh() const
			{
				return m_ref.m_pMesh;
			}
			constexpr bool variable_mesh() const
			{
				return false;
			}

			const eng::RenderPass<vertex_t>* render_pass() const
			{
				return m_ref.m_pMaterial->signature().render_pass_bindings()[m_ref.m_position].render_pass.get();
			}
			constexpr bool variable_render_pass() const
			{
				return true;
			}

			mesh_material_arguments<vertex_t, overrides_iterator_t> arguments() const
			{
				mesh_material_arguments<vertex_t, overrides_iterator_t> out;
				out.m_begin.m_pMaterial = m_ref.m_pMaterial;
				out.m_end.m_pMaterial = m_ref.m_pMaterial;

				out.m_begin.m_render_pass_index = m_ref.m_position;
				out.m_end.m_render_pass_index = m_ref.m_position;

				out.m_begin.m_nullOverrides_begin = m_ref.m_nullOverrides_begin;
				out.m_end.m_nullOverrides_begin = m_ref.m_nullOverrides_begin;

				out.m_begin.m_num_nullOverrideResources = m_ref.m_num_nullOverrideResources;
				out.m_end.m_num_nullOverrideResources = m_ref.m_num_nullOverrideResources;

				out.m_begin.m_position = 0;
				out.m_end.m_position = m_ref.m_pMaterial->signature().render_pass_bindings()[m_ref.m_position].parameter_bindings.size();

				return out;
			}

			std::array<size_t, 0> const& variable_argument_indices() const
			{
				return m_variable_argument_indices;
			}
		};

		struct p_dereferenced
		{
			dereferenced m_value;

			const dereferenced* operator->() const
			{
				return &m_value;
			}
		};

		dereferenced operator*() const
		{
			return dereferenced(this);
		}
		p_dereferenced operator->() const
		{
			return p_dereferenced{ this->operator*() };
		}
	};

	template<typename vertex_t, typename overrides_iterator_t>
	auto draw_mesh(const eng::DeviceMesh<vertex_t>* pMesh, const eng::Material<vertex_t>* pMaterial, overrides_iterator_t override_resources_begin, overrides_iterator_t override_resources_end)
	{
		mesh_material_iterator<vertex_t, overrides_iterator_t> begin = {};
		mesh_material_iterator<vertex_t, overrides_iterator_t> end = {};

		begin.m_pMesh = pMesh;
		end.m_pMesh = pMesh;

		begin.m_pMaterial = pMaterial;
		end.m_pMaterial = pMaterial;
		
		begin.m_nullOverrides_begin = override_resources_begin;
		begin.m_num_nullOverrideResources = override_resources_begin - override_resources_end;
		end.m_nullOverrides_begin = override_resources_begin;
		end.m_num_nullOverrideResources = override_resources_begin - override_resources_end;

		begin.m_position = 0;
		end.m_position = pMaterial->signature().render_pass_bindings().size();

		return ext::range(begin, end );
	}

	class RenderTarget : public IDeviceTexture2
	{
	private:

		mutable std::shared_mutex	m_mutex;

		struct frame_device_resources
		{
			UINT64												fenceValue = 0;

			Microsoft::WRL::ComPtr<ID3D12Resource>				pRenderTarget;

			Microsoft::WRL::ComPtr<ID3D12CommandAllocator>		pCommandAllocator;
			Microsoft::WRL::ComPtr<ID3D12CommandAllocator>		pBundleAllocator;
		};

		std::array<frame_device_resources, eng::D3D12::c_bufferCount>	m_frames;
		UINT															m_current_frame_i = 0;

		std::vector<std::shared_ptr<eng::D3D12::DeviceResource>>		m_current_frame_used_resources;

		gtm::vector2u	m_scale;
		gtm::vector2u	m_next_scale;

		gtm::vector2u	m_depthStencilSize;

		struct device_resources
		{

			Microsoft::WRL::ComPtr<IDXGISwapChain3>				pSwapChain = nullptr;
			Microsoft::WRL::ComPtr<ID3D12DescriptorHeap>		pRtvDescriptorHeap;
			Microsoft::WRL::ComPtr<ID3D12DescriptorHeap>		pDsvHeap;
			Microsoft::WRL::ComPtr<ID3D12Resource>				pDepthStencil;


			std::shared_ptr<D3D12::PresentBuffer>				pPresentBuffer = nullptr;
			std::shared_ptr<D3D12::PresentBuffer>				pNextPresentBuffer = nullptr;

			const void*											pCachedRenderPass = nullptr;
			Microsoft::WRL::ComPtr<ID3D12GraphicsCommandList>	pGraphicsCommandList = nullptr;
		};

		device_resources									m_deviceResources;

		D3D12_VIEWPORT										m_viewport;

		UINT												m_RtvDescriptorHandleSize = 0;

		void resizeSwapChain(gtm::vector2u size);
		void createRenderTargetViews(ID3D12Device* pDevice, ID3D12DescriptorHeap* pRtvHeap);
		static void createDepthStencil(ID3D12Device* pDevice, ID3D12Resource** ppResource, ID3D12DescriptorHeap* pDsvHeap, gtm::vector2u size);


		void initializeSizeIndependentResources();
		void initializeSizeDependentResourcesForHWND(HWND window, gtm::vector2u size);
		void initializeSizeDependentResources(gtm::vector2u size);


		void D3D12_beginRecord(ID3D12GraphicsCommandList* pGraphicsCommandList);
		void D3D12_endRecord(ID3D12GraphicsCommandList* pGraphicsCommandList);

		void D3D12_advance_and_present(UINT64 fence);

		template<typename vertex_t>
		void D3D12_bind_shader_resource(ID3D12GraphicsCommandList* pCommandList, const eng::RenderPass<vertex_t>* pRenderPass, size_t parameter_i, const void* pShaderResource)
		{

			switch (pRenderPass->_getParameters()[parameter_i].type)
			{
			case eng::shader_resource::device_buffer:
			{
				D3D12::device_buffer dx12ConstantBuffer = pRenderPass->_getParameters()[parameter_i].constantBufferParameter;

				eng::DeviceBuffer const * pConstantBuffer = reinterpret_cast<const eng::DeviceBuffer*>(pShaderResource);

				eng::D3D12::resource_gpu_descriptor constBuffer_descriptor;
				std::shared_ptr<eng::D3D12::DeviceResource> pDeviceOwnedConstantBuffer;
				std::tie(constBuffer_descriptor, pDeviceOwnedConstantBuffer) = pConstantBuffer->D3D12_ConstantBuffer_transaction();
				m_current_frame_used_resources.push_back(std::move(pDeviceOwnedConstantBuffer));

				pCommandList->SetDescriptorHeaps(1, &constBuffer_descriptor.pDescriptorHeap);
				pCommandList->SetGraphicsRootDescriptorTable(dx12ConstantBuffer.constantBufferDescriptor_rootParamIndex, constBuffer_descriptor.gpu_descriptor_handle);
			}
			break;
			case eng::shader_resource::device_texture:
			{
				D3D12::device_texture dx12_texture = pRenderPass->_getParameters()[parameter_i].textureParameter;

				eng::IDeviceTexture2 const * pTexture2 = reinterpret_cast<eng::IDeviceTexture2 const *>(pShaderResource);


				eng::D3D12::texture_gpu_descriptor texture_descriptor;
				std::shared_ptr<eng::D3D12::DeviceResource> pDeviceOwnedTextur2;
				std::tie(texture_descriptor, pDeviceOwnedTextur2) = pTexture2->D3D12_get_SRV_and_Sampler();
				m_current_frame_used_resources.push_back(std::move(pDeviceOwnedTextur2));


				pCommandList->SetDescriptorHeaps(1, &texture_descriptor.texture.pDescriptorHeap);
				pCommandList->SetGraphicsRootDescriptorTable(dx12_texture.shaderResourceView_rootParamIndex, texture_descriptor.texture.gpu_descriptor_handle);

				pCommandList->SetDescriptorHeaps(1, &texture_descriptor.sampler.pDescriptorHeap);
				pCommandList->SetGraphicsRootDescriptorTable(dx12_texture.sampler_rootParamIndex, texture_descriptor.sampler.gpu_descriptor_handle);
			}
			break;
			default:
				assert(0);
				break;
			}
		}

		template<typename vertex_t>
		void D3D12_bind_render_pass(ID3D12GraphicsCommandList* pCommandList, const eng::RenderPass<vertex_t>* pRenderPass)
		{
			pCommandList->SetPipelineState(pRenderPass->D3D12_RenderPass()->_getPipelineState());
			pCommandList->SetGraphicsRootSignature(pRenderPass->D3D12_RenderPass()->_getRootSignature());
			m_current_frame_used_resources.push_back(pRenderPass->D3D12_RenderPass());
		}

	public:

		virtual std::tuple<D3D12::texture_gpu_descriptor, std::shared_ptr<eng::D3D12::DeviceResource>> D3D12_get_SRV_and_Sampler() const override;

		static RenderTarget * WINAPI_make_renderTarget_for_HWND(HWND window, gtm::vector2u size);

		virtual gtm::vector2u get_scale() const override;
		void set_next_scale(gtm::vector2u scale);
		gtm::vector2u get_next_scale() const;

		template<typename it_t>
		void draw(it_t begin, it_t end)
		{
			if (begin == end)
			{
				return;
			}

			ID3D12GraphicsCommandList* pCommandList = m_deviceResources.pGraphicsCommandList.Get();

			const bool variable_mesh = begin->variable_mesh();
			const bool variable_render_pass = begin->variable_render_pass();

			auto pCurrentMesh = begin->mesh();
			pCommandList->IASetVertexBuffers(0, 1, &pCurrentMesh->D3D12_getMesh()->m_vertexBufferView);
			m_current_frame_used_resources.push_back(pCurrentMesh->D3D12_getMesh());

			D3D12_bind_render_pass(pCommandList, begin->render_pass());

			for (auto arg_i = begin->arguments().begin(); arg_i != begin->arguments().end(); arg_i++)
			{
				D3D12_bind_shader_resource(pCommandList, begin->render_pass(), arg_i - begin->arguments().begin(), *arg_i);
			}


			//first draw call
			{
				//drawing front if not culled
				if (begin->render_pass()->_getCullMode() != eng::D3D12::cull_mode::front)
				{
					pCommandList->IASetIndexBuffer(&pCurrentMesh->D3D12_getMesh()->m_indexBufferView);
					pCommandList->DrawIndexedInstanced(static_cast<UINT>(pCurrentMesh->D3D12_getMesh()->m_indexCount), 1, 0, 0, 0);
				}
				//drawing back if not culled
				if (begin->render_pass()->_getCullMode() != eng::D3D12::cull_mode::back)
				{
					pCommandList->IASetIndexBuffer(&pCurrentMesh->D3D12_getMesh()->m_reverseIndexBufferView);
					pCommandList->DrawIndexedInstanced(static_cast<UINT>(pCurrentMesh->D3D12_getMesh()->m_indexCount), 1, 0, 0, 0);
				}
			}

			for (auto i = begin + 1; i != end; i++)
			{
				if (variable_mesh)
				{
					pCurrentMesh = i->mesh();
					pCommandList->IASetVertexBuffers(0, 1, &pCurrentMesh->D3D12_getMesh()->m_vertexBufferView);
					m_current_frame_used_resources.push_back(pCurrentMesh->D3D12_getMesh());
				}

				if (variable_render_pass)
				{
					D3D12_bind_render_pass(pCommandList, i->render_pass());

					for (auto arg_i = i->arguments().begin(); arg_i != i->arguments().end(); arg_i++)
					{
						D3D12_bind_shader_resource(pCommandList, i->render_pass(), arg_i - i->arguments().begin(), *arg_i);
					}
				}
				else
				{
					for (auto var_arg_indicies_i = i->variable_argument_indices().begin(); var_arg_indicies_i != i->variable_argument_indices().end(); var_arg_indicies_i++)
					{
						size_t arg_i = *var_arg_indicies_i;

						D3D12_bind_shader_resource(pCommandList, i->render_pass(), arg_i, *(i->arguments().begin() + arg_i));
					}
				}


				//Drawing
				{
					//drawing front if not culled
					if (i->render_pass()->_getCullMode() != eng::D3D12::cull_mode::front)
					{
						pCommandList->IASetIndexBuffer(&pCurrentMesh->D3D12_getMesh()->m_indexBufferView);
						pCommandList->DrawIndexedInstanced(static_cast<UINT>(pCurrentMesh->D3D12_getMesh()->m_indexCount), 1, 0, 0, 0);
					}
					//drawing back if not culled
					if (i->render_pass()->_getCullMode() != eng::D3D12::cull_mode::back)
					{
						pCommandList->IASetIndexBuffer(&pCurrentMesh->D3D12_getMesh()->m_reverseIndexBufferView);
						pCommandList->DrawIndexedInstanced(static_cast<UINT>(pCurrentMesh->D3D12_getMesh()->m_indexCount), 1, 0, 0, 0);
					}
				}
			}
		}

		virtual void begin_recording();
		virtual void end_recording();

	private:
		RenderTarget(HWND window, gtm::vector2u size);
	public:
		RenderTarget(gtm::vector2u size);
		virtual ~RenderTarget();
	};
}