#pragma once
#include <string>

#include <fstream>
#include <vector>
#include <map>

#include <eng/color.h>
#include <gtm/math.h>

#include <eng/Bitmap2.h>
#include <eng/DeviceTexture2.h>

namespace eng
{
	enum class Text_Alignment
	{
		Left,
		Center,
		Right,
	};

	class Font
	{
	public:
		struct glyph
		{
			gtm::vector2f		uv_top_left;
			gtm::vector2f		uv_bottom_right;

			gtm::vector2i		bearing;
			gtm::vector2i		scale;
			int					advance;
		};
	private:

		std::map<wchar_t, glyph> m_charMap;

		unsigned int	m_heightInPixels = 0;
		int				m_ascenderInPixels = 0;

		std::unique_ptr<DeviceTexture2>  m_fontTexture;

#pragma region freeTypeInterfaces

#pragma endregion

	public:

		const eng::DeviceTexture2* fontTexture() const;

		unsigned int getHeightInPixels() const;

		glyph getGlyphData(wchar_t c) const;

		Bitmap2 render_string(std::wstring string) const;

		Font(std::wstring filePath, unsigned int heightInPixels);
		Font(const Font& rValue) = delete;
		Font(Font&& rValue) = delete;
		virtual ~Font() noexcept;
	};

	unsigned int calculate_rendered_length(const std::wstring& text, const eng::Font* pFont);
}