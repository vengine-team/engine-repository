#pragma once
#include <string>

std::wstring string_to_wstring(const std::string& s);
std::string wstring_to_string(const std::wstring& s);
