#pragma once
#include <string>
#include <memory>

#include <eng/IDeviceTexture2.h>
#include <eng/D3D12_DeviceTexture2.h>

namespace eng
{
	class DeviceTexture2 : public IDeviceTexture2
	{
	private:

		eng::Bitmap2 m_bitmap;
		
		std::shared_ptr<eng::D3D12::DeviceTexture2> m_pD3D12_DeviceTexture2 = nullptr;

	public:

#ifdef _DIRECTX
		virtual std::tuple<eng::D3D12::texture_gpu_descriptor, std::shared_ptr<eng::D3D12::DeviceResource>> D3D12_get_SRV_and_Sampler() const override;
#endif // _DIRECTX

		gtm::vector2u get_scale() const;

		const Bitmap2& bitmap() const;

		DeviceTexture2(const eng::Bitmap2& bitmap);
		DeviceTexture2(eng::Bitmap2&& bitmap);
		virtual ~DeviceTexture2();
	};
}