#pragma once
#include <vector>
#include <gtm/math.h>
#include <d3d12.h>
#include <array>

namespace eng
{
	using index_type =  unsigned int;
	
	using line_indices = std::array<index_type, 2> ;

	using triangle_indices = std::array<index_type, 3>;

	template<typename vertex_type>
	class Mesh
	{
		std::vector<triangle_indices> m_indices;
		std::vector<vertex_type> m_vertices;

	public:
		Mesh()
		{

		}
		Mesh(const Mesh<vertex_type>& source) :
			m_indices(source.m_indices),
			m_vertices(source.m_vertices)
		{

		}
		Mesh(Mesh<vertex_type>&& source) : 
			m_indices(std::move(source.m_indices)), 
			m_vertices(std::move(source.m_vertices))
		{

		}

		Mesh<vertex_type>& operator=(const Mesh<vertex_type>& source)
		{
			m_indices = source.m_indices;
			m_vertices = source.m_vertices;
			return *this;
		}
		Mesh<vertex_type>& operator=(Mesh<vertex_type>&& source)
		{
			m_indices = std::move(source.m_indices);
			m_vertices = std::move(source.m_vertices);
			return *this;
		}

		const std::vector<triangle_indices>& triangles() const
		{
			return m_indices;
		}
		std::vector<triangle_indices>& triangles()
		{
			return m_indices;
		}
		
		const std::vector<vertex_type>& vertices() const
		{
			return m_vertices;
		}
		std::vector<vertex_type>& vertices()
		{
			return m_vertices;
		}
	};
};