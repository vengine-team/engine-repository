#pragma once
#include "MaterialSignature.h"
#include <vector>
#include <limits>

#include <eng/DeviceBuffer.h>
#include <eng/IDeviceTexture2.h>

namespace eng
{

	template<typename vertex_type>
	class Material : public RenderLayer
	{
	private:

		std::vector<void const*> m_bound_resources;

		const MaterialSignature<vertex_type> m_materialSignature;

		void setParameterResourcePointer(size_t parameterIndex, void const* pResource)
		{
			m_bound_resources[parameterIndex] = pResource;
		}

	public:

		const std::vector<const void*>& bound_resources() const
		{
			return m_bound_resources;
		}

		void setConstBufferShaderResource(size_t parameterIndex, eng::DeviceBuffer const* pConstantBuffer)
		{
			setParameterResourcePointer(parameterIndex, reinterpret_cast<const void*>(pConstantBuffer));
		}
		void setTextureShaderResource(size_t parameterIndex, IDeviceTexture2 const* pTexture)
		{
			setParameterResourcePointer(parameterIndex, reinterpret_cast<const void*>(pTexture));
		}

		MaterialSignature<vertex_type> const & signature() const
		{
			return m_materialSignature;
		}

		Material(MaterialSignature<vertex_type> const & signature) : m_materialSignature(signature), eng::RenderLayer(0)
		{
			m_bound_resources.resize(m_materialSignature.parameters().size(), nullptr);
		}
		virtual ~Material()
		{

		}
	};
}
