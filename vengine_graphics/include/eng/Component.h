#pragma once
#include "Event_Node.h"
#include <assert.h>

namespace eng
{
	template<typename SType, typename NType, typename CType>
	class Component
	{
	private:
		NType* m_node;
	public:

		NType* node();
		NType const * node() const;

		Component(NType* node);
		virtual ~Component();
	};

	template<typename SType, typename NType, typename CType>
	inline NType * Component<SType, NType, CType>::node()
	{
		return m_node;
	}

	template<typename SType, typename NType, typename CType>
	inline NType const * Component<SType, NType, CType>::node() const
	{
		return m_node;
	}

	template<typename SType, typename NType, typename CType>
	inline Component<SType, NType, CType>::Component(NType * node)
	{
		//argument cannot be null
		assert(m_node);

		m_node = node;
		static_cast<SNCNode<SType, NType, CType>*>(node)->m_pComponents.push_back(this);
	}
	template<typename SType, typename NType, typename CType>
	inline Component<SType, NType, CType>::~Component()
	{
		for (auto i = static_cast<SNCNode<SType, NType, CType>*>(m_node)->m_pComponents.begin(); i != static_cast<SNCNode<SType, NType, CType>*>(m_node)->m_pComponents.end(); i++)
		{
			if (*i == this)
			{
				static_cast<SNCNode<SType, NType, CType>*>(m_node)->m_pComponents.erase(i);
				return;
			}
		}

		//undefined behavior: "this component tried to unregister, but never registered at m_node"
		assert(0);
	}
};