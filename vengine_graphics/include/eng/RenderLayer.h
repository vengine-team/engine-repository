#pragma once
#include <ext/ticket_table.h>

namespace eng
{
	class RenderLayer
	{
		//friend class RenderLayerQueue;

		unsigned int m_stepIndex;

		//std::vector<eng::ticket_table<std::pair<eng::IRenderer3D*, size_t>>*> m_pData;

	public:

		RenderLayer(unsigned int queueStep);
		virtual ~RenderLayer();

		unsigned int getQueueStep() const;
		void setQueueStep(unsigned int queueStep);
	};
}
