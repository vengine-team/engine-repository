#pragma once
#include <eng/D3D12_RenderPass.h>


namespace eng
{
	template<typename vertex_type>
	class RenderPass
	{
#ifdef _DIRECTX
	private:
		std::shared_ptr<eng::D3D12::RenderPass<vertex_type>> m_pD3D12RenderPass;
		
		std::vector<render_pass_parameter> m_parameters;

		eng::D3D12::cull_mode m_cullMode;

	public:

		std::shared_ptr<eng::D3D12::RenderPass<vertex_type>> const & D3D12_RenderPass() const
		{
			return m_pD3D12RenderPass;
		}

		const auto& _getParameters() const
		{
			return m_parameters;
		}

		D3D12::cull_mode _getCullMode() const
		{
			return m_cullMode;
		}

#endif // _DIRECTX
	public:

		RenderPass(const D3D12::RenderPass_Desc& desc)
		{
			m_pD3D12RenderPass = std::make_shared<eng::D3D12::RenderPass<vertex_type>>(desc);

			m_parameters.resize(desc.numParameters);
			for (size_t i = 0; i < desc.numParameters; i++)
			{
				m_parameters[i] = desc.pParameters[i];
			}

			m_cullMode = desc.cullMode;
		}
	};
}