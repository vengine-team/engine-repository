#pragma once

namespace eng
{
	class ShaderResource
	{
	public:
		virtual ~ShaderResource() = default;
	};

	enum class shader_resource
	{
		device_buffer,
		device_texture
	};
};