#pragma once
#include "defaultDraw.h"
#include "Font.h"

namespace eng
{
	eng::Mesh3f generate_text_mesh3F(const std::wstring& text, eng::Font const * pFont, float scale, float center);
	eng::Mesh2F generate_text_mesh2F(const std::wstring& text, eng::Font const * pFont, float scale, float center);

	eng::Mesh<eng::vertex3f> generate_cubeSphere_mesh3F(size_t detail, float radius);
}