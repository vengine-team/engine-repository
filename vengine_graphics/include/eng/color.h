#pragma once

namespace eng
{
	struct color
	{
		using channel_t = unsigned char;

		channel_t r;
		channel_t g;
		channel_t b;
		channel_t a;
	};
};