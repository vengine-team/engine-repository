#pragma once
int engMain();


int main()
{
	return engMain();
}


#ifdef _WINDOWS

#include "engineWindows.h"
 #include <Windows.h>

int CALLBACK WinMain(_In_ HINSTANCE hInstance, _In_ HINSTANCE hPrevInstance, _In_ LPSTR lpCmdLine, _In_ int nCmdShow)
{
	eng::windows::engineFromWinMain(hInstance, hPrevInstance, lpCmdLine, nCmdShow);
	
	//platform independent entry point
	return engMain();
}

#endif // _WINDOWS