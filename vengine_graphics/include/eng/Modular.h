#pragma once
#include <ext/ticket_table.h>
#include <shared_mutex>
#include <mutex>
#include <cstdlib>
#include <limits>
#include <tuple>

namespace eng
{
	template<typename ModuleT>
	struct module_key
	{
	public:
		size_t m_number;
		
		module_key()
		{

		}
		module_key& operator=(const module_key<ModuleT>& source) = delete;
		module_key& operator=(module_key<ModuleT>&& source) = delete;
		module_key(const module_key<ModuleT>& source) = delete;
		module_key(module_key<ModuleT>&& source) = delete;
		virtual ~module_key()
		{

		}
	};

	template<typename Target, typename ModuleInterfaceT>
	class Modular;

	template<typename modular_t>
	class module_base
	{
		template<typename target_t, typename module_interface_t>
		friend class Modular;
		modular_t* m_pModular = nullptr;
	public:
		modular_t& modular()
		{
			//cannot call modular() during constructor or destructor
			assert(m_pModular);
			return *m_pModular;
		}
		const modular_t& modular() const
		{			
			//cannot call modular() during constructor or destructor
			assert(m_pModular);
			return *m_pModular;
		}
		virtual ~module_base()
		{

		}
	};

	template<typename module_interface_t>
	class IModuleFactory
	{
	public:
		virtual std::pair<void*, std::unique_ptr<module_interface_t>> construct_module() const = 0;
		virtual ~IModuleFactory()
		{

		}
	};

	template<typename module_interface_t, typename module_t>
	class ModuleFactory : public IModuleFactory<module_interface_t>
	{
	public:
		virtual std::pair<void*, std::unique_ptr<module_interface_t>> construct_module() const override
		{
			auto pModule = std::make_unique<module_t>();
			return { reinterpret_cast<void*>(pModule.get()), std::move(pModule) };
		}
		virtual ~ModuleFactory()
		{

		}
	};

	template<typename Target, typename ModuleInterfaceT>
	class Modular
	{
	public:
		using module_interface_t = ModuleInterfaceT;
	private:

		struct static_data
		{
			std::shared_mutex shared_mutex;

			std::vector<std::pair<std::unique_ptr<IModuleFactory<module_interface_t>>, size_t*>> module_factories;
			ext::ticket_table<Modular<Target, module_interface_t>*> all_of_this_type;
		};

		//static
		static static_data& s_data()
		{
			static static_data data;
			return data;
		}

		//non static members
		static_data* const m_p_static_data = &s_data();
		ext::ticket m_all_of_this_type_ticket;
		std::vector<std::pair<void*, std::unique_ptr<module_interface_t>>> m_module_instances;

	protected:

		template<typename...P>
		void callModuleInterfaceFunction(void(module_interface_t::*pFunction)(P...), P... params)
		{
			std::shared_lock<std::shared_mutex> lock(m_p_static_data->shared_mutex);

			for (size_t i = 0; i < m_module_instances.size(); i++)
			{
				(m_module_instances[i].second.get()->*pFunction)(params...);
			}
		}

	public:

		Modular()
		{
			std::scoped_lock<std::shared_mutex> lock(m_p_static_data->shared_mutex);

			m_p_static_data->all_of_this_type.insert({ this, m_all_of_this_type_ticket });

			m_module_instances.resize(m_p_static_data->module_factories.size());
			for (size_t i = 0; i < m_p_static_data->module_factories.size(); i++)
			{
				m_module_instances[i] = m_p_static_data->module_factories[i].first->construct_module();
				m_module_instances[i].second->m_pModular = static_cast<Target*>(this);
			}
		}

		virtual ~Modular()
		{
			std::scoped_lock<std::shared_mutex> lock(m_p_static_data->shared_mutex);

			m_p_static_data->all_of_this_type.erase(m_all_of_this_type_ticket);

			for (auto i = m_module_instances.begin(); i != m_module_instances.end(); i++)
			{
				m_module_instances.back().second->m_pModular = nullptr;
			}
		}

		template<typename ModuleT>
		static void addModule(module_key<ModuleT>& key)
		{
			static_data* p_static_data = &s_data();
			std::scoped_lock<std::shared_mutex> lock(p_static_data->shared_mutex);

			key.m_number = p_static_data->module_factories.size();

			p_static_data->module_factories.push_back({  });
			p_static_data->module_factories.back().first = std::unique_ptr<ModuleFactory<module_interface_t, ModuleT>>( new ModuleFactory<module_interface_t, ModuleT>() );
			p_static_data->module_factories.back().second = &key.m_number;


			for (auto i = p_static_data->all_of_this_type.begin(); i != p_static_data->all_of_this_type.end(); i++)
			{
				(*i)->m_module_instances.push_back({});
				(*i)->m_module_instances.back() = p_static_data->module_factories.back().first->construct_module();
				(*i)->m_module_instances.back().second->m_pModular = static_cast<Target*>(*i);
			}
		}

		template<typename ModuleT>
		static void removeModule(module_key<ModuleT>& key)
		{
			static_data* p_static_data = &s_data();
			std::scoped_lock<std::shared_mutex> lock(p_static_data->shared_mutex);

			p_static_data->module_factories[key.m_number] = std::move(p_static_data->module_factories.back());
			*p_static_data->module_factories[key.m_number].second = key.m_number;
			p_static_data->module_factories.pop_back();

			for (auto i = p_static_data->all_of_this_type.begin(); i != p_static_data->all_of_this_type.end(); i++)
			{
				(*i)->m_module_instances[key.m_number] = std::move((*i)->m_module_instances.back());
				(*i)->m_module_instances.back().second->m_pModular = nullptr;
				(*i)->m_module_instances.pop_back();
			}

			key.m_number = (std::numeric_limits<size_t>::max)();
		}

		template<typename module_t>
		module_t* getModule(const module_key<module_t>& key)
		{
			std::shared_lock<std::shared_mutex> lock(m_p_static_data->shared_mutex);
			module_t* out = reinterpret_cast<module_t*>(m_module_instances[key.m_number].first);
			return out;
		}

		template<typename module_t>
		const module_t* getModule(const module_key<module_t>& key) const
		{
			std::shared_lock<std::shared_mutex> lock(m_p_static_data->shared_mutex);
			module_t* out = reinterpret_cast<module_t*>(m_module_instances[key.m_number].first);
			return out;
		}
	};

	template<typename modular_t, typename module_t>
	class module_guard
	{
	private:
		module_key<module_t>* m_pModuleKey;
	public:

		module_guard(module_key<module_t>& key)
		{
			m_pModuleKey = &key;
			modular_t::addModule(key);
		}

		virtual ~module_guard()
		{
			modular_t::removeModule(*m_pModuleKey);
		}
	};

	template<typename target_t, typename modular_t>
	class make_module : public modular_t::module_interface_t
	{
		static eng::module_key<target_t>& get_module_key()
		{
			static eng::module_key<target_t> s_moduleKey;
			static eng::module_guard<modular_t, target_t> s_moduleGuard = { s_moduleKey };
			return s_moduleKey;
		}
	public:
		static target_t& get(modular_t& modular)
		{
			return *modular.getModule(get_module_key());
		}
	};
};