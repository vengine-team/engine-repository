#pragma once
#include <eng/IioFrame.h>
#include <eng/engineWindows.h>
#include <ext/event.h>

#include <par/task_queue.h>

#include <gtm/math.h>

#include <memory>
#include <array>
#include <algorithm>

namespace eng
{
	namespace WindowStyle
	{
		enum Type
		{
			SystemDefaultMenu,
			SystemDefault,
			Borderless,
		};
	};

	namespace windows
	{
		class MessageLoopThread
		{
			bool m_initialized = false;
			std::thread m_thread;
			std::atomic<bool> m_cancelThread;

			eng::TaskQueue mt_taskQueue;

			void thread_function();

		public:

			void initialize();

			TaskQueue& task_queue();

			MessageLoopThread();
			~MessageLoopThread();
		};
	}

	class Window : public IioFrame 
	{
	private:

		ext::event<Window&> m_request_close_event;

		gtm::vector2u m_minimumSize = { 200, 200 };
		gtm::vector2u m_maximumSize = { 4096, 4096 };


		std::unique_ptr<eng::RenderTarget> m_pHwndRenderTarget;

		bool m_initialized = false;

		//these properties will be used to create the window on begin
		std::wstring m_windowTitle = L"title_missing";
		
		gtm::vector2i m_windowPosition = { 300, 300 };
		gtm::vector2u m_clientScale = { 100, 100 };

		WindowStyle::Type m_windowStyle = WindowStyle::SystemDefault;

		bool m_sizeAble = false;

#ifdef _WINDOWS

		windows::MessageLoopThread m_mlt;

		static LRESULT WndProc_Ptr_Callback(HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam);
		LRESULT wndProc(HWND hwnd, UINT message,WPARAM wParam, LPARAM lParam);

		HWND m_hwnd;

		//these functions are only used to send information from eng::Window to the Os
		void mlt_createWindow();
		void mlt_initialize();
		void mlt_close();
		void mlt_setTitle(std::wstring title);

		void mlt_destroyWindow();

		void mlt_setClientSize(gtm::vector2u size);


		std::mutex m_fetchMutex;

		bool m_mlt_request_close = false;

		gtm::vector2u m_mlt_clientScale;
#endif
		eng::FrameInput_Desc m_input_desc = eng::FrameInput_Desc();
		eng::Input m_input = eng::Input(eng::FrameInput_Desc{});

	public:

		ext::event_subscribe_access<Window&> request_close_event();

		bool is_open();
		void open();
		void close();

		virtual void setTitle(const std::wstring& title);
		std::wstring getTitle() const;

		virtual WindowStyle::Type getStyle() const;

		virtual bool getSizeable() const;

		virtual const gtm::vector2u& getClientScale() const override;

		virtual const gtm::vector2i& getWindowPosition() const;

		virtual eng::RenderTarget& renderTarget();
		virtual const eng::RenderTarget& renderTarget() const;

		virtual const eng::Input& input() const;

		virtual void fetch() override;


		Window(std::wstring title, gtm::vector2i pos, gtm::vector2u size, eng::WindowStyle::Type style, bool sizeAble);
		virtual ~Window();
	};

};

