#pragma once
#include <vector>
#include <assert.h>

namespace eng
{
	template<typename _Child_t>
	class Forward_Node;

	template<typename _Child_t>
	class Forward_Node_Children
	{
		using container_t = std::vector<_Child_t*>;
		container_t m_container;
		Forward_Node<_Child_t>& m_parent_ref;

	public:

		Forward_Node_Children(Forward_Node<_Child_t>& parent_ref) : m_parent_ref(parent_ref)
		{

		}

		using const_iterator = typename container_t::const_iterator;
		const_iterator begin() const
		{
			return m_container.begin();
		}
		const_iterator end() const
		{
			return m_container.end();
		}

		const_iterator cbegin() const
		{
			return m_container.cbegin();
		}
		const_iterator cend() const
		{
			return m_container.cend();
		}

		using const_reverse_iterator = typename container_t::const_reverse_iterator;
		const_reverse_iterator rbegin() const
		{
			return m_container.rbegin();
		}
		const_reverse_iterator rend() const
		{
			return m_container.rend();
		}

		const_reverse_iterator crbegin() const
		{
			return m_container.crbegin();
		}
		const_reverse_iterator crend() const
		{
			return m_container.crend();
		}

		size_t size() const
		{
			return m_container.size();
		}

		bool empty() const
		{
			return m_container.empty();
		}

		void push_back(_Child_t* child_ref);

		const_iterator erase(const_iterator& It);

		void pop_back();
	};

	template<typename _Child_t>
	class Forward_Node
	{
	private:

		friend class Forward_Node_Children<_Child_t>;

		using child_container_t = Forward_Node_Children<_Child_t>;

		child_container_t m_children = { *this };

	protected:

		using Child_t = _Child_t;

		const child_container_t& children() const
		{
			return m_children;
		}
		child_container_t& children()
		{
			return m_children;
		}

		virtual void on_child_added(_Child_t* pChild)
		{

		}

		virtual void on_child_removed(_Child_t* pChild)
		{

		}

		Forward_Node()
		{

		}
		virtual ~Forward_Node()
		{

		}
	};


	template<typename _Child_t>
	inline void Forward_Node_Children<_Child_t>::push_back(_Child_t * pChild)
	{
		m_container.push_back(pChild);
		m_parent_ref.on_child_added(pChild);
	}
	template<typename _Child_t>
	inline typename Forward_Node_Children<_Child_t>::const_iterator Forward_Node_Children<_Child_t>::erase(const_iterator & It)
	{
		_Child_t * pChild = *It;
		auto out = m_container.erase(It);
		m_parent_ref.on_child_added(pChild);
		return out;
	}
	template<typename _Child_t>
	inline void Forward_Node_Children<_Child_t>::pop_back()
	{
		_Child_t * pChild = m_container.back();
		m_container.pop_back();
		m_parent_ref.on_child_added(pChild);
	}
};