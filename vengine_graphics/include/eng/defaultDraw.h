#pragma once
#include "RenderTarget.h"

#include "DeviceMesh.h"
#include "Material.h"

#include <array>

#include <gtm/math.h>

namespace eng
{
	template<typename _scalar_t>
	struct vertex3
	{
		using scalar_t = _scalar_t;

		gtm::vector<scalar_t, 3> position;
		gtm::vector<scalar_t, 3> normal;
		gtm::vector<scalar_t, 3> extrant;
		std::array<gtm::vector<scalar_t, 2>, 4> uv;
	private:
		static std::array<D3D12_INPUT_ELEMENT_DESC, 7> s_inputLayout;
	public:
		static D3D12_INPUT_LAYOUT_DESC D3D12_inputLayoutDesc()
		{
			return { s_inputLayout.data() , static_cast<UINT>(s_inputLayout.size()) };
		}
	};

	template<typename scalar_t>
	std::array<D3D12_INPUT_ELEMENT_DESC, 7> vertex3<scalar_t>::s_inputLayout =
	{
		D3D12_INPUT_ELEMENT_DESC{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0 , D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 },
		D3D12_INPUT_ELEMENT_DESC{ "NORMAL"	, 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 12, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 },
		D3D12_INPUT_ELEMENT_DESC{ "EXTRANT"	, 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 24, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 },
		D3D12_INPUT_ELEMENT_DESC{ "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT	, 0, 36, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 },
		D3D12_INPUT_ELEMENT_DESC{ "TEXCOORD", 1, DXGI_FORMAT_R32G32_FLOAT	, 0, 44, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 },
		D3D12_INPUT_ELEMENT_DESC{ "TEXCOORD", 2, DXGI_FORMAT_R32G32_FLOAT	, 0, 52, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 },
		D3D12_INPUT_ELEMENT_DESC{ "TEXCOORD", 3, DXGI_FORMAT_R32G32_FLOAT	, 0, 60, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 }
	};

	template<typename scalar_t, typename lerp_t>
	eng::vertex3<scalar_t> lerp(const eng::vertex3<scalar_t>& a, const eng::vertex3<scalar_t>& b, lerp_t t)
	{
		eng::vertex3<scalar_t> out;
		out.position = lerp(a.position, b.position, t);
		out.normal = lerp(a.position, b.position, t);
		out.extrant = lerp(a.position, b.position, t);
		for (size_t i = 0; i < out.uv.size(); i++)
		{
			out.uv[i] = gtm::lerp(a.uv[i], b.uv[i], t);
		}

		return out;
	}

	using vertex3f = vertex3<float>;

	using RenderPass3f = eng::RenderPass<vertex3f>;
	using MaterialSignature3f = eng::MaterialSignature<vertex3f>;
	using Material3f = eng::Material<vertex3f>;
	using Mesh3f = eng::Mesh<eng::vertex3f>;
	using DeviceMesh3f = eng::DeviceMesh<eng::vertex3f>;

	template<typename scalar_t>
	void recalculateNormals(eng::Mesh<eng::vertex3<scalar_t>>& mesh)
	{
		for (size_t vertex_i = 0; vertex_i < mesh.vertices().size(); vertex_i++)
		{
			mesh.vertices()[vertex_i].normal = { 0,0,0 };
		}

		for (size_t indices_i = 0; indices_i < mesh.triangles().size(); indices_i++)
		{
			size_t a_i = static_cast<size_t>(mesh.triangles()[indices_i][0]);
			size_t b_i = static_cast<size_t>(mesh.triangles()[indices_i][1]);
			size_t c_i = static_cast<size_t>(mesh.triangles()[indices_i][2]);

			gtm::vector<scalar_t, 3> a = mesh.vertices()[a_i].position;
			gtm::vector<scalar_t, 3> b = mesh.vertices()[b_i].position;
			gtm::vector<scalar_t, 3> c = mesh.vertices()[c_i].position;

			gtm::vector<scalar_t, 3> a_minus_c = a - c;
			gtm::vector<scalar_t, 3> a_minus_b = a - b;
			float angle_a = angle(a_minus_b, a_minus_c);

			gtm::vector<scalar_t, 3> c_minus_b = c - b;
			float angle_b = angle(a_minus_b, c_minus_b);
			float angle_c = angle(a_minus_c, c_minus_b);

			//FIX this^^

			gtm::vector<scalar_t, 3> normal = gtm::cross(a_minus_c, a_minus_b);

			mesh.vertices()[a_i].normal += normal * angle_a;
			mesh.vertices()[b_i].normal += normal * angle_b;
			mesh.vertices()[c_i].normal += normal * angle_c;
		}

		for (size_t vertex_i = 0; vertex_i < mesh.vertices().size(); vertex_i++)
		{
			mesh.vertices()[vertex_i].normal = normalize(mesh.vertices()[vertex_i].normal);
		}
	}

	template<typename _scalar_t>
	struct vertex2
	{
		using scalar_t = _scalar_t;

		gtm::vector<scalar_t, 2> position;
		std::array<gtm::vector<scalar_t, 2>, 4> uv;
	private:
		static std::array<D3D12_INPUT_ELEMENT_DESC, 5> s_inputLayout;
	public:
		static D3D12_INPUT_LAYOUT_DESC D3D12_inputLayoutDesc()
		{
			return { s_inputLayout.data() , static_cast<UINT>(s_inputLayout.size()) };
		}
	};

	template<typename scalar_t>
	std::array<D3D12_INPUT_ELEMENT_DESC, 5> vertex2<scalar_t>::s_inputLayout =
	{
		D3D12_INPUT_ELEMENT_DESC{ "POSITION", 0, DXGI_FORMAT_R32G32_FLOAT	, 0, 0	, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 },
		D3D12_INPUT_ELEMENT_DESC{ "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT	, 0, 8	, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 },
		D3D12_INPUT_ELEMENT_DESC{ "TEXCOORD", 1, DXGI_FORMAT_R32G32_FLOAT	, 0, 16	, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 },
		D3D12_INPUT_ELEMENT_DESC{ "TEXCOORD", 2, DXGI_FORMAT_R32G32_FLOAT	, 0, 24	, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 },
		D3D12_INPUT_ELEMENT_DESC{ "TEXCOORD", 3, DXGI_FORMAT_R32G32_FLOAT	, 0, 32	, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 }
	};

	using vertex2f = eng::vertex2<float>;

	using RenderPass2F = eng::RenderPass<vertex2f>;
	using MaterialSignature2F = eng::MaterialSignature<vertex2f>;
	using Material2F = eng::Material<vertex2f>;
	using Mesh2F = eng::Mesh<eng::vertex2f>;
	using DeviceMesh2F = eng::DeviceMesh<eng::vertex2f>;
}