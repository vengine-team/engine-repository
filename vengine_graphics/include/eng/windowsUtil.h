#pragma once
#include <Windows.h>
#include <string>

void AssertIfFailed(HRESULT hr, std::string message);