#pragma once
#include <assert.h>
#include <algorithm>
#include <vector>

namespace eng
{
	template<typename _Parent_t>
	class Basic_Child;

	template<typename _Child_t>
	class Basic_Parent
	{
	public:

		using Child_t = _Child_t;

	private:

		std::vector<Child_t*> m_pChildren;

	public:

		size_t child_count() const
		{
			return m_pChildren.size();
		}

		void push_back_child(Child_t* pChild)
		{
			pChild->m_pParent = static_cast<typename Child_t::Parent_t*>(this);
			m_pChildren.push_back(pChild);
			pChild->parent_set(pChild->m_pParent);
		}

		void pop_back_child()
		{
			Child_t* pRemoved = m_pChildren.back();
			m_pChildren.pop_back();
			pRemoved->parent_set(nullptr);
		}

		const std::vector<Child_t*>& getChildren() const
		{
			return m_pChildren;
		}

		Basic_Parent()
		{

		}
		Basic_Parent(const Basic_Parent<Child_t>& source) = delete;
		Basic_Parent(Basic_Parent<Child_t>&& source) = delete;
		Basic_Parent<Child_t>& operator=(const Basic_Parent<Child_t>& source) = delete;
		Basic_Parent<Child_t>& operator=(Basic_Parent<Child_t>&& source) = delete;
		virtual ~Basic_Parent()
		{
			assert(m_pChildren.empty());
		}
	};

	template<typename _Parent_t>
	class Basic_Child
	{
	public:
		using Parent_t = _Parent_t;

		template<typename _any> 
		friend class Basic_Parent;
	
	private:
		const Parent_t * m_pParent = nullptr;
	public:

		virtual void parent_set(const Parent_t* pCurrentParent)
		{

		}

		virtual void parent_removed(const Parent_t* pPreviousParent)
		{

		}

		const Parent_t* parent() const
		{
			return m_pParent;
		}

		Basic_Child()
		{

		}
		Basic_Child(const Basic_Child<Parent_t>& source) = delete;
		Basic_Child(Basic_Child<Parent_t>&& source) = delete;
		Basic_Child<Parent_t>& operator=(const Basic_Child<Parent_t>& source) = delete;
		Basic_Child<Parent_t>& operator=(Basic_Child<Parent_t>&& source) = delete;
		virtual ~Basic_Child() noexcept
		{
			assert(m_pParent == nullptr);
		}
	};
}