#pragma once
#include <gtm/math.h>
#include "RenderTarget.h"
#include "Input.h"

namespace eng
{
	class IioFrame
	{
	public:
		virtual const gtm::vector2u& getClientScale() const = 0;

		virtual eng::RenderTarget& renderTarget() = 0;
		virtual const eng::RenderTarget& renderTarget() const = 0;


		virtual const eng::Input& input() const = 0;

		IioFrame() = default;
		virtual ~IioFrame() = default;

		virtual void fetch() = 0;
	};
};