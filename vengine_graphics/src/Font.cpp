#include <eng/Font.h>
#include <assert.h>
#include <ft2build.h>
#include FT_FREETYPE_H

eng::Bitmap2 read_Pixels_From_FT_Bitmap(unsigned char * buffer, unsigned int width, unsigned int rows, unsigned char pixelMode)
{
	eng::Bitmap2 out({ static_cast<size_t>(width), static_cast<size_t>(rows) });

	if (pixelMode == FT_PIXEL_MODE_GRAY)
	{
		for (size_t i_y = 0; i_y < rows; i_y++)
		{
			for (size_t i_x = 0; i_x < width; i_x++)
			{
				unsigned char greyscale = buffer[i_y * width + i_x];
				eng::color pixel = eng::color{ 255, 255, 255, greyscale };
				out.pixel({ i_x,i_y }) = pixel;
			}
		}
	}
	else
	{
		assert(0);
	}
	return out;
}

void load_all_glyphs_and_bitmaps(FT_Face face, std::vector<eng::Font::glyph>& glyphsOut, std::vector<eng::Bitmap2>& bitmapsOut)
{
	FT_UInt num_glyphs_UInt = 256;

	glyphsOut.resize(static_cast<size_t>(num_glyphs_UInt));
	bitmapsOut.resize(static_cast<size_t>(num_glyphs_UInt));


	for (FT_UInt glyph_i = 0; glyph_i < num_glyphs_UInt; glyph_i++)
	{
		size_t glyph_i_size_t = static_cast<size_t>(glyph_i);

		FT_Error error = FT_Load_Glyph(face, glyph_i, FT_LOAD_DEFAULT);
		assert(!error);
		if (face->glyph->format != FT_GLYPH_FORMAT_BITMAP)
		{
			error = FT_Render_Glyph(face->glyph, FT_Render_Mode::FT_RENDER_MODE_NORMAL);
			assert(!error);
		}

		glyphsOut[glyph_i_size_t].advance = face->glyph->advance.x >> 6;

		glyphsOut[glyph_i_size_t].bearing[0] = face->glyph->bitmap_left;
		glyphsOut[glyph_i_size_t].bearing[1] = -face->glyph->bitmap_top;

		glyphsOut[glyph_i_size_t].scale[0] = static_cast<unsigned int>(face->glyph->bitmap.width);
		glyphsOut[glyph_i_size_t].scale[1] = static_cast<unsigned int>(face->glyph->bitmap.rows);

		bitmapsOut[glyph_i_size_t] = read_Pixels_From_FT_Bitmap
		(
			face->glyph->bitmap.buffer,
			face->glyph->bitmap.width,
			face->glyph->bitmap.rows,
			face->glyph->bitmap.pixel_mode
		);
	}
}

eng::Bitmap2 combine_Bitmaps_and_set_uv(std::vector<eng::Font::glyph>& glyphs_uv_Out, const std::vector<eng::Bitmap2>& bitmaps)
{
	gtm::vector<size_t, 2> largestBBox = {0,0};
	for (size_t i = 0; i < bitmaps.size(); i++)
	{
		if (largestBBox[0] < bitmaps[i].scale()[0])
		{
			largestBBox[0] = bitmaps[i].scale()[0];
		}
		if (largestBBox[1] < bitmaps[i].scale()[1])
		{
			largestBBox[1] = bitmaps[i].scale()[1];
		}
	}
	
	signed long long row_glyph_count = static_cast<signed long long>(sqrt(static_cast<double>(bitmaps.size())) + 0.5);
	signed long long row_count = row_glyph_count;
	
	gtm::vector<signed long long, 2> scale =
	{ 
		row_glyph_count * static_cast<signed long long>(largestBBox[0]),
		row_count * static_cast<signed long long>(largestBBox[1])
	};
	eng::Bitmap2 out(static_cast<gtm::vector<size_t, 2>>(scale), { 255,255,255,0 });


	size_t glyphIndex = 0;
	for (signed long long y_i = 0; y_i < row_count; y_i++)
	{
		signed long long y_write_pos = y_i * largestBBox[1];

		for (signed long long x_i = 0; x_i < row_glyph_count; x_i++)
		{
			signed long long x_write_pos = x_i * largestBBox[0];

			gtm::vector<signed long long, 2> posTopLeft = { x_write_pos, y_write_pos };
			gtm::vector<signed long long, 2> posBottomRight = posTopLeft + bitmaps[glyphIndex].scale();

			gtm::vector2f uvTopLeft = static_cast<gtm::vector2f>(posTopLeft);
			uvTopLeft[0] /= scale[0];
			uvTopLeft[1] /= scale[1];
			gtm::vector2f uvBottomRight = static_cast<gtm::vector2f>(posBottomRight);
			uvBottomRight[0] /= scale[0];
			uvBottomRight[1] /= scale[1];

			glyphs_uv_Out[glyphIndex].uv_top_left = uvTopLeft;
			glyphs_uv_Out[glyphIndex].uv_bottom_right = uvBottomRight;
			out.copy_to_Position({ posTopLeft[0], posTopLeft[1] }, bitmaps[glyphIndex]);

			glyphIndex++;
			if (glyphIndex >= bitmaps.size())
			{
				break;
			}
		}
	}

	return out;
}

const eng::DeviceTexture2 * eng::Font::fontTexture() const
{
	return m_fontTexture.get();
}

unsigned int eng::Font::getHeightInPixels() const
{
	return m_heightInPixels;
}

eng::Font::glyph eng::Font::getGlyphData(wchar_t c) const
{
	auto i = m_charMap.find(c);
	if (i != m_charMap.end())
	{
		return i->second;
	}
	return eng::Font::glyph();
}

eng::Bitmap2 eng::Font::render_string(std::wstring string) const
{
	size_t width_in_pixels = 1;

	unsigned long space_between_chars = 2;

	std::map<wchar_t, glyph> usedGlyphs;


	//creating the map of used glyphs and calculating width of output bitmap
	for (size_t i = 0; i < string.size(); i++)
	{
		wchar_t unicodeCharcode = string[i];

		auto iterator = usedGlyphs.find(unicodeCharcode);

		if (iterator == usedGlyphs.end())
		{
			glyph data = {};// = render_glyph({ m_heightInPixels, m_heightInPixels }, unicodeCharcode);

			//width_in_pixels += static_cast<size_t>(data.advance);
			usedGlyphs.insert(std::make_pair(unicodeCharcode, std::move(data)));
		}
		else
		{
			width_in_pixels += static_cast<size_t>(iterator->second.advance);
		}
	}
	


	eng::Bitmap2 out({ width_in_pixels, m_heightInPixels }, { 0,0,0,255 });
	signed long long pen_x = 1;
	for (size_t i = 0; i < string.size(); i++)
	{
		glyph& data = usedGlyphs[string[i]];
		//out.copy_to_Position
		//(
		//	{ 
		//		pen_x + data.bearing.x,
		//		data.bearing.y + m_ascenderInPixels
		//	}, 
		//	data.bitmap
		//);
		pen_x += data.advance;
	}

	return out;
}

eng::Font::Font(std::wstring filePath, unsigned int heightInPixels)
{
	m_heightInPixels = heightInPixels;




	FT_Library		ftLibrary;
	FT_Face			face;


	std::ifstream ifStream(filePath, std::ios::ate | std::ios::binary);
	std::streampos pos = ifStream.tellg();
	std::streamsize length = static_cast<std::streamsize>(pos);


	auto pBytes = std::make_unique<char[]>(pos);
	ifStream.seekg(0, std::ios::beg);
	ifStream.read(pBytes.get(), length);
		
	ifStream.close();

	//accessing freeType
	{
		FT_Error error;

		error = FT_Init_FreeType(&ftLibrary);
		if (error)
		{
			throw std::exception("an error occured during freeType library initialization");
		}

		error = FT_New_Memory_Face(ftLibrary, reinterpret_cast<FT_Byte*>(pBytes.get()), static_cast<FT_Long>(length), 0, &face);
		if (error)
		{
			throw std::exception("an error occured during freeType face initialization");
		}
		error = FT_Set_Pixel_Sizes(face, heightInPixels, heightInPixels);
		if (error)
		{
			throw std::exception("an error occured while setting face sizes");
		}

		double ascenderMinusDescender_double = static_cast<double>(face->size->metrics.ascender - face->size->metrics.descender);
		double ascender_double = static_cast<double>(face->size->metrics.ascender);
		double heightInPixels_double = static_cast<double>(m_heightInPixels);

		m_ascenderInPixels = static_cast<int>(heightInPixels_double * (ascender_double / ascenderMinusDescender_double) + 0.5);

		std::vector<eng::Font::glyph> glyphs;
		std::vector<eng::Bitmap2> glyphBitmaps;

		load_all_glyphs_and_bitmaps(face, glyphs, glyphBitmaps);
		m_fontTexture.reset(new eng::DeviceTexture2(combine_Bitmaps_and_set_uv(glyphs, glyphBitmaps)));

		//filling charmap
		{
			FT_UInt index;
			FT_ULong character = FT_Get_First_Char(face, &index);

			while (index)
			{
				// to do something

				size_t index_size_t = static_cast<size_t>(index);
				if (index_size_t < glyphs.size())
				{
					m_charMap.insert({ static_cast<WCHAR>(character), glyphs[index_size_t] });
				}

				character = FT_Get_Next_Char(face, character, &index);
			}
		}
	}

	//freeing ft_lib
	{
		FT_Error error;

		error = FT_Done_Face(face);
		if (error)
		{
			//an error occured during freeType face uninitialization
			assert(0);
		}


		error = FT_Done_FreeType(ftLibrary);
		if (error)
		{
			//an error occured during freeType library uninitialization
			assert(0);
		}
	}
}

eng::Font::~Font() noexcept
{

}

unsigned int eng::calculate_rendered_length(const std::wstring & text, const eng::Font * pFont)
{
	unsigned int out = 0;

	for (size_t i = 0; i < text.size(); i++)
	{
		out += pFont->getGlyphData(text[i]).advance;
	}

	return out;
}
