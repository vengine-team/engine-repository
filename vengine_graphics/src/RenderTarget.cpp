#include <eng/RenderTarget.h>
#include <eng/windowsUtil.h>
#include <eng/D3D12_Device.h>
#include <comdef.h>
#include <assert.h>

void createCommandAllocator(ID3D12Device * pDevice, UINT allocatorIndex, ID3D12CommandAllocator** ppCommandAllocator)
{
	AssertIfFailed
	(
		pDevice->CreateCommandAllocator(D3D12_COMMAND_LIST_TYPE_DIRECT, IID_PPV_ARGS(ppCommandAllocator)),
		"createCommandAllocators() pDevice->CreateCommandAllocator() failed"
	);
	std::wstring name(L"Graphics::m_pCommandAllocator[");
	name.append(std::to_wstring(allocatorIndex));
	name.append(L"]");

	(*ppCommandAllocator)->SetName(name.c_str());
}

void createBundleAllocator(ID3D12Device * pDevice, UINT allocatorIndex, ID3D12CommandAllocator** ppCommandAllocator)
{
	AssertIfFailed
	(
		pDevice->CreateCommandAllocator(D3D12_COMMAND_LIST_TYPE_BUNDLE, IID_PPV_ARGS(ppCommandAllocator)),
		"createCommandAllocators() pDevice->CreateCommandAllocator() failed"
	);
	std::wstring name(L"Graphics::m_pBundleAllocator[");
	name.append(std::to_wstring(allocatorIndex));
	name.append(L"]");

	(*ppCommandAllocator)->SetName(name.c_str());
}

void createDsvHeap(ID3D12Device * pDevice, ID3D12DescriptorHeap ** ppDescriptorHeap)
{
	D3D12_DESCRIPTOR_HEAP_DESC dsvHeapDesc = {};
	dsvHeapDesc.NumDescriptors = 1;
	dsvHeapDesc.Type = D3D12_DESCRIPTOR_HEAP_TYPE_DSV;
	dsvHeapDesc.Flags = D3D12_DESCRIPTOR_HEAP_FLAG_NONE;
	AssertIfFailed(pDevice->CreateDescriptorHeap(&dsvHeapDesc, IID_PPV_ARGS(ppDescriptorHeap)), "Graphics::createDsvHeap() pDevice->CreateDescriptorHeap() failed");
	(*ppDescriptorHeap)->SetName(L"Graphics::m_pDsvHeap");
}

void queueCreateSwapChain(IDXGIFactory4* pFactory, ID3D12CommandQueue* pCommandQueue, gtm::vector2u size, Microsoft::WRL::Details::ComPtrRef<Microsoft::WRL::ComPtr<IDXGISwapChain3>> pSwapChain_ref)
{
	DXGI_SWAP_CHAIN_DESC1 swapChainDesc = {};

	swapChainDesc.Width = size[0];
	swapChainDesc.Height = size[1];

	swapChainDesc.Format = eng::D3D12::c_textureFormat;
	swapChainDesc.Stereo = false;

	swapChainDesc.SampleDesc.Count = 1;
	swapChainDesc.SampleDesc.Quality = 0;

	swapChainDesc.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
	swapChainDesc.BufferCount = eng::D3D12::c_bufferCount;

	swapChainDesc.SwapEffect = DXGI_SWAP_EFFECT_FLIP_SEQUENTIAL;
	swapChainDesc.Flags;
	swapChainDesc.Scaling = DXGI_SCALING_STRETCH;
	swapChainDesc.AlphaMode = DXGI_ALPHA_MODE_IGNORE;

	Microsoft::WRL::ComPtr<IDXGISwapChain1> _pSwapChain;

	AssertIfFailed(pFactory->CreateSwapChainForComposition
	(
		pCommandQueue,
		&swapChainDesc,
		nullptr,
		&_pSwapChain
	), "queueCreateSwapChainForHwnd() pFactory->CreateSwapChainForHwnd() failed");

	AssertIfFailed(_pSwapChain.As(pSwapChain_ref), "queueCreateSwapChainForHwnd()  _pSwapChain.As(&pSwapChain_ref) failed");
}

void queueCreateSwapChainForHwnd(IDXGIFactory4* pFactory, ID3D12CommandQueue* pCommandQueue, HWND hwnd, gtm::vector2u size, Microsoft::WRL::Details::ComPtrRef<Microsoft::WRL::ComPtr<IDXGISwapChain3>> pSwapChain_ref)
{
	DXGI_SWAP_CHAIN_DESC1 swapChainDesc = {};

	swapChainDesc.Width = size[0];
	swapChainDesc.Height = size[1];

	swapChainDesc.Format = eng::D3D12::c_textureFormat;
	swapChainDesc.Stereo = false;

	swapChainDesc.SampleDesc.Count = 1;
	swapChainDesc.SampleDesc.Quality = 0;

	swapChainDesc.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
	swapChainDesc.BufferCount = eng::D3D12::c_bufferCount;

	swapChainDesc.SwapEffect = DXGI_SWAP_EFFECT_FLIP_DISCARD;
	swapChainDesc.Flags;
	swapChainDesc.Scaling = DXGI_SCALING_NONE;
	swapChainDesc.AlphaMode = DXGI_ALPHA_MODE_IGNORE;

	Microsoft::WRL::ComPtr<IDXGISwapChain1> _pSwapChain;

	AssertIfFailed(pFactory->CreateSwapChainForHwnd
	(
		pCommandQueue,
		hwnd,
		&swapChainDesc,
		nullptr,
		nullptr,
		&_pSwapChain
	), "queueCreateSwapChainForHwnd() pFactory->CreateSwapChainForHwnd() failed");

	AssertIfFailed(_pSwapChain.As(pSwapChain_ref), "queueCreateSwapChainForHwnd()  _pSwapChain.As(&pSwapChain_ref) failed");
}

void createRtvDescriptorHeap(ID3D12Device* pDevice, ID3D12DescriptorHeap** ppDescriptorHeap, UINT* pDescriptorSize)
{
	D3D12_DESCRIPTOR_HEAP_DESC rtvHeapDesc = {};
	rtvHeapDesc.NumDescriptors = eng::D3D12::c_bufferCount;
	rtvHeapDesc.Type = D3D12_DESCRIPTOR_HEAP_TYPE_RTV;
	rtvHeapDesc.Flags = D3D12_DESCRIPTOR_HEAP_FLAG_NONE;

	AssertIfFailed
	(
		pDevice->CreateDescriptorHeap
		(
			&rtvHeapDesc,
			IID_PPV_ARGS(ppDescriptorHeap)
		),
		"pDevice->CreateDescriptorHeap() failed"
	);
	(*ppDescriptorHeap)->SetName(L"RenderTarget::m_pRtvHeap");

	*pDescriptorSize = pDevice->GetDescriptorHandleIncrementSize(rtvHeapDesc.Type);
}


void eng::RenderTarget::createDepthStencil(ID3D12Device * pDevice, ID3D12Resource ** ppResource, ID3D12DescriptorHeap * pDsvHeap, gtm::vector2u size)
{
	D3D12_HEAP_PROPERTIES depthHeapProperties = CD3DX12_HEAP_PROPERTIES(D3D12_HEAP_TYPE_DEFAULT);

	D3D12_RESOURCE_DESC depthResourceDesc = CD3DX12_RESOURCE_DESC::Tex2D(D3D12::c_depthBufferFormat, size[0], size[1], 1, 1);
	depthResourceDesc.Flags |= D3D12_RESOURCE_FLAG_ALLOW_DEPTH_STENCIL;

	CD3DX12_CLEAR_VALUE depthOptimizedClearValue(D3D12::c_depthBufferFormat, 1.0f, 0);

	AssertIfFailed(pDevice->CreateCommittedResource
	(
		&depthHeapProperties,
		D3D12_HEAP_FLAG_NONE,
		&depthResourceDesc,
		D3D12_RESOURCE_STATE_DEPTH_WRITE,
		&depthOptimizedClearValue,
		IID_PPV_ARGS(ppResource)
	), "Graphics::createDepthStencil() pDevice->CreateCommittedResource()");

	(*ppResource)->SetName(L"Graphics::m_pDepthStencil");

	D3D12_DEPTH_STENCIL_VIEW_DESC dsvDesc = {};
	dsvDesc.Format = eng::D3D12::c_depthBufferFormat;
	dsvDesc.ViewDimension = D3D12_DSV_DIMENSION_TEXTURE2D;
	dsvDesc.Flags = D3D12_DSV_FLAG_NONE;

	pDevice->CreateDepthStencilView(*ppResource, &dsvDesc, pDsvHeap->GetCPUDescriptorHandleForHeapStart());
}

void eng::RenderTarget::resizeSwapChain(gtm::vector2u size)
{
	for (UINT i = 0; i < D3D12::c_bufferCount; i++)
	{
		D3D12::instance().getCommandQueue()->concurrentWaitForCompletion(m_frames[i].fenceValue);
		m_frames[i].pRenderTarget.Reset();
	}

	HRESULT hr = m_deviceResources.pSwapChain->ResizeBuffers(D3D12::c_bufferCount, size[0], size[1], D3D12::c_textureFormat, 0);

	if (hr == DXGI_ERROR_DEVICE_REMOVED || hr == DXGI_ERROR_DEVICE_RESET)
	{
		AssertIfFailed(hr, "device resources recreation not yet implemented");
		return;
	}
	else
	{
		AssertIfFailed(hr, "unexpected Error, cannot recreate device resources");
	}

	createRenderTargetViews(D3D12::instance().getDevice(), m_deviceResources.pRtvDescriptorHeap.Get());
}

void eng::RenderTarget::createRenderTargetViews(ID3D12Device * pDevice, ID3D12DescriptorHeap * pRtvHeap)
{
	m_current_frame_i = m_deviceResources.pSwapChain->GetCurrentBackBufferIndex();

	CD3DX12_CPU_DESCRIPTOR_HANDLE rtvDescriptor(pRtvHeap->GetCPUDescriptorHandleForHeapStart());

	for (UINT i = 0; i < D3D12::c_bufferCount; i++)
	{
		AssertIfFailed(m_deviceResources.pSwapChain->GetBuffer(i, IID_PPV_ARGS(&m_frames[i].pRenderTarget)), "RenderTarget::createRenderTargetViews() m_pSwapChain->GetBuffer() failed");
		pDevice->CreateRenderTargetView(m_frames[i].pRenderTarget.Get(), nullptr, rtvDescriptor);
		rtvDescriptor.Offset(m_RtvDescriptorHandleSize);

		std::wstring name(L"RenderTarget::m_pRenderTargets[");
		name.append(std::to_wstring(i));
		name.append(L"]");

		m_frames[i].pRenderTarget->SetName(name.c_str());
	}
}

void eng::RenderTarget::initializeSizeIndependentResources()
{
	createRtvDescriptorHeap(eng::D3D12::instance().getDevice(), &m_deviceResources.pRtvDescriptorHeap, &m_RtvDescriptorHandleSize);
}

void eng::RenderTarget::initializeSizeDependentResourcesForHWND(HWND window, gtm::vector2u size)
{
	queueCreateSwapChainForHwnd
	(
		eng::D3D12::instance().getDXGIFactory(),
		D3D12::instance().getCommandQueue()->queue(),
		window,
		size,
		&m_deviceResources.pSwapChain
	);

	resizeSwapChain(size);

	m_deviceResources.pPresentBuffer = std::make_shared<D3D12::PresentBuffer>(size);
}

void eng::RenderTarget::initializeSizeDependentResources(gtm::vector2u size)
{
	queueCreateSwapChain
	(
		eng::D3D12::instance().getDXGIFactory(),
		D3D12::instance().getCommandQueue()->queue(),
		size,
		&m_deviceResources.pSwapChain
	);

	resizeSwapChain(size);

	m_deviceResources.pPresentBuffer = std::make_shared<D3D12::PresentBuffer>(size);
}

std::tuple<eng::D3D12::texture_gpu_descriptor, std::shared_ptr<eng::D3D12::DeviceResource>> eng::RenderTarget::D3D12_get_SRV_and_Sampler() const
{
	std::shared_lock<std::shared_mutex> lock(m_mutex);

	return
	{
		{
			{ m_deviceResources.pPresentBuffer->D3D12_getPresentBufferDescriptorHeap(), m_deviceResources.pPresentBuffer->D3D12_getPresentBufferGPUDescriptorHandle() },
			{ m_deviceResources.pPresentBuffer->D3D12_getSamplerDescriptorHeap(), m_deviceResources.pPresentBuffer->D3D12_getSamplerGPUDescriptorHandle() }
		},
		{ std::static_pointer_cast<eng::D3D12::DeviceResource>(m_deviceResources.pPresentBuffer) }
	};
}

eng::RenderTarget * eng::RenderTarget::WINAPI_make_renderTarget_for_HWND(HWND window, gtm::vector2u size)
{
	return new RenderTarget(window, size);
}

gtm::vector2u eng::RenderTarget::get_scale() const
{
	std::scoped_lock<std::shared_mutex> lock(m_mutex);

	return m_scale;
}

void eng::RenderTarget::set_next_scale(gtm::vector2u nextScale)
{
	std::scoped_lock<std::shared_mutex> lock(m_mutex);

	m_viewport = { 0.0f, 0.0f, static_cast<FLOAT>(nextScale[0]),static_cast<FLOAT>(nextScale[1]), 0.0f, 1.0f };

	if (m_next_scale != nextScale)
	{
		resizeSwapChain(nextScale);

		m_deviceResources.pNextPresentBuffer = std::make_shared<D3D12::PresentBuffer>(nextScale);
		m_next_scale = nextScale;
	}
}

gtm::vector2u eng::RenderTarget::get_next_scale() const
{
	std::scoped_lock<std::shared_mutex> lock(m_mutex);

	return m_next_scale;
}

void eng::RenderTarget::D3D12_beginRecord(ID3D12GraphicsCommandList * pGraphicsCommandList)
{
	std::scoped_lock<std::shared_mutex> lock(m_mutex);

	CD3DX12_RESOURCE_BARRIER presentResourceBarrier =
		CD3DX12_RESOURCE_BARRIER::Transition(m_frames[m_current_frame_i].pRenderTarget.Get(), D3D12_RESOURCE_STATE_PRESENT, D3D12_RESOURCE_STATE_RENDER_TARGET);
	pGraphicsCommandList->ResourceBarrier(1, &presentResourceBarrier);
}

void eng::RenderTarget::D3D12_endRecord(ID3D12GraphicsCommandList * pGraphicsCommandList)
{
	std::scoped_lock<std::shared_mutex> lock(m_mutex);
	//copy texture into present buffer
	std::shared_ptr<D3D12::PresentBuffer> pTarget = m_deviceResources.pPresentBuffer;
	if (m_deviceResources.pNextPresentBuffer != nullptr)
	{
		pTarget = m_deviceResources.pNextPresentBuffer;
	}

	m_current_frame_used_resources.push_back(pTarget);

	{
		{
			CD3DX12_RESOURCE_BARRIER copySourceResourceBarrier =
				CD3DX12_RESOURCE_BARRIER::Transition(m_frames[m_current_frame_i].pRenderTarget.Get(), D3D12_RESOURCE_STATE_RENDER_TARGET, D3D12_RESOURCE_STATE_COPY_SOURCE);
			pGraphicsCommandList->ResourceBarrier(1, &copySourceResourceBarrier);

			CD3DX12_RESOURCE_BARRIER copyDestinationResourceBarrier =
				CD3DX12_RESOURCE_BARRIER::Transition(pTarget->D3D12_getPresentBuffer(), D3D12_RESOURCE_STATE_PIXEL_SHADER_RESOURCE, D3D12_RESOURCE_STATE_COPY_DEST);
			pGraphicsCommandList->ResourceBarrier(1, &copyDestinationResourceBarrier);

		}

		pGraphicsCommandList->CopyResource(pTarget->D3D12_getPresentBuffer(), m_frames[m_current_frame_i].pRenderTarget.Get());

		{
			CD3DX12_RESOURCE_BARRIER pixelShaderResourceBarrier =
				CD3DX12_RESOURCE_BARRIER::Transition(pTarget->D3D12_getPresentBuffer(), D3D12_RESOURCE_STATE_COPY_DEST, D3D12_RESOURCE_STATE_PIXEL_SHADER_RESOURCE);
			pGraphicsCommandList->ResourceBarrier(1, &pixelShaderResourceBarrier);

			CD3DX12_RESOURCE_BARRIER presentResourceBarrier =
				CD3DX12_RESOURCE_BARRIER::Transition(m_frames[m_current_frame_i].pRenderTarget.Get(), D3D12_RESOURCE_STATE_COPY_SOURCE, D3D12_RESOURCE_STATE_PRESENT);
			pGraphicsCommandList->ResourceBarrier(1, &presentResourceBarrier);
		}
	}
}

void eng::RenderTarget::D3D12_advance_and_present(UINT64 fenceValue)
{
	std::scoped_lock<std::shared_mutex> lock(m_mutex);
	if (m_deviceResources.pNextPresentBuffer != nullptr)
	{
		m_deviceResources.pPresentBuffer = std::move(m_deviceResources.pNextPresentBuffer);
	}

	m_frames[m_current_frame_i].fenceValue = fenceValue;

	HRESULT hr = m_deviceResources.pSwapChain->Present(0, 0);

	if (hr == DXGI_ERROR_DEVICE_REMOVED || hr == DXGI_ERROR_DEVICE_RESET)
	{
		AssertIfFailed(hr, "device resources recreation not yet implemented");
	}
	else
	{
		AssertIfFailed(hr, "unexpected Error, cannot recreate device resources");

		m_current_frame_i = m_deviceResources.pSwapChain->GetCurrentBackBufferIndex();
		D3D12::instance().getCommandQueue()->concurrentWaitForCompletion(m_frames[m_current_frame_i].fenceValue);
	}
	m_scale = m_next_scale;
}


void eng::RenderTarget::begin_recording()
{
	m_deviceResources.pCachedRenderPass = nullptr;

	auto next_scale = get_next_scale(); //synchronized by mutex

	if (m_depthStencilSize != next_scale)
	{
		for (UINT i = 0; i < D3D12::c_bufferCount; i++)
		{
			D3D12::instance().getCommandQueue()->concurrentWaitForCompletion(m_frames[i].fenceValue);
		}
		next_scale = get_next_scale();
		createDepthStencil(D3D12::instance().getDevice(), &m_deviceResources.pDepthStencil, m_deviceResources.pDsvHeap.Get(), next_scale);
		m_depthStencilSize = next_scale;
	}

	AssertIfFailed(m_frames[m_current_frame_i].pCommandAllocator->Reset(), "eng::ControlGraphics::beginRender() m_deviceResources.pCommandAllocator.get()->Reset() failed");
	AssertIfFailed(m_frames[m_current_frame_i].pBundleAllocator->Reset(), "eng::ControlGraphics::beginRender() m_deviceResources.pBundleAllocator.get()->Reset() failed");

	if (m_deviceResources.pGraphicsCommandList == nullptr)
	{
		eng::D3D12::instance().getDevice()->CreateCommandList
		(
			0,
			D3D12_COMMAND_LIST_TYPE_DIRECT,
			m_frames[m_current_frame_i].pCommandAllocator.Get(),
			nullptr,
			IID_PPV_ARGS(&m_deviceResources.pGraphicsCommandList)
		);
		m_deviceResources.pGraphicsCommandList->SetName(L"eng::RenderTarget Graphics Command List");
	}
	else
	{
		m_deviceResources.pGraphicsCommandList->Reset(m_frames[m_current_frame_i].pCommandAllocator.Get(), nullptr);
	}

	D3D12_beginRecord(m_deviceResources.pGraphicsCommandList.Get());

	D3D12_CPU_DESCRIPTOR_HANDLE renderTargetView = CD3DX12_CPU_DESCRIPTOR_HANDLE(m_deviceResources.pRtvDescriptorHeap->GetCPUDescriptorHandleForHeapStart(), m_current_frame_i, m_RtvDescriptorHandleSize);
	D3D12_CPU_DESCRIPTOR_HANDLE depthStencilView = D3D12_CPU_DESCRIPTOR_HANDLE(m_deviceResources.pDsvHeap->GetCPUDescriptorHandleForHeapStart());

	float black[]{ 0.0f, 0.0f, 0.0f, 1.0f };
	float clearColor[]{ 0.392156899f, 0.584313750f, 0.929411829f, 1.000000000f };

	m_deviceResources.pGraphicsCommandList->ClearRenderTargetView(renderTargetView, black, 0, nullptr);
	m_deviceResources.pGraphicsCommandList->ClearDepthStencilView(depthStencilView, D3D12_CLEAR_FLAG_DEPTH, 1.0f, 0, 0, nullptr);


	D3D12_RECT rect = { 0, 0, static_cast<LONG>(next_scale[0]), static_cast<LONG>(next_scale[1]) };

	m_deviceResources.pGraphicsCommandList->RSSetScissorRects(1, &rect);
	m_deviceResources.pGraphicsCommandList->RSSetViewports(1, &m_viewport);

	m_deviceResources.pGraphicsCommandList->OMSetRenderTargets(1, &renderTargetView, false, &depthStencilView);

	m_deviceResources.pGraphicsCommandList->IASetPrimitiveTopology(D3D_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
}

void eng::RenderTarget::end_recording()
{
	D3D12_endRecord(m_deviceResources.pGraphicsCommandList.Get());

	m_deviceResources.pGraphicsCommandList->Close();

	ID3D12CommandList* pCommandList = m_deviceResources.pGraphicsCommandList.Get();

	{
		auto& m = D3D12::instance().commandQueueMutex();
		std::scoped_lock<std::mutex> lock(m);

		D3D12::instance().getCommandQueue()->queue()->ExecuteCommandLists(1, &pCommandList);
		D3D12::fence_t signal_fence = D3D12::instance().getCommandQueue()->concurrentSignal();

		D3D12_advance_and_present(signal_fence);
		
		eng::D3D12::instance().getAsyncTaskQueue()->concurrent_push(signal_fence, [current_frame_owned_resources{ std::move(m_current_frame_used_resources) }](){});
	}
}

eng::RenderTarget::RenderTarget(HWND window, gtm::vector2u size)
{
	m_viewport = { 0.0f, 0.0f, static_cast<FLOAT>(size[0]),static_cast<FLOAT>(size[1]), 0.0f, 1.0f };

	m_depthStencilSize = gtm::vector2u::zero();

	m_scale = size;
	m_next_scale = m_scale;

	for (size_t i = 0; i < D3D12::c_bufferCount; i++)
	{
		m_frames[i].fenceValue = 0;
	}

	initializeSizeIndependentResources();
	initializeSizeDependentResourcesForHWND(window, size);

	createDsvHeap(D3D12::instance().getDevice(), &m_deviceResources.pDsvHeap);

	for (UINT i = 0; i < D3D12::c_bufferCount; i++)
	{
		createCommandAllocator(D3D12::instance().getDevice(), i, &m_frames[i].pCommandAllocator);
		createBundleAllocator(D3D12::instance().getDevice(), i, &m_frames[i].pBundleAllocator);
	}
}

eng::RenderTarget::RenderTarget(gtm::vector2u size)
{
	m_viewport = { 0.0f, 0.0f, static_cast<FLOAT>(size[0]),static_cast<FLOAT>(size[1]), 0.0f, 1.0f };

	m_depthStencilSize = gtm::vector2u::zero();

	for (size_t i = 0; i < D3D12::c_bufferCount; i++)
	{
		m_frames[i].fenceValue = 0;
	}

	m_scale = size;
	m_next_scale = m_scale;

	initializeSizeIndependentResources();
	initializeSizeDependentResources(size);

	createDsvHeap(D3D12::instance().getDevice(), &m_deviceResources.pDsvHeap);

	for (UINT i = 0; i < D3D12::c_bufferCount; i++)
	{
		createCommandAllocator(D3D12::instance().getDevice(), i, &m_frames[i].pCommandAllocator);
		createBundleAllocator(D3D12::instance().getDevice(), i, &m_frames[i].pBundleAllocator);
	}
}

eng::RenderTarget::~RenderTarget()
{
	UINT64 latestFence = eng::D3D12::instance().getCommandQueue()->concurrentGetLatestFence();
	for (size_t i = 0; i < m_frames.size(); i++)
	{
		eng::D3D12::instance().getAsyncTaskQueue()->concurrent_push
		(
			latestFence,
			[current_frame_resources_to_be_deleted{ std::move(m_frames[i]) }](){}
		);
	}


	eng::D3D12::instance().getAsyncTaskQueue()->concurrent_push
	(
		latestFence,
		[device_resources_ro_be_deleted{ std::move(m_deviceResources) }](){}
	);
}
