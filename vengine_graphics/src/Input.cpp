#include <eng/Input.h>

void eng::Input::countMouseChanges()
{
	for (auto i = states_begin(); i != states_end(); i++)
	{
		if (i->type == input_type::Mouse)
		{
			if (i->current.mouse.left_button)
			{
				m_mouseLeftDownCount++;
			}
			else
			{
				m_mouseLeftUpCount++;
			}
		}
	}
}


const std::wstring & eng::Input::composition_string() const
{
	return m_composition_string;
}

const eng::input_state & eng::Input::initial_state() const
{
	return m_initial_state;
}

const eng::input_state & eng::Input::latest_state() const
{
	if (m_mouseStateChanges.empty())
	{
		return m_initial_state;
	}
	else
	{
		return m_mouseStateChanges.back().current;
	}
}

eng::Input::const_iterator eng::Input::states_begin() const
{
	return m_mouseStateChanges.begin();
}

eng::Input::const_iterator eng::Input::states_end() const
{
	return m_mouseStateChanges.end();
}

eng::Input::const_reverse_iterator eng::Input::states_rbegin() const
{
	return m_mouseStateChanges.rbegin();
}

eng::Input::const_reverse_iterator eng::Input::states_rend() const
{
	return m_mouseStateChanges.rend();
}

size_t eng::Input::mouse_states_count() const
{
	return m_mouseStateChanges.size();
}

unsigned int eng::Input::mouseLeftDownCount() const
{
	return m_mouseLeftDownCount;
}

unsigned int eng::Input::mouseLeftUpCount() const
{
	return m_mouseLeftUpCount;
}

eng::Input::Input(const Input & source)
{
	m_composition_string = source.m_composition_string;

	m_initial_state = source.m_initial_state;
	m_mouseStateChanges = source.m_mouseStateChanges;

	m_mouseLeftDownCount = source.m_mouseLeftDownCount;
	m_mouseLeftUpCount = source.m_mouseLeftUpCount;
}
eng::Input::Input(Input && source)
{
	m_composition_string = std::move(source.m_composition_string);

	m_initial_state = std::move(source.m_initial_state);
	m_mouseStateChanges = std::move(source.m_mouseStateChanges);

	m_mouseLeftDownCount = std::move(source.m_mouseLeftDownCount);
	m_mouseLeftUpCount = std::move(source.m_mouseLeftUpCount);
}

eng::Input & eng::Input::operator=(const Input & source)
{
	m_composition_string = source.m_composition_string;

	m_initial_state = source.m_initial_state;
	m_mouseStateChanges = source.m_mouseStateChanges;

	m_mouseLeftDownCount = source.m_mouseLeftDownCount;
	m_mouseLeftUpCount = source.m_mouseLeftUpCount;

	return *this;
}

eng::Input & eng::Input::operator=(Input && source)
{
	m_composition_string = std::move(source.m_composition_string);

	m_initial_state = std::move(source.m_initial_state);
	m_mouseStateChanges = std::move(source.m_mouseStateChanges);

	m_mouseLeftDownCount = std::move(source.m_mouseLeftDownCount);
	m_mouseLeftUpCount = std::move(source.m_mouseLeftUpCount);

	return *this;
}

eng::Input::Input(const FrameInput_Desc & desc)
{
	m_composition_string = desc.compositionString;

	m_initial_state = desc.initialState;
	m_mouseStateChanges = desc.mouseStateChanges;

	countMouseChanges();
}

eng::Input::Input(FrameInput_Desc && desc)
{
	m_composition_string = std::move(desc.compositionString);

	m_initial_state = std::move(desc.initialState);
	m_mouseStateChanges = std::move(desc.mouseStateChanges);

	countMouseChanges();
}

eng::Input::~Input()
{

}

const eng::input_state & eng::getLatestMouseState(const FrameInput_Desc & desc)
{
	if (desc.mouseStateChanges.empty())
	{
		return desc.initialState;
	}
	else
	{
		return desc.mouseStateChanges.back().current;
	}
}

