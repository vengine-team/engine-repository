#include <eng/graphicsUtil.h>

eng::Mesh3f eng::generate_text_mesh3F(const std::wstring& text, eng::Font const * pFont, float scale, float center)
{
	eng::Mesh2F mesh2F = eng::generate_text_mesh2F(text, pFont, scale, center);

	std::vector<eng::vertex3f> vertices;
	vertices.resize(mesh2F.vertices().size());
	for (size_t i = 0; i < mesh2F.vertices().size(); i++)
	{
		vertices[i] = 
		{
			gtm::vector3f{ mesh2F.vertices()[i].position[0], mesh2F.vertices()[i].position[1], 0 },
			gtm::vector3f{ 0, 0, 1 },
			gtm::vector3f{ 0, 0, 1 },
		};
		vertices[i].uv = mesh2F.vertices()[i].uv;
	}

	eng::Mesh3f mesh3F;
	mesh3F.vertices() = std::move(vertices);
	mesh3F.triangles() = std::move(mesh2F.triangles());

	return mesh3F;
}

eng::Mesh2F eng::generate_text_mesh2F(const std::wstring & text, eng::Font const * pFont, float scale, float center)
{
	std::vector<eng::vertex2f> vertices;
	vertices.resize(text.size() * 4);

	std::vector<eng::triangle_indices> triangles;
	triangles.resize(text.size() * 6);

	unsigned int textLength_in_pixels = 0;
	for (size_t char_i = 0; char_i < text.size(); char_i++)
	{
		eng::Font::glyph charGlyph = pFont->getGlyphData(text[char_i]);

		textLength_in_pixels += charGlyph.advance;
	}
	float offset_x = textLength_in_pixels * scale * center;


	float pen_x = 0.0f;
	for (size_t char_i = 0; char_i < text.size(); char_i++)
	{
		wchar_t c = text[char_i];

		eng::Font::glyph charGlyph = pFont->getGlyphData(text[char_i]);

		size_t current_vertex_index = char_i * 4;
		size_t current_triangles_index = char_i * 2;

		gtm::vector2f scaled_bearing = static_cast<gtm::vector2f>(charGlyph.bearing);
		scaled_bearing *= scale;
		gtm::vector2f scaled_scale = static_cast<gtm::vector2f>(charGlyph.scale);
		scaled_scale *= scale;

		gtm::vector2f topLeftpos = -1.0f * scaled_bearing - gtm::vector2f{ pen_x + offset_x, 0.0f };
		gtm::vector2f bottomRightpos = topLeftpos - scaled_scale;
		gtm::vector2f topRightpos = { bottomRightpos[0], topLeftpos[1] };
		gtm::vector2f bottomLeftpos = { topLeftpos[0], bottomRightpos[1] };


		gtm::vector2f uv_top_right = { charGlyph.uv_bottom_right[0], charGlyph.uv_top_left[1] };
		gtm::vector2f uv_bottom_left = { charGlyph.uv_top_left[0], charGlyph.uv_bottom_right[1] };

		vertices[current_vertex_index + 0] = eng::vertex2f{ gtm::vector2f{ -topLeftpos[0], topLeftpos[1] }, charGlyph.uv_top_left }; //top left
		vertices[current_vertex_index + 1] = eng::vertex2f{ gtm::vector2f{ -topRightpos[0], topRightpos[1] }, uv_top_right }; // top right
		vertices[current_vertex_index + 2] = eng::vertex2f{ gtm::vector2f{ -bottomLeftpos[0], bottomLeftpos[1] }, uv_bottom_left }; // bottom left
		vertices[current_vertex_index + 3] = eng::vertex2f{ gtm::vector2f{ -bottomRightpos[0], bottomRightpos[1] }, charGlyph.uv_bottom_right }; // bottom right

		triangles[current_triangles_index][0] = static_cast<eng::index_type>(current_vertex_index);
		triangles[current_triangles_index][1] = static_cast<eng::index_type>(current_vertex_index) + 1;
		triangles[current_triangles_index][2] = static_cast<eng::index_type>(current_vertex_index) + 2;

		triangles[current_triangles_index + 1][0] = static_cast<eng::index_type>(current_vertex_index + 3);
		triangles[current_triangles_index + 1][1] = static_cast<eng::index_type>(current_vertex_index + 2);
		triangles[current_triangles_index + 1][2] = static_cast<eng::index_type>(current_vertex_index + 1);

		pen_x += charGlyph.advance * scale;
	}


	eng::Mesh2F mesh;
	mesh.vertices() = std::move(vertices);
	mesh.triangles() = std::move(triangles);

	return mesh;
}


using lineIndices = std::pair<size_t, size_t>;
void writeFace(std::vector<eng::vertex3f>::iterator targetbegin, gtm::vector3f cornerA, gtm::vector3f cornerB, gtm::vector3f cornerC, gtm::vector3f cornerD, size_t detail)
{
	for (size_t v_i = 1; v_i < detail - 1; v_i++)
	{
		float v_t = static_cast<float>(v_i) / static_cast<float>(detail - 1);

		gtm::vector3f lerpAC = gtm::lerp(cornerA, cornerC, v_t);
		gtm::vector3f lerpBC = gtm::lerp(cornerB, cornerD, v_t);

		for (size_t u_i = 1; u_i < detail - 1; u_i++)
		{
			float u_t = static_cast<float>(u_i) / static_cast<float>(detail - 1);

			*targetbegin = { gtm::lerp(lerpAC, lerpBC, u_t) };

			targetbegin++;
		}
	}
}
void writeEdge(std::vector<eng::vertex3f>::iterator targetbegin, gtm::vector3f begin, gtm::vector3f end, size_t detail)
{
	for (size_t i = 1; i < detail - 1; i++)
	{
		float t = static_cast<float>(i) / static_cast<float>(detail - 1);

		*targetbegin = { gtm::lerp(begin, end, t) };

		targetbegin++;
	}
}
void findLineBeginIndexAndDir(lineIndices* cubeEdgeLineList, size_t begin_i, size_t end_i, size_t& lineIndicesIndex, bool& dirRef)
{
	for (size_t i = 0; i < 12; i++)
	{
		if (cubeEdgeLineList[i].first == begin_i && cubeEdgeLineList[i].second == end_i)
		{
			lineIndicesIndex = i;
			dirRef = true;
			return;
		}
		else if (cubeEdgeLineList[i].first == end_i && cubeEdgeLineList[i].second == begin_i)
		{
			lineIndicesIndex = i;
			dirRef = false;
			return;
		}
	}
	throw std::invalid_argument("Could not find matching line");
}
eng::Mesh<eng::vertex3f> eng::generate_cubeSphere_mesh3F(size_t detail, float radius)
{
	eng::Mesh<eng::vertex3f> out;

	//if (detail < 2)
	//{
	//	throw std::invalid_argument("detail must be 2 or bigger!");
	//}

	//size_t detailMinusEdge = detail - 2;

	//size_t vertexCount = 8 + detailMinusEdge * 12 + (detailMinusEdge) * (detailMinusEdge) * 6;


	//out.vertices().resize(vertexCount);

	//out.vertices()[0] = eng::vertex3f{ eng::vector3f{ 1.0f, 1.0f, 1.0f } };
	//out.vertices()[1] = eng::vertex3f{ eng::vector3f{ 1.0f, 1.0f,-1.0f } };
	//out.vertices()[2] = eng::vertex3f{ eng::vector3f{ 1.0f,-1.0f, 1.0f } };
	//out.vertices()[3] = eng::vertex3f{ eng::vector3f{ 1.0f,-1.0f,-1.0f } };

	//out.vertices()[4] = eng::vertex3f{ eng::vector3f{ -1.0f, 1.0f, 1.0f } };
	//out.vertices()[5] = eng::vertex3f{ eng::vector3f{ -1.0f, 1.0f,-1.0f } };
	//out.vertices()[6] = eng::vertex3f{ eng::vector3f{ -1.0f,-1.0f, 1.0f } };
	//out.vertices()[7] = eng::vertex3f{ eng::vector3f{ -1.0f,-1.0f,-1.0f } };

	//size_t write_head = 8;

	//size_t cornerIndices[24] =
	//{
	//	0,1,2,3, // +x
	//	4,6,5,7, // -x

	//	0,4,1,5, // +y
	//	2,3,6,7, // -y

	//	0,2,4,6, // +z
	//	1,5,3,7  // -z
	//};

	//lineIndices edge_Line_List[12] =
	//{
	//	{ 0,1 },{ 2,3 },
	//	{ 0,2 },{ 1,3 },

	//	{ 0,4 },
	//	{ 1,5 },
	//	{ 2,6 },
	//	{ 3,7 },

	//	{ 4,5 },{ 6,7 },
	//	{ 4,6 },{ 5,7 }
	//};

	//size_t edge_vertices_begin[12];


	//for (size_t edge_i = 0; edge_i < 12; edge_i++)
	//{
	//	Eigen::Vector3f edge_begin = out.triangles()[edge_Line_List[edge_i].first].position;

	//	Eigen::Vector3f edge_end = out.triangles()[edge_Line_List[edge_i].second].position;

	//	writeEdge(out.vertices().begin() + write_head, edge_begin, edge_end, detail);

	//	edge_vertices_begin[edge_i] = write_head;

	//	write_head += detailMinusEdge;
	//}

	//for (size_t side_i = 0; side_i < 6; side_i++)
	//{
	//	size_t current_side_corner_indices[4] =
	//	{
	//		cornerIndices[side_i * 4],
	//		cornerIndices[side_i * 4 + 1],
	//		cornerIndices[side_i * 4 + 2],
	//		cornerIndices[side_i * 4 + 3]
	//	};
	//	Eigen::Vector3f current_side_corners[4] =
	//	{
	//		out.vertices()[current_side_corner_indices[0]].position,
	//		out.vertices()[current_side_corner_indices[1]].position,
	//		out.vertices()[current_side_corner_indices[2]].position,
	//		out.vertices()[current_side_corner_indices[3]].position
	//	};

	//	std::pair<size_t, bool> edge_Index_Indices[4];
	//	findLineBeginIndexAndDir(edge_Line_List, current_side_corner_indices[0], current_side_corner_indices[1], edge_Index_Indices[0].first, edge_Index_Indices[0].second);
	//	findLineBeginIndexAndDir(edge_Line_List, current_side_corner_indices[0], current_side_corner_indices[2], edge_Index_Indices[1].first, edge_Index_Indices[1].second);
	//	findLineBeginIndexAndDir(edge_Line_List, current_side_corner_indices[2], current_side_corner_indices[3], edge_Index_Indices[2].first, edge_Index_Indices[2].second);
	//	findLineBeginIndexAndDir(edge_Line_List, current_side_corner_indices[1], current_side_corner_indices[3], edge_Index_Indices[3].first, edge_Index_Indices[3].second);

	//	for (size_t i = 0; i < 4; i++)
	//	{
	//		if (edge_Index_Indices[i].second)
	//		{
	//			edge_Index_Indices[i].first = edge_vertices_begin[edge_Index_Indices[i].first];
	//		}
	//		else
	//		{
	//			edge_Index_Indices[i].first = edge_vertices_begin[edge_Index_Indices[i].first] + detailMinusEdge - 1;
	//		}
	//	}



	//	writeFace(out.vertices().begin() + write_head, current_side_corners[0], current_side_corners[1], current_side_corners[2], current_side_corners[3], detail);



	//	{
	//		size_t AB_Line_begin = edge_Index_Indices[0].first;


	//		out.indices().push_back(static_cast<eng::index_type>(AB_Line_begin));
	//		out.indices().push_back(static_cast<eng::index_type>(write_head));
	//		out.indices().push_back(static_cast<eng::index_type>(current_side_corner_indices[0]));


	//		if (edge_Index_Indices[0].second)
	//		{
	//			for (size_t u = 0; u < detailMinusEdge - 1; u++)
	//			{
	//				out.indices().push_back(static_cast<eng::index_type>(write_head + u + 1));
	//				out.indices().push_back(static_cast<eng::index_type>(AB_Line_begin + u));
	//				out.indices().push_back(static_cast<eng::index_type>(AB_Line_begin + u + 1));

	//				out.indices().push_back(static_cast<eng::index_type>(write_head + u + 1));
	//				out.indices().push_back(static_cast<eng::index_type>(write_head + u));
	//				out.indices().push_back(static_cast<eng::index_type>(AB_Line_begin + u));
	//			}
	//		}
	//		else
	//		{
	//			for (size_t u = 0; u < detailMinusEdge - 1; u++)
	//			{
	//				out.indices().push_back(static_cast<eng::index_type>(write_head + u + 1));
	//				out.indices().push_back(static_cast<eng::index_type>(AB_Line_begin - u));
	//				out.indices().push_back(static_cast<eng::index_type>(AB_Line_begin - u + 1));

	//				out.indices().push_back(static_cast<eng::index_type>(write_head + u + 1));
	//				out.indices().push_back(static_cast<eng::index_type>(write_head + u));
	//				out.indices().push_back(static_cast<eng::index_type>(AB_Line_begin - u));
	//			}
	//		}


	//		out.indices().push_back(static_cast<eng::index_type>(AB_Line_begin + detailMinusEdge - 1));
	//		out.indices().push_back(static_cast<eng::index_type>(current_side_corner_indices[1]));
	//		out.indices().push_back(static_cast<eng::index_type>(write_head + detailMinusEdge - 1));
	//	}

	//	{
	//		size_t AC_Line_i = edge_Index_Indices[1].first;
	//		size_t BD_Line_i = edge_Index_Indices[3].first;

	//		out.indices().push_back(static_cast<eng::index_type>(AC_Line_i));
	//		out.indices().push_back(static_cast<eng::index_type>(current_side_corner_indices[0]));
	//		out.indices().push_back(static_cast<eng::index_type>(write_head));

	//		out.indices().push_back(static_cast<eng::index_type>(BD_Line_i));
	//		out.indices().push_back(static_cast<eng::index_type>(write_head + detailMinusEdge - 1));
	//		out.indices().push_back(static_cast<eng::index_type>(current_side_corner_indices[1]));

	//		for (size_t v = 0; v < detailMinusEdge - 1; v++)
	//		{
	//			size_t next_AC_Line_i;
	//			size_t next_BD_Line_i;

	//			if (edge_Index_Indices[1].second)
	//			{
	//				next_AC_Line_i = AC_Line_i + 1;
	//			}
	//			else
	//			{
	//				next_AC_Line_i = AC_Line_i - 1;
	//			}

	//			if (edge_Index_Indices[3].second)
	//			{
	//				next_BD_Line_i = BD_Line_i + 1;
	//			}
	//			else
	//			{
	//				next_BD_Line_i = BD_Line_i - 1;
	//			}

	//			out.indices().push_back(static_cast<eng::index_type>(write_head + (v + 1) * detailMinusEdge));
	//			out.indices().push_back(static_cast<eng::index_type>(AC_Line_i));
	//			out.indices().push_back(static_cast<eng::index_type>(write_head + v * detailMinusEdge));


	//			out.indices().push_back(static_cast<eng::index_type>(AC_Line_i));
	//			out.indices().push_back(static_cast<eng::index_type>(write_head + (v + 1) * detailMinusEdge));
	//			out.indices().push_back(static_cast<eng::index_type>(next_AC_Line_i));



	//			for (size_t u = 0; u < detailMinusEdge - 1; u++)
	//			{
	//				out.indices().push_back(static_cast<eng::index_type>(write_head + (v + 1) * detailMinusEdge + u + 1));
	//				out.indices().push_back(static_cast<eng::index_type>(write_head + v * detailMinusEdge + u));
	//				out.indices().push_back(static_cast<eng::index_type>(write_head + v * detailMinusEdge + u + 1));

	//				out.indices().push_back(static_cast<eng::index_type>(write_head + v * detailMinusEdge + u));
	//				out.indices().push_back(static_cast<eng::index_type>(write_head + (v + 1) * detailMinusEdge + u + 1));
	//				out.indices().push_back(static_cast<eng::index_type>(write_head + (v + 1) * detailMinusEdge + u));
	//			}


	//			out.indices().push_back(static_cast<eng::index_type>(next_BD_Line_i));
	//			out.indices().push_back(static_cast<eng::index_type>(write_head + (v + 1) * detailMinusEdge + detailMinusEdge - 1));
	//			out.indices().push_back(static_cast<eng::index_type>(BD_Line_i));

	//			out.indices().push_back(static_cast<eng::index_type>(write_head + (v + 1) * detailMinusEdge - 1));
	//			out.indices().push_back(static_cast<eng::index_type>(BD_Line_i));
	//			out.indices().push_back(static_cast<eng::index_type>(write_head + (v + 2) * detailMinusEdge - 1));


	//			AC_Line_i = next_AC_Line_i;
	//			BD_Line_i = next_BD_Line_i;
	//		}

	//		write_head += detailMinusEdge * (detailMinusEdge - 1);

	//		out.indices().push_back(static_cast<eng::index_type>(AC_Line_i));
	//		out.indices().push_back(static_cast<eng::index_type>(write_head));
	//		out.indices().push_back(static_cast<eng::index_type>(current_side_corner_indices[2]));

	//		out.indices().push_back(static_cast<eng::index_type>(BD_Line_i));
	//		out.indices().push_back(static_cast<eng::index_type>(current_side_corner_indices[3]));
	//		out.indices().push_back(static_cast<eng::index_type>(write_head + detailMinusEdge - 1));
	//	}

	//	{
	//		size_t CD_Line_begin = edge_Index_Indices[2].first;

	//		out.indices().push_back(static_cast<eng::index_type>(CD_Line_begin));
	//		out.indices().push_back(static_cast<eng::index_type>(current_side_corner_indices[2]));
	//		out.indices().push_back(static_cast<eng::index_type>(write_head));

	//		if (edge_Index_Indices[2].second)
	//		{
	//			for (size_t u = 0; u < detailMinusEdge - 1; u++)
	//			{
	//				out.indices().push_back(static_cast<eng::index_type>(write_head + u + 1));
	//				out.indices().push_back(static_cast<eng::index_type>(CD_Line_begin + u + 1));
	//				out.indices().push_back(static_cast<eng::index_type>(CD_Line_begin + u));

	//				out.indices().push_back(static_cast<eng::index_type>(write_head + u + 1));
	//				out.indices().push_back(static_cast<eng::index_type>(CD_Line_begin + u));
	//				out.indices().push_back(static_cast<eng::index_type>(write_head + u));
	//			}
	//		}
	//		else
	//		{
	//			for (size_t u = 0; u < detailMinusEdge - 1; u++)
	//			{
	//				out.indices().push_back(static_cast<eng::index_type>(write_head + u + 1));
	//				out.indices().push_back(static_cast<eng::index_type>(CD_Line_begin - u + 1));
	//				out.indices().push_back(static_cast<eng::index_type>(CD_Line_begin - u));

	//				out.indices().push_back(static_cast<eng::index_type>(write_head + u + 1));
	//				out.indices().push_back(static_cast<eng::index_type>(CD_Line_begin - u));
	//				out.indices().push_back(static_cast<eng::index_type>(write_head + u));
	//			}
	//		}

	//		out.indices().push_back(static_cast<eng::index_type>(CD_Line_begin + detailMinusEdge - 1));
	//		out.indices().push_back(static_cast<eng::index_type>(write_head + detailMinusEdge - 1));
	//		out.indices().push_back(static_cast<eng::index_type>(current_side_corner_indices[3]));

	//	}
	//	write_head += detailMinusEdge;
	//}

	//for (size_t i = 0; i < out.vertices().size(); i++)
	//{
	//	out.vertices()[i].position = normalize(out.vertices()[i].position);
	//	out.vertices()[i].normal = out.vertices()[i].position;
	//	out.vertices()[i].extrant = out.vertices()[i].position;
	//	out.vertices()[i].position *= radius;
	//}

	return out;
}
