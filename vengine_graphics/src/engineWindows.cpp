#include <eng/engineWindows.h>
#include <regex>


HINSTANCE engine_hInstance;
std::wstring applicationPath;
std::wstring applicationDir;

void eng::windows::engineFromWinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow)
{
	engine_hInstance = hInstance;

	WCHAR path[MAX_PATH];

	DWORD length = GetModuleFileNameW(hInstance, path, MAX_PATH);

	applicationPath = std::wstring(path);

	std::wregex fileDirRegex(L".(.*)\\\\");


	std::wsmatch m;

	bool found = std::regex_search(applicationPath, m, fileDirRegex);
	if (!found)
	{
		throw std::exception("error initializing eng framework: application dir not found");
	}
	applicationDir = m[0].str();
}

HINSTANCE eng::windows::getApplicationHInstance()
{
	return engine_hInstance;
}

std::wstring eng::windows::getApplicationPath()
{
	return applicationPath;
}

std::wstring eng::windows::getApplicationDirectory()
{
	return applicationDir;
}
