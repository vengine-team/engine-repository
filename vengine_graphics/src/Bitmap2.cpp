#include <eng/Bitmap2.h>
#include <png.h>
#include <assert.h>

gtm::vector<size_t, 2> eng::Bitmap2::scale() const
{
	return m_scale;
}

const eng::color& eng::Bitmap2::pixel(const gtm::vector<size_t, 2>& pos) const
{
	return m_pixels[m_scale[0] * pos[1] + pos[0]];
}
eng::color& eng::Bitmap2::pixel(const gtm::vector<size_t, 2>& pos)
{
	return m_pixels[m_scale[0] * pos[1] + pos[0]];
}

const std::vector<eng::color>& eng::Bitmap2::pixels() const
{
	return m_pixels;
}

void eng::Bitmap2::copy_to_Position(gtm::vector<signed long long, 2> pos, const Bitmap2& source)
{
	if (pos[0] >= static_cast<signed long long>(m_scale[0]) || pos[1] >= static_cast<signed long long>(m_scale[1]))
	{
		return;
	}

	gtm::vector<size_t, 2> beginPositionInSource;
	if (pos[0] < 0)
	{
		beginPositionInSource[0] = -pos[0];
	}
	else
	{
		beginPositionInSource[0] = 0;
	}
	if (pos[1] < 0)
	{
		beginPositionInSource[1] = -pos[1];
	}
	else
	{
		beginPositionInSource[1] = 0;
	}



	gtm::vector<size_t, 2> endSizeInSource;

	if (pos[0] + static_cast<signed long long>(source.scale()[0]) > static_cast<signed long long>(scale()[0]))
	{
		endSizeInSource[0] = scale()[0] - pos[0];
	}
	else
	{
		endSizeInSource[0] = source.scale()[0];
	}
	if (pos[1] + static_cast<signed long long>(source.scale()[1]) > static_cast<signed long long>(scale()[1]))
	{
		endSizeInSource[1] = scale()[1] - pos[1];
	}
	else
	{
		endSizeInSource[1] = source.scale()[1];
	}



	for (size_t source_x_i = beginPositionInSource[0]; source_x_i < endSizeInSource[0]; source_x_i++)
	{
		size_t this_x = source_x_i + pos[0];

		for (size_t source_y_i = beginPositionInSource[1]; source_y_i < endSizeInSource[1]; source_y_i++)
		{
			size_t this_y = source_y_i + pos[1];

			pixel({ this_x,this_y }) = source.pixel({ source_x_i, source_y_i });
		}
	}
}


eng::Bitmap2::Bitmap2(size_t height, const std::vector<color>& pixels)
{
	m_scale = { static_cast<unsigned int>(pixels.size()) / height, height };

	m_pixels = pixels;
}
eng::Bitmap2::Bitmap2(size_t height, std::vector<color>&& pixels)
{
	m_scale = { static_cast<unsigned int>(pixels.size()) / height, height };

	m_pixels = pixels;
}
eng::Bitmap2::Bitmap2(gtm::vector<size_t, 2> scale, color _color)
{
	m_scale = scale;

	m_pixels.resize(scale[0] * scale[1], _color);
}
eng::Bitmap2::Bitmap2(gtm::vector<size_t, 2> scale)
{
	m_scale = scale;
	m_pixels.resize(scale[0] * scale[1]);
}


eng::Bitmap2& eng::Bitmap2::operator=(const Bitmap2 & source)
{
	m_scale = source.m_scale;
	m_pixels = source.m_pixels;

	return *this;
}
eng::Bitmap2& eng::Bitmap2::operator=(Bitmap2 && source)
{
	m_scale = source.m_scale;
	m_pixels = std::move(source.m_pixels);

	source.m_scale = { 0, 0 };
	source.m_pixels = {};

	return *this;
}

eng::Bitmap2::Bitmap2()
{
	m_scale = { 0, 0 };
	m_pixels = {};
}
eng::Bitmap2::Bitmap2(const Bitmap2& source)
{
	m_scale = source.m_scale;
	m_pixels = source.m_pixels;
}
eng::Bitmap2::Bitmap2(Bitmap2&& source)
{
	m_scale = source.m_scale;
	m_pixels = std::move(source.m_pixels);

	source.m_scale = { 0, 0 };
	source.m_pixels = {};
}

eng::Bitmap2::~Bitmap2() noexcept
{

}


eng::Bitmap2 eng::load_png(std::wstring path)
{
	FILE* pFile = nullptr;

	_wfopen_s(&pFile, path.c_str(), L"rb");

	if (!pFile)
	{
		throw std::exception("File could not be opened");
	}
	// access libpng
	else
	{
		png_structp pRead_Struct = png_create_read_struct(PNG_LIBPNG_VER_STRING, (png_voidp)nullptr, nullptr, nullptr);
		if (!pRead_Struct)
		{
			assert(0);
		}


		png_infop pInfo_Struct = png_create_info_struct(pRead_Struct);
		if (!pInfo_Struct)
		{
			png_destroy_read_struct(&pRead_Struct, (png_infopp)nullptr, (png_infopp)nullptr);
			assert(0);
		}

		//read png
		{
			png_init_io(pRead_Struct, pFile);


			png_read_png(pRead_Struct, pInfo_Struct, 0, nullptr);

			png_uint_32 image_height = png_get_image_height(pRead_Struct, pInfo_Struct);
			png_uint_32 image_width = png_get_image_width(pRead_Struct, pInfo_Struct);

			png_byte channelCount = png_get_channels(pRead_Struct, pInfo_Struct);
			png_byte bitDepth = png_get_bit_depth(pRead_Struct, pInfo_Struct);

			png_size_t rowBytes = png_get_rowbytes(pRead_Struct, pInfo_Struct);
			png_size_t pixelSize = rowBytes / image_width;

			png_bytepp row_pointers_array = png_get_rows(pRead_Struct, pInfo_Struct);

			std::vector<eng::color> out_vector(image_width * image_height);



			if (pixelSize == 4)
			{
				size_t out_pixel_pos = 0;

				for (size_t y = 0; y < image_height; y++)
				{
					png_bytep pRow = row_pointers_array[y];

					for (size_t x = 0; x < image_width; x++)
					{
						png_bytep pPixel = pRow + x * pixelSize;

						out_vector[out_pixel_pos].r = *pPixel;
						out_vector[out_pixel_pos].g = *(pPixel + 1);
						out_vector[out_pixel_pos].b = *(pPixel + 2);
						out_vector[out_pixel_pos].a = *(pPixel + 3);

						out_pixel_pos++;
					}
				}
			}
			else if (pixelSize == 3)
			{
				size_t out_pixel_pos = 0;

				for (size_t y = 0; y < image_height; y++)
				{
					png_bytep pRow = row_pointers_array[y];

					for (size_t x = 0; x < image_width; x++)
					{
						png_bytep pPixel = pRow + x * pixelSize;

						out_vector[out_pixel_pos].r = *pPixel;
						out_vector[out_pixel_pos].g = *(pPixel + 1);
						out_vector[out_pixel_pos].b = *(pPixel + 2);
						out_vector[out_pixel_pos].a = 255;

						out_pixel_pos++;
					}
				}
			}
			else
			{
				throw std::exception("png format is not supported");
			}

			return eng::Bitmap2(static_cast<unsigned int>(image_height), std::move(out_vector));
		}

		png_infop pEnd_info_struct = png_create_info_struct(pRead_Struct);
		if (!pEnd_info_struct)
		{
			png_destroy_read_struct(&pRead_Struct, &pInfo_Struct, (png_infopp)nullptr);
		}
		else
		{
			png_destroy_read_struct(&pRead_Struct, &pInfo_Struct, &pEnd_info_struct);
		}

		fclose(pFile);
	}
}
