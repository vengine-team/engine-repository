#include <eng/RenderLayer.h>
#include <assert.h>

eng::RenderLayer::RenderLayer(unsigned int stepIndex)
{
	m_stepIndex = stepIndex;

	//RenderLayerQueue::concurrentRenderStepCreated(this, m_stepIndex);
}

eng::RenderLayer::~RenderLayer()
{
	//RenderLayerQueue::concurrentRenderStepDestroyed(this);
}

unsigned int eng::RenderLayer::getQueueStep() const
{
	return m_stepIndex;
}

void eng::RenderLayer::setQueueStep(unsigned int stepIndex)
{
	m_stepIndex = stepIndex;
	//RenderLayerQueue::concurrentRenderStepOrderChanged(this, m_stepIndex);
}
