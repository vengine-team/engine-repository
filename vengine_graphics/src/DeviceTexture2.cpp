#include <eng/DeviceTexture2.h>
#include <eng/D3D12_Device.h>
#include <assert.h>
#include <d3dx12.h>
#include <eng/windowsUtil.h>

std::tuple<eng::D3D12::texture_gpu_descriptor, std::shared_ptr<eng::D3D12::DeviceResource>> eng::DeviceTexture2::D3D12_get_SRV_and_Sampler() const
{
	return 
	{ 
		{ 
			{ m_pD3D12_DeviceTexture2->dx12_textureDescriptorHeap, m_pD3D12_DeviceTexture2->dx12_textureGPUDescriptorHandle },
			{ m_pD3D12_DeviceTexture2->dx12_samplerDescriptorHeap, m_pD3D12_DeviceTexture2->dx12_samplerGPUDescriptorHandle } 
		},
		{ std::static_pointer_cast<eng::D3D12::DeviceResource>(m_pD3D12_DeviceTexture2) }
	};
}

gtm::vector2u eng::DeviceTexture2::get_scale() const
{
	return m_bitmap.scale();
}

const eng::Bitmap2 & eng::DeviceTexture2::bitmap() const
{
	return m_bitmap;
}

eng::DeviceTexture2::DeviceTexture2(const eng::Bitmap2& bitmap) : 
	m_bitmap(bitmap),
	m_pD3D12_DeviceTexture2(std::make_shared<eng::D3D12::DeviceTexture2>(m_bitmap))
{

}

eng::DeviceTexture2::DeviceTexture2(eng::Bitmap2&& bitmap) : 
	m_bitmap(std::move(bitmap)),
	m_pD3D12_DeviceTexture2(std::make_shared<eng::D3D12::DeviceTexture2>(std::move(m_bitmap)))
{

}

eng::DeviceTexture2::~DeviceTexture2()
{

}
