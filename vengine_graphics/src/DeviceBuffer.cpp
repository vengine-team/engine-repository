#include <eng/DeviceBuffer.h>

std::tuple<eng::D3D12::resource_gpu_descriptor, std::shared_ptr<eng::D3D12::DeviceResource>> eng::DeviceBuffer::D3D12_ConstantBuffer_transaction() const
{
	return 
	{ 
		m_d3d12ConstantBuffers[m_current_bufferIndex]->descriptor(),
		std::static_pointer_cast<eng::D3D12::DeviceResource>(m_d3d12ConstantBuffers[m_current_bufferIndex]) 
	};
}

void eng::DeviceBuffer::store(void* pData, size_t size_in_bytes)
{
	assert(size_in_bytes <= m_size_in_bytes);

	m_current_bufferIndex = (m_current_bufferIndex + 1) % m_d3d12ConstantBuffers.size();

	if (m_d3d12ConstantBuffers[m_current_bufferIndex].use_count() != 1)
	{
		m_d3d12ConstantBuffers[m_current_bufferIndex] = std::make_unique<eng::D3D12::DeviceConstantBuffer>(m_size_in_bytes);
	}

	memcpy(m_d3d12ConstantBuffers[m_current_bufferIndex]->mappedBuffer(), pData, size_in_bytes);
}

eng::DeviceBuffer::DeviceBuffer(size_t size_in_bytes) : m_size_in_bytes(size_in_bytes)
{
	m_d3d12ConstantBuffers[m_current_bufferIndex] = std::make_unique<eng::D3D12::DeviceConstantBuffer>(size_in_bytes);
}

eng::DeviceBuffer::DeviceBuffer(DeviceBuffer && other)
{
	m_d3d12ConstantBuffers = std::move(other.m_d3d12ConstantBuffers);
	m_current_bufferIndex = other.m_current_bufferIndex;

	m_size_in_bytes = other.m_size_in_bytes;

	other.m_current_bufferIndex = 0;
}

eng::DeviceBuffer & eng::DeviceBuffer::operator=(DeviceBuffer && other)
{
	m_d3d12ConstantBuffers = std::move(other.m_d3d12ConstantBuffers);
	m_current_bufferIndex = other.m_current_bufferIndex;

	m_size_in_bytes = other.m_size_in_bytes;

	other.m_current_bufferIndex = 0;

	return *this;
}

eng::DeviceBuffer::~DeviceBuffer()
{

}
