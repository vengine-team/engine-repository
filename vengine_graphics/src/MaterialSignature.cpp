#include <eng/MaterialSignature.h>

size_t eng::RootSignature::parameterCount() const
{
	return m_numParameters;
}

const eng::RootParameter_Desc & eng::RootSignature::getRootParameter(size_t parameterIndex) const
{
	return reinterpret_cast<const RootParameter_Desc*>(m_pScene.get())[parameterIndex];
}

eng::RootSignature::RootSignature(const RootSignature_Desc& desc)
{
	size_t sizesDescriptorSize = desc.numParameters * sizeof(RootParameter_Desc);
	size_t valuesDescriptorSize = 0;


	for (size_t i = 0; i < desc.numParameters; i++)
	{
		valuesDescriptorSize += desc.pParameters[i].num_shaderResourcesBindings * sizeof(eng::resource_Binding_Desc);
	}
	m_pScene.reset(reinterpret_cast<char*>(std::malloc(sizesDescriptorSize + valuesDescriptorSize)), std_free_deleter{});



	//the bytes for the shader resource types start after the last Parameter size
	m_pResourceIndices_begin = reinterpret_cast<resource_Binding_Desc*>(m_pScene.get() + sizesDescriptorSize);
	m_numParameters = desc.numParameters;



	RootParameter_Desc* sizesDescriptorBegin = reinterpret_cast<RootParameter_Desc*>(m_pScene.get());
	resource_Binding_Desc* value_i_ptr = m_pResourceIndices_begin;
	for (size_t param_i = 0; param_i < desc.numParameters; param_i++)
	{
		//the descriptor is copied, but the pShaderResourcesBindings field is changed to the begin of the copied array
		sizesDescriptorBegin[param_i].shaderResourceType = desc.pParameters[param_i].shaderResourceType;

		sizesDescriptorBegin[param_i].num_shaderResourcesBindings = desc.pParameters[param_i].num_shaderResourcesBindings;
		sizesDescriptorBegin[param_i].pShaderResourcesBindings = value_i_ptr;

		for (size_t shaderResource_i = 0; shaderResource_i < desc.pParameters[param_i].num_shaderResourcesBindings; shaderResource_i++)
		{
			value_i_ptr[shaderResource_i] = desc.pParameters[param_i].pShaderResourcesBindings[shaderResource_i];
		}

		value_i_ptr += desc.pParameters[param_i].num_shaderResourcesBindings;
	}
}
eng::RootSignature::RootSignature(const RootSignature& source) : m_pScene(source.m_pScene)
{
	m_pResourceIndices_begin = source.m_pResourceIndices_begin;
	m_numParameters = source.m_numParameters;
}
eng::RootSignature::RootSignature(RootSignature&& source) noexcept : m_pScene(std::move(source.m_pScene))
{
	m_pResourceIndices_begin = source.m_pResourceIndices_begin;
	m_numParameters = source.m_numParameters;

	source.m_numParameters = 0;
}

std::string std::to_string(const eng::RootSignature & value)
{
	std::string out;
	out.append(std::to_string(value.parameterCount()));
	out.append(" parameters: ");

	for (size_t p_i = 0; p_i < value.parameterCount(); p_i++)
	{
		const eng::resource_Binding_Desc* pResources = value.getRootParameter(p_i).pShaderResourcesBindings;

		out.append("{ ");
		for (size_t r_i = 0; r_i < value.getRootParameter(p_i).num_shaderResourcesBindings; r_i++)
		{
			out.append("{ ");
			out.append(std::to_string(pResources[r_i].render_pass_index));
			out.append(", ");
			out.append(std::to_string(pResources[r_i].resource_index));
			out.append(" } ");
		}
		out.append("}, ");
	}

	return out;
}

std::wstring std::to_wstring(const eng::RootSignature & value)
{
	std::wstring out;
	out.append(std::to_wstring(value.parameterCount()));
	out.append(L" parameters: ");

	for (size_t p_i = 0; p_i < value.parameterCount(); p_i++)
	{
		const eng::resource_Binding_Desc* pResources = value.getRootParameter(p_i).pShaderResourcesBindings;

		out.append(L"{ ");
		for (size_t r_i = 0; r_i < value.getRootParameter(p_i).num_shaderResourcesBindings; r_i++)
		{
			out.append(L"{ ");
			out.append(std::to_wstring(pResources[r_i].render_pass_index));
			out.append(L", ");
			out.append(std::to_wstring(pResources[r_i].resource_index));
			out.append(L" } ");
		}
		out.append(L"}, ");
	}

	return out;
}

void eng::RootSignature::std_free_deleter::operator()(char * pCharArray)
{
	std::free(pCharArray);
}
