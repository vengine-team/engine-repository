#include <eng/windowsUtil.h>
#include <comdef.h>
#include <eng/engUtil.h>
#include <eng/D3D12_Device.h>
#include <assert.h>

void AssertIfFailed(HRESULT hr, std::string message)
{
	if (!SUCCEEDED(hr))
	{
		_com_error error(hr);

		std::wstring out = string_to_wstring(message);
		out.append(L" ; ");
		out.append(error.ErrorMessage());

		HRESULT hr2 = eng::D3D12::instance().getDevice()->GetDeviceRemovedReason();

		_com_error error2(hr2);

		std::wstring s = error2.ErrorMessage();

		OutputDebugString(out.c_str());

		assert(false);
	}
}