#include <eng/Window.h>
#include <windowsx.h>
#include <eng/engUtil.h>
#include <dwmapi.h>


void eng::windows::MessageLoopThread::thread_function()
{
	MSG msg;

	while (!m_cancelThread.load())// new Message
	{
		try
		{
			mt_taskQueue.process();
		}
		catch (const std::exception&)
		{
			assert(0);
		}

		while (PeekMessage(&msg, nullptr, 0, 0, PM_REMOVE))
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
		std::this_thread::sleep_for(std::chrono::microseconds(10));
	}
}

void eng::windows::MessageLoopThread::initialize()
{
	if (m_initialized == true)
	{
		assert(false);
	}
	m_initialized = true;
	m_thread = std::thread(&eng::windows::MessageLoopThread::thread_function, this);
}

eng::TaskQueue & eng::windows::MessageLoopThread::task_queue()
{
	return mt_taskQueue;
}

eng::windows::MessageLoopThread::MessageLoopThread()
{
	m_cancelThread.store(false);
	initialize();
}
eng::windows::MessageLoopThread::~MessageLoopThread()
{
	if (m_initialized)
	{
		m_cancelThread.store(true);
		m_thread.join();
	}
}






void waitForValue(const std::atomic<bool>& flag, bool expectedValue)
{
	for (size_t i = 0; i < 100000 && flag.load() != expectedValue; i++);

	while (flag.load() != expectedValue)
	{
		std::this_thread::sleep_for(std::chrono::microseconds(10));
	}
}


LRESULT CALLBACK eng::Window::WndProc_Ptr_Callback(HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	//return DefWindowProc(hwnd, message, wParam, lParam);

	LRESULT result = 0;

	//register pointer to IOWindow object, if new window has been created
	if (message == WM_CREATE)
	{

		LPCREATESTRUCT pcs = (LPCREATESTRUCT)lParam;
		eng::Window* wndThreadPtr_ptr = reinterpret_cast<eng::Window*>(pcs->lpCreateParams);

		LONG_PTR long_ptr = reinterpret_cast<LONG_PTR>(wndThreadPtr_ptr);

		SetWindowLongPtrW(hwnd, GWLP_USERDATA, long_ptr);

		result = 1;
	}
	//sends the call to an existing object
	else
	{
		LONG_PTR longptr = GetWindowLongPtrW(hwnd, GWLP_USERDATA);

		eng::Window *pWindow = reinterpret_cast<eng::Window*>(longptr);

		if (pWindow != nullptr)
		{
			result = pWindow->wndProc(hwnd, message, wParam, lParam);
		}
		else
		{
			result = DefWindowProc(hwnd, message, wParam, lParam);
		}
	}

	return result;
}
LRESULT eng::Window::wndProc(HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	//if the windowProc Thread interacts with m_changesFromOS, it must never wait for the owner thread of this
	//the owner thread always has to wait for the windowProcThread if it interacts
	LRESULT out;
	switch (message)
	{
	break;
	case WM_KEYDOWN:
	{
		std::scoped_lock<std::mutex> lock(m_fetchMutex);

		const eng::input_state& latest_state = eng::getLatestMouseState(m_input_desc);

		eng::input_state_change current_change;
		current_change.type = input_type::Keyboard;
		current_change.changed_keycode = static_cast<size_t>(wParam);
		current_change.before = latest_state;
		current_change.current = current_change.before;
		current_change.current.keyboard.keyStates[wParam] = true;

		m_input_desc.mouseStateChanges.push_back(current_change);

		out = 0;
	}
	break;
	case WM_KEYUP:
	{
		std::scoped_lock<std::mutex> lock(m_fetchMutex);

		const eng::input_state& latest_state = eng::getLatestMouseState(m_input_desc);

		eng::input_state_change current_change;
		current_change.type = input_type::Keyboard;
		current_change.changed_keycode = static_cast<size_t>(wParam);
		current_change.before = latest_state;
		current_change.current = current_change.before;
		current_change.current.keyboard.keyStates[wParam] = false;


		m_input_desc.mouseStateChanges.push_back(current_change);

		out = 0;
	}
	break;
	case WM_CHAR:
	{
		char c = static_cast<char>(wParam);

		std::scoped_lock<std::mutex> lock(m_fetchMutex);

		m_input_desc.compositionString.push_back(c);

		out = 0;
	}
	break;
	case WM_MOUSEMOVE:
	{
		std::scoped_lock<std::mutex> lock(m_fetchMutex);

		const eng::input_state& latest_state = eng::getLatestMouseState(m_input_desc);

		eng::input_state_change current_change;
		current_change.type = input_type::Mouse;
		current_change.mouse_property = mouse_property::Position;
		current_change.before = latest_state;
		current_change.current = current_change.before;
		current_change.current.mouse.position = { GET_X_LPARAM(lParam), GET_Y_LPARAM(lParam) };

		m_input_desc.mouseStateChanges.push_back(current_change);

		out = 0;
	}
	break;
#pragma region InputHandeling
	case WM_LBUTTONDOWN:
	{
		std::scoped_lock<std::mutex> lock(m_fetchMutex);

		const eng::input_state& latest_state = eng::getLatestMouseState(m_input_desc);

		eng::input_state_change current_change;
		current_change.type = input_type::Mouse;
		current_change.mouse_property = mouse_property::LeftButton;
		current_change.before = latest_state;
		current_change.current = current_change.before;
		current_change.current.mouse.left_button = true;

		m_input_desc.mouseStateChanges.push_back(current_change);

		out = 0;
	}
	break;
	case WM_LBUTTONUP:
	{
		std::scoped_lock<std::mutex> lock(m_fetchMutex);

		const eng::input_state& latest_state = eng::getLatestMouseState(m_input_desc);

		eng::input_state_change current_change;
		current_change.type = input_type::Mouse;
		current_change.mouse_property = mouse_property::LeftButton;
		current_change.before = latest_state;
		current_change.current = current_change.before;
		current_change.current.mouse.left_button = false;

		m_input_desc.mouseStateChanges.push_back(current_change);

		out = 0;
	}
	break;
	case WM_RBUTTONDOWN:
	{
		std::scoped_lock<std::mutex> lock(m_fetchMutex);

		const eng::input_state& latest_state = eng::getLatestMouseState(m_input_desc);

		eng::input_state_change current_change;
		current_change.type = input_type::Mouse;
		current_change.mouse_property = mouse_property::RightButton;
		current_change.before = latest_state;
		current_change.current = current_change.before;
		current_change.current.mouse.right_button = true;

		m_input_desc.mouseStateChanges.push_back(current_change);

		out = 0;
	}
	break;
	case WM_RBUTTONUP:
	{
		std::scoped_lock<std::mutex> lock(m_fetchMutex);

		const eng::input_state& latest_state = eng::getLatestMouseState(m_input_desc);

		eng::input_state_change current_change;
		current_change.type = input_type::Mouse;
		current_change.mouse_property = mouse_property::RightButton;
		current_change.before = latest_state;
		current_change.current = current_change.before;
		current_change.current.mouse.right_button = false;

		m_input_desc.mouseStateChanges.push_back(current_change);

		out = 0;
	}
	break;
#pragma endregion
	case WM_CLOSE:
	{
		std::scoped_lock<std::mutex> lock(m_fetchMutex);

		m_mlt_request_close = true;
		out = 0;
	}
	break;
	case WM_NCHITTEST:
	{
		if (true)
		{

			if (m_windowStyle == WindowStyle::Borderless)
			{
				POINT cursor;

				if (GetCursorPos(&cursor))
				{
					if (ScreenToClient(hwnd, &cursor))
					{

						RECT windowRect;
						GetWindowRect(hwnd, &windowRect);

						const LONG borderSize = 6;
						const LONG edgeAreaMul = 2;

						const LONG windowWidth = windowRect.right - windowRect.left;
						const LONG windowHeight = windowRect.bottom - windowRect.top;

						if (cursor.x < borderSize) // left
						{
							if (cursor.y > windowHeight - borderSize * edgeAreaMul)
							{
								return HTBOTTOMLEFT;
							}
							else if (cursor.y < borderSize * edgeAreaMul)
							{
								return HTTOPLEFT;
							}
							else
							{
								return HTLEFT;
							}
						}
						else if (cursor.x > windowWidth - borderSize) // right
						{
							if (cursor.y > windowHeight - borderSize * edgeAreaMul)
							{
								return HTBOTTOMRIGHT;
							}
							else if (cursor.y < borderSize * edgeAreaMul)
							{
								return HTTOPRIGHT;
							}
							else
							{
								return HTRIGHT;
							}
						}
						else if (cursor.y < borderSize) // top
						{
							if (cursor.x > windowWidth - borderSize * edgeAreaMul) // right
							{
								return HTTOPRIGHT;
							}
							else if (cursor.x < borderSize * edgeAreaMul) //left
							{
								return HTTOPLEFT;
							}
							else
							{
								return HTTOP;
							}
						}
						else if (cursor.y > windowHeight - borderSize) // bottom
						{
							if (cursor.x > windowWidth - borderSize * edgeAreaMul) // rights
							{
								return HTBOTTOMRIGHT;
							}
							else if (cursor.x < borderSize * edgeAreaMul) //left
							{
								return HTBOTTOMLEFT;
							}
							else
							{
								return HTBOTTOM;
							}
						}
						else
						{
							return HTCLIENT;
						}
					}
				}
			}
			else
			{
				return DefWindowProc(hwnd, message, wParam, lParam);
			}
		}
		else
		{
			out = DefWindowProc(hwnd, message, wParam, lParam);
		}
	}
	break;
	case WM_NCCALCSIZE:
	{
		if (m_windowStyle == WindowStyle::Borderless)
		{
			if (wParam == TRUE)
			{				
				out = FALSE;
			}
			else
			{			

				out = DefWindowProc(hwnd, message, wParam, lParam);
			}
		}
		else
		{
			out = DefWindowProc(hwnd, message, wParam, lParam);
		}
	}
	break;
	case WM_GETMINMAXINFO:
	{
		MINMAXINFO* pMinMaxInfo = reinterpret_cast<MINMAXINFO*>(lParam);

		RECT wndRect;
		GetWindowRect(hwnd, &wndRect);
		RECT clientRect;
		GetClientRect(hwnd, &clientRect);

		pMinMaxInfo->ptMinTrackSize.x = m_minimumSize[0] + ((wndRect.right - wndRect.left) - clientRect.right);
		pMinMaxInfo->ptMinTrackSize.y = m_minimumSize[1] + ((wndRect.bottom - wndRect.top) - clientRect.bottom);

		out = TRUE;
	}
	break;
	case WM_SIZE:
	{
		UINT width = LOWORD(lParam);
		UINT height = HIWORD(lParam);


		std::scoped_lock<std::mutex> lock(m_fetchMutex);

		m_mlt_clientScale = { static_cast<unsigned int>(width),static_cast<unsigned int>(height) };
		if (m_mlt_clientScale[0] < m_minimumSize[0])
		{
			m_mlt_clientScale[0] = m_minimumSize[0];
		}
		if (m_mlt_clientScale[1] < m_minimumSize[1])
		{
			m_mlt_clientScale[1] = m_minimumSize[1];
		}

		out = TRUE;
	}
	break;
	default:
	{
		out = DefWindowProc(hwnd, message, wParam, lParam);
	}
	break;
	}
	return out;
}

void eng::Window::mlt_createWindow()
{
	//setup
	WNDCLASSEX wClassEx;
	wClassEx.cbSize = sizeof(WNDCLASSEX);
	wClassEx.style = CS_HREDRAW | CS_VREDRAW;
	wClassEx.lpfnWndProc = &eng::Window::WndProc_Ptr_Callback;
	wClassEx.cbClsExtra = 0;
	wClassEx.cbWndExtra = sizeof(LONG_PTR);
	wClassEx.hInstance = eng::windows::getApplicationHInstance();
	wClassEx.hIcon = nullptr;
	wClassEx.hCursor = LoadCursor(nullptr, IDC_ARROW);
	wClassEx.hbrBackground = nullptr;
	wClassEx.lpszMenuName = nullptr;
	wClassEx.hIconSm = nullptr;
	if (m_windowTitle.size())
	{
		wClassEx.lpszClassName = m_windowTitle.c_str();
	}
	else
	{
		wClassEx.lpszClassName = L" ";
	}
	DWORD errorb = GetLastError();
	HRESULT hrb = HRESULT_FROM_WIN32(errorb);

	RegisterClassEx(&wClassEx);
	DWORD errort = GetLastError();
	HRESULT hrt = HRESULT_FROM_WIN32(errort);



	int windowStyle = 0;
	{
		//basic style	^/
		switch (m_windowStyle)
		{
		case WindowStyle::Borderless:
			windowStyle = WS_POPUP;
			break;
		case WindowStyle::SystemDefault:
			windowStyle = WS_TILED | WS_CAPTION | WS_BORDER;
			break;
		case WindowStyle::SystemDefaultMenu:
			windowStyle = WS_TILED | WS_CAPTION | WS_BORDER | WS_SYSMENU;
			break;
		default:
			assert(0);
			break;
		};

		//additional stuff
		if (m_sizeAble)
		{
			windowStyle = windowStyle | WS_THICKFRAME | WS_MINIMIZEBOX | WS_MAXIMIZEBOX;
		}
	}

	gtm::vector2u tempSize = m_clientScale;

	

	RECT tempRect = { 0, 0, static_cast<LONG>(tempSize[0]), static_cast<LONG>(tempSize[1]) };
	AdjustWindowRect(&tempRect, windowStyle, FALSE);

	m_hwnd = CreateWindow
	(
		wClassEx.lpszClassName, 
		wClassEx.lpszClassName, 
		windowStyle,
		m_windowPosition[0],
		m_windowPosition[1],
		tempRect.right, 
		tempRect.bottom, 
		nullptr, 
		nullptr, 
		windows::getApplicationHInstance(), 
		this
	);

	if (m_hwnd == nullptr)
	{
		DWORD error = GetLastError();
		HRESULT hr = HRESULT_FROM_WIN32(error);

		MessageBox(nullptr, L"CreateWindowEx() failed", L"Error", MB_OK);
		PostQuitMessage(0);
		return;
	}
}
void eng::Window::mlt_initialize()
{
	ShowWindow(m_hwnd, SW_SHOW);
	UpdateWindow(m_hwnd);
	SetForegroundWindow(m_hwnd);
	SetFocus(m_hwnd);
}
void eng::Window::mlt_close()
{
	ShowWindow(m_hwnd, SW_HIDE);
}
void eng::Window::mlt_setTitle(std::wstring title)
{
	if (title.size())
	{
		SetWindowText(m_hwnd, title.c_str());
	}
	else
	{
		SetWindowText(m_hwnd, title.c_str());
	}

}
void eng::Window::mlt_destroyWindow()
{
	DestroyWindow(m_hwnd);
}
void eng::Window::mlt_setClientSize(gtm::vector2u size)
{

}



ext::event_subscribe_access<eng::Window&> eng::Window::request_close_event()
{
	return { m_request_close_event };
}

bool eng::Window::is_open()
{
	return m_initialized;
}
void eng::Window::open()
{
	if (m_initialized)
	{
		throw std::logic_error("Window is already initialized");
	}
	m_initialized = true;
	m_mlt.task_queue().concurrent_push([&](){eng::Window::mlt_initialize();});
}
void eng::Window::close()
{
	if (!m_initialized)
	{
		throw std::logic_error("Window is already closed");
	}
	m_initialized = false;
	m_mlt.task_queue().concurrent_push([&]() { eng::Window::mlt_close(); });
}


void eng::Window::setTitle(const std::wstring & title)
{
	m_windowTitle = title;
	m_mlt.task_queue().concurrent_push([&]() { eng::Window::mlt_setTitle(m_windowTitle); });
}
std::wstring eng::Window::getTitle() const
{
	return m_windowTitle;
}



eng::WindowStyle::Type eng::Window::getStyle() const
{
	return WindowStyle::Type();
}

bool eng::Window::getSizeable() const
{
	return m_sizeAble;
}



const gtm::vector2u& eng::Window::getClientScale() const
{
	return m_clientScale;
}
//void eng::Window::setClientSize(const Vector2U& sizeU)
//{
//	m_clientScale = sizeU;
//
//	if (!m_callback)
//	{
//		completedFlag flag;
//
//		m_mlt.insertTask(new eng::ConcurrentTask<void(eng::Window::*)(Vector2U)>(&eng::Window::mlt_setClientSize, this, sizeU, &flag));
//
//		waitUntilCompleted(flag);
//	}
//	m_pHwndRenderTarget->setScale(sizeU);
//}



const gtm::vector2i & eng::Window::getWindowPosition() const
{
	return m_windowPosition;
}
//void eng::Window::(const Vector2I & sizeU)
//{
//	assert(0);
//}



eng::RenderTarget & eng::Window::renderTarget()
{
	return *m_pHwndRenderTarget.get();
}
const eng::RenderTarget & eng::Window::renderTarget() const
{
	return *m_pHwndRenderTarget.get();
}

const eng::Input & eng::Window::input() const
{
	return m_input;
}



void eng::Window::fetch()
{
	std::scoped_lock<std::mutex> lock(m_fetchMutex);
	//update input
	{
		m_input = eng::Input(m_input_desc);

		m_input_desc.compositionString = L"";

		m_input_desc.initialState = m_input.latest_state();

		m_input_desc.mouseStateChanges.clear();
	}
	//update client scale
	{
		if (m_mlt_request_close)
		{
			m_mlt_request_close = false;
			m_request_close_event(*this);
		}

		m_clientScale = m_mlt_clientScale;
		m_clientScale[0] = gtm::clamp(m_minimumSize[0], m_maximumSize[0], m_clientScale[0]);
		m_clientScale[1] = gtm::clamp(m_minimumSize[1], m_maximumSize[1], m_clientScale[1]);
		m_pHwndRenderTarget->set_next_scale(m_clientScale);
	}
}

eng::Window::Window(std::wstring title, gtm::vector2i pos, gtm::vector2u size, eng::WindowStyle::Type style, bool sizeAble)
{
	m_initialized = false;

	m_pHwndRenderTarget = nullptr;

	m_windowTitle = title;
	m_windowPosition = pos;
	m_clientScale = size;
	m_windowStyle = style;
	m_sizeAble = sizeAble;

	std::condition_variable condition;
	std::mutex mutex;
	std::unique_lock<std::mutex> unique_lock(mutex);

	m_mlt.task_queue().concurrent_push([&]() { std::scoped_lock<std::mutex> lock(mutex); eng::Window::mlt_createWindow(); condition.notify_one(); });

	condition.wait(unique_lock);

	m_pHwndRenderTarget.reset(RenderTarget::WINAPI_make_renderTarget_for_HWND(m_hwnd, m_clientScale));
}
eng::Window::~Window()
{
	m_pHwndRenderTarget.reset();

	std::condition_variable condition;
	std::mutex mutex;
	std::unique_lock<std::mutex> unique_lock(mutex);

	m_mlt.task_queue().concurrent_push([&]() { std::scoped_lock<std::mutex> lock(mutex); eng::Window::mlt_destroyWindow(); condition.notify_one(); });

	condition.wait(unique_lock);
}

