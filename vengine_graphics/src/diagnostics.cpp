#include <eng/Diagnostics.h>

void eng::diagnostics::TimeRecorder::beginRead()
{
	m_mutex.lock_shared();
}

eng::diagnostics::TimeRecorder::const_iterator eng::diagnostics::TimeRecorder::durations_begin(std::string s)
{
	return m_recorded[s].second.begin();
}

eng::diagnostics::TimeRecorder::const_iterator eng::diagnostics::TimeRecorder::durations_end(std::string s)
{
	return m_recorded[s].second.end();
}

void eng::diagnostics::TimeRecorder::endRead()
{
	m_mutex.unlock_shared();
}

void eng::diagnostics::TimeRecorder::beginStep()
{
	std::scoped_lock<std::shared_mutex> lock(m_mutex);

	for (auto i = m_recorded.begin(); i != m_recorded.begin(); i++)
	{
		if (i->second.first)
		{
			i = m_recorded.erase(i);
		}
		else
		{
			i->second.second[m_currentIndex].duration = std::chrono::steady_clock::duration::zero();
		}
	}
}

void eng::diagnostics::TimeRecorder::endStep()
{
	std::scoped_lock<std::shared_mutex> lock(m_mutex);

	m_currentIndex++;
	if (m_currentIndex == m_maxSteps)
	{
		m_currentIndex = 0;
	}
}

void eng::diagnostics::TimeRecorder::reportBegin(std::string key)
{
	std::scoped_lock<std::shared_mutex> lock(m_mutex);

	auto i = m_recorded.find(key);

	if (i != m_recorded.end())
	{
		i->second.second[m_currentIndex].begin = std::chrono::steady_clock::now();
	}
	else
	{
		std::vector<recorder_pair> in;
		in.resize(m_maxSteps);
		in[m_currentIndex].begin = std::chrono::steady_clock::now();

		m_recorded.insert({ key,{ true, std::move(in) } });
	}
}

std::chrono::steady_clock::duration eng::diagnostics::TimeRecorder::reportEnd(std::string key)
{
	std::scoped_lock<std::shared_mutex> lock(m_mutex);

	auto i = m_recorded.find(key);
	// Cannot report end of a not existing key
	assert((i != m_recorded.end()));

	std::chrono::steady_clock::duration dur = std::chrono::steady_clock::now() - i->second.second[m_currentIndex].begin;

	i->second.second[m_currentIndex].duration += dur;
	
	return dur;
}

eng::diagnostics::InformationWindow::InformationWindow()
{

}
