#pragma once
#include <eng/tsg/ModelMatrix.h>
#include <eng/tsg/Camera3.h>
#include <eng/defaultDraw.h>

#include <ext/ticket_table.h>
#include <ext/range.h>

#include <unordered_map>

namespace eng
{
	namespace tsg
	{
		template<typename target_t, typename object3_t, bool variable_mesh, size_t override_arguments_size>
		class BasicRenderer3;

		template<typename target_t, typename object3_t, bool _variable_mesh, size_t override_arguments_size>
		class renderer_pool final : public IRenderer3<Scene>
		{
			eng::Material3f const* m_pMaterial;
			ext::ticket_table<const target_t*> m_renderers;

			void draw_single_pass_material(const eng::DeviceBuffer& view_projection, size_t argument, eng::RenderTarget& target) const
			{
				struct static_draw_data
				{
					eng::DeviceBuffer const* m_view_projection;
					size_t m_argument;
					eng::RenderTarget* m_target;

					eng::Material3f const * m_material;
					ext::range<size_t const *> m_variable_indices;
				};

				struct draw_data
				{
					eng::DeviceMesh3f const * m_mesh;
					static_draw_data const * m_static_data;
					std::array<void const *, override_arguments_size> m_overrides;

					struct argument_translator
					{
						static_draw_data const * m_static_data;
						std::array<void const *, override_arguments_size> m_overrides;

						void const * operator()(size_t i) const
						{
							eng::Material3f const* material = m_static_data->m_material;

							size_t tag = material->signature().parameters()[i].tag;
							if (tag < override_arguments_size)
							{
								return m_overrides[tag];
							}
							size_t binding = material->signature().render_pass_bindings()[0].parameter_bindings[i];
							return material->bound_resources()[binding];
						}
					};

					using arguments_iterator = ext::translation_iterator<std::vector<size_t>::const_iterator, argument_translator>;
					ext::range<arguments_iterator> m_arguments;

					draw_data(eng::DeviceMesh3f const * mesh, static_draw_data const * static_data, std::array<void const *, override_arguments_size>& overrides) :
						m_mesh(mesh),
						m_static_data(static_data),
						m_overrides(overrides),
						m_arguments
						(
							ext::range<arguments_iterator>
							{
								arguments_iterator{ m_static_data->m_material->signature().render_pass_bindings()[0].parameter_bindings.begin(), argument_translator{ m_static_data, overrides } },
								arguments_iterator{ m_static_data->m_material->signature().render_pass_bindings()[0].parameter_bindings.end(), argument_translator{ m_static_data, overrides } }
							}
						)
					{

					}

					eng::DeviceMesh3f const * mesh() const
					{
						return m_mesh;
					}
					constexpr bool variable_mesh() const
					{
						return _variable_mesh;
					}

					eng::RenderPass3f const * render_pass() const
					{
						return m_static_data->m_material->signature().render_pass_bindings()[0].render_pass.get();
					}
					constexpr bool variable_render_pass() const
					{
						return false;
					}

					ext::range<arguments_iterator> const& arguments() const
					{
						return m_arguments;
					}

					ext::range<const size_t*> const& variable_argument_indices() const
					{
						return m_static_data->m_variable_indices;
					}
				};

				struct translate_to_draw_data
				{
					eng::DeviceMesh3f const* m_mesh;
					static_draw_data const* m_static_data;

					draw_data operator()(const target_t* pRenderer) const
					{
						eng::DeviceMesh3f const* mesh;
						if (_variable_mesh)
						{
							mesh = pRenderer->mesh();
						}
						else
						{
							mesh = m_mesh;
						}
						return { pRenderer->mesh(), m_static_data, pRenderer->overrides(*m_static_data->m_view_projection, m_static_data->m_argument, *m_static_data->m_target) };
					}
				};

				using draw_iterator = ext::translation_iterator<typename ext::ticket_table<const target_t*>::const_iterator, translate_to_draw_data>;

				std::array<size_t, 128> variable_indices_buffer;
				size_t num_override_arguments = 0;

				for (size_t i = 0; i < m_pMaterial->signature().render_pass_bindings()[0].parameter_bindings.size(); i++)
				{
					size_t bound_parameter = m_pMaterial->signature().render_pass_bindings()[0].parameter_bindings[i];
					const size_t tag = m_pMaterial->signature().parameters()[bound_parameter].tag;
					if (tag < override_arguments_size)
					{
						variable_indices_buffer[num_override_arguments] = i;
						num_override_arguments++;
					}
				}

				static_draw_data data;
				data.m_view_projection = &view_projection;
				data.m_argument = argument;
				data.m_target = &target;

				data.m_material = m_pMaterial;
				data.m_variable_indices = { variable_indices_buffer.data(), variable_indices_buffer.data() + num_override_arguments };

				eng::DeviceMesh3f const* mesh = nullptr;
				if (!_variable_mesh)
				{
					if (!m_renderers.empty())
					{
						mesh = (*m_renderers.begin())->mesh();
					}
				}

				draw_iterator begin_draw = { m_renderers.begin(), { mesh, &data } };
				draw_iterator end_draw = { m_renderers.end(), { mesh, &data } };

				target.draw(begin_draw, end_draw);
			}

		public:
			renderer_pool(const eng::Material3f* pMaterial) : m_pMaterial(pMaterial)
			{

			}
			~renderer_pool()
			{

			}

			bool empty() const
			{
				return m_renderers.empty();
			}

			void insert_renderer(const target_t* pRenderer, ext::ticket& _ticket)
			{
				m_renderers.insert({ pRenderer, _ticket });
			}
			void erase_renderer(ext::ticket& _ticket)
			{
				m_renderers.erase(_ticket);
			}

			virtual void draw(const eng::DeviceBuffer& view_projection, size_t argument, eng::RenderTarget& target) const
			{
				if (m_pMaterial->signature().render_pass_bindings().size() == 1)
				{
					draw_single_pass_material(view_projection, argument, target);
				}
				else
				{
					for (auto i = m_renderers.begin(); i != m_renderers.end(); i++)
					{
						std::array<const void*, override_arguments_size> overrides = (*i)->overrides(view_projection, argument, target);
						auto draw_range = eng::draw_mesh((*i)->mesh(), m_pMaterial, overrides.begin(), overrides.end());
						target.draw(draw_range.begin(), draw_range.end());
					}
				}
			}
		};

		template<typename target_t, typename object3_t, bool variable_mesh, size_t override_arguments_size>
		class renderer_pool_module final : public eng::make_module<renderer_pool_module<target_t, object3_t, variable_mesh, override_arguments_size>, Scene>
		{
			using renderer_pool_t = renderer_pool<target_t, object3_t, variable_mesh, override_arguments_size>;

			struct renderer_pool_ticket_pair
			{
				renderer_pool_t renderer_pool;
				ext::ticket ticket;

				renderer_pool_ticket_pair(const eng::Material3f* pMaterial) : renderer_pool(pMaterial)
				{

				}
			};

			std::unordered_map<const eng::Material3f*, std::unique_ptr<renderer_pool_ticket_pair>> m_pools;

		public:

			void insert_renderer(const target_t* pRenderer, const eng::Material3f* pMaterial, ext::ticket& _ticket)
			{
				auto[it, succeed] = m_pools.insert({ pMaterial, std::make_unique<renderer_pool_ticket_pair>(pMaterial) });

				if (succeed)
				{
					RenderQueue_Module<Scene>::get(this->modular()).addRenderer(&it->second->renderer_pool, 0, it->second->ticket);
				}

				it->second->renderer_pool.insert_renderer(pRenderer, _ticket);
			}

			void erase_renderer(const eng::Material3f* pMaterial, ext::ticket& _ticket)
			{
				auto it = m_pools.find(pMaterial);
				assert(it != m_pools.end());

				it->second->renderer_pool.erase_renderer(_ticket);

				if (it->second->renderer_pool.empty())
				{
					RenderQueue_Module<Scene>::get(this->modular()).removeRenderer(it->second->ticket);
					m_pools.erase(it);
				}
			}
		};

		template<bool variable_mesh, size_t override_argument_size>
		struct renderer_draw_data
		{
			const eng::DeviceMesh3f* m_pMesh;
			const eng::DeviceMesh3f* mesh() const
			{
				return m_pMesh;
			}
			constexpr bool variable_mesh() const
			{
				return variable_mesh;
			}


			std::array<const void*, override_argument_size> m_override_arguments;
			std::array<const void*, override_argument_size> override_arguments() const
			{
				return m_override_arguments;
			}
		};

		template<typename target_t, typename object3_t, bool variable_mesh, size_t override_arguments_size>
		class BasicRenderer3 : public component<object3_t, ModelMatrix>
		{
			using pool_module_t = renderer_pool_module<target_t, object3_t, variable_mesh, override_arguments_size>;

			const eng::Material3f* m_pMaterial = nullptr;
			ext::ticket m_material_ticket;

		public:

			void set_scene(typename BasicRenderer3<target_t, object3_t, variable_mesh, override_arguments_size>::scene_t* pScene)
			{
				if (m_pMaterial && scene())
				{
					pool_module_t::get(*scene()).erase_renderer(m_pMaterial, m_material_ticket);
				}

				next::set_scene(pScene);

				if (m_pMaterial && scene())
				{
					pool_module_t::get(*scene()).insert_renderer(static_cast<target_t*>(this), m_pMaterial, m_material_ticket);
				}
			}

			void set_material(const eng::Material3f* pMaterial)
			{
				if (m_pMaterial && this->scene())
				{
					pool_module_t::get(*scene()).insert_renderer(static_cast<target_t*>(this), m_pMaterial, m_material_ticket);
				}

				m_pMaterial = pMaterial;

				if (m_pMaterial && this->scene())
				{
					pool_module_t::get(*scene()).erase_renderer(m_pMaterial, m_material_ticket);
				}
			}

			const eng::Material3f* get_material() const
			{
				return m_pMaterial;
			}

			BasicRenderer3()
			{

			}
			virtual ~BasicRenderer3()
			{
				if (m_pMaterial && scene())
				{
					pool_module_t::get(*scene()).erase_renderer(m_pMaterial, m_material_ticket);
				}
			}
		};
	}
}