#pragma once
#include <eng/tsg/BasicRenderer3.h>

namespace eng
{
	namespace tsg
	{
		template<typename object3_t>
		class SpriteRenderer3 : public eng::tsg::BasicRenderer3<SpriteRenderer3<object3_t>, object3_t, false, 2>
		{
		private:

			static std::once_flag						s_spriteMeshFlag;
			static std::unique_ptr<eng::DeviceMesh3f>	s_pSpriteMesh;
			static void initializeSpriteMesh()
			{
				std::vector<eng::triangle_indices> triangles =
				{
					{ 0, 1, 3 }, // +z
					{ 0, 3, 2 },
				};

				std::vector<eng::vertex3f> vertices =
				{
					{ gtm::vector3f{-0.5f, -0.5f, 0.0 }, gtm::vector3f{ 0,0,1 }, gtm::vector3f{ 0, 1, 0 }, { gtm::vector2f{ 0, 1 } } },
					{ gtm::vector3f{-0.5f,  0.5f, 0.0 }, gtm::vector3f{ 0,0,1 }, gtm::vector3f{ 0, 0, 0 }, { gtm::vector2f{ 0, 0 } } },
					{ gtm::vector3f{ 0.5f, -0.5f, 0.0 }, gtm::vector3f{ 0,0,1 }, gtm::vector3f{ 1, 1, 0 }, { gtm::vector2f{ 1, 1 } } },
					{ gtm::vector3f{ 0.5f,  0.5f, 0.0 }, gtm::vector3f{ 0,0,1 }, gtm::vector3f{ 1, 0, 0 }, { gtm::vector2f{ 1, 0 } } },
				};

				eng::Mesh3f mesh;
				mesh.vertices() = std::move(vertices);
				mesh.triangles() = std::move(triangles);

				s_pSpriteMesh.reset(new eng::DeviceMesh3f(std::move(mesh)));
			}

		public:

			static const eng::DeviceMesh3f* mesh()
			{
				std::call_once(s_spriteMeshFlag, &initializeSpriteMesh);
				return s_pSpriteMesh.get();
			}

			std::array<const void*, 2> overrides(eng::DeviceBuffer const & viewProjectionBuffer, size_t layerArgs, eng::RenderTarget& target) const
			{
				return { &model_matrix(), &viewProjectionBuffer };
			}

			SpriteRenderer3()
			{

			}
			virtual ~SpriteRenderer3()
			{

			}
		};

		template<typename object3_t>
		std::once_flag SpriteRenderer3<object3_t>::s_spriteMeshFlag;
		template<typename object3_t>
		std::unique_ptr<eng::DeviceMesh3f> SpriteRenderer3<object3_t>::s_pSpriteMesh = nullptr;
	};
};