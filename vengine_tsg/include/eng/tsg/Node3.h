#pragma once
#include <gtm/geometry.h>

#include <ext/event.h>

#include <eng/tsg/unordered_node.h>
#include <eng/tsg/Scene.h>


namespace eng
{
	namespace tsg
	{
		template<typename numeric_t>
		using Transform = gtm::affine<numeric_t, 3>;

		template<typename _numeric_t, typename _node_t, typename _scene_t>
		class Node3 : public unordered_child<_node_t>, public unordered_parent<_node_t>
		{
		public:
			
			using numeric_t = _numeric_t;
			using node_t = _node_t;
			using scene_t = _scene_t;

			using transform_t = Transform<numeric_t>;
		private:
			transform_t m_local_transform = transform_t::identity();
			transform_t m_transform = transform_t::identity();
			void set_parented_transform(const transform_t& parent_transform)
			{
				this->m_transform = gtm::transform(parent_transform, this->m_local_transform);
				for (auto i : this->children())
				{
					i->set_parented_transform(this->m_transform);
				}
			}

			scene_t* m_pScene = nullptr;

			ext::event<node_t&, scene_t*, scene_t*> m_scene_changed_event;

		public:
			const transform_t& transform() const
			{
				return this->m_transform;
			}
			const transform_t& local_transform() const
			{
				return this->m_local_transform;
			}
			virtual void set_local_transform(const transform_t& m)
			{
				this->m_local_transform = m;
				if (this->parent() != nullptr)
				{
					this->m_transform = gtm::transform(this->parent()->transform(), this->m_local_transform);
				}
				else
				{
					this->m_transform = this->m_local_transform;
				}

				for (auto i : this->children())
				{
					i->set_parented_transform(this->m_transform);
				}
			}

			ext::event_subscribe_access<node_t&, scene_t*, scene_t*> scene_changed_event()
			{
				return { m_scene_changed_event };
			}

			virtual void parent_changed(node_t* pOld, node_t* pCurrent) override
			{
				if (parent() != nullptr)
				{
					scene_t* pOldScene = this->m_pScene;
					this->m_pScene = parent()->scene();

					if (pOldScene != this->m_pScene)
					{
						set_scene(this->m_pScene);
					}
				}
			}

			scene_t* scene() const
			{
				return this->m_pScene;
			}
			virtual void set_scene(scene_t* pScene)
			{
				assert(this->parent() == nullptr);
				this->m_pScene = pScene;

				for(auto i : this->children())
				{
					i->set_scene(pScene);
				}
			}

			virtual ~Node3()
			{

			}
		};

		class Node3d : public Node3<double, Node3d, eng::tsg::Scene>
		{

		};

		class Node3f : public Node3<float, Node3f, eng::tsg::Scene>
		{

		};
	}
}