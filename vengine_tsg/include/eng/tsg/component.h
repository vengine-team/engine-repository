#pragma once
#include <type_traits>

namespace eng
{
	namespace tsg
	{
		template<bool test, typename true_t, typename false_t>
		constexpr auto conditional(true_t, false_t)
		{
			return std::conditional_t<test, true_t, false_t>{};
		}

		template<template<typename...> typename T>
		struct template_type
		{
			template<typename ...P>
			using value = T<P...>;
		};

		template<typename t>
		struct type
		{
			using value = t;
		};

		template<typename ...types>
		class type_list;

		template<>
		class type_list<>
		{
		public:

			static constexpr bool empty()
			{
				return true;
			}

			template<typename t>
			static constexpr bool contains(type<t>)
			{
				return false;
			}

			template<typename t>
			static constexpr bool contains()
			{
				return false;
			}
		};

		template<typename _head, typename ..._tail>
		class type_list<_head, _tail...>
		{
		public:
			static constexpr type<_head> head()
			{
				return {};
			}

			static constexpr type_list<_tail...> tail()
			{
				return {};
			}

			static constexpr bool empty()
			{
				return false;
			}

			template<typename t>
			static constexpr bool contains()
			{
				return std::is_same_v<_head, t> || tail().contains<t>();
			}

			template<typename t>
			static constexpr bool contains(type<t>)
			{
				return contains<t>();
			}

			template<typename t>
			static constexpr auto sublist()
			{
				return conditional<std::is_same<t, _head>>
					(
						type_list<_head, _tail...>,
						sublist<t>()
						)
			}


			template<typename t>
			static constexpr auto sublist(type<t>)
			{
				return sublist<t>();
			}

		};

		template<typename a, typename b>
		constexpr type_list<a, b> merge(type<a>, type<b>)
		{
			return {};
		}

		template<typename a, typename ...b>
		constexpr type_list<a, b...> merge(type<a>, type_list<b...>)
		{
			return {};
		}

		template<typename ...a, typename b>
		constexpr type_list<a..., b> merge(type_list<a...>, type<b>)
		{
			return {};
		}

		template<typename ...a, typename ...b>
		constexpr type_list<a..., b...> merge(type_list<a...>, type_list<b...>)
		{
			return {};
		}

		constexpr type_list<> reverse(type_list<> l)
		{
			return {};
		}

		template<typename ...a>
		constexpr auto reverse(type_list<a...> l)
		{
			return merge(reverse(l.tail()), l.head());
		}

		template<typename ...first>
		constexpr auto prepend_if_not_exits(type_list<first...> a, type_list<> b)
		{
			return a;
		}

		template<typename ...first, typename ...second>
		constexpr auto prepend_if_not_exits(type_list<first...> a, type_list<second...> b)
		{
			return conditional<b.empty()>
			(
				a,
				conditional<a.contains(b.head())>
				(
					prepend_if_not_exits(a, b.tail()),
					prepend_if_not_exits(merge(b.head(), a), b.tail())
				)
			);
		}

		template<typename base_t>
		constexpr type_list<> get_required(type<base_t>, type_list<> l)
		{
			return l;
		}

		struct _empty
		{

		};

		template<typename base_t, typename component_list>
		struct _carry_type;

		template<>
		struct _carry_type<void, type_list<>>
		{
			using next = _empty;
		};

		template<typename base_t>
		struct _carry_type<base_t, type_list<>>
		{
			using next = base_t;
		};

		template<typename base_t, template<typename> typename head, typename ...tail>
		struct _carry_type<base_t, type_list<template_type<head>, tail...>>
		{
			using next = head<_carry_type<base_t, type_list<tail...>>>;
		};

		template<typename base_t, typename head, typename ...tail>
		constexpr auto get_required(type<base_t>, type_list<head, tail...> l);


		template<typename base_t, template<typename> typename ...components>
		class add_components : public _carry_type<base_t, decltype(get_required(type<base_t>{}, type_list<template_type<components>...>{})) > ::next
		{
		public:
			using _components = decltype(get_required(type<base_t>{}, type_list<template_type<components>...>{}));
			using next = typename _carry_type<base_t, _components>::next;
		};

		template<template<typename> typename ...templates>
		struct to_template_type_list;

		template<>
		struct to_template_type_list<>
		{
			using result = type_list<>;
		};

		template<template<typename> typename head, template<typename> typename ...tail>
		struct to_template_type_list<head, tail...>
		{
			using result = decltype(merge(type_list<template_type<head>>{}, typename to_template_type_list<tail...>::result{}));
		};

		template<typename _carry_t, template<typename> typename ...required_components>
		class component;

		template<typename base_t, typename component_list, template<typename> typename ...required_components>
		class component<_carry_type<base_t, component_list>, required_components...> : public _carry_type<base_t, component_list>::next
		{
		public:
			using _required = typename to_template_type_list<required_components...>::result;
			using next = typename _carry_type<base_t, component_list>::next;
		};

		template<typename base_t>
		struct add_components_flag
		{

		};

		template<typename base_t, template<typename> typename ...required_components>
		class component<add_components_flag<base_t>, required_components...> : public add_components<base_t, required_components...>
		{
		public:
			using _required = typename to_template_type_list<required_components...>::result;
			using next = typename add_components<base_t, required_components...>::next;
		};

		template<typename base_t, typename head, typename ...tail>
		constexpr auto get_required(type<base_t>, type_list<head, tail...> l)
		{
			auto head_required = get_required(type<base_t>{}, typename head::value<add_components_flag<base_t>>::_required{});
			auto left_merged = prepend_if_not_exits(head_required, reverse(get_required(type<base_t>{}, l.tail())));
			return merge(l.head(), left_merged);
		}

		template<template<typename...> typename comp, typename ...bindings>
		class bind
		{
			template<typename T>
			using t = comp<T, bindings...>;
		};
	}
}