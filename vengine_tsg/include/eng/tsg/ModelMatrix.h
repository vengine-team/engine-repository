#pragma once
#include <eng/tsg/component.h>
#include <eng/DeviceBuffer.h>
#include <ext/event.h>

namespace eng
{
	namespace tsg
	{
		template<typename object3_t>
		class ModelMatrix : public component<object3_t>
		{
			eng::DeviceBuffer m_model_matrix_buffer = { sizeof(gtm::matrix<float, 4, 4>) };

			void update_model_matrix_buffer()
			{
				gtm::matrix<float, 4, 4> data = static_cast<gtm::matrix<float, 4, 4>>(gtm::to_matrix(transform()));

				// Update the constant buffer resource.
				m_model_matrix_buffer.store(&data, sizeof(data));
			}

			ext::event_handler<ext::functor<&update_model_matrix_buffer>> m_on_pre_render = { { this } };

		public:

			void set_scene(typename ModelMatrix<object3_t>::scene_t* pScene)
			{
				if (scene())
				{
					scene()->preRenderEvent() -= m_on_pre_render;
				}

				next::set_scene(pScene);

				if (scene())
				{
					scene()->preRenderEvent() += m_on_pre_render;
				}
			}

			const eng::DeviceBuffer& model_matrix() const
			{
				return m_model_matrix_buffer;
			}

			ModelMatrix()
			{
				if (scene())
				{
					scene()->preRenderEvent() += m_on_pre_render;
				}
			}
			virtual ~ModelMatrix()
			{
				if (scene())
				{
					scene()->preRenderEvent() -= m_on_pre_render;
				}
			}
		};
	};
};