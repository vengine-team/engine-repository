#pragma once
#include <ext/ticket_table.h>

namespace eng
{
	namespace tsg
	{
		template<typename _Child_t>
		class unordered_parent_Children;

		template<typename _parent_t>
		class unordered_child
		{
		public:
			using parent_t = _parent_t;
		private:
			template<typename T>
			friend class unordered_parent;

			ext::ticket m_parentTicket;

			parent_t* m_pParent = nullptr;
		public:

			virtual void parent_changed(parent_t* old, parent_t* pCurrent)
			{

			}

			parent_t * parent() const
			{
				return m_pParent;
			}
		};

		template<typename child_t>
		class unordered_parent;

		template<typename _Child_t>
		class unordered_parent_Children
		{
			using container_t = ext::ticket_table<_Child_t*>;

			friend class unordered_parent<_Child_t>;

			container_t m_container;

		public:

			unordered_parent_Children()
			{

			}

			using const_iterator = typename container_t::const_iterator;
			const_iterator begin() const
			{
				return m_container.begin();
			}
			const_iterator end() const
			{
				return m_container.end();
			}

			const_iterator cbegin() const
			{
				return m_container.cbegin();
			}
			const_iterator cend() const
			{
				return m_container.cend();
			}

			using const_reverse_iterator = typename container_t::const_reverse_iterator;
			const_reverse_iterator rbegin() const
			{
				return m_container.rbegin();
			}
			const_reverse_iterator rend() const
			{
				return m_container.rend();
			}

			const_reverse_iterator crbegin() const
			{
				return m_container.crbegin();
			}
			const_reverse_iterator crend() const
			{
				return m_container.crend();
			}

			size_t size() const
			{
				return m_container.size();
			}

			bool empty() const
			{
				return m_container.empty();
			}
		};


		template<typename child_t>
		class unordered_parent
		{

			using child_container_t = unordered_parent_Children<child_t>;

			child_container_t m_children;

		public:

			unordered_parent()
			{

			}
			unordered_parent(const unordered_parent<child_t>&) = delete;
			unordered_parent(unordered_parent<child_t>&&) = delete;
			unordered_parent<child_t>& operator=(const unordered_parent<child_t>&) = delete;
			unordered_parent<child_t>& operator=(unordered_parent<child_t>&&) = delete;
			virtual ~unordered_parent()
			{
				assert(m_children.size() == 0);
			}

			const child_container_t& children() const
			{
				return m_children;
			}
			child_container_t& children()
			{
				return m_children;
			}

			virtual void insert_child(child_t* pChild)
			{
				m_children.m_container.insert({ pChild, pChild->m_parentTicket });
				typename child_t::parent_t* pOldParent = pChild->m_pParent;
				pChild->m_pParent = static_cast<typename child_t::parent_t*>(this);
				pChild->parent_changed(pOldParent, pChild->m_pParent);
			}

			virtual void erase_child(child_t* pChild)
			{
				assert(0);
				//m_children.m_container.remove(pChild->m_parentTicket);
			}
		};
	}
}