#pragma once
#include <eng/tsg/Node3.h>
#include <eng/tsg/Scene.h>
#include <eng/tsg/component.h>

#include <ext/event.h>

#include <eng/RenderTarget.h>

namespace eng
{
	namespace tsg
	{
		template<typename scene_t>
		class IRenderer3
		{
		public:
			virtual void draw(const eng::DeviceBuffer& pViewProjection, size_t argument, eng::RenderTarget& target) const = 0;
			virtual ~IRenderer3() = default;
		};

		template<typename scene_t>
		class RenderQueue_Module : public make_module<RenderQueue_Module<scene_t>, scene_t>
		{
		public:
			ext::ticket_table<std::pair<const IRenderer3<scene_t>*, size_t>> m_renderers;

			static eng::module_key<RenderQueue_Module<scene_t>>& get_moduleKey()
			{
				static eng::module_key<RenderQueue_Module<scene_t>> s_moduleKey;
				static eng::module_guard<scene_t, RenderQueue_Module<scene_t>> s_moduleGuard = { s_moduleKey };
				return s_moduleKey;
			}

		public:

			const ext::ticket_table<std::pair<const IRenderer3<scene_t>*, size_t>>& renderers() const
			{
				return m_renderers;
			}

			void addRenderer(const IRenderer3<scene_t>* pRenderer, size_t recordId, ext::ticket & _ticket)
			{
				m_renderers.insert({ { pRenderer, recordId }, _ticket });
			}
			void removeRenderer(ext::ticket & _ticket)
			{
				m_renderers.erase(_ticket);
			}

			RenderQueue_Module()
			{

			}
			virtual ~RenderQueue_Module()
			{

			}
		};

		template<typename object3_t>
		class Camera3 : public component<object3_t>
		{
		private:
			RenderTarget * m_pRenderTarget = nullptr;

			void on_render()
			{
				update_constant_buffer();

				if (m_pRenderTarget)
				{
					m_pRenderTarget->begin_recording();
					{
						std::chrono::steady_clock::time_point t0 = std::chrono::steady_clock::now();

						RenderQueue_Module<Scene>& renderLayerQueue = RenderQueue_Module<Scene>::get(*scene());

						for (auto i : renderLayerQueue.renderers())
						{
							i.first->draw
							(
								_getCurrentFrameViewProjectionConstantBuffer(),
								i.second,
								*m_pRenderTarget
							);
						}

						std::chrono::steady_clock::time_point t1 = std::chrono::steady_clock::now();

						std::chrono::duration<double, std::milli> dur = t1 - t0;

						/*std::wstring out = L"CPU render time in ms: ";
						out.append(std::to_wstring(dur.count()));
						out.append(L"\n");

						OutputDebugString(out.c_str());*/
					}
					m_pRenderTarget->end_recording();
				}
			}


			ext::event_handler<ext::functor<&Camera3::on_render>> m_on_render = { { this } };

			eng::DeviceBuffer m_viewProjectionConstBuffer = { sizeof(ViewProjectionConstBufferData) };

#ifdef _DIRECTX

			Microsoft::WRL::ComPtr<ID3D12GraphicsCommandList> m_pCommandList;

			struct ViewProjectionConstBufferData
			{
				DirectX::XMFLOAT4X4 view;
				DirectX::XMFLOAT4X4 projection;
			};


			void update_constant_buffer()
			{
				if (m_pRenderTarget)
				{
					ViewProjectionConstBufferData constBufferData;

					DirectX::XMStoreFloat4x4(&constBufferData.view, this->_getViewMatrix());
					DirectX::XMStoreFloat4x4(&constBufferData.projection, this->_getProjectionMatrix());

					// Update the constant buffer resource.
					m_viewProjectionConstBuffer.store(&constBufferData, sizeof(constBufferData));
				}
			}

		public:

			DirectX::XMMATRIX _getViewMatrix() const
			{
				gtm::vector3d worldPosD = this->transform().translation();
				gtm::vector3f worldPos = static_cast<gtm::vector3f>(worldPosD);

				gtm::matrix<double,3,3> linear = this->transform().linear();

				gtm::vector3d worldViewDirD = gtm::matrix_mul(linear, s_local_View_Direction);
				gtm::vector3d worldUpDirD = gtm::matrix_mul(linear,  s_local_Up_Direction);

				gtm::vector3f worldViewDir = normalize(static_cast<gtm::vector3f>(worldViewDirD));
				gtm::vector3f worldUpDir = normalize(static_cast<gtm::vector3f>(worldUpDirD));

				DirectX::XMVECTORF32 eyeAt = { worldPos[0]	  ,  worldPos[1]   ,  worldPos[2]    , 0.0f };
				DirectX::XMVECTORF32 eyeDir = { -worldViewDir[0],  -worldViewDir[1],-worldViewDir[2], 0.0f };
				DirectX::XMVECTORF32 up = { worldUpDir[0]  ,  worldUpDir[1]  , worldUpDir[2]  , 0.0f };

				return DirectX::XMMatrixTranspose(DirectX::XMMatrixLookToRH(eyeAt, eyeDir, up));
			}
			DirectX::XMMATRIX _getProjectionMatrix() const
			{
				gtm::vector2u size = m_pRenderTarget->get_scale();

				DirectX::XMMATRIX out = DirectX::XMMatrixPerspectiveFovRH(
					70 * DirectX::XM_PI / 180.0f,
					static_cast<float>(size[0]) / static_cast<float>(size[1]),
					0.01f,
					100.0f
				);
				return XMMatrixTranspose(out);
			}
			const eng::DeviceBuffer& _getCurrentFrameViewProjectionConstantBuffer() const
			{
				return m_viewProjectionConstBuffer;
			}

#endif // _DIRECTX

		public:

			void set_scene(typename Camera3<object3_t>::scene_t* pScene)
			{
				if (scene() != nullptr)
				{
					scene()->renderEvent() -= m_on_render;
				}

				next::set_scene(pScene);

				if (scene() != nullptr)
				{
					scene()->renderEvent() += m_on_render;
				}
			}

			const gtm::vector3d s_local_View_Direction = { 0, 0,-1 };
			const gtm::vector3d s_local_Up_Direction = { 0, 1, 0 };

			void set_renderTarget(RenderTarget* pRenderTarget)
			{
				m_pRenderTarget = pRenderTarget;
			}
			RenderTarget* get_renderTarget() const
			{
				return m_pRenderTarget;
			}

			Camera3()
			{

			}
			virtual ~Camera3()
			{

			}
		};
	}
}