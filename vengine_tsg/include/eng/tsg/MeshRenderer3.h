#pragma once
#include <eng/tsg/BasicRenderer3.h>

namespace eng
{
	namespace tsg
	{
		template<typename object3_t>
		class MeshRenderer3 : public eng::tsg::BasicRenderer3<MeshRenderer3<object3_t>, object3_t, true, 2>
		{
		private:

			DeviceMesh3f const * m_pMesh = nullptr;

		public:

			void set_mesh(const DeviceMesh3f * pMesh)
			{
				m_pMesh = pMesh;
			}

			const eng::DeviceMesh3f* mesh() const
			{
				return m_pMesh;
			}

			std::array<const void*, 2> overrides(eng::DeviceBuffer const & viewProjectionBuffer, size_t layerArgs, eng::RenderTarget& target) const
			{
				return { &model_matrix(), &viewProjectionBuffer };
			}
		};
	}
}