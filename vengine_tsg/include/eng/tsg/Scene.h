#pragma once
#include <ext/event.h>
#include <par/task_queue.h>
#include <par/IUpdateable.h>
#include <eng/Modular.h>

namespace eng
{
	namespace tsg
	{
		template<typename clock_t>
		class Basic_Scene;

		template<typename scene_t>
		class ISceneModule : public eng::module_base<scene_t>
		{
		public:
			virtual void on_early_update(scene_t* pScene)
			{
				
			}
			virtual void on_update(scene_t* pScene)
			{

			}
			virtual void on_late_update(scene_t* pScene)
			{

			}

			virtual void on_pre_render(scene_t* pScene)
			{

			}
			virtual void on_render(scene_t* pScene)
			{

			}
			virtual void on_post_render(scene_t* pScene)
			{

			}

			virtual ~ISceneModule()
			{

			}
		};

		class general_purpose_clock
		{
		public:
			using rep = std::chrono::steady_clock::rep;
			using period = std::chrono::steady_clock::period;
			using duration = std::chrono::steady_clock::duration;
			using time_point = std::chrono::time_point<general_purpose_clock>;
			static constexpr bool is_steady = false;
		private:
			duration m_elapsed = duration::zero();
		public:
			time_point now()
			{
				return time_point{ m_elapsed };
			}
			void advance(std::chrono::steady_clock::duration target_frame_duration)
			{
				m_elapsed += target_frame_duration;
			}
		};

		template<typename clock_t>
		class Basic_Scene : public IUpdateable, public Modular<Basic_Scene<clock_t>, ISceneModule<Basic_Scene<clock_t>>>
		{
		private:
			clock_t m_clock;

			unsigned long long m_frameCount = 0;
			double m_targetFrameRate = 60.0;


			ext::event<> m_earlyUpdateEvent;
			ext::event<> m_updateEvent;
			ext::event<> m_lateUpdateEvent;


			ext::event<> m_preRenderEvent;
			ext::event<> m_renderEvent;
			ext::event<> m_postRenderEvent;

			eng::TaskQueue m_immediate_task_queue;

		public:

			clock_t & clock()
			{
				return m_clock;
			}
			const clock_t & clock() const
			{
				return m_clock;
			}

			eng::TaskQueue& immediate_task_queue()
			{
				return m_immediate_task_queue;
			}

			virtual void updateThreadRegistered(eng::UpdateThread* pNewUpdateThread) override
			{

			}

			virtual void updateThreadUnregistered(eng::UpdateThread* pNewUpdateThread) override
			{

			}

			virtual std::chrono::steady_clock::time_point updateFrame(std::chrono::steady_clock::time_point expected_end) override
			{
				std::chrono::steady_clock::time_point frameBegin = std::chrono::steady_clock::now();

				std::chrono::steady_clock::duration frameDuration = std::chrono::duration_cast<std::chrono::steady_clock::duration>(std::chrono::seconds(1) / getTargetFrameRate());

				m_frameCount++;

				m_immediate_task_queue.process();

				callModuleInterfaceFunction(&ISceneModule<Basic_Scene<clock_t>>::on_early_update, this);
				m_earlyUpdateEvent();
				callModuleInterfaceFunction(&ISceneModule<Basic_Scene<clock_t>>::on_update, this);
				m_updateEvent();
				callModuleInterfaceFunction(&ISceneModule<Basic_Scene<clock_t>>::on_late_update, this);
				m_lateUpdateEvent();

				callModuleInterfaceFunction(&ISceneModule<Basic_Scene<clock_t>>::on_pre_render, this);
				m_preRenderEvent();
				callModuleInterfaceFunction(&ISceneModule<Basic_Scene<clock_t>>::on_render, this);
				m_renderEvent();
				callModuleInterfaceFunction(&ISceneModule<Basic_Scene<clock_t>>::on_post_render, this);
				m_postRenderEvent();

				m_clock.advance(frameDuration);

				return frameBegin + frameDuration;
			}

			double getTargetFrameRate() const
			{
				return m_targetFrameRate;
			}
			void setTargetFrameRate(double frameRate)
			{
				m_targetFrameRate = frameRate;
			}
			unsigned long long getFrameCount()
			{
				return m_frameCount;
			}

			ext::event_subscribe_access<> earlyUpdateEvent()
			{
				return { m_earlyUpdateEvent };
			}

			ext::event_subscribe_access<> updateEvent()
			{
				return { m_updateEvent };
			}

			ext::event_subscribe_access<> lateUpdateEvent()
			{
				return { m_lateUpdateEvent };
			}


			ext::event_subscribe_access<> preRenderEvent()
			{
				return { m_preRenderEvent };
			}
			ext::event_subscribe_access<> renderEvent()
			{
				return { m_renderEvent };
			}
			ext::event_subscribe_access<> postRenderEvent()
			{
				return { m_postRenderEvent };
			}

			Basic_Scene()
			{

			}
			Basic_Scene(const Basic_Scene<clock_t>&) = delete;
			Basic_Scene<clock_t>& operator=(const Basic_Scene<clock_t>&) = delete;
			Basic_Scene(Basic_Scene<clock_t>&&) = delete;
			Basic_Scene<clock_t>& operator=(Basic_Scene<clock_t>&&) = delete;
			virtual ~Basic_Scene()
			{

			}
		};

		using Scene = Basic_Scene<general_purpose_clock>;
	}
}