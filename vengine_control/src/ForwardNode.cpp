#include <eng/control/Forward_Node.h>
#include <eng/control/default_draw.h>
#include <eng/control/LocalInput.h>


unsigned int eng::control::Control::calculateMinimumHeight() const
{
	return 16;
}
void eng::control::Control::setEnabled(bool enabled)
{
	m_enabled = enabled;
}
bool eng::control::Control::getEnabled() const
{
	return m_enabled;
}
unsigned int eng::control::Control::calculateMinimumWidth() const
{
	return 16;
}
unsigned int eng::control::Control::calculateHeight(unsigned int width) const
{
	return calculateMinimumHeight();
}
unsigned int eng::control::Control::calculateWidth(unsigned int height) const
{
	return calculateMinimumWidth();
}


bool eng::control::Control::localHitTest(gtm::vector2i position) const
{
	if (getEnabled())
	{
		return contains({ { 0, 0 }, static_cast<gtm::vector2i>(getScale()) }, position);
	}
	else
	{
		return false;
	}
}

void eng::control::Control::localRender(eng::control::LocalGraphics & localGraphics) const
{

}
void eng::control::Control::onLocalInput(const eng::control::local_input_state_change & onLocalInput)
{

}

bool eng::control::Control::onInput(const eng::input_state_change& change, gtm::vector2i offset, bool obstructed)
{
	eng::control::local_input_state_change local_input;
	local_input.hit_obstructed = obstructed;

	local_input.change = change;

	local_input.change.before.mouse.position -= offset;
	local_input.change.current.mouse.position -= offset;

	local_input.hit = localHitTest(local_input.change.current.mouse.position);

	onLocalInput(local_input);

	return local_input.hit;
}
void eng::control::Control::update()
{

}
void eng::control::Control::render(eng::control::Graphics * graphics, gtm::vector2i offset) const
{
	if (getEnabled())
	{
		eng::control::LocalGraphics localGraphics = eng::control::LocalGraphics(graphics, offset);
		localRender(localGraphics);
	}
}


void eng::control::Control::setLocalPosition(gtm::vector2i localPosition)
{
	m_localPosition = static_cast<gtm::vector2u>(localPosition);
}
gtm::vector2i eng::control::Control::getLocalPosition() const
{
	return m_localPosition;
}

void eng::control::Control::setScale(gtm::vector2u scale)
{
	gtm::vector2u formerScale = m_scale;
	m_scale = scale;
}
gtm::vector2u eng::control::Control::getScale() const
{
	return m_scale;
}

eng::control::Control::Control()
{

}
eng::control::Control::~Control()
{

}
