#pragma once
#include <eng/control/Graphics.h>
#include <eng/windowsUtil.h>

void eng::control::Graphics::initializeQuadMesh()
{
	std::vector<eng::triangle_indices> triangles =
	{
		{ 0, 1, 3 }, // +z
		{ 0, 3, 2 },
	};

	std::vector<eng::vertex2f> vertices =
	{
		{ gtm::vector2f{-0.5f, -0.5f }, { gtm::vector2f{ 0, 1 } } },
		{ gtm::vector2f{-0.5f,  0.5f }, { gtm::vector2f{ 0, 0 } } },
		{ gtm::vector2f{ 0.5f, -0.5f }, { gtm::vector2f{ 1, 1 } } },
		{ gtm::vector2f{ 0.5f,  0.5f }, { gtm::vector2f{ 1, 0 } } },
	};

	eng::Mesh2F mesh;
	mesh.vertices() = std::move(vertices);
	mesh.triangles() = std::move(triangles);

	m_pQuadMesh.reset(new eng::DeviceMesh2F(std::move(mesh)));
}


eng::control::Graphics::Graphics()
{
	initializeQuadMesh();
}

eng::control::Graphics::~Graphics()
{

}


eng::RenderTarget * eng::control::Graphics::getRenderTarget()
{
	return m_pTarget;
}

eng::DeviceMesh2F * eng::control::Graphics::quadMesh()
{
	return m_pQuadMesh.get();
}

void eng::control::Graphics::beginRecording(eng::RenderTarget* pTarget)
{
	m_pTarget = pTarget;

	m_frameIndex = (m_frameIndex + 1) % 2;

	pTarget->begin_recording();
}

void eng::control::Graphics::endRecording()
{
	m_pTarget->end_recording();
}


