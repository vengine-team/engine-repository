#include <eng/control/layout.h>
#include <algorithm>

void eng::control::append_vertical(int& pen_y, gtm::rectangle<int> rect, eng::control::Control & control)
{
	assert(rect.left < rect.right &&  rect.top < rect.bottom);

	int available_width = rect.right - rect.left;
	unsigned int min_width = control.calculateMinimumWidth();

	unsigned int applied_width = (std::max)((unsigned int)available_width, min_width);

	control.setLocalPosition({ rect.left, pen_y });
	control.setScale({applied_width, control.calculateHeight(applied_width)});

	pen_y += (int)control.getScale()[1];
}

void eng::control::append_vertical_fill(int& pen_y, gtm::rectangle<int> rect, eng::control::Control & control)
{
	assert(rect.left < rect.right &&  rect.top < rect.bottom);

	int available_width = rect.right - rect.left;
	unsigned int min_width = control.calculateMinimumWidth();

	unsigned int applied_width = (std::max)((unsigned int)available_width, min_width);


	int remaining_height = rect.bottom - pen_y;
	unsigned int calculated_heigth = control.calculateHeight(applied_width);

	unsigned int applied_height = (std::max)((unsigned int)remaining_height, calculated_heigth);

	control.setLocalPosition({ rect.left, pen_y });
	control.setScale({ applied_width, applied_height });

	pen_y += (int)control.getScale()[1];
}

gtm::rectangle<int> eng::control::padded_rectangle(gtm::vector2u scale, gtm::vector2u padding)
{
	return { { (int)padding[0], (int)padding[1] }, { (int)(scale[0] - padding[0]), (int)(scale[1] - padding[1]) } };
}

gtm::vector2u eng::control::calculate_widest_scale(const eng::control::Control& control)
{
	unsigned int height = control.calculateMinimumHeight();
	return { control.calculateWidth(height), height };
}
