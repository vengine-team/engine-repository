#include <eng/gpl/MenuBar.h>

const gtm::vector2i c_buttonPadding = { 2, 2 };
const unsigned int c_buttonKerning = 2;
const unsigned int c_buttonLinegap = 2;

void eng::gpl::MenuBar::rescaleButtons()
{
	for (auto i = m_buttons.begin(); i != m_buttons.end(); i++)
	{
		unsigned int i_min_Height = (*i)->calculateMinimumHeight();
		(*i)->setScale({ (*i)->calculateWidth(i_min_Height), i_min_Height });
	}
}

void eng::gpl::MenuBar::positionButtons(unsigned int horizontalScale)
{
	unsigned int highest_button = calculate_highest_button();

	gtm::vector2i pos = gtm::vector2i::zero();

	for (size_t i = 0; i < m_buttons.size(); i++)
	{
		pos = calculate_next_position(highest_button, horizontalScale, pos, i);

		m_buttons[i]->setLocalPosition(pos);
	}
}


gtm::vector2i eng::gpl::MenuBar::calculate_next_position(unsigned int highest_element, unsigned int width, gtm::vector2i last_position, size_t button_index) const
{
	if (button_index == 0)
	{
		return c_buttonPadding;
	}
	else
	{
		unsigned int last_button_width = m_buttons[button_index - 1]->calculateWidth(m_buttons[button_index - 1]->calculateMinimumHeight());
		unsigned int current_button_width = m_buttons[button_index]->calculateWidth(m_buttons[button_index]->calculateMinimumHeight());

		last_position[0] += last_button_width + c_buttonKerning;

		if (last_position[0] + (int)current_button_width > (int)width - c_buttonPadding[0])
		{
			last_position[0] = c_buttonPadding[0];
			last_position[1] += highest_element + c_buttonLinegap;
		}

		return last_position;
	}
}

unsigned int eng::gpl::MenuBar::calculate_highest_button() const
{
	unsigned int highest = 0;

	for (auto i = m_buttons.begin(); i != m_buttons.end(); i++)
	{
		unsigned int i_height = (*i)->calculateHeight((*i)->calculateMinimumWidth());

		if (i_height > highest)
		{
			highest = i_height;
		}
	}

	return highest;
}

size_t eng::gpl::MenuBar::get_submenu_element_count() const
{
	return m_buttons.size();
}
const eng::IMenu& eng::gpl::MenuBar::get_submenu_element(size_t element_index) const
{
	return *m_buttons[element_index];
}
eng::IMenu& eng::gpl::MenuBar::get_submenu_element(size_t element_index)
{
	return *m_buttons[element_index];
}

unsigned int eng::gpl::MenuBar::calculateMinimumWidth() const
{
	unsigned int largest = 0;

	for (auto i = m_buttons.begin(); i != m_buttons.end(); i++)
	{
		unsigned int i_width = (*i)->calculateWidth((*i)->calculateMinimumHeight());

		if (i_width > largest)
		{
			largest = i_width;
		}
	}

	return largest;
}

unsigned int eng::gpl::MenuBar::calculateMinimumHeight() const
{
	return calculate_highest_button();
}

unsigned int eng::gpl::MenuBar::calculateHeight(unsigned int width) const
{
	unsigned int highest_button = calculate_highest_button();

	gtm::vector2i pos = gtm::vector2i::zero();

	for (size_t i = 0; i < m_buttons.size(); i++)
	{
		pos = calculate_next_position(highest_button, width, pos, i);

		m_buttons[i]->setLocalPosition(pos);
	}

	return pos[1] + highest_button + c_buttonPadding[1];
}

void eng::gpl::MenuBar::set_Font(const eng::Font * pFont)
{
	ControlParent::set_Font(pFont);
	rescaleButtons();
	positionButtons(getScale()[0]);
}

void eng::gpl::MenuBar::setTextSize(unsigned int size)
{
	m_textSize = size;
}
unsigned int eng::gpl::MenuBar::getTextSize() const
{
	return m_textSize;
}

void eng::gpl::MenuBar::setScale(gtm::vector2u scale)
{
	ControlParent::setScale(scale);
	rescaleButtons();
	positionButtons(scale[0]);
}

eng::gpl::MenuBar::MenuBar(size_t numElements, const eng::gpl::Menu_Element_Desc* pDescs)
{
	m_buttons.resize(numElements);

	for (size_t i = numElements; i > 0; i--)
	{
		m_buttons[i - 1].reset(new eng::gpl::DropDownMenu(pDescs[i - 1]));
		m_buttons[i - 1]->setOutlineEnabled(true);
		ControlParent::children().push_back(m_buttons[i - 1].get());
	}

	rescaleButtons();
}
eng::gpl::MenuBar::~MenuBar()
{
	while (children().size() > 0)
	{
		children().pop_back();
	}
}
