#include <eng/gpl/Forward_Node.h>
#include <eng/control/Graphics.h>
#include <eng/control/default_draw.h>

void eng::gpl::Control::localRender(eng::control::LocalGraphics & localGraphics) const
{
	if (m_pBackgroundMaterial)
	{
		eng::control::draw_quad(localGraphics, { gtm::vector2d{ 0, 0 }, (gtm::vector2d)getScale() - gtm::vector2d{ 0, 0 } }, m_pBackgroundMaterial);
	}
	if (m_pOutlineMaterial && m_outline_enabled)
	{
		eng::control::draw_rectangle(localGraphics, { { 0.5, 0.5 }, (gtm::vector2d)getScale() - gtm::vector2d{ 0.5,0.5 } }, 1, m_pOutlineMaterial);
	}
}

void eng::gpl::Control::set_Font(const eng::Font * pFont)
{
	m_pFont = pFont;
}
void eng::gpl::Control::setFontMaterial(const eng::Material2F * pMaterial)
{
	m_pFontMaterial = pMaterial;
}

void eng::gpl::Control::setTextureMaterial(const eng::Material2F * pMaterial)
{
	m_pTextureMaterial = pMaterial;
}

void eng::gpl::Control::setBackgroundMaterial(const eng::Material2F * pMaterial)
{
	m_pBackgroundMaterial = pMaterial;
}
void eng::gpl::Control::setHighlightedMaterial(const eng::Material2F * pMaterial)
{
	m_pHighlightedMaterial = pMaterial;
}
void eng::gpl::Control::setPressedMaterial(const eng::Material2F * pMaterial)
{
	m_pPressedMaterial = pMaterial;
}
void eng::gpl::Control::setOutlineMaterial(const eng::Material2F * pMaterial)
{
	m_pOutlineMaterial = pMaterial;
}

const eng::Font * eng::gpl::Control::get_Font() const
{
	return m_pFont;
}
const eng::Material2F * eng::gpl::Control::getFontMaterial() const
{
	return m_pFontMaterial;
}

const eng::Material2F * eng::gpl::Control::getTextureMaterial() const
{
	return m_pTextureMaterial;
}

void eng::gpl::Control::setOutlineEnabled(bool b)
{
	m_outline_enabled = b;
}

bool eng::gpl::Control::getOutlineEnabled() const
{
	return m_outline_enabled;
}

const eng::Material2F * eng::gpl::Control::getBackgoundMaterial() const
{
	return m_pBackgroundMaterial;
}
const eng::Material2F * eng::gpl::Control::getHighlightedMaterial() const
{
	return m_pHighlightedMaterial;
}
const eng::Material2F * eng::gpl::Control::getPressedMaterial() const
{
	return m_pPressedMaterial;
}
const eng::Material2F * eng::gpl::Control::getOutlineMaterial() const
{
	return m_pOutlineMaterial;
}

eng::gpl::Control::Control()
{

}
eng::gpl::Control::~Control()
{

}