#include <eng/gpl/Button.h>

const gtm::vector2u c_buttonScale = { 10, 20 };


ext::event_subscribe_access<eng::IButton&> eng::gpl::Button::downEvent()
{
	return { m_down_event };
}
ext::event_subscribe_access<eng::IButton&> eng::gpl::Button::upEvent()
{
	return { m_up_event };
}

bool eng::gpl::Button::pressed() const
{
	return m_pressed;
}


void eng::gpl::Button::onLocalInput(const eng::control::local_input_state_change& localInput)
{
	m_highlightArea_hit = contains({ { 0,0 }, static_cast<gtm::vector2i>(getScale()) }, localInput.change.current.mouse.position);

	if (getEnabled())
	{
		bool before_pressed = m_pressed;

		m_pressed =
			localInput.hit &&
			!localInput.hit_obstructed &&
			contains({ { 0,0 }, static_cast<gtm::vector2i>(getScale()) }, localInput.change.current.mouse.position) &&
			localInput.change.current.mouse.left_button;

		if (!before_pressed && m_pressed)
		{
			m_down_event(*this);
		}
		else if (before_pressed && !m_pressed)
		{
			m_up_event(*this);
		}
	}
	else
	{
		m_pressed = false;
	}
}

unsigned int eng::gpl::Button::calculateMinimumWidth() const
{
	if (get_Font())
	{
		return eng::calculate_rendered_length(get_text(), get_Font()) + c_buttonScale[0] * 2;
	}
	else
	{
		return c_buttonScale[0] * 2;
	}
}

unsigned int eng::gpl::Button::calculateMinimumHeight() const
{
	return { c_buttonScale[1] };
}

void eng::gpl::Button::set_text(const std::wstring & text)
{
	m_textDeviceMesh.set_text(text);
}

const std::wstring & eng::gpl::Button::get_text() const
{
	return m_textDeviceMesh.get_text();
}

void eng::gpl::Button::localRender(eng::control::LocalGraphics & graphics) const
{
	gtm::vector2d scale = static_cast<gtm::vector2d>(getScale());

	if (getBackgoundMaterial())
	{
		eng::control::draw_quad(graphics, { { 0, 0 } , scale }, getBackgoundMaterial());
	}
	if (getHighlightedMaterial() && m_highlightArea_hit && !m_pressed)
	{
		eng::control::draw_quad(graphics, { { 0, 0 }, scale }, getHighlightedMaterial());
	}
	else if (getPressedMaterial() && m_pressed)
	{
		eng::control::draw_quad(graphics, { { 0, 0 } , scale }, getPressedMaterial());
	}
	if (getOutlineMaterial() && getOutlineEnabled())
	{
		eng::control::draw_rectangle(graphics, { { 0.5, 0.5 }, scale - gtm::vector2d{ 0.5,0.5 } }, 1, getOutlineMaterial());
	}
	if (getFontMaterial())
	{
		gtm::vector2d pos =
		{
			(double)((signed int)(getScale()[0] / 2) - (signed int)m_textDeviceMesh.get_Horizontal_Scale() / 2),
			(double)(getScale()[1] * 3 / 4)
		};

		m_textDeviceMesh.draw(graphics, pos, 0, { 1,1 }, getFontMaterial());
	}
}

void eng::gpl::Button::set_Font(const eng::Font * pFont)
{
	Control::set_Font(pFont);
	m_textDeviceMesh.set_Font(pFont);
}

eng::gpl::Button::Button() : eng::gpl::Control()
{

}

eng::gpl::Button::~Button()
{

}
