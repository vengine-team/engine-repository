//#include "ControlLocalInput.h"
//
//eng::control::ControlLocalInput::const_mouse_input_change_iterator::const_mouse_input_change_iterator(const eng::Input::const_iterator& underlying, const eng::Input::const_iterator& underlying_end, vector2I offset)
//{
//	m_underlying = underlying;
//	m_underlying_end = underlying_end;
//	m_offset = offset;
//	if (m_underlying != m_underlying_end)
//	{
//		m_composedState = *m_underlying;
//		m_composedState.post_change_state.position -= m_offset;
//	}
//}
//
//const eng::input_state_change& eng::control::ControlLocalInput::const_mouse_input_change_iterator::operator*() const
//{
//	return m_composedState;
//}
//
//const eng::input_state_change * eng::control::ControlLocalInput::const_mouse_input_change_iterator::operator->() const
//{
//	return &m_composedState;
//}
//
//bool eng::control::ControlLocalInput::const_mouse_input_change_iterator::operator==(const const_mouse_input_change_iterator & other) const
//{
//	return m_underlying == other.m_underlying;
//}
//
//bool eng::control::ControlLocalInput::const_mouse_input_change_iterator::operator!=(const const_mouse_input_change_iterator & other) const
//{
//	return m_underlying != other.m_underlying;
//}
//
//eng::control::ControlLocalInput::const_mouse_input_change_iterator eng::control::ControlLocalInput::const_mouse_input_change_iterator::operator++()
//{
//	m_underlying++;
//	if (m_underlying != m_underlying_end)
//	{
//		m_composedState = *m_underlying;
//		m_composedState.post_change_state.position -= m_offset;
//	}
//	return *this;
//}
//
//eng::control::ControlLocalInput::const_mouse_input_change_iterator eng::control::ControlLocalInput::const_mouse_input_change_iterator::operator--()
//{
//	m_underlying--;
//	if (m_underlying != m_underlying_end)
//	{
//		m_composedState = *m_underlying;
//		m_composedState.post_change_state.position -= m_offset;
//	}
//	return *this;
//}
//
//eng::control::ControlLocalInput::const_mouse_input_change_iterator eng::control::ControlLocalInput::const_mouse_input_change_iterator::operator++(int)
//{
//	ControlLocalInput::const_mouse_input_change_iterator before = *this;
//	m_underlying++;
//	if (m_underlying != m_underlying_end)
//	{
//		m_composedState = *m_underlying;
//		m_composedState.post_change_state.position -= m_offset;
//	}
//	return before;
//}
//
//eng::control::ControlLocalInput::const_mouse_input_change_iterator eng::control::ControlLocalInput::const_mouse_input_change_iterator::operator--(int)
//{
//	ControlLocalInput::const_mouse_input_change_iterator before = *this;
//	m_underlying--;
//	if (m_underlying != m_underlying_end)
//	{
//		m_composedState = *m_underlying;
//		m_composedState.post_change_state.position -= m_offset;
//	}
//	return before;
//}
//
//eng::control::ControlLocalInput::const_mouse_input_change_reverse_iterator::const_mouse_input_change_reverse_iterator(const eng::Input::const_reverse_iterator& underlying, const eng::Input::const_reverse_iterator& underlying_end, vector2I offset)
//{
//	m_underlying = underlying;
//	m_underlying_rend = underlying_end;
//	m_offset = offset;
//
//	if (m_underlying != m_underlying_rend)
//	{
//		m_composedState = *m_underlying;
//		m_composedState.post_change_state.position -= m_offset;
//	}
//}
//
//const eng::mouse_input_state_change& eng::control::ControlLocalInput::const_mouse_input_change_reverse_iterator::operator*() const
//{
//	return m_composedState;
//}
//
//const eng::mouse_input_state_change * eng::control::ControlLocalInput::const_mouse_input_change_reverse_iterator::operator->() const
//{
//	return &m_composedState;
//}
//
//bool eng::control::ControlLocalInput::const_mouse_input_change_reverse_iterator::operator==(const const_mouse_input_change_reverse_iterator & other) const
//{
//	return m_underlying == other.m_underlying;
//}
//
//bool eng::control::ControlLocalInput::const_mouse_input_change_reverse_iterator::operator!=(const const_mouse_input_change_reverse_iterator & other) const
//{
//	return m_underlying != other.m_underlying;
//}
//
//eng::control::ControlLocalInput::const_mouse_input_change_reverse_iterator eng::control::ControlLocalInput::const_mouse_input_change_reverse_iterator::operator++()
//{
//	m_underlying++;
//	if (m_underlying != m_underlying_rend)
//	{
//		m_composedState = *m_underlying;
//		m_composedState.post_change_state.position -= m_offset;
//	}
//	return *this;
//}
//
//eng::control::ControlLocalInput::const_mouse_input_change_reverse_iterator eng::control::ControlLocalInput::const_mouse_input_change_reverse_iterator::operator--()
//{
//	m_underlying--;
//	if (m_underlying != m_underlying_rend)
//	{
//		m_composedState = *m_underlying;
//		m_composedState.post_change_state.position -= m_offset;
//	}
//	return *this;
//}
//
//eng::control::ControlLocalInput::const_mouse_input_change_reverse_iterator eng::control::ControlLocalInput::const_mouse_input_change_reverse_iterator::operator++(int)
//{
//	ControlLocalInput::const_mouse_input_change_reverse_iterator before = *this;
//	m_underlying++;
//	if (m_underlying != m_underlying_rend)
//	{
//		m_composedState = *m_underlying;
//		m_composedState.post_change_state.position -= m_offset;
//	}
//	return before;
//
//}
//
//eng::control::ControlLocalInput::const_mouse_input_change_reverse_iterator eng::control::ControlLocalInput::const_mouse_input_change_reverse_iterator::operator--(int)
//{
//	ControlLocalInput::const_mouse_input_change_reverse_iterator before = *this;
//	m_underlying--;	
//	if (m_underlying != m_underlying_rend)
//	{
//		m_composedState = *m_underlying;
//		m_composedState.post_change_state.position -= m_offset;
//	}
//	return before;
//}
//
//
//eng::control::ControlLocalInput::ControlLocalInput(const eng::Input * pFrameInput, eng::vector2I offset)
//{
//	m_pInput = pFrameInput;
//	m_offset = offset;
//}
//
//
//const eng::Input & eng::control::ControlLocalInput::input() const
//{
//	return *m_pInput;
//}
//
//eng::vector2I eng::control::ControlLocalInput::offset() const
//{
//	return m_offset;
//}
//
//const eng::mouse_input_state eng::control::ControlLocalInput::initialState() const
//{
//	eng::mouse_input_state out = input().initialState();
//	out.position -= offset();
//	return out;
//}
//
//const eng::mouse_input_state eng::control::ControlLocalInput::latestState() const
//{
//	eng::mouse_input_state out = input().latestState();
//	out.position -= offset();
//	return out;
//}
//
//eng::control::ControlLocalInput::const_iterator eng::control::ControlLocalInput::mouseStates_begin() const
//{
//	return { m_pInput->mouseStates_begin(), m_pInput->mouseStates_end(), m_offset };
//}
//eng::control::ControlLocalInput::const_iterator eng::control::ControlLocalInput::mouseStates_end() const
//{
//	return { m_pInput->mouseStates_end(), m_pInput->mouseStates_end(), m_offset };
//}
//
//eng::control::ControlLocalInput::const_reverse_iterator eng::control::ControlLocalInput::mouseStates_rbegin() const
//{
//	return { m_pInput->mouseStates_rbegin(), m_pInput->mouseStates_rend(), m_offset };
//}
//eng::control::ControlLocalInput::const_reverse_iterator eng::control::ControlLocalInput::mouseStates_rend() const
//{
//	return { m_pInput->mouseStates_rend(), m_pInput->mouseStates_rend(), m_offset };
//}
//
//eng::control::ControlLocalInput::ControlLocalInput(ControlLocalInput && source)
//{
//	m_pInput = source.m_pInput;
//	m_offset = source.m_offset;
//	source.m_pInput = nullptr;
//}
//
//eng::control::ControlLocalInput::ControlLocalInput(const eng::Input * pFrameInput, eng::vector2I offset)
//{
//
//}
//
//eng::FrameInput_Desc eng::control::to_desc(const ControlLocalInput & source)
//{
//	FrameInput_Desc out;
//
//	out.keyStates = source.input().keyStates();
//	out.compositionString = source.input().compositionString();
//	out.initialState = source.initialState();
//	out.mouseStateChanges;
//	for (auto i = source.mouseStates_begin(); i != source.mouseStates_end(); i++)
//	{
//		out.mouseStateChanges.push_back(*i);
//	}
//
//	return out;
//}
