#include <eng/gpl/Label.h>


void eng::gpl::Label::set_Font(const eng::Font* pFont)
{
	eng::gpl::Control::set_Font(pFont);
	m_textDeviceMesh.set_Font(pFont);
}


void eng::gpl::Label::localRender(control::LocalGraphics & graphics) const
{
	gtm::vector2d scale = static_cast<gtm::vector2d>(getScale());

	if (getBackgoundMaterial())
	{
		eng::control::draw_quad(graphics, { gtm::vector2d{ 0.0, 0.0 }, scale - gtm::vector2d{ 0.0, 0.0 } }, getBackgoundMaterial());
	}
	if (getOutlineMaterial() && getOutlineEnabled())
	{
		eng::control::draw_rectangle(graphics, { { 0.5, 0.5 }, scale - gtm::vector2d{ 0.5,0.5 } }, 1, getOutlineMaterial());
	}
	if (getFontMaterial())
	{
		gtm::vector2d pos =
		{
			(double)((signed int)(getScale()[0] / 2) - (signed int)m_textDeviceMesh.get_Horizontal_Scale() / 2),
			(double)(getScale()[1] * 3 / 4)
		};

		m_textDeviceMesh.draw(graphics, pos, 0, { 1,1 }, getFontMaterial());
	}
}

void eng::gpl::Label::set_text_padding(gtm::vector2u padding)
{
	m_text_padding = padding;
}

gtm::vector2u eng::gpl::Label::get_text_padding() const
{
	return m_text_padding;
}

unsigned int eng::gpl::Label::calculateMinimumWidth() const
{
	return m_textDeviceMesh.get_Horizontal_Scale() + m_text_padding[0] * 2;
}

unsigned int eng::gpl::Label::calculateMinimumHeight() const
{
	return m_textDeviceMesh.get_Font()->getHeightInPixels() + m_text_padding[1] * 2;
}


void eng::gpl::Label::set_text(const std::wstring & s)
{
	m_textDeviceMesh.set_text(s);
}
const std::wstring & eng::gpl::Label::get_text() const
{
	return m_textDeviceMesh.get_text();
}

eng::gpl::Label::Label()
{

}

eng::gpl::Label::~Label()
{

}
