#include <eng/control/ioFrame.h>
#include <eng/control/default_draw.h>

void eng::control::ioFrame::localRender(eng::control::LocalGraphics & localGraphics) const
{
	if (getRenderTargetMaterial())
	{
		const void* additionalOverrides[1];
		additionalOverrides[0] = &renderTarget();
		eng::control::draw_meshEx<_countof(additionalOverrides)>
		(
			localGraphics, 
			localGraphics.getGraphicsRoot()->quadMesh(), 
			static_cast<gtm::vector2d>(getScale()) / 2.0,
			0, 
			static_cast<gtm::vector2d>(getScale()),
			getRenderTargetMaterial(), 
			additionalOverrides
		);
	}
}

void eng::control::ioFrame::setRenderTargetMaterial(const eng::Material2F* pMaterial)
{
	m_pRenderTargetMaterial = pMaterial;
}

const eng::Material2F * eng::control::ioFrame::getRenderTargetMaterial() const
{
	return m_pRenderTargetMaterial;
}

void eng::control::ioFrame::setScale(gtm::vector2u scale)
{
	std::scoped_lock<std::mutex> lock(m_fetchMutex);
	eng::control::Control::setScale(scale);
}

void eng::control::ioFrame::onLocalInput(const eng::control::local_input_state_change& localInput)
{
	std::scoped_lock<std::mutex> lock(m_fetchMutex);
	m_staged_input_changes.push_back(localInput.change);
}

eng::control::ioFrame::ioFrame() : m_renderTarget({ 1,1 }), m_input(eng::FrameInput_Desc())
{

}
eng::control::ioFrame::~ioFrame()
{

}

const gtm::vector2u& eng::control::ioFrame::getClientScale() const
{
	return m_clientScale;
}

eng::RenderTarget & eng::control::ioFrame::renderTarget()
{
	return m_renderTarget;
}

const eng::RenderTarget & eng::control::ioFrame::renderTarget() const
{
	return m_renderTarget;
}

const eng::Input & eng::control::ioFrame::input() const
{
	return m_input;
}

void eng::control::ioFrame::fetch()
{
	std::scoped_lock<std::mutex> lock(m_fetchMutex);

	m_clientScale = getScale();

	if (m_clientScale[0] == 0)
	{
		m_clientScale[0] = 1;
	}
	else if(m_clientScale[0] > 4096)
	{
		m_clientScale[0] = 4096;
	}
	if (m_clientScale[1] == 0)
	{
		m_clientScale[1] = 1;
	}
	else if (m_clientScale[1] > 4096)
	{
		m_clientScale[1] = 4096;
	}

	m_renderTarget.set_next_scale(m_clientScale);
#pragma warning("fix hit test")


	FrameInput_Desc forwared_desc = { };
	forwared_desc.initialState = m_staged_input_initial;
	forwared_desc.mouseStateChanges = m_staged_input_changes;

	m_input = eng::Input(std::move(forwared_desc));

	if (!m_staged_input_changes.empty())
	{
		m_staged_input_initial = m_staged_input_changes.back().current;
	}
	m_staged_input_changes.clear();
}
