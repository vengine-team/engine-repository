#include <eng/gpl/DropDownMenu.h>

const gtm::vector2u c_textPadding = { 10, 20 };
const unsigned int c_element_padding = 2;
const unsigned int c_outline_width = 1;
const double c_d_outline_width = (double)c_outline_width;

unsigned int eng::gpl::DropDownMenu::calculateWidestElement() const
{
	unsigned int widest = 0;

	for (size_t i = 0; i < m_elements.size(); i++)
	{
		unsigned int current = m_elements[i]->calculateWidth(m_elements[i]->calculateMinimumHeight());

		if (current > widest)
		{
			widest = current;
		}
	}
	
	return widest;
}

unsigned int eng::gpl::DropDownMenu::calculateVerticalDropLength() const
{
	unsigned int combined = 0;

	for (size_t i = 0; i < m_elements.size(); i++)
	{
		combined += m_elements[i]->calculateMinimumHeight();
	}

	return combined;
}
unsigned int eng::gpl::DropDownMenu::calculateHorizontalDropLength() const
{
	unsigned int combined = 0;

	for (size_t i = 0; i < m_elements.size(); i++)
	{
		combined += m_elements[i]->calculateWidth(m_elements[i]->calculateMinimumHeight());
	}

	return combined;
}

void eng::gpl::DropDownMenu::rescaleElements()
{
	unsigned int widest = calculateWidestElement();

	for (auto i = m_elements.begin(); i != m_elements.end(); i++)
	{
		unsigned int i_min_Height = (*i)->calculateMinimumHeight();
		(*i)->setScale({ widest, i_min_Height });
	}

}
void eng::gpl::DropDownMenu::repositionElements()
{
	unsigned int widest_element = calculateWidestElement();

	const int frame_width = c_element_padding;

	m_drop_frame.left = 0;
	m_drop_frame.right = widest_element + frame_width * 2;

	if (m_elements.size() > 0)
	{
		gtm::vector2i pen;

		switch (m_drop_properties.edge) 
		{
		case direction::Right:
			pen = { (gtm::vector2i::numeric_t)getScale()[0] + frame_width, frame_width };
			m_drop_frame.left = pen[0] - frame_width;
			m_drop_frame.right = pen[0] + widest_element + frame_width;
			break;
		case direction::Left:
			pen = { -(gtm::vector2i::numeric_t)m_elements.front()->getScale()[0] - frame_width, frame_width };
			m_drop_frame.right = pen[0] - frame_width;
			m_drop_frame.left = pen[0] + frame_width;
			break;
		case direction::Up:
			pen = { frame_width, -(gtm::vector2i::numeric_t)m_elements.front()->getScale()[1] - frame_width };
			m_drop_frame.bottom = 0;
			m_drop_frame.top = pen[1] - frame_width;
			break;
		case direction::Down:
			pen = { frame_width, (gtm::vector2i::numeric_t)getScale()[1] + frame_width };
			m_drop_frame.top = pen[1] - frame_width;
			m_drop_frame.bottom = pen[1] + (gtm::vector2i::numeric_t)getScale()[1] + frame_width;
			break;
		}

		switch (m_drop_properties.dropDir)
		{
		case direction::Right:
			m_drop_frame.left = pen[0] - frame_width;
			for (auto i = m_elements.begin(); i != m_elements.end(); i++)
			{
				(*i)->setLocalPosition(pen);

				pen[0] += (int)(*i)->getScale()[0];
			}
			m_drop_frame.right = pen[0] + frame_width;
			break;
		case direction::Left:
			m_drop_frame.right = pen[0] + (gtm::vector2i::numeric_t)m_elements.front()->getScale()[0] + frame_width;
			for (auto i = m_elements.begin(); i != m_elements.end(); i++)
			{
				(*i)->setLocalPosition(pen);

				pen[0] -= (int)(*i)->getScale()[0];
			}
			m_drop_frame.left = pen[0] - frame_width;
			break;
		case direction::Up:
			m_drop_frame.bottom = pen[1] + frame_width;
			for (auto i = m_elements.begin(); i != m_elements.end(); i++)
			{
				(*i)->setLocalPosition(pen);

				pen[1] -= (int)(*i)->getScale()[1];
			}
			m_drop_frame.top = pen[1] - frame_width;
			break;
		case direction::Down:
			m_drop_frame.top = pen[1] - frame_width;
			for (auto i = m_elements.begin(); i != m_elements.end(); i++)
			{
				(*i)->setLocalPosition(pen);

				pen[1] += (int)(*i)->getScale()[1];
			}
			m_drop_frame.bottom = pen[1] + frame_width;
			break;
		}
	}
}

void eng::gpl::DropDownMenu::drawLeftOutline(control::LocalGraphics & graphics) const
{

	eng::control::draw_quad(graphics, { { 0.0, 0.0 },{ c_d_outline_width, (double)getScale()[1] } }, getOutlineMaterial());
}
void eng::gpl::DropDownMenu::drawRightOutline(control::LocalGraphics & graphics) const
{
	eng::control::draw_quad(graphics, { { (double)getScale()[0] - c_d_outline_width,  c_d_outline_width }, {(double)getScale()[0] , (double)getScale()[1] } }, getOutlineMaterial());
}
void eng::gpl::DropDownMenu::drawTopOutline(control::LocalGraphics & graphics) const
{
	eng::control::draw_quad(graphics, { { 0.0, 0.0 }, { (double)getScale()[0], c_d_outline_width }}, getOutlineMaterial());
}
void eng::gpl::DropDownMenu::drawBottomOutline(control::LocalGraphics & graphics) const
{
	eng::control::draw_quad(graphics, { { 0.0, (double)getScale()[1] - c_d_outline_width },{ (double)getScale()[0], (double)getScale()[1] } }, getOutlineMaterial());
}


void eng::gpl::DropDownMenu::drawLeftOutlineJoint(control::LocalGraphics & graphics) const
{
	eng::control::draw_quad
	(
		graphics,
		{
			{ (double)getScale()[0], (double)getScale()[1] - c_d_outline_width },
			{ (double)(m_drop_frame.left + c_outline_width), (double)m_drop_frame.bottom }
		},
		getOutlineMaterial()
	);
}
void eng::gpl::DropDownMenu::drawRightOutlineJoint(control::LocalGraphics & graphics) const
{

}
void eng::gpl::DropDownMenu::drawTopOutlineJoint(control::LocalGraphics & graphics) const
{
	eng::control::draw_quad
	(
		graphics,
		{
			{ (double)getScale()[0] - c_d_outline_width,(double)getScale()[1] },
			{ (double)(m_drop_frame.right),(double)m_drop_frame.top + c_outline_width }
		},
		getOutlineMaterial()
	);
}
void eng::gpl::DropDownMenu::drawBottomOutlineJoint(control::LocalGraphics & graphics) const
{

}


void eng::gpl::DropDownMenu::drawLeftDropOutline(control::LocalGraphics & graphics) const
{
	eng::control::draw_quad
	(
		graphics,
		{
			{ (double)m_drop_frame.left,(double)m_drop_frame.top },
			{ (double)(m_drop_frame.left + c_outline_width),(double)m_drop_frame.bottom }
		},
		getOutlineMaterial()
	);
}
void eng::gpl::DropDownMenu::drawRightDropOutline(control::LocalGraphics & graphics) const
{
	eng::control::draw_quad
	(
		graphics,
		{
			{ (double)(m_drop_frame.right - c_outline_width),(double)m_drop_frame.top },
			{ (double)m_drop_frame.right, (double)m_drop_frame.bottom }
		},
		getOutlineMaterial()
	);
}
void eng::gpl::DropDownMenu::drawTopDropOutline(control::LocalGraphics & graphics) const
{
	eng::control::draw_quad
	(
		graphics,
		{
			{(double)m_drop_frame.left,
			(double)m_drop_frame.top},
		{(double)m_drop_frame.right,
			(double)(m_drop_frame.top + c_outline_width)}
		},
		getOutlineMaterial()
	);
}
void eng::gpl::DropDownMenu::drawBottomDropOutline(control::LocalGraphics & graphics) const
{
	eng::control::draw_quad
	(
		graphics,
		{
			{(double)m_drop_frame.left,
			(double)(m_drop_frame.bottom - c_outline_width) },
			{(double)m_drop_frame.right,
			(double)m_drop_frame.bottom}
		},
		getOutlineMaterial()
	);
}


bool eng::gpl::DropDownMenu::localHitTest(gtm::vector2i local_position) const
{
	if (m_dropped)
	{
		if (contains({ { m_drop_frame.left, m_drop_frame.top }, { m_drop_frame.right, m_drop_frame.bottom } }, local_position))
		{
			return true;
		}
		else
		{
			return false || eng::gpl::Control::localHitTest(local_position);
		}
	}
	else
	{
		return eng::gpl::Control::localHitTest(local_position);
	}
}

void eng::gpl::DropDownMenu::onLocalInput(const eng::control::local_input_state_change& localInput)
{
	m_highlightArea_hit = contains({ { 0,0 }, (gtm::vector2i)getScale() }, localInput.change.current.mouse.position);

	//works like a button
	if (getEnabled())
	{
		bool before_pressed = m_pressed;

		m_pressed =
			localInput.hit &&
			!localInput.hit_obstructed &&
			contains({ { 0,0 }, (gtm::vector2i)getScale() }, localInput.change.current.mouse.position) &&
			localInput.change.current.mouse.left_button;

		if (!before_pressed && m_pressed)
		{
			m_downEvent(*this);
		}
		else if (before_pressed && !m_pressed)
		{
			m_upEvent(*this);
		}
	}
	else
	{
		m_pressed = false;
	}


	if(!m_elements.empty() && getEnabled())
	{
		// actually is a drop down menu
		if (!m_dropped)
		{
			if (localInput.hit && !localInput.hit_obstructed)
			{
				if (localInput.change.type == eng::input_type::Mouse)
				{
					if (localInput.change.mouse_property == mouse_property::LeftButton)
					{
						m_dropped = true;

						for (size_t i = 0; i < m_elements.size(); i++)
						{
							m_elements[i]->setEnabled(m_dropped);
						}
					}
				}
			}
		}
		else
		{
			if (!localInput.hit && !m_children_hit)
			{
				if (localInput.change.type == eng::input_type::Mouse)
				{
					if (localInput.change.mouse_property == mouse_property::LeftButton && localInput.change.current.mouse.left_button)
					{
						m_dropped = false;

						for (size_t i = 0; i < m_elements.size(); i++)
						{
							m_elements[i]->setEnabled(m_dropped);
						}
					}
				}
			}
		}
	}
	else
	{
		m_dropped = false;

		for (size_t i = 0; i < m_elements.size(); i++)
		{
			m_elements[i]->setEnabled(m_dropped);
		}
	}
}

void eng::gpl::DropDownMenu::localRender(control::LocalGraphics & graphics) const
{
	gtm::vector2d scale = static_cast<gtm::vector2d>(getScale());

	if (getBackgoundMaterial())
	{
		eng::control::draw_quad(graphics, { { 0, 0 } , scale }, getBackgoundMaterial());
	}
	if (getPressedMaterial() && m_pressed && !m_dropped)
	{
		eng::control::draw_quad(graphics, { { 0, 0 } , scale }, getPressedMaterial());
	}


	if (!m_dropped && getHighlightedMaterial() && m_highlightArea_hit && !m_pressed)
	{
		eng::control::draw_quad(graphics, { { 0, 0 }, scale }, getHighlightedMaterial());
	}
	else if (m_dropped && !m_elements.empty() && getOutlineMaterial())
	{
		// draw background and outline of the dropped menu
		eng::control::draw_quad(graphics, m_drop_frame, getBackgoundMaterial());

		switch (m_drop_properties.edge)
		{
		case direction::Left:
			drawTopOutline(graphics);
			drawRightOutline(graphics);
			drawBottomOutline(graphics);

			drawRightOutlineJoint(graphics);

			drawLeftDropOutline(graphics);
			drawBottomDropOutline(graphics);
			drawTopDropOutline(graphics);
			break;
		case direction::Right:
			drawTopOutline(graphics);
			drawLeftOutline(graphics);
			drawBottomOutline(graphics);

			drawLeftOutlineJoint(graphics);

			drawRightDropOutline(graphics);
			drawBottomDropOutline(graphics);
			drawTopDropOutline(graphics);
			break;
		case direction::Up:
			drawLeftOutline(graphics);
			drawRightOutline(graphics);
			drawBottomOutline(graphics);

			drawBottomOutlineJoint(graphics);

			drawRightDropOutline(graphics);
			drawLeftDropOutline(graphics);
			drawTopDropOutline(graphics);
			break;
		case direction::Down:
			drawTopOutline(graphics);
			drawRightOutline(graphics);
			drawLeftOutline(graphics);

			drawTopOutlineJoint(graphics);

			drawRightDropOutline(graphics);
			drawLeftDropOutline(graphics);
			drawBottomDropOutline(graphics);
			break;
		}
	}


	if (getFontMaterial())
	{
		gtm::vector2d pos;
		pos[1] = (double)(getScale()[1] * 3 / 4);
		switch (m_text_alignment)
		{
		case eng::Text_Alignment::Left:
			pos[0] = c_textPadding[0];
			break;
		case eng::Text_Alignment::Center:
			pos[0] = (double)((signed int)(getScale()[0] / 2) - (signed int)m_textDeviceMesh.get_Horizontal_Scale() / 2);
			break;
		case eng::Text_Alignment::Right:
			pos[0] = (double)(getScale()[0] - (signed int)m_textDeviceMesh.get_Horizontal_Scale()) - c_textPadding[0];
			break;
		};


		m_textDeviceMesh.draw(graphics, pos, 0, { 1,1 }, getFontMaterial());
	}
}

ext::event_subscribe_access<eng::IButton&> eng::gpl::DropDownMenu::downEvent()
{
	return m_downEvent;
}
ext::event_subscribe_access<eng::IButton&> eng::gpl::DropDownMenu::upEvent()
{
	return m_upEvent;
}

bool eng::gpl::DropDownMenu::pressed() const
{
	return m_pressed;
}

size_t eng::gpl::DropDownMenu::get_submenu_element_count() const
{
	return m_elements.size();
}
const eng::IMenu& eng::gpl::DropDownMenu::get_submenu_element(size_t element_index) const
{
	return *m_elements[element_index];
}
eng::IMenu& eng::gpl::DropDownMenu::get_submenu_element(size_t element_index)
{
	return *m_elements[element_index];
}

bool eng::gpl::DropDownMenu::onInput(const eng::input_state_change& change, gtm::vector2i offset, bool obstructed)
{
	m_children_hit = false;

	for (auto i = std::begin(this->children()); i != std::end(this->children()); i++)
	{
		if ((*i)->onInput(change, offset + (*i)->getLocalPosition(), m_children_hit))
		{
			m_children_hit = true;
		}
	}

	eng::control::local_input_state_change local_input;
	local_input.hit_obstructed = obstructed || m_children_hit;
	local_input.hit = m_children_hit;

	local_input.change = change;

	local_input.change.before.mouse.position -= offset;
	local_input.change.current.mouse.position -= offset;

	local_input.hit = localHitTest(local_input.change.current.mouse.position);

	onLocalInput(local_input);

	return m_children_hit || local_input.hit;
}

void eng::gpl::DropDownMenu::render(eng::control::Graphics* pGraphics, gtm::vector2i offset) const
{
	if (getEnabled())
	{
		eng::control::LocalGraphics localGraphics = { pGraphics, offset };
		localRender(localGraphics);
		for (auto i = std::begin(this->children()); i != std::end(this->children()); i++)
		{
			(*i)->render(pGraphics, offset + (*i)->getLocalPosition());
		}
	}
}

void eng::gpl::DropDownMenu::setDropProperties(drop_properties drop_properties)
{
	m_drop_properties = drop_properties;
}

eng::gpl::drop_properties eng::gpl::DropDownMenu::getDropProperties() const
{
	return m_drop_properties;
}


unsigned int eng::gpl::DropDownMenu::calculateMinimumWidth() const
{
	if (get_Font())
	{
		return eng::calculate_rendered_length(get_text(), get_Font()) + c_textPadding[0] * 2;
	}
	else
	{
		return c_textPadding[0] * 2;
	}
}
unsigned int eng::gpl::DropDownMenu::calculateMinimumHeight() const
{
	return { c_textPadding[1] };
}

void eng::gpl::DropDownMenu::setScale(gtm::vector2u scale)
{
	eng::gpl::ControlParent<DropDownMenu>::setScale(scale);

	rescaleElements();
	repositionElements();
}

void eng::gpl::DropDownMenu::setTextAlignment(eng::Text_Alignment alignment)
{
	m_text_alignment = alignment;
}
eng::Text_Alignment eng::gpl::DropDownMenu::getTextAlignment() const
{
	return m_text_alignment;
}
void eng::gpl::DropDownMenu::set_text(const std::wstring & text)
{
	m_textDeviceMesh.set_text(text);
}
const std::wstring & eng::gpl::DropDownMenu::get_text() const
{
	return m_textDeviceMesh.get_text();
}

void eng::gpl::DropDownMenu::set_Font(const eng::Font * pFont)
{
	eng::gpl::ControlParent<DropDownMenu>::set_Font(pFont);
	m_textDeviceMesh.set_Font(pFont);

	rescaleElements();
	repositionElements();
}

eng::gpl::DropDownMenu::DropDownMenu(const Menu_Element_Desc& desc)
{
	m_textDeviceMesh.set_text(desc.text);

	m_elements.resize(desc.numSubmenuElements);

	for (size_t i = 0; i < desc.numSubmenuElements; i++)
	{
		m_elements[i].reset(new DropDownMenu(desc.pSubmenuElements[i]));
		m_elements[i]->setTextAlignment(eng::Text_Alignment::Left);
		m_elements[i]->setDropProperties({ direction::Right, direction::Down, direction::Down });
		m_elements[i]->setEnabled(false);
		children().push_back(m_elements[i].get());
	}

	rescaleElements();
	repositionElements();
}
eng::gpl::DropDownMenu::~DropDownMenu()
{
	while (children().size() > 0)
	{
		children().pop_back();
	}
}
