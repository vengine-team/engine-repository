#include <eng/control/default_draw.h>
#include <eng/graphicsUtil.h>

void eng::control::TextDeviceMesh2F::recalculateDeviceMesh()
{
	if (m_pFont && m_text.size() > 0)
	{
		m_deviceMesh.set(generate_text_mesh2F(m_text, m_pFont, 1, m_horizontalOffset));
		m_horizontalLength = calculate_rendered_length(m_text, m_pFont);
	}
	else
	{
		m_horizontalLength = 0;
	}
}

const eng::DeviceMesh2F & eng::control::TextDeviceMesh2F::deviceMesh() const
{
 	return m_deviceMesh;
}

void eng::control::TextDeviceMesh2F::set_Font(const eng::Font * pFont)
{
	m_pFont = pFont;
	recalculateDeviceMesh();
}
const eng::Font * eng::control::TextDeviceMesh2F::get_Font() const
{
	return m_pFont;
}

unsigned int eng::control::TextDeviceMesh2F::get_Horizontal_Scale() const
{
	return m_horizontalLength;
}

void eng::control::TextDeviceMesh2F::set_text(std::wstring && text)
{
	m_text = std::move(text);
	recalculateDeviceMesh();
}
void eng::control::TextDeviceMesh2F::set_text(const std::wstring & text)
{
	m_text = text;
	recalculateDeviceMesh();
}
const std::wstring & eng::control::TextDeviceMesh2F::get_text() const
{
	return m_text;
}

void eng::control::TextDeviceMesh2F::draw(LocalGraphics & graphics, gtm::vector2d position, double rotation, gtm::vector2d scale, const Material2F * pMaterial) const
{
	if (m_pFont && m_text.size() > 0)
	{
		const void* additionalOverrides[1];
		additionalOverrides[0] = m_pFont->fontTexture();
		eng::control::draw_meshEx<_countof(additionalOverrides)>(graphics, &deviceMesh(), position, rotation, scale, pMaterial, additionalOverrides);
	}
}

eng::control::TextDeviceMesh2F::TextDeviceMesh2F() : m_deviceMesh({})
{

}

eng::control::TextDeviceMesh2F::~TextDeviceMesh2F()
{

}





void eng::control::draw_mesh(LocalGraphics& graphics, const eng::DeviceMesh2F* pDeviceMesh, gtm::vector2d position, double rotation, gtm::vector2d scale, const Material2F * pMaterial)
{
	gtm::matrix<float, 4, 4> transform = gtm::identity_matrix<float, 4, 4>();
	transform[0][3] = static_cast<float>(position[0]);
	transform[1][3] = -static_cast<float>(position[1]);

	transform[0][0] = static_cast<float>(scale[0]);
	transform[1][1] = static_cast<float>(scale[1]);
	transform[2][2] = 1;

	//allocate transform DeviceBuffer
	DeviceBuffer transformConstantBuffer{ sizeof(transform) };
	transformConstantBuffer.store(&transform, sizeof(transform));

	std::array<void const*, 2> pNullOverrides
	{
		graphics.getLocalConstantBuffer(),
		&transformConstantBuffer
	};

	auto draw_range = eng::draw_mesh(pDeviceMesh, pMaterial, pNullOverrides.begin(), pNullOverrides.end());
	
	graphics.getGraphicsRoot()->getRenderTarget()->draw(draw_range.begin(), draw_range.end());
}


void eng::control::draw_quad(LocalGraphics & graphics, gtm::vector2d position, double rotation, gtm::vector2d scale, const Material2F * pMaterial)
{
	eng::control::draw_mesh(graphics, graphics.getGraphicsRoot()->quadMesh(), position, rotation, scale, pMaterial);
}

void eng::control::draw_quad(LocalGraphics & graphics, gtm::rectangle<double> rect, const Material2F * pMaterial)
{
	gtm::vector2d center = { gtm::lerp(rect.left, rect.right, 0.5), gtm::lerp(rect.top, rect.bottom, 0.5) };

	draw_quad(graphics, center, 0, { rect.left - rect.right, rect.bottom - rect.top }, pMaterial);
}

void eng::control::draw_rectangle(LocalGraphics & graphics, gtm::rectangle<double> rect, double thickness, const Material2F * pMaterial)
{

	gtm::vector2d horizontalLineScale = { rect.right - rect.left, thickness };
	gtm::vector2d verticalLineScale = { thickness, rect.bottom - rect.top };

	gtm::vector2d topLineCenter = { gtm::lerp(rect.left + thickness, rect.right, 0.5) , rect.top };
	draw_quad(graphics, topLineCenter, 0, horizontalLineScale, pMaterial);

	gtm::vector2d rightLineCenter = { rect.right , gtm::lerp(rect.top, rect.bottom + thickness, 0.5) };
	draw_quad(graphics, rightLineCenter, 0, verticalLineScale, pMaterial);

	gtm::vector2d bottomLineCenter = { gtm::lerp(rect.left - thickness, rect.right,0.5) , rect.bottom };
	draw_quad(graphics, bottomLineCenter, 0, horizontalLineScale, pMaterial);

	gtm::vector2d leftLineCenter = { rect.left , gtm::lerp(rect.top, rect.bottom - thickness, 0.5) };
	draw_quad(graphics, leftLineCenter, 0, verticalLineScale, pMaterial);
}
