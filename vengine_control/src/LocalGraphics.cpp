#include <eng/control/default_draw.h>

eng::control::Graphics * eng::control::LocalGraphics::getGraphicsRoot()
{
	return m_pControlGraphics;
}

const eng::DeviceBuffer * eng::control::LocalGraphics::getLocalConstantBuffer()
{
	return &m_localConstantBuffer;
}

eng::control::LocalGraphics::LocalGraphics(eng::control::LocalGraphics && source) : 
	m_localConstantBuffer(std::move(source.m_localConstantBuffer))
{
	m_pControlGraphics = source.m_pControlGraphics;
	source.m_pControlGraphics = nullptr;
}

eng::control::LocalGraphics& eng::control::LocalGraphics::operator=(LocalGraphics && source)

{
	m_localConstantBuffer = std::move(source.m_localConstantBuffer);
	m_pControlGraphics = source.m_pControlGraphics;
	source.m_pControlGraphics = nullptr;

	return *this;
}

eng::control::LocalGraphics::LocalGraphics(eng::control::Graphics * controlGraphics, gtm::vector2i position) : m_localConstantBuffer(sizeof(gtm::matrix<float, 4, 4>))
{
	m_pControlGraphics = controlGraphics;

	gtm::matrix<float, 4, 4> transform = gtm::identity_matrix<float, 4, 4>();
	transform[0][0] = 2.0f / controlGraphics->getRenderTarget()->get_next_scale()[0];
	transform[1][1] = 2.0f / controlGraphics->getRenderTarget()->get_next_scale()[1];

	transform[0][3] = position[0] * transform[0][0] - 1;
	transform[1][3] = -position[1] * transform[1][1] + 1;

	m_localConstantBuffer.store(&transform, sizeof(gtm::matrix<float, 4, 4>));
}
eng::control::LocalGraphics::~LocalGraphics()
{

}