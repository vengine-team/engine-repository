#include <eng/gpl/Control_ioFrame.h>
#include <eng/control/LocalGraphics.h>
#include <eng/control/default_draw.h>

void eng::gpl::Control_ioFrame::localRender(eng::control::LocalGraphics & localGraphics) const
{
	if (getBackgoundMaterial())
	{
		eng::control::draw_quad(localGraphics, { { 0.0,0.0 }, static_cast<gtm::vector2d>(getScale()) }, getBackgoundMaterial());
	}

	eng::control::ioFrame::localRender(localGraphics);
}

eng::gpl::Control_ioFrame::~Control_ioFrame()
{

}