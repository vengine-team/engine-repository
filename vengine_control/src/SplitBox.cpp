#include <eng/gpl/SplitBox.h>
#include <eng/control/default_draw.h>

const gtm::vector2u c_splitBoxPadding = { 0, 0 };
const unsigned int c_splitBoxDividerScale = 10;
const unsigned int c_smallestChildScale = 10;

void eng::gpl::SplitBox_Tree::localRender(eng::control::LocalGraphics & graphics) const
{
}

void eng::gpl::SplitBox_Tree::setScale(gtm::vector2u scale)
{
	base_t::setScale(scale);

	if (base_t::root())
	{
		base_t::root()->setLocalPosition((gtm::vector2i)c_splitBoxPadding);

		base_t::root()->setScale({ getScale()[0] - c_splitBoxPadding[0] * 2 , getScale()[1] - c_splitBoxPadding[1] * 2 });
	}
}
	

eng::gpl::SplitBox_Tree::SplitBox_Tree(SplitBox_System& system) : base_t(system)
{

}
eng::gpl::SplitBox_Tree::~SplitBox_Tree()
{

}



unsigned int eng::gpl::SplitBox_Node::getSlideWidth() const
{
	int slide_width = (int)getScale()[0] - c_splitBoxDividerScale - c_smallestChildScale;
	if (slide_width < c_smallestChildScale * 2)
	{
		slide_width = c_smallestChildScale * 2;
	}

	return (unsigned int)slide_width;
}

void eng::gpl::SplitBox_Node::onLocalInput(const eng::control::local_input_state_change & localchange)
{
	unsigned int slide_width = getSlideWidth();

	base_t::onLocalInput(localchange);

	int distance_to_divider = (int)((double)slide_width * m_divider_begin);


	if (!m_dragging)
	{

		gtm::vector2i divider_top_left = { distance_to_divider, 0 };

		gtm::vector2i divider_bottom_right = { distance_to_divider + (int)c_splitBoxDividerScale, (int)getScale()[1] };
		
		if (contains({ divider_top_left, divider_bottom_right }, localchange.change.current.mouse.position))
		{
			if (!localchange.change.before.mouse.left_button && localchange.change.current.mouse.left_button)
			{
				m_dragging = true;
				m_distanceToDividerBegin = distance_to_divider;
				m_dragBegin = localchange.change.current.mouse.position[0];
			}
		}
	}
	else
	{
		if (!localchange.change.current.mouse.left_button)
		{
			m_dragging = false;
		}
		else
		{

			if (localchange.change.current.mouse.position[0] - localchange.change.before.mouse.position[0] != 0)
			{
				distance_to_divider = m_distanceToDividerBegin + (localchange.change.before.mouse.position[0] - m_dragBegin);

				if (distance_to_divider < (int)c_smallestChildScale)
				{
					distance_to_divider = c_smallestChildScale;
				}
				else if (distance_to_divider >(int)slide_width)
				{
					distance_to_divider = (int)slide_width;
				}

				m_divider_begin = (double)distance_to_divider / (double)slide_width;

				setScale(getScale());
			}
		}
	}
}

void eng::gpl::SplitBox_Node::localRender(eng::control::LocalGraphics & graphics) const
{

}

void eng::gpl::SplitBox_Node::setScale(gtm::vector2u scale)
{
	base_t::setScale(scale);

	unsigned int heigth = getScale()[1];
	if (heigth < c_smallestChildScale)
	{
		heigth = c_smallestChildScale;
	}

	unsigned int slide_width = getSlideWidth();

	unsigned int distance_to_divider = (unsigned int)((double)slide_width * m_divider_begin);

	eng::gpl::Control& left = get_SplitBoxChild(0);
	left.setLocalPosition({0,0});
	left.setScale({ (unsigned int)distance_to_divider, heigth });
	eng::gpl::Control& right = get_SplitBoxChild(1);
	right.setLocalPosition({ (int)distance_to_divider + (int)c_splitBoxDividerScale, 0 });
	right.setScale({ getScale()[0] - distance_to_divider - c_splitBoxDividerScale, heigth });
}

eng::gpl::SplitBox_Node::SplitBox_Node(std::array<std::unique_ptr<eng::gpl::SplitBox_Child>, 2>&& pChildren) : base_t(std::move(pChildren))
{

}
eng::gpl::SplitBox_Node::~SplitBox_Node()
{

}



void eng::gpl::SplitBox_Leaf::localRender(eng::control::LocalGraphics & graphics) const
{
	if (getPressedMaterial())
	{
		eng::control::draw_quad(graphics, gtm::rectangle<double>( { 0.0,0.0 }, (gtm::vector2d)getScale() ), getPressedMaterial());
	}
}

void eng::gpl::SplitBox_Leaf::setScale(gtm::vector2u scale)
{
	base_t::setScale(scale);

	get_element_Control().setLocalPosition((gtm::vector2i)c_splitBoxPadding);
	get_element_Control().setScale({ getScale()[0] - c_splitBoxPadding[0] * 2 , getScale()[1] - c_splitBoxPadding[1] * 2 });
}
eng::gpl::SplitBox_Leaf::SplitBox_Leaf(SplitBox_Element& element) : base_t(element)
{
	get_element_Control().setLocalPosition({ 0,0 });
}
eng::gpl::SplitBox_Leaf::~SplitBox_Leaf()
{

}
