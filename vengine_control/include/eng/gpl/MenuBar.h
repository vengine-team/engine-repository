#pragma once
#include <eng/gpl/Forward_Node.h>
#include <eng/gpl/DropDownMenu.h>
#include <eng/IMenu.h>

#include <vector>


namespace eng
{
	namespace gpl
	{
		class MenuBar : public eng::gpl::ControlParent<eng::gpl::Control>
		{
		public:
			unsigned int m_textSize;

			std::vector<std::unique_ptr<eng::gpl::DropDownMenu>> m_buttons;

			void rescaleButtons();
			void positionButtons(unsigned int horizontalScale);
			
			gtm::vector2i calculate_next_position(unsigned int highest_element, unsigned int width, gtm::vector2i last_position, size_t button_index) const;

			unsigned int calculate_highest_button() const;

		public:

			size_t get_submenu_element_count() const;
			const eng::IMenu& get_submenu_element(size_t element_index) const;
			eng::IMenu& get_submenu_element(size_t element_index);

			virtual unsigned int calculateMinimumWidth() const override;
			virtual unsigned int calculateMinimumHeight() const override;

			virtual unsigned int calculateHeight(unsigned int width) const override;

			virtual void set_Font(const eng::Font* pFont);

			virtual void setTextSize(unsigned int size);
			unsigned int getTextSize() const;

			virtual void setScale(gtm::vector2u scale) override;

			MenuBar(size_t numElements, const eng::gpl::Menu_Element_Desc* pDescs);
			virtual ~MenuBar();
		};
	};
};