#pragma once
#include <eng/control/ioFrame.h>
#include <eng/gpl/Forward_Node.h>

namespace eng
{
	namespace gpl
	{
		class Control_ioFrame : public Control, public eng::control::ioFrame
		{
		public:
			virtual void localRender(eng::control::LocalGraphics& localGraphics) const override;
			virtual ~Control_ioFrame();
		};
	};
};