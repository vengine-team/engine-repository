#pragma once
#include <eng/gpl/Forward_Node.h>
#include <eng/control/default_draw.h>

namespace eng
{
	namespace gpl
	{
		class Label : public eng::gpl::Control
		{
			gtm::vector2u m_text_padding = { 6, 6 };

			eng::control::TextDeviceMesh2F m_textDeviceMesh;

		public:

			virtual void set_Font(const eng::Font* pFont) override;

			virtual void localRender(control::LocalGraphics& graphics) const override;

			void set_text_padding(gtm::vector2u padding);
			gtm::vector2u get_text_padding() const;

			virtual unsigned int calculateMinimumWidth() const;
			virtual unsigned int calculateMinimumHeight() const;

			void set_text(const std::wstring& s);
			const std::wstring& get_text() const;

			Label();
			virtual ~Label();
		};
	};
};