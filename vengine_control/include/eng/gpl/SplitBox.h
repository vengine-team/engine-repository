#pragma once
#include "eng/gpl/Forward_Node.h"
#include "eng/control/SplitBox.h"

namespace eng
{
	namespace gpl
	{
		class SplitBox_Leaf;
		class SplitBox_Node;
		class SplitBox_Tree;

		using SplitBox_Child = eng::control::SplitBox_Child<gpl::ControlParent<gpl::Control>, gpl::Control, gpl::SplitBox_Tree, gpl::SplitBox_Node, gpl::SplitBox_Leaf>;
		using SplitBox_System = eng::control::SplitBox_System<gpl::ControlParent<gpl::Control>, gpl::Control, gpl::SplitBox_Tree, gpl::SplitBox_Node, gpl::SplitBox_Leaf>;
		using SplitBox_Element = eng::control::SplitBox_Element<gpl::ControlParent<gpl::Control>, gpl::Control, gpl::SplitBox_Tree, gpl::SplitBox_Node, gpl::SplitBox_Leaf>;

		class SplitBox_Tree : 
			public eng::control::SplitBox_Tree
			<gpl::ControlParent<gpl::Control>, gpl::Control, gpl::SplitBox_Tree, gpl::SplitBox_Node, gpl::SplitBox_Leaf>
		{
			using base_t = eng::control::SplitBox_Tree<gpl::ControlParent<gpl::Control>, gpl::Control, gpl::SplitBox_Tree, gpl::SplitBox_Node, gpl::SplitBox_Leaf>;

		public:

			virtual void localRender(eng::control::LocalGraphics& graphics) const override;

			virtual void setScale(gtm::vector2u scale) override;

			SplitBox_Tree(SplitBox_System& system);
			virtual ~SplitBox_Tree();
		};

		class SplitBox_Node :
			public eng::control::SplitBox_Node
			<gpl::ControlParent<gpl::Control>, gpl::Control, gpl::SplitBox_Tree, gpl::SplitBox_Node, gpl::SplitBox_Leaf>
		{
			using base_t = eng::control::SplitBox_Node<gpl::ControlParent<gpl::Control>, gpl::Control, gpl::SplitBox_Tree, gpl::SplitBox_Node, gpl::SplitBox_Leaf>;
		
			bool m_dragging = false;
			int m_distanceToDividerBegin = 0;
			int m_dragBegin = 0;

			double m_divider_begin = 0.5;

			unsigned int getSlideWidth() const;

		public:

			virtual void onLocalInput(const eng::control::local_input_state_change& change) override;

			virtual void localRender(eng::control::LocalGraphics& graphics) const override;

			virtual void setScale(gtm::vector2u scale) override;

			SplitBox_Node(std::array<std::unique_ptr<eng::gpl::SplitBox_Child>, 2>&& pChildren);
			virtual ~SplitBox_Node();
		};

		class SplitBox_Leaf :
			public eng::control::SplitBox_Leaf
			<gpl::ControlParent<gpl::Control>, gpl::Control, gpl::SplitBox_Tree, gpl::SplitBox_Node, gpl::SplitBox_Leaf>
		{
			using base_t = eng::control::SplitBox_Leaf<gpl::ControlParent<gpl::Control>, gpl::Control, gpl::SplitBox_Tree, gpl::SplitBox_Node, gpl::SplitBox_Leaf>;

		public:

			virtual void localRender(eng::control::LocalGraphics& graphics) const override;

			virtual void setScale(gtm::vector2u scale) override;

			SplitBox_Leaf(SplitBox_Element& element);
			virtual ~SplitBox_Leaf();
		};

	};
};