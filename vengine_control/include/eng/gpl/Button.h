#pragma once
#include <eng/gpl/Forward_Node.h>
#include <eng/IButton.h>
#include <eng/control/default_draw.h>

namespace eng
{
	namespace gpl
	{
		class Button : public virtual eng::gpl::Control, public eng::IButton
		{
		private:

			ext::event<IButton&>					m_down_event;
			ext::event<IButton&>					m_up_event;

			bool							m_highlightArea_hit = false;
			bool							m_pressed = false;

			eng::control::TextDeviceMesh2F	m_textDeviceMesh;

		protected:

			virtual void onLocalInput(const eng::control::local_input_state_change& localInput) override;

		public:

			virtual ext::event_subscribe_access<IButton&> downEvent() override;
			virtual ext::event_subscribe_access<IButton&> upEvent() override;

			virtual bool pressed() const override;

			virtual unsigned int calculateMinimumWidth() const override;
			virtual unsigned int calculateMinimumHeight() const override;

			virtual void set_text(const std::wstring& text);
			const std::wstring& get_text() const;

			virtual void localRender(eng::control::LocalGraphics& graphics) const override;

			virtual void set_Font(const eng::Font* pFont) override;

			Button();
			virtual ~Button();
		};
	}
}