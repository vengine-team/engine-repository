#pragma once
#pragma warning( disable : 4250)  
#include "eng/control/Forward_Node.h"
#include <eng/Material.h>
#include <eng/Font.h>

namespace eng
{
	namespace gpl
	{
		class Control : public virtual eng::control::Control
		{
		private:

			const eng::Font*		m_pFont = nullptr;
			const eng::Material2F*	m_pFontMaterial = nullptr;
			const eng::Material2F*	m_pTextureMaterial = nullptr;

			bool					m_outline_enabled = false;

			const eng::Material2F*	m_pBackgroundMaterial = nullptr;
			const eng::Material2F*	m_pHighlightedMaterial = nullptr;
			const eng::Material2F*	m_pPressedMaterial = nullptr;
			const eng::Material2F*	m_pOutlineMaterial = nullptr;

		public:

			virtual void localRender(eng::control::LocalGraphics& localGraphics) const override;


			virtual void set_Font(const eng::Font* pFont);
			virtual void setFontMaterial(const eng::Material2F* pMaterial);
			virtual void setTextureMaterial(const eng::Material2F* pMaterial);

			virtual void setBackgroundMaterial(const eng::Material2F* pMaterial);
			virtual void setHighlightedMaterial(const eng::Material2F* pMaterial);
			virtual void setPressedMaterial(const eng::Material2F* pMaterial);
			virtual void setOutlineMaterial(const eng::Material2F* pMaterial);


			const eng::Font* get_Font() const;
			const eng::Material2F* getFontMaterial() const;
			const eng::Material2F* getTextureMaterial() const;

			virtual void setOutlineEnabled(bool b);
			bool getOutlineEnabled() const;

			const eng::Material2F* getBackgoundMaterial() const;
			const eng::Material2F* getHighlightedMaterial() const;
			const eng::Material2F* getPressedMaterial() const;
			const eng::Material2F* getOutlineMaterial() const;

			Control();
			virtual ~Control();
		};

		template<typename Child_t>
		class ControlParent :
			public virtual eng::gpl::Control,
			public eng::control::Basic_ControlParent<Control>
		{
		public:

			virtual void on_child_added(eng::gpl::Control* pChild) override
			{
				pChild->set_Font(get_Font());
				pChild->setFontMaterial(getFontMaterial());
				pChild->setTextureMaterial(getTextureMaterial());

				pChild->setBackgroundMaterial(getBackgoundMaterial());
				pChild->setHighlightedMaterial(getHighlightedMaterial());
				pChild->setPressedMaterial(getPressedMaterial());
				pChild->setOutlineMaterial(getOutlineMaterial());
			}

			virtual void set_Font(const eng::Font* pFont)
			{
				Control::set_Font(pFont);

				for (auto i = std::begin(this->children()); i != std::end(this->children()); i++)
				{
					(*i)->set_Font(pFont);
				}
			}
			virtual void setFontMaterial(const eng::Material2F* pMaterial)
			{
				Control::setFontMaterial(pMaterial);

				for (auto i = std::begin(this->children()); i != std::end(this->children()); i++)
				{
					(*i)->setFontMaterial(pMaterial);
				}
			}
			virtual void setTextureMaterial(const eng::Material2F* pMaterial)
			{
				Control::setTextureMaterial(pMaterial);

				for (auto i = std::begin(this->children()); i != std::end(this->children()); i++)
				{
					(*i)->setTextureMaterial(pMaterial);
				}
			}

			virtual void setBackgroundMaterial(const eng::Material2F* pMaterial)
			{
				Control::setBackgroundMaterial(pMaterial);

				for (auto i = std::begin(this->children()); i != std::end(this->children()); i++)
				{
					(*i)->setBackgroundMaterial(pMaterial);
				}
			}
			virtual void setHighlightedMaterial(const eng::Material2F* pMaterial)
			{
				Control::setHighlightedMaterial(pMaterial);

				for (auto i = std::begin(this->children()); i != std::end(this->children()); i++)
				{
					(*i)->setHighlightedMaterial(pMaterial);
				}
			}
			virtual void setPressedMaterial(const eng::Material2F* pMaterial)
			{
				Control::setPressedMaterial(pMaterial);

				for (auto i = std::begin(this->children()); i != std::end(this->children()); i++)
				{
					(*i)->setPressedMaterial(pMaterial);
				}
			}
			virtual void setOutlineMaterial(const eng::Material2F* pMaterial)
			{
				Control::setOutlineMaterial(pMaterial);

				for (auto i = std::begin(this->children()); i != std::end(this->children()); i++)
				{
					(*i)->setOutlineMaterial(pMaterial);
				}
			}

			ControlParent()
			{

			}
			virtual ~ControlParent()
			{

			}
		};
	};
};
