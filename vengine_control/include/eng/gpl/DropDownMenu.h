#pragma once
#include <string>

#include <eng/gpl/Forward_Node.h>
#include <eng/control/default_draw.h>
#include <eng/IMenu.h>

namespace eng
{
	namespace gpl
	{
		struct Menu_Element_Desc
		{
			std::wstring				text = L"no Text";
			size_t						numSubmenuElements = 0;
			const Menu_Element_Desc*	pSubmenuElements;
		};

		enum class direction
		{
			Right,
			Left,
			Up,
			Down
		};

		struct drop_properties
		{
			direction edge = direction::Down;
			direction dropDir = direction::Down;
			direction overflowDir = direction::Right;
		};

		class DropDownMenu : public virtual eng::gpl::Control, protected eng::gpl::ControlParent<eng::gpl::DropDownMenu>, public IMenu
		{
		private:

			ext::event<eng::IButton&>		m_downEvent;
			ext::event<eng::IButton&>		m_upEvent;

			gtm::rectangle<int>				m_drop_frame;

			drop_properties					m_drop_properties;
			
			std::vector<std::unique_ptr<DropDownMenu>>		m_elements;
			gtm::vector2u					m_elements_scale;

			bool							m_pressed = false;
			bool							m_highlightArea_hit = false;
			bool							m_children_hit = false;
			bool							m_dropped = false;

			eng::Text_Alignment				m_text_alignment = eng::Text_Alignment::Center;
			eng::control::TextDeviceMesh2F	m_textDeviceMesh;

			unsigned int calculateWidestElement() const;

			unsigned int calculateVerticalDropLength() const;
			unsigned int calculateHorizontalDropLength() const;

			void rescaleElements();
			void repositionElements();

			void drawLeftOutline(control::LocalGraphics & graphics) const;
			void drawRightOutline(control::LocalGraphics & graphics) const;
			void drawTopOutline(control::LocalGraphics & graphics) const;
			void drawBottomOutline(control::LocalGraphics & graphics) const;


			void drawLeftOutlineJoint(control::LocalGraphics & graphics) const;
			void drawRightOutlineJoint(control::LocalGraphics & graphics) const;
			void drawTopOutlineJoint(control::LocalGraphics & graphics) const;
			void drawBottomOutlineJoint(control::LocalGraphics & graphics) const;


			void drawLeftDropOutline(control::LocalGraphics & graphics) const;
			void drawRightDropOutline(control::LocalGraphics & graphics) const;
			void drawTopDropOutline(control::LocalGraphics & graphics) const;
			void drawBottomDropOutline(control::LocalGraphics & graphics) const;

		protected:

			virtual bool localHitTest(gtm::vector2i position) const;

			virtual void onLocalInput(const eng::control::local_input_state_change& localInput) override;

			virtual void localRender(control::LocalGraphics& graphics) const override;

		public:

			virtual ext::event_subscribe_access<eng::IButton&> downEvent() override;
			virtual ext::event_subscribe_access<eng::IButton&> upEvent() override;

			virtual bool pressed() const override;

			virtual size_t get_submenu_element_count() const override;
			virtual const eng::IMenu& get_submenu_element(size_t element_index) const override;
			virtual eng::IMenu& get_submenu_element(size_t element_index) override;



			virtual bool onInput(const eng::input_state_change& change, gtm::vector2i offset, bool obstructed) override;
			
			virtual void render(eng::control::Graphics* pGraphics, gtm::vector2i offset) const override;

			void setDropProperties(drop_properties drop_properties);
			drop_properties getDropProperties() const;

			virtual unsigned int calculateMinimumWidth() const override;
			virtual unsigned int calculateMinimumHeight() const override;

			virtual void setScale(gtm::vector2u scale) override;

			virtual void setTextAlignment(eng::Text_Alignment alignment);
			eng::Text_Alignment getTextAlignment() const;
			virtual void set_text(const std::wstring& text);
			const std::wstring& get_text() const;

			virtual void set_Font(const eng::Font* pFont) override;

			DropDownMenu(const Menu_Element_Desc& desc);
			virtual ~DropDownMenu();
		};
	};
};