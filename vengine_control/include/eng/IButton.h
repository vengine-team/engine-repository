#pragma once
#include <ext/event.h>

namespace eng
{
	class IButton
	{
	public:

		virtual ext::event_subscribe_access<IButton&> downEvent() = 0;
		virtual ext::event_subscribe_access<IButton&> upEvent() = 0;

		virtual bool pressed() const = 0;

		virtual ~IButton() = default;
	};
}