#pragma once
#include <eng/IButton.h>

namespace eng
{
	class IMenu : public IButton
	{
	public:

		virtual size_t get_submenu_element_count() const = 0;
		virtual const eng::IMenu& get_submenu_element(size_t element_index) const = 0;
		virtual eng::IMenu& get_submenu_element(size_t element_index) = 0;

		virtual ~IMenu() = default;
	};
}