#pragma once
#include "eng/control/Forward_Node.h"

namespace eng
{
	namespace control
	{
		void append_vertical(int& pen_y, gtm::rectangle<int> rect, eng::control::Control& control);
		void append_vertical_fill(int& pen_y, gtm::rectangle<int> rect, eng::control::Control& control);

		gtm::rectangle<int> padded_rectangle(gtm::vector2u scale, gtm::vector2u padding);

		gtm::vector2u calculate_widest_scale(const eng::control::Control& control);
	};
};
