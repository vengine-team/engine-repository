#pragma once
#include <eng/Input.h>

namespace eng
{
	namespace control
	{
		struct local_input_state_change final
		{
			bool hit;

			bool hit_obstructed;

			input_state_change change;
		};

		/*class ControlLocalInput
		{
			const eng::Input* m_pInput;
			eng::vector2I m_offset;

		public:

			struct const_mouse_input_change_iterator final
			{
				friend class ControlLocalInput;
			private:
				eng::Input::const_iterator m_underlying_end;
				eng::Input::const_iterator m_underlying;
				eng::vector2I m_offset;
				eng::mouse_input_state_change m_composedState;
				const_mouse_input_change_iterator(const eng::Input::const_iterator& underlying, const eng::Input::const_iterator& underlying_end, vector2I offset);
			public:

				const eng::mouse_input_state_change& operator*() const;
				const eng::mouse_input_state_change* operator->() const;
				bool operator==(const const_mouse_input_change_iterator& other) const;
				bool operator!=(const const_mouse_input_change_iterator& other) const;
				const_mouse_input_change_iterator operator++();
				const_mouse_input_change_iterator operator--();
				const_mouse_input_change_iterator operator++(int);
				const_mouse_input_change_iterator operator--(int);
			};
			struct const_mouse_input_change_reverse_iterator final
			{
				friend class ControlLocalInput;
			private:				
				eng::Input::const_reverse_iterator m_underlying_rend;
				eng::Input::const_reverse_iterator m_underlying;
				eng::vector2I m_offset;
				eng::mouse_input_state_change m_composedState;
				const_mouse_input_change_reverse_iterator(const eng::Input::const_reverse_iterator& underlying, const eng::Input::const_reverse_iterator& underlying_end, vector2I offset);
			public:
				const eng::mouse_input_state_change& operator*() const;
				const eng::mouse_input_state_change* operator->() const;
				bool operator==(const const_mouse_input_change_reverse_iterator& other) const;
				bool operator!=(const const_mouse_input_change_reverse_iterator& other) const;
				const_mouse_input_change_reverse_iterator operator++();
				const_mouse_input_change_reverse_iterator operator--();
				const_mouse_input_change_reverse_iterator operator++(int);
				const_mouse_input_change_reverse_iterator operator--(int);
			};

			typedef const_mouse_input_change_iterator const_iterator;
			typedef const_mouse_input_change_reverse_iterator const_reverse_iterator;

			const mouse_input_state initialState() const;
			const mouse_input_state latestState() const;

			const_iterator mouseStates_begin() const;
			const_iterator mouseStates_end() const;

			const_reverse_iterator mouseStates_rbegin() const;
			const_reverse_iterator mouseStates_rend() const;

			ControlLocalInput(ControlLocalInput&& source);
			ControlLocalInput(const ControlLocalInput& source) = delete;
			ControlLocalInput(const eng::Input* pFrameInput, eng::vector2I offset);

			const eng::Input& input() const;
			eng::vector2I offset() const;
		};
*/
/*
		eng::FrameInput_Desc to_desc(const ControlLocalInput & source);*/
	};
};