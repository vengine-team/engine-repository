#pragma once
#include <eng/control/LocalGraphics.h>
#include <eng/defaultDraw.h>

#include <eng/Font.h>

#include <gtm/math.h>


namespace eng
{
	namespace control
	{
		class TextDeviceMesh2F
		{
		private:
			eng::DeviceMesh2F	m_deviceMesh;

			const eng::Font*	m_pFont = nullptr;

			std::wstring		m_text = L"";

			float				m_horizontalOffset = 0;

			unsigned int		m_horizontalLength;

			void recalculateDeviceMesh();

		public:
			const eng::DeviceMesh2F& deviceMesh() const;
			
			void set_Font(const eng::Font* pFont);
			const eng::Font* get_Font() const;

			unsigned int get_Horizontal_Scale() const;

			void set_text(std::wstring&& text);
			void set_text(const std::wstring& text);
			const std::wstring& get_text() const;

			void draw(control::LocalGraphics& graphics, gtm::vector2d position, double rotation, gtm::vector2d scale, const Material2F * pMaterial) const;

			TextDeviceMesh2F();
			virtual ~TextDeviceMesh2F();
		};

		void draw_mesh(LocalGraphics& graphics, const eng::DeviceMesh2F* pDeviceMesh, gtm::vector2d position, double rotation, gtm::vector2d scale, const Material2F * pMaterial);

		template<size_t num_AdditionalNullOverrides>
		void draw_meshEx(LocalGraphics & graphics, const eng::DeviceMesh2F * pDeviceMesh, gtm::vector2d position, double rotation, gtm::vector2d scale, const Material2F * pMaterial, const void ** additionalNullOverrides)
		{
			gtm::matrix<float, 4, 4> transform = gtm::identity_matrix<float, 4, 4>();
			transform[0][3] = static_cast<float>(position[0]);
			transform[1][3] = -static_cast<float>(position[1]);

			transform[0][0] = static_cast<float>(scale[0]);
			transform[1][1] = static_cast<float>(scale[1]);
			transform[2][2] = 1;

			//allocate transform DeviceBuffer
			DeviceBuffer transformConstantBuffer{ sizeof(transform) };
			transformConstantBuffer.store(&transform, sizeof(transform));

			std::array<void const*, 2 + num_AdditionalNullOverrides> nullOverrides
			{
				graphics.getLocalConstantBuffer(),
				&transformConstantBuffer
			};

			for (size_t i = 0; i < num_AdditionalNullOverrides; i++)
			{
				nullOverrides[i + 2] = additionalNullOverrides[i];
			}

			auto draw_range = eng::draw_mesh(pDeviceMesh, pMaterial, nullOverrides.begin(), nullOverrides.end());

			graphics.getGraphicsRoot()->getRenderTarget()->draw(draw_range.begin(), draw_range.end());
		}

		void draw_quad(LocalGraphics& graphics, gtm::vector2d position, double rotation, gtm::vector2d scale, const Material2F * pMaterial);

		void draw_quad(LocalGraphics& graphics, gtm::aligned_box<double, 2> rect, const Material2F * pMaterial);

		void draw_rectangle(LocalGraphics& graphics, gtm::aligned_box<double, 2> rect, double thickness, const Material2F* pMaterial);
	}
};