#pragma once
#include <eng/control/Forward_Node.h>
#include <eng/IioFrame.h>
#include <par/IUpdateable.h>
#include <memory>

namespace eng
{
	namespace control
	{
		//concept control_t
		//{
		//	setScale()
		//}
		template<typename control_t>
		class Root : public eng::IUpdateable
		{
		private:

			IioFrame* m_pIioFrame = nullptr;
			double m_targetFPS = 60;

			eng::control::Graphics m_graphicsRoot;

			std::unique_ptr<control_t> m_pControl = nullptr;

		public:

			virtual void setControl(std::unique_ptr<control_t>&& pControl)
			{
				m_pControl = std::move(pControl);

				if (getIioFrame())
				{
					m_pControl->setScale(m_pIioFrame->getClientScale());
				}
			}

			const control_t* control() const
			{
				return m_pControl.get();
			}
			control_t* control()
			{
				return m_pControl.get();
			}

			virtual std::unique_ptr<control_t> releaseControl()
			{
				return std::move(m_pControl);
			}

			virtual void setIioFrame(IioFrame* pIioFrame)
			{
				m_pIioFrame = pIioFrame;
				if (pIioFrame != nullptr)
				{
					m_pIioFrame->fetch();
					if (m_pControl)
					{
						m_pControl->setScale(m_pIioFrame->getClientScale());
					}
				}
			}
			IioFrame* getIioFrame()
			{
				return m_pIioFrame;
			}
			const IioFrame* getIioFrame() const
			{
				return m_pIioFrame;
			}

			virtual void setTargetFrameRate(double frameRate)
			{
				m_targetFPS = frameRate;
			}
			double getTargetFrameRate() const
			{
				return m_targetFPS;
			}

			virtual void updateThreadRegistered(UpdateThread* updateThread) override
			{

			}

			virtual void updateThreadUnregistered(UpdateThread* updateThread) override
			{

			}

			virtual std::chrono::steady_clock::time_point updateFrame(std::chrono::steady_clock::time_point expectedEnd) override
			{
				std::chrono::steady_clock::duration frameDuration = std::chrono::duration_cast<std::chrono::steady_clock::duration>(std::chrono::seconds(1) / getTargetFrameRate());
				std::chrono::steady_clock::time_point nextFrameBegin = std::chrono::steady_clock::now() + frameDuration;

				if (m_pIioFrame != nullptr)
				{
					m_pIioFrame->fetch();
					
					if (m_pControl)
					{
						for (auto i = m_pIioFrame->input().states_begin(); i != m_pIioFrame->input().states_end(); i++)
						{
							m_pControl->onInput(*i, { 0,0 }, false);
						}

						if (m_pControl->getScale() != m_pIioFrame->getClientScale())
						{
							m_pControl->setScale(m_pIioFrame->getClientScale());
						}

						m_pControl->update();

						m_graphicsRoot.beginRecording(&m_pIioFrame->renderTarget());
						{
							m_pControl->render(&m_graphicsRoot, { 0,0 });
						}
						m_graphicsRoot.endRecording();
					}
				}

				return nextFrameBegin;
			}

			Root()
			{

			}
			virtual ~Root()
			{

			}
		};
	};
};