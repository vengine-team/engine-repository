#pragma once
#include <eng/RenderTarget.h>
#include <eng/DeviceMesh.h>
#include <wrl.h>
#include <eng/control/Graphics.h>
#include <eng/DeviceBuffer.h>

namespace eng
{
	namespace control
	{
		class LocalGraphics
		{
		private:

			eng::DeviceBuffer m_localConstantBuffer;

			eng::control::Graphics* m_pControlGraphics = nullptr;

		public:

			eng::control::Graphics* getGraphicsRoot();

			const eng::DeviceBuffer* getLocalConstantBuffer();

			LocalGraphics(LocalGraphics&& source);
			LocalGraphics& operator=(LocalGraphics&& source);
			LocalGraphics(const LocalGraphics& source) = delete;
			LocalGraphics(eng::control::Graphics* controlGraphics, gtm::vector2i position);
			virtual ~LocalGraphics();
		};
	};
};