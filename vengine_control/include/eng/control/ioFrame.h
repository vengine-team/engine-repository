#pragma once
#include <eng/control/Forward_Node.h>
#include <eng/IioFrame.h>

namespace eng
{
	namespace control
	{
		class ioFrame : public virtual eng::control::Control, public eng::IioFrame
		{
		private:

			eng::input_state						m_staged_input_initial;
			std::vector<eng::input_state_change>	m_staged_input_changes;

			std::mutex								m_fetchMutex;

			gtm::vector2u							m_clientScale = { 1, 1 };
			eng::RenderTarget						m_renderTarget;
			eng::Input								m_input;
			
			const eng::Material2F*					m_pRenderTargetMaterial = nullptr;

		public:

			virtual void localRender(eng::control::LocalGraphics& localGraphics) const override;

			virtual void setRenderTargetMaterial(const eng::Material2F* pMaterial);
			const eng::Material2F* getRenderTargetMaterial() const;

			virtual void setScale(gtm::vector2u scale) override;
			virtual void onLocalInput(const eng::control::local_input_state_change& localInput);

			ioFrame();
			virtual ~ioFrame();

			///
			/// must be called synchronized with the thread calling fetch
			///
			virtual const gtm::vector2u& getClientScale() const override;

			///
			/// must be called synchronized with the thread calling fetch
			/// may be used as eng::IDeviceTexture2 by any thread at the same time
			///
			virtual eng::RenderTarget& renderTarget() override;
			virtual const eng::RenderTarget& renderTarget() const override;

			///
			/// must be called synchronized with the thread calling fetch
			///
			virtual const eng::Input& input() const override;

			///
			/// may be called from another thread than the Update Thread of this control
			///
			virtual void fetch() override;
		};
	};
};