#pragma once
#include <eng/gpl/Forward_Node.h>

namespace eng
{
	namespace control
	{
		template<typename parent_base_t>
		class TilePanel : public parent_base_t
		{
		private:
			
			gtm::vector2u m_element_padding = { 0,0 };
			gtm::vector2u m_element_gap = { 0,0 };

			size_t m_elements_per_row;
			unsigned int m_element_scale;

			void calculateLayout()
			{
				m_elements_per_row = children().size();
				m_element_scale = 0;

				for (size_t i = 1; i <= children().size(); i++)
				{
					m_element_scale = (getScale()[0] - ((unsigned int)i - 1) * m_element_gap[0] - m_element_padding[0] * 2) / (unsigned int)i;

					size_t row_count = children().size() / i;
					if (children().size() % i != 0)
					{
						row_count += 1;
					} 

					unsigned int height = (unsigned int)(row_count) * m_element_scale + ((unsigned int)row_count - 1) * m_element_gap[1] + m_element_padding[1] * 2;

					if (height <= (unsigned int)getScale()[1])
					{
						m_elements_per_row = i;
						return;
					}
				}
				if (getScale()[1] >= m_element_padding[1] * 2)
				{
					m_element_scale = getScale()[1] - m_element_padding[1] * 2;
				}
				else
				{
					m_element_scale = 0;
				}
			}

			void scaleChildren()
			{
				for (auto i = std::begin(children()); i != std::end(children()); i++)
				{
					(*i)->setScale({ m_element_scale, m_element_scale });
				}
			}

			void positionChildren()
			{
				unsigned int pen_x = m_element_padding[0];
				unsigned int pen_y = m_element_padding[1];

				auto i = std::begin(children());

				if (i == std::end(children()))
				{
					return;
				}


				while(true)
				{

					for (size_t row_i = 0; row_i < m_elements_per_row; row_i++)
					{
						if (i == std::end(children()))
						{
							return;
						}

						(*i)->setLocalPosition({ (int)pen_x, (int)pen_y });

						pen_x += m_element_scale + m_element_gap[0];

						i++;
					}

					pen_y += m_element_scale + m_element_gap[1];
					pen_x = m_element_padding[0];
				}
			}

		public:

			virtual void setElementPadding(gtm::vector2u padding)
			{
				m_element_padding = padding;
			}
			gtm::vector2u getElementPadding() const
			{
				return m_element_padding;
			}

			virtual void setElementGap(gtm::vector2u gap)
			{
				m_element_gap = gap;
			}
			gtm::vector2u getElementGap() const
			{
				return m_element_gap;
			}

			virtual void setScale(gtm::vector2u scale) override
			{
				parent_base_t::setScale(scale);
				calculateLayout();
				scaleChildren();
				positionChildren();

			}
		};
	};
};