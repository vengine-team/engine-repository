#pragma once
#include <D3D12.h>
#include <eng/RenderTarget.h>
#include <eng/DeviceMesh.h>
#include <ext/ticket_table.h>
#include <eng/defaultDraw.h>

namespace eng
{
	namespace D3D12
	{
		struct alignas(256) matrixCBuffer
		{
			DirectX::XMFLOAT4X4 matrix;
		};
	}

	namespace control
	{

		class Graphics
		{
		private:

			eng::RenderTarget* m_pTarget = nullptr;

#ifdef _DIRECTX
			std::unique_ptr<eng::DeviceMesh2F> m_pQuadMesh;
			void initializeQuadMesh();

			UINT m_frameIndex = 0;


			UINT64 m_latestFenceValue = 0;
#endif // _DIRECTX

		public:

			eng::RenderTarget* getRenderTarget();

			eng::DeviceMesh2F* quadMesh();


			virtual void beginRecording(eng::RenderTarget* pTarget);
			virtual void endRecording();

			Graphics();
			virtual ~Graphics();
		};
	};
};