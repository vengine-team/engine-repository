#pragma once
#include <memory>
#include <array>
#include <tuple>

#include <Control.h>
#include <ext/event.h>

namespace eng
{
	namespace control
	{
		template<typename parent_t, typename control_t, typename tree_t, typename node_t, typename leaf_t>
		class SplitBox_Tree;

		template<typename parent_t, typename control_t, typename tree_t, typename node_t, typename leaf_t>
		class SplitBox_System
		{
			ext::ticket_table<SplitBox_Tree<parent_t, control_t, tree_t, node_t, leaf_t>*> m_trees;

		public:

			friend class SplitBox_Tree<parent_t, control_t, tree_t, node_t, leaf_t>;

			SplitBox_System()
			{

			}
			virtual ~SplitBox_System()
			{

			}
		};

		template<typename parent_t, typename control_t, typename tree_t, typename node_t, typename leaf_t>
		class SplitBox_Leaf;

		template<typename parent_t, typename control_t, typename tree_t, typename node_t, typename leaf_t>
		class SplitBox_Element
		{
			control_t& m_control;

			SplitBox_System<parent_t, control_t, tree_t, node_t, leaf_t>* m_pAssociatedSystem;
			SplitBox_Leaf<parent_t, control_t, tree_t, node_t, leaf_t>* m_pAssociatedLead;

			ext::event<> m_request_remove_event;

		public:

			control_t & control()
			{
				return m_control;
			}
			const control_t & control() const
			{
				return m_control;
			}

			ext::event_subscribe_access<> request_remove_event()
			{
				return { m_request_remove_event };
			}

			SplitBox_Element(control_t& control) : m_control(control)
			{

			}
			virtual ~SplitBox_Element()
			{

			}
		};

		template<typename parent_t, typename control_t, typename tree_t, typename node_t, typename leaf_t>
		class SplitBox_Child : public virtual control_t
		{
		public:

			//				is_leaf, did removed, replacement object
			virtual std::tuple<bool, bool, std::unique_ptr<SplitBox_Child<parent_t, control_t, tree_t, node_t, leaf_t>>> remove(SplitBox_Element<parent_t, control_t, tree_t, node_t, leaf_t>& element) = 0;

			virtual ~SplitBox_Child()
			{

			}
		};


		template<typename parent_t, typename control_t, typename tree_t, typename node_t, typename leaf_t>
		class SplitBox_Leaf : public parent_t, public SplitBox_Child<parent_t, control_t, tree_t, node_t, leaf_t>
		{
		private:
			SplitBox_Element<parent_t, control_t, tree_t, node_t, leaf_t>& m_element;

		public:

			virtual std::tuple<bool, bool, std::unique_ptr<SplitBox_Child<parent_t, control_t, tree_t, node_t, leaf_t>>> remove(SplitBox_Element<parent_t, control_t, tree_t, node_t, leaf_t>& element)
			{
				if (&m_element == &element)
				{
					parent_t::children().erase(std::find(std::begin(parent_t::children()), std::end(parent_t::children()), &m_element.control()));
					return { true, true, nullptr };
				}
				else
				{
					return { true, false, nullptr };
				}
			}

			control_t& get_element_Control()
			{
				return m_element.control();
			}
			const control_t& get_element_Control() const
			{
				return m_element.control();
			}

			SplitBox_Leaf(SplitBox_Element<parent_t, control_t, tree_t, node_t, leaf_t>& element) : m_element(element)
			{
				parent_t::children().push_back(&m_element.control());
			}
			virtual ~SplitBox_Leaf()
			{
				assert(std::find(std::begin(parent_t::children()), std::end(parent_t::children()), &m_element.control()) == std::end(parent_t::children()));
			}
		};

		template<typename parent_t, typename control_t, typename tree_t, typename node_t, typename leaf_t>
		class SplitBox_Node : public virtual control_t, protected parent_t, public SplitBox_Child<parent_t, control_t, tree_t, node_t, leaf_t>
		{
		public:

			static const size_t c_child_count = 2;
		
		private:
		
			bool m_isVertical = false;

			std::array<std::unique_ptr<SplitBox_Child<parent_t, control_t, tree_t, node_t, leaf_t>>, 2> m_pChildren;

		public:

			virtual std::tuple<bool, bool, std::unique_ptr<SplitBox_Child<parent_t, control_t, tree_t, node_t, leaf_t>>> remove(SplitBox_Element<parent_t, control_t, tree_t, node_t, leaf_t>& element)
			{
				for (size_t i = 0; i < c_child_count; i++)
				{
					bool is_leaf;
					bool succeed;
					std::unique_ptr<SplitBox_Child<parent_t, control_t, tree_t, node_t, leaf_t>> pReplacement;

					std::tie(is_leaf, succeed, pReplacement) = m_pChildren[i]->remove(element);

					if (succeed)
					{
						if (pReplacement != nullptr)
						{
							parent_t::children().erase(std::find(std::begin(parent_t::children()), std::end(parent_t::children()), m_pChildren[i].get()));
							m_pChildren[i] = std::move(pReplacement);
							parent_t::children().push_back(m_pChildren[i].get());

							setScale(getScale());

							return { false, true, nullptr };
						}
						else
						{
							if (is_leaf)
							{
								parent_t::children().erase(std::find(std::begin(parent_t::children()), std::end(parent_t::children()), m_pChildren[i].get()));

								parent_t::children().erase(std::find(std::begin(parent_t::children()), std::end(parent_t::children()), m_pChildren[(i + 1) % 2].get()));
								return { false, true, std::move(m_pChildren[(i + 1) % 2]) };
							}
							else
							{
								return { false, true, nullptr };
							}
						}
					}
				}

				return { false, false, nullptr };
			}

			const control_t & get_SplitBoxChild(size_t index) const
			{
				return *m_pChildren[index];
			}

			control_t & get_SplitBoxChild(size_t index)
			{
				return *m_pChildren[index];
			}

			SplitBox_Node(std::array<std::unique_ptr<SplitBox_Child<parent_t, control_t, tree_t, node_t, leaf_t>>, 2>&& pChildren)
			{
				m_pChildren = std::move(pChildren);
				for (auto i = m_pChildren.begin(); i != m_pChildren.end(); i++)
				{
					parent_t::children().push_back(i->get());
				}
			}
			virtual ~SplitBox_Node()
			{
				
			}
		};

		//template<typename parent_t, typename control_t, typename tree_t, typename node_t, typename leaf_t>
		//size_t SplitBox_Node<parent_t, control_t, tree_t, node_t, leaf_t>::c_child_count = 2;

		template<typename parent_t, typename control_t, typename tree_t, typename node_t, typename leaf_t>
		class SplitBox_Tree : public virtual control_t, protected parent_t
		{
			ext::ticket m_system_ticket;
			SplitBox_System<parent_t, control_t, tree_t, node_t, leaf_t>& m_system;

			std::unique_ptr<SplitBox_Child<parent_t, control_t, tree_t, node_t, leaf_t>> m_pRoot = nullptr;
		
		public:

			control_t* root()
			{
				return m_pRoot.get();
			}

			const control_t* root() const
			{
				return m_pRoot.get();
			}

			void insert(SplitBox_Element<parent_t, control_t, tree_t, node_t, leaf_t>& element)
			{
				if (m_pRoot != nullptr)
				{
					std::array<std::unique_ptr<SplitBox_Child<parent_t, control_t, tree_t, node_t, leaf_t>>, 2> temp;
					temp[1].reset(new leaf_t(element));
					parent_t::children().erase(std::find(std::begin(parent_t::children()), std::end(parent_t::children()), m_pRoot.get()));
					temp[0] = std::move(m_pRoot);

					m_pRoot.reset(new node_t(std::move(temp)));
					parent_t::children().push_back(m_pRoot.get());
				}
				else
				{
					leaf_t* pLeaf = new leaf_t(element);
					m_pRoot.reset(pLeaf);
					parent_t::children().push_back(m_pRoot.get());
				}

				setScale(getScale());
			}

			void remove(SplitBox_Element<parent_t, control_t, tree_t, node_t, leaf_t>& element)
			{
				assert(m_pRoot != nullptr);

				bool is_leaf;
				bool succeed;
				std::unique_ptr<SplitBox_Child<parent_t, control_t, tree_t, node_t, leaf_t>> pReplacement;

				std::tie(is_leaf, succeed, pReplacement) = m_pRoot->remove(element);

				assert(succeed);

				if (pReplacement != nullptr)
				{
					parent_t::children().erase(std::find(std::begin(parent_t::children()), std::end(parent_t::children()), m_pRoot.get()));
					m_pRoot = std::move(pReplacement);
					parent_t::children().push_back(m_pRoot.get());
					setScale(getScale());
				}
				else
				{
					if (is_leaf)
					{
						parent_t::children().erase(std::find(std::begin(parent_t::children()), std::end(parent_t::children()), m_pRoot.get()));
						m_pRoot = nullptr;
					}
				}
			}

			SplitBox_Tree(SplitBox_System<parent_t, control_t, tree_t, node_t, leaf_t>& system) : m_system(system)
			{
				system.m_trees.insert({ this, m_system_ticket });
			}
			virtual ~SplitBox_Tree()
			{
				while (parent_t::children().size() > 0)
				{
					parent_t::children().pop_back();
				}

				m_system.m_trees.erase(m_system_ticket);
			}
		};
	};
};