#pragma once
#include <eng/Forward_Node.h>
#include <gtm/math.h>
#include <ext/event.h>
#include <eng/control/LocalGraphics.h>
#include <eng/control/LocalInput.h>

namespace eng
{
	namespace control
	{
		class Control
		{
		private:

			gtm::vector2u		m_localPosition = { 0,0 };
			gtm::vector2u		m_scale = { 32,32 };

			bool			m_enabled = true;

		protected:

			virtual bool localHitTest(gtm::vector2i position) const;

			virtual void localRender(eng::control::LocalGraphics& localGraphics) const;
			virtual void onLocalInput(const eng::control::local_input_state_change& onLocalInput);

		public:

			virtual void setEnabled(bool enabled);
			bool getEnabled() const;

			virtual unsigned int calculateMinimumWidth() const;
			virtual unsigned int calculateMinimumHeight() const;

			virtual unsigned int calculateWidth(unsigned int height) const;
			virtual unsigned int calculateHeight(unsigned int width) const;

			virtual bool onInput(const eng::input_state_change& change, gtm::vector2i offset, bool obstructed);
			virtual void update();
			virtual void render(eng::control::Graphics* graphics, gtm::vector2i offset) const;

			virtual void setLocalPosition(gtm::vector2i localPosition);
			gtm::vector2i getLocalPosition() const;

			virtual void setScale(gtm::vector2u scale);
			gtm::vector2u getScale() const;

			Control();
			virtual ~Control();
		};

		template<typename Child_t>
		class Basic_ControlParent : public virtual Control, public Forward_Node<Child_t>
		{
		public:
			virtual void setEnabled(bool enabled) override
			{
				Control::setEnabled(enabled);

				for (auto i = std::begin(this->children()); i != std::end(this->children()); i++)
				{
					(*i)->setEnabled(enabled);
				}
			}

			virtual void update() override
			{
				for (auto i = std::begin(this->children()); i != std::end(this->children()); i++)
				{
					(*i)->update();
				}
			}

			virtual bool onInput(const eng::input_state_change& change, gtm::vector2i offset, bool obstructed) override
			{
				bool child_hit = false;

				for (auto i = std::begin(this->children()); i != std::end(this->children()); i++)
				{
					if((*i)->onInput(change, offset + (*i)->getLocalPosition(), child_hit))
					{
						child_hit = true;
					}
				}

				eng::control::local_input_state_change local_input;
				local_input.hit_obstructed = obstructed || child_hit;

				local_input.change = change;

				local_input.change.before.mouse.position -= offset;
				local_input.change.current.mouse.position -= offset;

				local_input.hit = localHitTest(local_input.change.current.mouse.position);

				this->onLocalInput(local_input);

				return child_hit || local_input.hit;
			}
			virtual void render(eng::control::Graphics* pGraphics, gtm::vector2i offset) const override
			{
				if (getEnabled())
				{
					eng::control::LocalGraphics localGraphics = { pGraphics, offset };
					localRender(localGraphics);
					for (auto i = std::begin(children()); i != std::end(children()); i++)
					{
						(*i)->render(pGraphics, offset + (*i)->getLocalPosition());
					}
				}
			}

			Basic_ControlParent()
			{

			}
			virtual ~Basic_ControlParent()
			{

			}
		};
	};
};