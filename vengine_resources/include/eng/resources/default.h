#pragma once
#include <eng/DeviceMesh.h>
#include <eng/Material.h>
#include <eng/Font.h>

#include "eng/defaultDraw.h"

namespace eng
{
	namespace resources
	{
		///
		/// loads all resources available in engDefault::resouces namespace
		///
		void load_resources();

		const eng::DeviceMesh3f* smoothCubeMesh();
		const eng::DeviceMesh3f* flatCubeMesh();
		const eng::DeviceMesh3f* flatPyramidMesh();
		const eng::DeviceMesh3f* flatTetrahedronMesh();
		const eng::DeviceMesh3f* smoothCubeSphereMesh();

		const eng::Material3f* sampleMaterial();
		const eng::Material3f* sampleTextureMaterial();
		const eng::Material3f* simpleLightMaterial();
	}
}