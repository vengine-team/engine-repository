#include "eng/resources/default.h"

#include <eng/graphicsUtil.h>
#include <gtm/noise.h>

std::shared_ptr<eng::D3D12::Shader> pSampleVertexShader;
std::shared_ptr<eng::D3D12::Shader> pSampleFragmentShader;
std::shared_ptr<eng::D3D12::Shader>	pSampleTextureFragmentShader;

std::shared_ptr<eng::D3D12::Shader> pSimpleLightVertexShader;
std::shared_ptr<eng::D3D12::Shader> pSimpleLightFragmentShader;


std::shared_ptr<eng::DeviceMesh3f> pSmoothCubeMesh;
std::shared_ptr<eng::DeviceMesh3f> pFlatCubeMesh;
std::shared_ptr<eng::DeviceMesh3f> pFlatPyramidMesh;
std::shared_ptr<eng::DeviceMesh3f> pFlatTetrahedronMesh;
std::shared_ptr<eng::DeviceMesh3f> pSmoothCubeSphereMesh;



std::shared_ptr<eng::RenderPass3f> pSampleRenderPass;
std::unique_ptr<eng::MaterialSignature3f> pSampleMaterialSignature;
std::shared_ptr<eng::Material3f> pSampleMaterial;

std::shared_ptr<eng::RenderPass3f> pSampleTextureRenderPass;
std::unique_ptr<eng::MaterialSignature3f> pSampleTextureMaterialSignature;
std::shared_ptr<eng::Material3f> pSampleTextureMaterial;

std::shared_ptr<eng::DeviceTexture2> pSampleTexture;

std::shared_ptr<eng::RenderPass3f> pSimpleLightRenderPass;
std::unique_ptr<eng::MaterialSignature3f> pSimpleLightMaterialSignature;
std::shared_ptr<eng::Material3f> pSimpleLightMaterial;

void eng::resources::load_resources()
{
	//smooth cube mesh
	{
		std::vector<eng::triangle_indices> cubeIndices =
		{
			{0, 2, 1}, // -x
			{1, 2, 3},

			{4, 5, 6}, // +x
			{5, 7, 6},

			{0, 1, 5}, // -y
			{0, 5, 4},

			{2, 6, 7}, // +y
			{2, 7, 3},

			{0, 4, 6}, // -z
			{0, 6, 2},

			{1, 3, 7}, // +z
			{1, 7, 5},
		};
		std::vector<eng::vertex3f> cubeVertices =
		{
			{ gtm::vector3f{ -0.5f, -0.5f, -0.5f } },
			{ gtm::vector3f{ -0.5f, -0.5f,  0.5f } },
			{ gtm::vector3f{ -0.5f,  0.5f, -0.5f } },
			{ gtm::vector3f{ -0.5f,  0.5f,  0.5f } },
			{ gtm::vector3f{ 0.5f, -0.5f, -0.5f } },
			{ gtm::vector3f{ 0.5f, -0.5f,  0.5f } },
			{ gtm::vector3f{ 0.5f,  0.5f, -0.5f } },
			{ gtm::vector3f{ 0.5f,  0.5f,  0.5f } },
		};


		eng::Mesh3f mesh;
		mesh.triangles() = std::move(cubeIndices);
		mesh.vertices() = std::move(cubeVertices);

		eng::recalculateNormals(mesh);

		pSmoothCubeMesh = std::make_shared<eng::DeviceMesh3f>(std::move(mesh));
	}

	//flat cube mesh
	{
		std::vector<eng::triangle_indices> cubeIndices =
		{
			{0, 2, 1}, // -x
			{1, 2, 3},

			{4, 5, 6}, // +x
			{5, 7, 6},


			{8, 10, 9}, // -y
			{9, 10, 11},

			{12, 13, 14}, //+y
			{13, 15, 14},


			{16, 18, 17}, // -z
			{17, 18, 19},

			{20, 21, 22}, // +z
			{21, 23, 22}
		};
		std::vector<eng::vertex3f> cubeVertices =
		{
			// -x
			{ gtm::vector3f{-0.5f, -0.5f, -0.5f }, gtm::vector3f{-1.0f, 0.0f, 0.0f }, gtm::vector3f{-1.0f, 0.0f, 0.0f }, gtm::vector2f{ 0, 1 } },
			{ gtm::vector3f{-0.5f, -0.5f,  0.5f }, gtm::vector3f{-1.0f, 0.0f, 0.0f }, gtm::vector3f{-1.0f, 0.0f, 0.0f }, gtm::vector2f{ 1, 1 } },
			{ gtm::vector3f{-0.5f,  0.5f, -0.5f }, gtm::vector3f{-1.0f, 0.0f, 0.0f }, gtm::vector3f{-1.0f, 0.0f, 0.0f }, gtm::vector2f{ 0, 0 } },
			{ gtm::vector3f{-0.5f,  0.5f,  0.5f }, gtm::vector3f{-1.0f, 0.0f, 0.0f }, gtm::vector3f{-1.0f, 0.0f, 0.0f }, gtm::vector2f{ 1, 0 } },
												   									  									 
			// +x								   									  									 
			{ gtm::vector3f{ 0.5f, -0.5f, -0.5f }, gtm::vector3f{ 1.0f, 0.0f, 0.0f }, gtm::vector3f{ 1.0f, 0.0f, 0.0f }, gtm::vector2f{ 1, 1 } },
			{ gtm::vector3f{ 0.5f, -0.5f,  0.5f }, gtm::vector3f{ 1.0f, 0.0f, 0.0f }, gtm::vector3f{ 1.0f, 0.0f, 0.0f }, gtm::vector2f{ 0, 1 } },
			{ gtm::vector3f{ 0.5f,  0.5f, -0.5f }, gtm::vector3f{ 1.0f, 0.0f, 0.0f }, gtm::vector3f{ 1.0f, 0.0f, 0.0f }, gtm::vector2f{ 1, 0 } },
			{ gtm::vector3f{ 0.5f,  0.5f,  0.5f }, gtm::vector3f{ 1.0f, 0.0f, 0.0f }, gtm::vector3f{ 1.0f, 0.0f, 0.0f }, gtm::vector2f{ 0, 0 } },
												   									  									 
												   									  									 
												   									  									 
			// -y								   									  									 
			{ gtm::vector3f{ 0.5f, -0.5f, -0.5f }, gtm::vector3f{ 0.0f,-1.0f, 0.0f }, gtm::vector3f{ 0.0f,-1.0f, 0.0f }, gtm::vector2f{ 0, 0 } },
			{ gtm::vector3f{ 0.5f, -0.5f,  0.5f }, gtm::vector3f{ 0.0f,-1.0f, 0.0f }, gtm::vector3f{ 0.0f,-1.0f, 0.0f }, gtm::vector2f{ 1, 0 } },
			{ gtm::vector3f{-0.5f, -0.5f, -0.5f }, gtm::vector3f{ 0.0f,-1.0f, 0.0f }, gtm::vector3f{ 0.0f,-1.0f, 0.0f }, gtm::vector2f{ 0, 1 } },
			{ gtm::vector3f{-0.5f, -0.5f,  0.5f }, gtm::vector3f{ 0.0f,-1.0f, 0.0f }, gtm::vector3f{ 0.0f,-1.0f, 0.0f }, gtm::vector2f{ 1, 1 } },
												   									  									 
			// +y								   									  									 
			{ gtm::vector3f{ 0.5f,  0.5f, -0.5f }, gtm::vector3f{ 0.0f, 1.0f, 0.0f }, gtm::vector3f{ 0.0f, 1.0f, 0.0f }, gtm::vector2f{ 0, 0 } },
			{ gtm::vector3f{ 0.5f,  0.5f,  0.5f }, gtm::vector3f{ 0.0f, 1.0f, 0.0f }, gtm::vector3f{ 0.0f, 1.0f, 0.0f }, gtm::vector2f{ 1, 0 } },
			{ gtm::vector3f{-0.5f,  0.5f, -0.5f }, gtm::vector3f{ 0.0f, 1.0f, 0.0f }, gtm::vector3f{ 0.0f, 1.0f, 0.0f }, gtm::vector2f{ 0, 1 } },
			{ gtm::vector3f{-0.5f,  0.5f,  0.5f }, gtm::vector3f{ 0.0f, 1.0f, 0.0f }, gtm::vector3f{ 0.0f, 1.0f, 0.0f }, gtm::vector2f{ 1, 1 } },
												   									  									 
												   									  									 
												   									  									 
			// -z								   									  									 
			{ gtm::vector3f{ 0.5f,  0.5f, -0.5f }, gtm::vector3f{ 0.0f, 0.0f,-1.0f }, gtm::vector3f{ 0.0f, 0.0f,-1.0f }, gtm::vector2f{ 0, 0 } },
			{ gtm::vector3f{ 0.5f, -0.5f, -0.5f }, gtm::vector3f{ 0.0f, 0.0f,-1.0f }, gtm::vector3f{ 0.0f, 0.0f,-1.0f }, gtm::vector2f{ 0, 1 } },
			{ gtm::vector3f{-0.5f,  0.5f, -0.5f }, gtm::vector3f{ 0.0f, 0.0f,-1.0f }, gtm::vector3f{ 0.0f, 0.0f,-1.0f }, gtm::vector2f{ 1, 0 } },
			{ gtm::vector3f{-0.5f, -0.5f, -0.5f }, gtm::vector3f{ 0.0f, 0.0f,-1.0f }, gtm::vector3f{ 0.0f, 0.0f,-1.0f }, gtm::vector2f{ 1, 1 } },
												   									  									 
			// +z								   									  									 
			{ gtm::vector3f{ 0.5f,  0.5f,  0.5f }, gtm::vector3f{ 0.0f, 0.0f, 1.0f }, gtm::vector3f{ 0.0f, 0.0f, 1.0f }, gtm::vector2f{ 1, 0 } },
			{ gtm::vector3f{ 0.5f, -0.5f,  0.5f }, gtm::vector3f{ 0.0f, 0.0f, 1.0f }, gtm::vector3f{ 0.0f, 0.0f, 1.0f }, gtm::vector2f{ 1, 1 } },
			{ gtm::vector3f{-0.5f,  0.5f,  0.5f }, gtm::vector3f{ 0.0f, 0.0f, 1.0f }, gtm::vector3f{ 0.0f, 0.0f, 1.0f }, gtm::vector2f{ 0, 0 } },
			{ gtm::vector3f{-0.5f, -0.5f,  0.5f }, gtm::vector3f{ 0.0f, 0.0f, 1.0f }, gtm::vector3f{ 0.0f, 0.0f, 1.0f }, gtm::vector2f{ 0, 1 } },
		};

		eng::Mesh3f mesh;
		mesh.vertices() = std::move(cubeVertices);
		mesh.triangles() = std::move(cubeIndices);

		pFlatCubeMesh = std::make_shared<eng::DeviceMesh3f>(std::move(mesh));
	}

	//pyramidMesh
	{
		std::vector<eng::triangle_indices> pyramidIndices =
		{
			{0, 1, 2}, // -x
			{1, 3, 2 },

			{0, 2, 4},

			{0, 4, 1},

			{2, 3, 4},

			{3, 1, 4}
		};

		std::vector<eng::vertex3f> pyramidVertices =
		{
			{ gtm::vector3f{-1.0f, 0.0f, -1.0f }, gtm::vector3f{ 0.0f, 0.0f, 0.0f } },
			{ gtm::vector3f{-1.0f, 0.0f,  1.0f }, gtm::vector3f{ 0.0f, 0.0f, 1.0f } },
			{ gtm::vector3f{ 1.0f, 0.0f, -1.0f }, gtm::vector3f{ 1.0f, 0.0f, 0.0f } },
			{ gtm::vector3f{ 1.0f, 0.0f,  1.0f }, gtm::vector3f{ 1.0f, 0.0f, 1.0f } },
			{ gtm::vector3f{ 0.0f, 2.0f,  0.0f }, gtm::vector3f{ 1.0f, 1.0f, 1.0f } },
		};

		eng::Mesh3f mesh;
		mesh.vertices() = std::move(pyramidVertices);
		mesh.triangles() = std::move(pyramidIndices);

		pFlatPyramidMesh = std::make_shared<eng::DeviceMesh3f>(std::move(mesh));
	}

	//smooth cube sphere
	{
		pSmoothCubeSphereMesh.reset(new eng::DeviceMesh3f(eng::generate_cubeSphere_mesh3F(7, 0.5)));
	}

	//sample texture
	{
		eng::Bitmap2 noise_bitmap({ 128, 128 });

		for (size_t x = 0; x < noise_bitmap.scale()[0]; x++)
		{
			for (size_t y = 0; y < noise_bitmap.scale()[1]; y++)
			{
				float brightness = gtm::perlin(gtm::vector2f{ (float)x * 0.005f, (float)y * 0.005f });

				unsigned char brightness_c = static_cast<unsigned char>((brightness / 2 + 0.5f) * 255);

				noise_bitmap.pixel({ x, y }) = { brightness_c, brightness_c, brightness_c, 255 };
			}
		}

		pSampleTexture = std::make_unique<eng::DeviceTexture2>(noise_bitmap);
	}

	//Materials
	{
		std::string sampleVertexShaderSrcCode = "\
															\
		cbuffer ModelConstantBuffer : register(b0)			\
		{													\
			matrix model;									\
		};													\
															\
		cbuffer ViewProjectionConstantBuffer : register(b1)	\
		{													\
			matrix view;									\
			matrix projection;								\
		};													\
															\
		struct VertexShaderInput							\
		{													\
			float3 pos : POSITION;							\
			float3 normal : NORMAL;							\
			float2 uv0 : TEXCOORD0;							\
			float2 uv1 : TEXCOORD1;							\
			float2 uv2 : TEXCOORD2;							\
			float2 uv3 : TEXCOORD3;							\
		};													\
															\
		struct FragmentShaderInput							\
		{													\
			float4 pos : SV_POSITION;						\
			float3 color : NORMAL;							\
			float2 uv0 : TEXCOORD0;							\
		};													\
															\
		FragmentShaderInput main(VertexShaderInput input)	\
		{													\
			FragmentShaderInput output;						\
			float4 pos = float4(input.pos, 1.0f);			\
															\
			pos = mul(pos, model);							\
			pos = mul(pos, view);							\
			pos = mul(pos, projection);						\
															\
			output.pos = pos;								\
			output.color = input.normal;					\
			output.uv0 = input.uv0;							\
															\
			return output;									\
		}";		

		std::string sampleFragmentShaderSrcCode = "\
		struct FragmentShaderInput											\
		{																	\
			float4 pos : SV_POSITION;										\
			float3 normal : NORMAL;											\
			float2 uv0 : TEXCOORD0;											\
		};																	\
																			\
		float4 main(FragmentShaderInput input) : SV_TARGET					\
		{																	\
			return float4(normalize(input.normal) / 2 + float3(0.5, 0.5, 0.5), 1.0f);	\
		}";

		std::string sampleTextureFragmentShaderSrcCode = "\
															\
		Texture2D<float4> tex1 : register(t2);				\
															\
		SamplerState sampler1 : register(s3);				\
															\
		struct FragmentShaderInput							\
		{													\
			float4 pos : SV_POSITION;						\
			float3 color : NORMAL;							\
			float2 uv0 : TEXCOORD0;							\
		};													\
															\
		float4 main(FragmentShaderInput input) : SV_TARGET	\
		{													\
			return tex1.Sample(sampler1, input.uv0);		\
		}";

		std::string simpleLightVertexShaderSrcCode = "\
		cbuffer ModelConstantBuffer : register(b0)																			\
		{																													\
			matrix model;																									\
		};																													\
																															\
		cbuffer ViewProjectionConstantBuffer : register(b1)																	\
		{																													\
			matrix view;																									\
			matrix projection;																								\
		};																													\
																															\
		struct VertexShaderInput																							\
		{																													\
			float3 pos : POSITION;																							\
			float3 normal : NORMAL;																							\
			float2 uv0 : TEXCOORD0;																							\
			float2 uv1 : TEXCOORD1;																							\
			float2 uv2 : TEXCOORD2;																							\
			float2 uv3 : TEXCOORD3;																							\
		};																													\
																															\
		struct FragmentShaderInput																							\
		{																													\
			float4 pos : SV_POSITION;																						\
			float3 normal : NORMAL;																							\
			float3 light : LIGHT;																							\
			float2 uv0 : TEXCOORD0;																							\
		};																													\
																															\
		FragmentShaderInput main(VertexShaderInput input)																	\
		{																													\
			float3 lightVector = normalize(float3(0.2, -1.0, 1.0));															\
																															\
			FragmentShaderInput output;																						\
																															\
																															\
			float4 pos = float4(input.pos, 1.0f);																			\
			float4 normal = float4(input.normal, 0.0f);																		\
																															\
			pos = mul(pos, model);																							\
			normal = mul(normal, model);																					\
																															\
			float angle_light_normal = acos(dot(normal.xyz, -lightVector) / (length(normal.xyz) * length(-lightVector)));	\
																															\
			pos = mul(pos, view);																							\
			normal = mul(normal, view);																						\
																															\
			pos = mul(pos, projection);																						\
			normal = mul(normal, projection);																				\
																															\
																															\
			float angle_derived_intensity = cos(abs(angle_light_normal));													\
			output.light = float3(1.0, 1.0, 1.0) * angle_derived_intensity;													\
			output.pos = pos;																								\
			output.normal = normal.xyz;																						\
			output.uv0 = input.uv0;																							\
																															\
			return output;																									\
		}";		


		std::string simpleLightFragmentShaderSrcCode = "\
																								\
		struct FragmentShaderInput																\
		{																						\
			float4 pos : SV_POSITION;															\
			float3 color : NORMAL;																\
			float3 light : LIGHT;																\
			float2 uv0 : TEXCOORD0;																\
		};																						\
		float4 main(FragmentShaderInput input) : SV_TARGET										\
		{																						\
			return float4(1.0 * input.light.x, 1.0 * input.light.y, 1.0 * input.light.z, 1.0);	\
		}";

		//Compile Shaders
		pSampleVertexShader.reset(new eng::D3D12::Shader(sampleVertexShaderSrcCode.data(), sampleVertexShaderSrcCode.size(), "main", "vs_4_0"));
		pSampleFragmentShader.reset(new eng::D3D12::Shader(sampleFragmentShaderSrcCode.data(), sampleFragmentShaderSrcCode.size(), "main", "ps_4_0"));
		pSampleTextureFragmentShader.reset(new eng::D3D12::Shader(sampleTextureFragmentShaderSrcCode.data(), sampleTextureFragmentShaderSrcCode.size(), "main", "ps_4_0"));

		pSimpleLightVertexShader.reset(new eng::D3D12::Shader(simpleLightVertexShaderSrcCode.data(), simpleLightVertexShaderSrcCode.size(), "main", "vs_4_0"));
		pSimpleLightFragmentShader.reset(new eng::D3D12::Shader(simpleLightFragmentShaderSrcCode.data(), simpleLightFragmentShaderSrcCode.size(), "main", "ps_4_0"));



		//sampleRenderPass
		{
			CD3DX12_ROOT_PARAMETER rootParameters[2];
			 
			CD3DX12_DESCRIPTOR_RANGE range0;
			range0.Init(D3D12_DESCRIPTOR_RANGE_TYPE_CBV, 1, 0);
			rootParameters[0].InitAsDescriptorTable(1, &range0);

			CD3DX12_DESCRIPTOR_RANGE range1;
			range1.Init(D3D12_DESCRIPTOR_RANGE_TYPE_CBV, 1, 1);
			rootParameters[1].InitAsDescriptorTable(1, &range1);

			D3D12_ROOT_SIGNATURE_FLAGS rootSignatureFlags =
				D3D12_ROOT_SIGNATURE_FLAG_ALLOW_INPUT_ASSEMBLER_INPUT_LAYOUT | // Only the input assembler stage needs access to the constant buffer.
				D3D12_ROOT_SIGNATURE_FLAG_DENY_DOMAIN_SHADER_ROOT_ACCESS |
				D3D12_ROOT_SIGNATURE_FLAG_DENY_GEOMETRY_SHADER_ROOT_ACCESS |
				D3D12_ROOT_SIGNATURE_FLAG_DENY_HULL_SHADER_ROOT_ACCESS |
				D3D12_ROOT_SIGNATURE_FLAG_DENY_PIXEL_SHADER_ROOT_ACCESS;

			CD3DX12_ROOT_SIGNATURE_DESC sample_root_signature_desc;
			sample_root_signature_desc.Init
			(
				_countof(rootParameters),
				rootParameters,
				0,
				nullptr,
				rootSignatureFlags
			);


			eng::render_pass_parameter dx12_parameters[2];
			dx12_parameters[0].type = eng::shader_resource::device_buffer;
			dx12_parameters[0].constantBufferParameter.constantBufferDescriptor_rootParamIndex = 0;
			dx12_parameters[1].type = eng::shader_resource::device_buffer;
			dx12_parameters[1].constantBufferParameter.constantBufferDescriptor_rootParamIndex = 1;

			eng::D3D12::RenderPass_Desc renderPassDesc = {};
			renderPassDesc.vertexShader = pSampleVertexShader.get();
			renderPassDesc.pixelShader = pSampleFragmentShader.get();
			renderPassDesc.numParameters = _countof(dx12_parameters);
			renderPassDesc.pParameters = dx12_parameters;
			renderPassDesc.root_signature_desc = sample_root_signature_desc;
			renderPassDesc.cullMode = eng::D3D12::cull_mode::none;
			renderPassDesc.cullMode = eng::D3D12::cull_mode::back;

			pSampleRenderPass.reset(new eng::RenderPass3f(renderPassDesc));
		}

		//sampleTextureRenderPass
		{
			CD3DX12_ROOT_PARAMETER rootParameters[4];

			CD3DX12_DESCRIPTOR_RANGE range0;
			range0.Init(D3D12_DESCRIPTOR_RANGE_TYPE_CBV, 1, 0);
			rootParameters[0].InitAsDescriptorTable(1, &range0);

			CD3DX12_DESCRIPTOR_RANGE range1;
			range1.Init(D3D12_DESCRIPTOR_RANGE_TYPE_CBV, 1, 1);
			rootParameters[1].InitAsDescriptorTable(1, &range1);

			CD3DX12_DESCRIPTOR_RANGE range2;
			range2.Init(D3D12_DESCRIPTOR_RANGE_TYPE_SRV, 1, 2);
			rootParameters[2].InitAsDescriptorTable(1, &range2);

			CD3DX12_DESCRIPTOR_RANGE range3;
			range3.Init(D3D12_DESCRIPTOR_RANGE_TYPE_SAMPLER, 1, 3);
			rootParameters[3].InitAsDescriptorTable(1, &range3);

			D3D12_ROOT_SIGNATURE_FLAGS rootSignatureFlags =
				D3D12_ROOT_SIGNATURE_FLAG_ALLOW_INPUT_ASSEMBLER_INPUT_LAYOUT | // Only the input assembler and the fragment shader stage needs access to the root signature
				D3D12_ROOT_SIGNATURE_FLAG_DENY_DOMAIN_SHADER_ROOT_ACCESS |
				D3D12_ROOT_SIGNATURE_FLAG_DENY_GEOMETRY_SHADER_ROOT_ACCESS |
				D3D12_ROOT_SIGNATURE_FLAG_DENY_HULL_SHADER_ROOT_ACCESS;

			CD3DX12_ROOT_SIGNATURE_DESC texture_root_signature_desc;
			texture_root_signature_desc.Init
			(
				_countof(rootParameters),
				rootParameters,
				0,
				nullptr,
				rootSignatureFlags
			);




			eng::render_pass_parameter dx12_parameters[3];

			dx12_parameters[0].type = eng::shader_resource::device_buffer;
			dx12_parameters[0].constantBufferParameter.constantBufferDescriptor_rootParamIndex = 0;

			dx12_parameters[1].type = eng::shader_resource::device_buffer;
			dx12_parameters[1].constantBufferParameter.constantBufferDescriptor_rootParamIndex = 1;

			dx12_parameters[2].type = eng::shader_resource::device_texture;
			dx12_parameters[2].textureParameter.shaderResourceView_rootParamIndex = 2;
			dx12_parameters[2].textureParameter.sampler_rootParamIndex = 3;


			eng::D3D12::RenderPass_Desc renderPassDesc = {};
			renderPassDesc.vertexShader = pSampleVertexShader.get();
			renderPassDesc.pixelShader = pSampleTextureFragmentShader.get();
			renderPassDesc.numParameters = _countof(dx12_parameters);
			renderPassDesc.pParameters = dx12_parameters;
			renderPassDesc.root_signature_desc = texture_root_signature_desc;
			renderPassDesc.depth_stencil_Desc.DepthEnable = TRUE;
			renderPassDesc.cullMode = eng::D3D12::cull_mode::back;

			pSampleTextureRenderPass.reset(new eng::RenderPass<eng::vertex3f>(renderPassDesc));
		}

		//LightRenderPass
		{
			CD3DX12_ROOT_PARAMETER rootParameters[2];

			CD3DX12_DESCRIPTOR_RANGE range0;
			range0.Init(D3D12_DESCRIPTOR_RANGE_TYPE_CBV, 1, 0);
			rootParameters[0].InitAsDescriptorTable(1, &range0);

			CD3DX12_DESCRIPTOR_RANGE range1;
			range1.Init(D3D12_DESCRIPTOR_RANGE_TYPE_CBV, 1, 1);
			rootParameters[1].InitAsDescriptorTable(1, &range1);

			D3D12_ROOT_SIGNATURE_FLAGS rootSignatureFlags =
				D3D12_ROOT_SIGNATURE_FLAG_ALLOW_INPUT_ASSEMBLER_INPUT_LAYOUT | // Only the input assembler stage needs access to the constant buffer.
				D3D12_ROOT_SIGNATURE_FLAG_DENY_DOMAIN_SHADER_ROOT_ACCESS |
				D3D12_ROOT_SIGNATURE_FLAG_DENY_GEOMETRY_SHADER_ROOT_ACCESS |
				D3D12_ROOT_SIGNATURE_FLAG_DENY_HULL_SHADER_ROOT_ACCESS |
				D3D12_ROOT_SIGNATURE_FLAG_DENY_PIXEL_SHADER_ROOT_ACCESS;

			CD3DX12_ROOT_SIGNATURE_DESC sample_root_signature_desc;
			sample_root_signature_desc.Init
			(
				_countof(rootParameters),
				rootParameters,
				0,
				nullptr,
				rootSignatureFlags
			);


			eng::render_pass_parameter dx12_parameters[2];
			dx12_parameters[0].type = eng::shader_resource::device_buffer;
			dx12_parameters[0].constantBufferParameter.constantBufferDescriptor_rootParamIndex = 0;
			dx12_parameters[1].type = eng::shader_resource::device_buffer;
			dx12_parameters[1].constantBufferParameter.constantBufferDescriptor_rootParamIndex = 1;

			eng::D3D12::RenderPass_Desc renderPassDesc = {};
			renderPassDesc.vertexShader = pSimpleLightVertexShader.get();
			renderPassDesc.pixelShader = pSimpleLightFragmentShader.get();
			renderPassDesc.numParameters = _countof(dx12_parameters);
			renderPassDesc.pParameters = dx12_parameters;
			renderPassDesc.root_signature_desc = sample_root_signature_desc;
			renderPassDesc.cullMode = eng::D3D12::cull_mode::back;

			pSimpleLightRenderPass.reset(new eng::RenderPass<eng::vertex3f>(renderPassDesc));
		}



		//sampleMaterialSignature
		{
			std::array<eng::material_parameter, 2> parameters
			{
				eng::material_parameter{ eng::shader_resource::device_buffer, 0 },
				eng::material_parameter{ eng::shader_resource::device_buffer, 1 }
			};

			std::array<eng::render_pass_binding<eng::vertex3f>, 1> bindings
			{
				eng::render_pass_binding<eng::vertex3f>{ pSampleRenderPass, { 0, 1 } }
			};

			pSampleMaterialSignature = std::make_unique<eng::MaterialSignature3f>(parameters.begin(), parameters.end(), bindings.begin(), bindings.end(), 100);
		}

		//sampleTextureMaterialSignature
		{
			std::array<eng::material_parameter, 3> parameters
			{
				eng::material_parameter{ eng::shader_resource::device_buffer, 0 },
				eng::material_parameter{ eng::shader_resource::device_buffer, 1 },
				eng::material_parameter{ eng::shader_resource::device_texture, 2 }
			};

			std::array<eng::render_pass_binding<eng::vertex3f>, 1> bindings
			{
				eng::render_pass_binding<eng::vertex3f>{ pSampleTextureRenderPass, { 0, 1, 2 } }
			};

			pSampleTextureMaterialSignature = std::make_unique<eng::MaterialSignature3f>(parameters.begin(), parameters.end(), bindings.begin(), bindings.end(), 100);
		}

		//simpleLightmaterialSignature
		{
			std::array<eng::material_parameter, 2> params =
			{
				eng::material_parameter{ eng::shader_resource::device_buffer, 0 },
				eng::material_parameter{ eng::shader_resource::device_buffer, 1 }
			};

			std::array<eng::render_pass_binding<eng::vertex3f>, 1> binding
			{
				eng::render_pass_binding<eng::vertex3f>{ pSimpleLightRenderPass, { 0, 1} }
			};

			pSimpleLightMaterialSignature = std::make_unique<eng::MaterialSignature3f>(params.begin(), params.end(), binding.begin(), binding.end(), 100);
		}



		//sampleMaterial
		{
			pSampleMaterial = std::make_shared<eng::Material3f>(*pSampleMaterialSignature);
		}

		//sampleTextureMaterial
		{
			auto tmp = std::make_unique<eng::Material3f>(*pSampleTextureMaterialSignature);
			tmp->setTextureShaderResource(2, pSampleTexture.get());
			pSampleTextureMaterial = std::move(tmp);
		}

		//simpleLightMaterial
		{
			pSimpleLightMaterial = std::make_shared<eng::Material3f>(*pSimpleLightMaterialSignature);
		}
	}
}

const eng::DeviceMesh3f * eng::resources::smoothCubeMesh()
{
	return pSmoothCubeMesh.get();
}
const eng::DeviceMesh3f * eng::resources::flatCubeMesh()
{
	return pFlatCubeMesh.get();
}
const eng::DeviceMesh3f * eng::resources::flatPyramidMesh()
{
	return pFlatPyramidMesh.get();
}
const eng::DeviceMesh3f * eng::resources::flatTetrahedronMesh()
{
	return pFlatTetrahedronMesh.get();
}
const eng::DeviceMesh3f * eng::resources::smoothCubeSphereMesh()
{
	return pSmoothCubeSphereMesh.get();
}


const eng::Material3f * eng::resources::sampleMaterial()
{
	return pSampleMaterial.get();
}
const eng::Material3f * eng::resources::sampleTextureMaterial()
{
	return pSampleTextureMaterial.get();
}
const eng::Material3f * eng::resources::simpleLightMaterial()
{
	return pSimpleLightMaterial.get();
}

