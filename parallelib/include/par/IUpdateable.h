#pragma once
#include <chrono>

namespace eng
{
	using clock_t = std::chrono::steady_clock;

	// forward declaration of Update thread to prevent header inclusion order errors
	class UpdateThread;

	///
	/// abstract interface for anything that can be updated
	///
	class IUpdateable
	{
	public:

		///
		/// called when this scene is registered, the argument is the new
		/// Update Thread this function is called from the new 
		/// thread before the first time update() is called
		///
		virtual void updateThreadRegistered(eng::UpdateThread* pNewUpdateThread) = 0;

		///
		/// called when the this scene is unregisterd, the argument is the previous 
		/// UpdateThread this function is still called from the same UpdateThread,
		/// after the last time update() from the old thread is called
		///
		virtual void updateThreadUnregistered(eng::UpdateThread* pOldUpdateThread) = 0;


		///
		/// this function is called by the update thread, the function should 
		/// return before expected_end argument. 
		///
		/// If there is more time left than the update needs, 
		/// the function should return as early as possible
		///
		/// an IUpdatable representing an object that updates at 60 fps should
		/// return   (begin of function time_point) + 1 / 60 of a second
		///
		/// if the function takes longer than the given time Frame,
		/// other IUpdateable objects in the same update thread
		/// will suffer from lower fps than their target fps
		///
		/// return value: time_point when to call updateFrame() again
		///
		virtual clock_t::time_point updateFrame(clock_t::time_point expected_end) = 0;

		///
		/// destructor must be virtual, otherwise memory leaks will may
		/// occur if a derived Object is deleted using a pointer to this interfaces
		///
		virtual ~IUpdateable() = default;
	};
};