#pragma once
#include <set>
#include <mutex>
#include <par/IUpdateable.h>
#include <par/task_queue.h>
#include <unordered_map>
#include <shared_mutex>
#include <atomic>

namespace eng
{
	class UpdateThread;

	UpdateThread* this_UpdateThread();

	class UpdateThread
	{
	private:
		struct updateElement
		{
		public:
			eng::IUpdateable* pIUpdateable;
			clock_t::time_point nextUpdate;

			bool operator<(const updateElement& other) const;
			bool operator>(const updateElement& other) const;
		};

		//static fields to keep track of all created instances
		static std::shared_mutex s_allUpdateThreadsMutex;
		static std::unordered_map<std::thread::id, UpdateThread*> s_allUpdateThreads;
		static UpdateThread* concurrentFindUpdateThread(std::thread::id id);

		std::thread m_thread;


		//pointers to resources owned by thread
		eng::TaskQueue* m_pThreadTaskQueue;
		std::set<updateElement, std::greater<updateElement>>* m_pThreadUpdateables;
		std::atomic<bool>* m_pThreadEnabled;
		bool* m_pThreadTerminate;

		//task functions
		bool addUpdateableTask(IUpdateable* pIUpdateable);
		bool removeUpdateableTask(IUpdateable* pIUpdateable);
		void setEnabledTask(bool enabled);
		void terminateTask();

		void updateThreadFunction(std::atomic<bool>* setupComplete);
	public:

		friend UpdateThread* this_UpdateThread();

		///
		/// gets the enabled State of the Update Thread
		/// function may be called by any Thread, even
		/// the encapsulated UpdateThread itself
		///
		bool concurrentGetEnabled() const;
		///
		/// function to set enabled state
		/// a disabled UpdateThread will
		/// not update its IUpdateables
		///
		void concurrentSetEnabled(bool b);
		///
		/// the UpdateThread takes ownership of the scene
		/// an enabled UpdateThread calls updateFrame()
		/// according to updateFrame() function return
		/// unsynchronized access of pIUpdateable after registration
		/// may cause undefined behavior
		///
		void concurrentRegisterUpdateable(IUpdateable* pIUpdateable);
		///
		/// pIUpdateable is removed from the UpdateThread.
		/// if this function is not called from the updating Thread itself,
		/// the last call to updateFrame() is guaranteed to return before
		/// concurrentUnregisterUpdateable() returns
		///
		void concurrentUnregisterUpdateable(IUpdateable* pIUpdateable);
		///
		/// terminates the thread, if a thread called join(),
		/// join() returns to the caller
		///
		void concurrentTerminate();
		///
		/// similar to std::thread::joinable();
		///
		bool joinable() const;
		///
		///, similar to std::thread::join();
		///
		void join();


		///
		/// update Thread is enabled by default
		///
		UpdateThread();
		virtual ~UpdateThread();
	};
};
