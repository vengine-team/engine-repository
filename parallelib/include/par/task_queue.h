#pragma once
#include <ext/any_queue.h>
#include <condition_variable>

namespace eng
{
	class TaskQueue
	{
	private:
		
		class functor_wrapper_base
		{
		public:

			virtual void invoke() = 0;

			virtual ~functor_wrapper_base() = default;
		};

		template<typename functor_t>
		class functor_wrapper final : public functor_wrapper_base
		{
			functor_t m_functor;
		public:
			virtual void invoke() override
			{
				m_functor();
			}
			functor_wrapper(functor_t&& functor) : m_functor(std::move(functor))
			{

			}
		};

		std::mutex m_queueMutex;
		std::condition_variable m_conditionVariable;
		eng::any_queue<functor_wrapper_base> m_queue;

	public:

		///
		/// processes task that has been in the queue for the longest time
		///
		void process_next();
		///
		/// processes all tasks that are in the queue at the time the function is called
		///
		void process();

		void wait();
		void wait_until(std::chrono::steady_clock::time_point t);
		void wait_for(std::chrono::steady_clock::duration d);

		template<typename functor_t>
		void concurrent_push(const functor_t& functor)
		{
			std::scoped_lock<std::mutex> lock(m_queueMutex);
			m_queue.push<functor_wrapper<functor_t>>(functor_wrapper<functor_t>(functor));
			m_conditionVariable.notify_one();
		}

		template<typename functor_t>
		void concurrent_push(functor_t&& functor)
		{
			std::scoped_lock<std::mutex> lock(m_queueMutex);
			m_queue.push<functor_wrapper<functor_t>>(std::move(functor_wrapper<functor_t>(std::move(functor))));
			m_conditionVariable.notify_one();
		}

		bool empty();

		size_t size();


		~TaskQueue();
	};
};