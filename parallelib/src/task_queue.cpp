#include <par/task_queue.h>

void eng::TaskQueue::process_next()
{
	std::unique_lock<std::mutex> unique_lock(m_queueMutex);

	if (!m_queue.empty())
	{
		functor_wrapper_base* pTemp = m_queue.front();

		unique_lock.unlock();

		pTemp->invoke();

		unique_lock.lock();
		m_queue.pop();
	}
}

void eng::TaskQueue::process()
{
	std::unique_lock<std::mutex> unique_lock(m_queueMutex);
	size_t queueSize = m_queue.size();

	for (size_t i = 0; i < queueSize; i++)
	{
		//calls threadsafe completeTask(), no need for mutex lock
		functor_wrapper_base* pTemp = m_queue.front();

		unique_lock.unlock();

		pTemp->invoke();

		//removes task
		unique_lock.lock();
		m_queue.pop();
	}
}

void eng::TaskQueue::wait()
{
	std::unique_lock<std::mutex> lock(m_queueMutex);
	if (!m_queue.empty())
	{
		return;
	}
	m_conditionVariable.wait(lock);
}

void eng::TaskQueue::wait_until(std::chrono::steady_clock::time_point t)
{
	std::unique_lock<std::mutex> lock(m_queueMutex);
	if (!m_queue.empty())
	{
		return;
	}
	m_conditionVariable.wait_until(lock, t);
}

void eng::TaskQueue::wait_for(std::chrono::steady_clock::duration d)
{
	std::unique_lock<std::mutex> lock(m_queueMutex);
	if (!m_queue.empty())
	{
		return;
	}
	m_conditionVariable.wait_for(lock, d);
}

bool eng::TaskQueue::empty()
{
	std::scoped_lock<std::mutex> lock(m_queueMutex);
	return m_queue.empty();
}

size_t eng::TaskQueue::size()
{
	std::scoped_lock<std::mutex> lock(m_queueMutex);
	return m_queue.size();
}

eng::TaskQueue::~TaskQueue()
{

}
