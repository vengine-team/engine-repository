#include <par/UpdateThread.h>
#include <assert.h>

const std::chrono::steady_clock::duration precision_margin = std::chrono::milliseconds(1);

bool eng::UpdateThread::updateElement::operator<(const updateElement & other) const
{
	return other.nextUpdate < nextUpdate;
}

bool eng::UpdateThread::updateElement::operator>(const updateElement & other) const
{
	return other.nextUpdate > nextUpdate;
}


std::shared_mutex eng::UpdateThread::s_allUpdateThreadsMutex;
std::unordered_map<std::thread::id, eng::UpdateThread*> eng::UpdateThread::s_allUpdateThreads;


bool eng::UpdateThread::addUpdateableTask(IUpdateable * pIUpdateable)
{
	for (auto i = m_pThreadUpdateables->begin(); i != m_pThreadUpdateables->end(); i++)
	{
		if (i->pIUpdateable == pIUpdateable)
		{
			return false;
		}
	}

	m_pThreadUpdateables->insert({ pIUpdateable, clock_t::now() });
	return true;
}

bool eng::UpdateThread::removeUpdateableTask(IUpdateable * pIUpdateable)
{
	for (auto i = m_pThreadUpdateables->begin(); i != m_pThreadUpdateables->end(); i++)
	{
		if (i->pIUpdateable == pIUpdateable)
		{
			m_pThreadUpdateables->erase(i);
			return true;
		}
	}
	return false;
}

void eng::UpdateThread::setEnabledTask(bool enabled)
{
	m_pThreadEnabled->store(enabled);
}

void eng::UpdateThread::terminateTask()
{
	*m_pThreadTerminate = true;
}

void eng::UpdateThread::updateThreadFunction(std::atomic<bool>* setupCompleteFlag)
{
	eng::TaskQueue taskQueue;
	std::set<updateElement, std::greater<updateElement>> updateables;
	std::atomic<bool> enabled;
	enabled.store(true);
	bool terminate = false;


	m_pThreadTaskQueue = &taskQueue;
	m_pThreadUpdateables = &updateables;
	m_pThreadEnabled = &enabled;
	m_pThreadTerminate = &terminate;

	//adds itself to the container containing all UpdateThreads
	{
		std::scoped_lock<std::shared_mutex> lock(s_allUpdateThreadsMutex);
		s_allUpdateThreads.insert({ std::this_thread::get_id(), this });
	}

	setupCompleteFlag->store(true);

	//checks if the cancel flag is true
	while (!terminate)
	{
		if (!updateables.empty() && enabled.load())
		{
			while (clock_t::now() < updateables.begin()->nextUpdate);

			IUpdateable* pCurrentUpdateable = updateables.begin()->pIUpdateable;
			clock_t::time_point nextUpdate;

			if (updateables.size() == 1)
			{
				// if there are no other updateables, the updateable 
				// has all the time in the world
				nextUpdate = pCurrentUpdateable->updateFrame
				(
					(clock_t::time_point::max)()
				);
			}
			else
			{
				auto i = updateables.begin();
				i++;
				// if there are multiple updateables, the next next updateable 
				// is given a time point when it should return
				nextUpdate = pCurrentUpdateable->updateFrame(i->nextUpdate);
			}
			// if the updateable is still in the set, 
			// it has not been removed and must be 
			// inserted again to change its time point value
			for (auto i = updateables.begin(); i != updateables.end(); i++)
			{
				if (i->pIUpdateable == pCurrentUpdateable)
				{
					updateables.erase(i);
					updateables.insert({ pCurrentUpdateable , nextUpdate });
					break;
				}
			}
			taskQueue.process();
			if (!updateables.empty())
			{
				while (clock_t::now() < updateables.begin()->nextUpdate - precision_margin)
				{
					taskQueue.wait_until(updateables.begin()->nextUpdate - precision_margin);
					taskQueue.process();
					if (updateables.empty())
					{
						break;
					}
				}
			}
		}
		else
		{	
			// returns immediatly if a task is already in the queue
			taskQueue.wait();
			taskQueue.process();
		}
	}
	// when the Update Thread finishes, 
	// removes itself from the container containing all UpdateThreads
	{
		std::scoped_lock<std::shared_mutex> lock(s_allUpdateThreadsMutex);
		s_allUpdateThreads.erase(std::this_thread::get_id());
	}
}



eng::UpdateThread* eng::UpdateThread::concurrentFindUpdateThread(std::thread::id id)
{
	s_allUpdateThreadsMutex.lock_shared();
	eng::UpdateThread* out;
	try
	{
		out = s_allUpdateThreads.at(id);
	}
	catch (const std::exception&)
	{
		out = nullptr;
	}

	s_allUpdateThreadsMutex.unlock_shared();
	
	return out;
}


bool eng::UpdateThread::concurrentGetEnabled() const
{
	return m_pThreadEnabled->load();
}

void eng::UpdateThread::concurrentSetEnabled(bool b)
{
	if (std::this_thread::get_id() == m_thread.get_id())
	{
		setEnabledTask(b);
	}
	else
	{
		std::condition_variable condition;
		std::mutex mutex;
		std::unique_lock<std::mutex> unique_lock(mutex);

		m_pThreadTaskQueue->concurrent_push
		(
			[&]()
			{
				std::scoped_lock<std::mutex> lock(mutex);
				UpdateThread::setEnabledTask(b);
				condition.notify_one();
			}
		);

		condition.wait(unique_lock);
	}
}

void eng::UpdateThread::concurrentRegisterUpdateable(IUpdateable * pIUpdateable)
{
	bool result;

	if (std::this_thread::get_id() == m_thread.get_id())
	{
		result = addUpdateableTask(pIUpdateable);
	}
	else
	{
		std::condition_variable condition;
		std::mutex mutex;
		std::unique_lock<std::mutex> unique_lock(mutex);

		m_pThreadTaskQueue->concurrent_push
		(
			[&]() 
			{ 
				std::scoped_lock<std::mutex> lock(mutex);  
				result = UpdateThread::addUpdateableTask(pIUpdateable); 
				condition.notify_one(); 
			}
		);

		condition.wait(unique_lock);
	}

	if (!result)
	{
		throw std::invalid_argument("Cannot register IUpdateable twice");
	}
}

void eng::UpdateThread::concurrentUnregisterUpdateable(IUpdateable * pIUpdateable)
{
	bool result;

	if (std::this_thread::get_id() == m_thread.get_id())
	{
		result = removeUpdateableTask(pIUpdateable);
	}
	else
	{
		std::condition_variable condition;
		std::mutex mutex;
		std::unique_lock<std::mutex> unique_lock(mutex);

		m_pThreadTaskQueue->concurrent_push
		(
			[&]()
			{
				std::scoped_lock<std::mutex> lock(mutex);
				result = UpdateThread::removeUpdateableTask(pIUpdateable);
				condition.notify_one();
			}
		);

		condition.wait(unique_lock);
	}

	if (!result)
	{
		throw std::invalid_argument("Cannot unregister IUpdateable that is not registered");
	}
}

void eng::UpdateThread::concurrentTerminate()
{
	if (std::this_thread::get_id() == m_thread.get_id())
	{
		terminateTask();
	}
	else
	{
		std::condition_variable condition;
		std::mutex mutex;
		std::unique_lock<std::mutex> unique_lock(mutex);

		m_pThreadTaskQueue->concurrent_push
		(
			[&]()
			{
				std::scoped_lock<std::mutex> lock(mutex);
				UpdateThread::terminateTask();
				condition.notify_one();
			}
		);

		condition.wait(unique_lock);
	}
}

bool eng::UpdateThread::joinable() const
{
	return m_thread.joinable();
}

void eng::UpdateThread::join()
{
	m_thread.join();
}


eng::UpdateThread::UpdateThread()
{
	std::atomic<bool> setupFlag;
	setupFlag.store(false);

	m_thread = std::thread(&UpdateThread::updateThreadFunction, this, &setupFlag);

	while (!setupFlag.load());
}

eng::UpdateThread::~UpdateThread()
{
	//terminate thread and join before deleting
	assert(!m_thread.joinable());
}

eng::UpdateThread * eng::this_UpdateThread()
{
	return eng::UpdateThread::concurrentFindUpdateThread(std::this_thread::get_id());
}
