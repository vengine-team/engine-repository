#pragma once
#include <gtm/geometry.h>

namespace gtm
{
	template<typename _numeric_t>
	struct angle_axis final
	{
		using numeric_t = _numeric_t;
		
		numeric_t angle;
		vector<numeric_t, 3> axis;
	};

	template<typename numeric_t, dim_t columns>
	inline matrix<numeric_t, 3, columns> transform(const angle_axis<numeric_t>& rot, const matrix<numeric_t, 3, columns>& m)
	{
		matrix<numeric_t, 3, columns> out;

		for (size_t i = 0; i < columns; i++)
		{
			vector<numeric_t, 3> _v = { m[0][i], m[1][i], m[2][i] };

			const numeric_t cos_a = cos(rot.angle);
			const numeric_t sin_a = sin(rot.angle);
			vector<numeric_t, 3> temp = cos_a * _v + sin_a * cross(rot.axis, _v) + (static_cast<numeric_t>(1) - cos_a) * dot(rot.axis, _v) * rot.axis;
			out[0][i] = temp[0];
			out[1][i] = temp[1];
			out[2][i] = temp[2];
		}

		return out;
	}

	template<typename numeric_t>
	inline matrix<numeric_t, 3, 3> to_matrix(const angle_axis<numeric_t>& a)
	{
		return transform(a, identity_matrix<double, 3, 3>());
	}

	using angle_axisf = angle_axis<float>;
	using angle_axisd = angle_axis<double>;




	template<typename _numeric_t>
	struct quaternion final
	{
		using numeric_t = _numeric_t;

		vector<numeric_t, 3> xyz;
		numeric_t w;

		constexpr quaternion(const numeric_t _w, const vector<numeric_t, 3>& _xyz) : xyz(_xyz), w(_w)
		{

		}
		explicit quaternion(const angle_axis<numeric_t>& rot) : xyz(rot * sin(rot.angle)), w(cos(rot.angle * static_cast<numeric_t>(0.5)))
		{

		}
	};

	template<typename numeric_t, dim_t columns>
	inline matrix<numeric_t, 3, columns> transform(const quaternion<numeric_t> rot, const matrix<numeric_t, 3, columns>& v)
	{
		vector<numeric_t, 3> cross_qVector_a = cross(rot.xyz, a);

		matrix<numeric_t, 3, columns> out;

		for (size_t i = 0; i < columns; i++)
		{
			vector<numeric_t, 3> temp = a + static_cast<numeric_t>(2) * rot.w * cross_qVector_a + static_cast<numeric_t>(2) * cross(rot.xyz, cross_qVector_a);
			out[0][i] = temp;
			out[1][i] = temp;
			out[2][i] = temp;
		}

		return out;
	}	

	template<typename numeric_t>
	inline matrix<numeric_t, 3, 3> to_matrix(const quaternion<numeric_t>& a)
	{
		return transform(a, identity_matrix<double, 3, 3>());
	}

	using angle_axisf = angle_axis<float>;
	using angle_axisd = angle_axis<double>;
}