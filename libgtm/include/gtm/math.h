#define _USE_MATH_DEFINES 
#include <math.h>

#pragma once // pragma once is here so that cmath gets included for _USE_MATH_DEFINES defines
#include <gtm/geometry.h>
#include <gtm/rotation3.h>

#include <type_traits>

namespace gtm
{
	template<typename base_t, typename exp_t>
	constexpr auto pow(base_t base, exp_t exp) -> decltype((base * exp) * exp)
	{
		static_assert(std::is_integral<base_t>::value, "base_t must be an integral");
		static_assert(std::is_integral<exp_t>::value, "exp_t must be an integral");

		decltype((base * exp) * exp) result = 1;
		for (;;)
		{
			if (exp & 1)
			{
				result *= base;
			}
			exp >>= 1;
			if (!exp)
			{
				break;
			}
			base *= base;
		}

		return result;
	}

	template<typename T>
	inline T clamp(const T& min, const T& max, const T& n)
	{
		if (n < min)
		{
			return min;
		}
		else if (n > max)
		{
			return max;
		}
		else
		{
			return n;
		}
	}

	template<typename T, typename scalar_t>
	inline T lerp(const T& a, const T& b, const scalar_t& t)
	{
		return a + (b - a) * t;
	}

	template<typename numeric_t, dim_t dimension>
	inline bool contains(const aligned_box<numeric_t, dimension>& space, const gtm::vector<numeric_t, dimension>& point)
	{
		for (size_t i = 0; i < dimension; i++)
		{
			if (point[i] < space.lower[i] || point[i] > space.upper[i])
			{
				return false;
			} 
		}
		return true;
	}
}