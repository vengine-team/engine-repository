#pragma once
#include <gtm/math.h>

namespace gtm
{
	template<typename T>
	inline T _perlin_smooth(const T& t)
	{
		return t * t * t * (t * (t * 6 - 15) + 10);
	}

	template<typename T>
	T _simplex_smooth(const T& value)
	{
		return value * 2f - 1;
	}

	template<typename T>
	T simplex_noise(const T& x)
	{
		int grid_x = static_cast<int>(std::floor(x));
		float grid_offset_x = x - grid_x;
		float f;

		return _simplex_smooth(x);
	}

	template<typename T>
	constexpr T _POW(T base, T expo)
	{
		return (expo != 0) ? base * _POW<T>(base, expo - 1) : 1;
	}

	template<typename numeric_t, int dim>
	struct hypercube
	{
		using vector_t = gtm::vector<numeric_t, dim>;

		std::array<vector_t, 2> _defVecs;

		constexpr int point_count() const
		{
			return _POW(2, dim);
		}

		vector_t operator[](size_t index) const
		{
			vector_t out;

			for (int i = 0; i < dim; i++)
			{
				const int n = _POW<int>(2, i + 1);

				int index_modulo_n = index % n;

				if (index_modulo_n >= n / 2)
				{
					out[i] = static_cast<numeric_t>(_defVecs[1][i]);
				}
				else
				{
					out[i] = static_cast<numeric_t>(_defVecs[0][i]);
				}
			}

			return out;
		}
	};

	template<typename T>
	inline T _perlin_hypercube_lerp(const std::array<T, 2>& edge_values, const vector<T, 1>& t)
	{
		return lerp(edge_values[0], edge_values[1], t[0]);
	}

	template<int dim>
	inline int _perlin_hash(const vector<int, dim>& pos)
	{
		constexpr std::array<int, 256> hash =
		{
			151,160,137, 91, 90, 15,131, 13,201, 95, 96, 53,194,233,  7,225,
			140, 36,103, 30, 69,142,  8, 99, 37,240, 21, 10, 23,190,  6,148,
			247,120,234, 75,  0, 26,197, 62, 94,252,219,203,117, 35, 11, 32,
			57,177, 33, 88,237,149, 56, 87,174, 20,125,136,171,168, 68,175,
			74,165, 71,134,139, 48, 27,166, 77,146,158,231, 83,111,229,122,
			60,211,133,230,220,105, 92, 41, 55, 46,245, 40,244,102,143, 54,
			65, 25, 63,161,  1,216, 80, 73,209, 76,132,187,208, 89, 18,169,
			200,196,135,130,116,188,159, 86,164,100,109,198,173,186,  3, 64,
			52,217,226,250,124,123,  5,202, 38,147,118,126,255, 82, 85,212,
			207,206, 59,227, 47, 16, 58, 17,182,189, 28, 42,223,183,170,213,
			119,248,152,  2, 44,154,163, 70,221,153,101,155,167, 43,172,  9,
			129, 22, 39,253, 19, 98,108,110, 79,113,224,232,178,185,112,104,
			218,246, 97,228,251, 34,242,193,238,210,144, 12,191,179,162,241,
			81, 51,145,235,249, 14,239,107, 49,192,214, 31,181,199,106,157,
			184, 84,204,176,115,121, 50, 45,127,  4,150,254,138,236,205, 93,
			222,114, 67, 29, 24, 72,243,141,128,195, 78, 66,215, 61,156,180
		};

		constexpr int hashMask = 255;


		int recursive_hash = hash[pos[0] & hashMask];
		for (size_t dim_i = 1; dim_i < dim; dim_i++)
		{
			recursive_hash = hash[recursive_hash + pos[dim_i] & hashMask];
		}

		return recursive_hash;
	}

	template<typename T, dim_t dim>
	inline T _perlin_hypercube_lerp(const std::array<T, _POW<dim_t>(2, dim)>& edge_values, const vector<T, dim>& t)
	{
		static_assert(dim > 1);

		constexpr size_t half_size = _POW<dim_t>(2, dim - 1);

		const std::array<T, half_size>* pEdgeValues = reinterpret_cast<const std::array<T, half_size>*>(edge_values.data());

		const gtm::vector<T, dim - 1>& t_one_dimension_less = *reinterpret_cast<const gtm::vector<T, dim - 1>*>(&t);

		const std::array<T, half_size>& first_half = *(pEdgeValues + 0);
		T first_lerpResult = _perlin_hypercube_lerp(first_half, t_one_dimension_less);
		const std::array<T, half_size>& second_half = *(pEdgeValues + 1);
		T last_lerpResult = _perlin_hypercube_lerp(second_half, t_one_dimension_less);

		return gtm::lerp(first_lerpResult, last_lerpResult, t[dim - 1]);
	}

	template<typename T, dim_t dim>
	T perlin(gtm::vector<T, dim> point)
	{
		using vector_t = vector<T, dim>;

		using grid_t = int;
		using grid_vector_t = vector<grid_t, dim>;


		hypercube<T, dim> gradient_hypercube = { vector_t::one(), vector_t::one() * static_cast<T>(-1) };
		std::array<vector_t, gradient_hypercube.point_count()> gradients;
		for (size_t i = 0; i < gradients.size(); i++)
		{
			gradients[i] = gradient_hypercube[i];
		}
		constexpr grid_t gradientsMask = gradient_hypercube.point_count() - 1;


		grid_vector_t floored_grid_point;//i0
		for (size_t i = 0; i < dim; i++)
		{
			floored_grid_point[i] = static_cast<grid_t>(std::floor(point[i]));
		}
		const grid_vector_t floored_grid_point1 = floored_grid_point + grid_vector_t::one();


		vector_t floored_grid_offset = point - static_cast<vector_t>(floored_grid_point);
		vector_t floored_grid_offset1 = floored_grid_offset - vector_t::one();

		hypercube<grid_t, dim> grid_cell = { floored_grid_point, floored_grid_point1 };
		std::array<vector_t, grid_cell.point_count()> hashed_corner_gradients;
		for (size_t corner_i = 0; corner_i < grid_cell.point_count(); corner_i++)
		{
			hashed_corner_gradients[corner_i] = gradient_hypercube[_perlin_hash<dim>(grid_cell[corner_i])];
		}

		hypercube<T, dim> position_cell = { floored_grid_offset, floored_grid_offset1 };
		std::array<T, position_cell.point_count()> dot_results;
		for (size_t gradient_i = 0; gradient_i < position_cell.point_count(); gradient_i++)
		{
			dot_results[gradient_i] = dot(hashed_corner_gradients[gradient_i], position_cell[gradient_i]);
		}

		vector_t smoothed_offset;
		for (size_t i = 0; i < dim; i++)
		{
			smoothed_offset[i] = _perlin_smooth(floored_grid_offset[i]);
		}

		return _perlin_hypercube_lerp(dot_results, smoothed_offset);
	}
}
