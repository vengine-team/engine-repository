#pragma once
#include <gtm/tensor.h>

namespace gtm
{
	template<typename _numeric_t, dim_t dimension>
	struct scaling final
	{
		vector<_numeric_t, dimension> vector;
	};

	template<typename numeric_t, dim_t dimension>
	void transform(const scaling<numeric_t, dimension>& scaling, const matrix<numeric_t, dimension, dimension>& m)
	{
		matrix<numeric_t, dimension, dimension> out = m;

		for (size_t i = 0; i < dimension; i++)
		{
			m[i] *= scaling.vector[i];
		}

		return m;
	}

	template<typename _numeric_t, dim_t dimension>
	class affine final
	{
	public:
		using numeric_t = _numeric_t;
	private:
		matrix<numeric_t, dimension, dimension> m_linear;
		vector<numeric_t, dimension> m_translation;
	public:
		matrix<numeric_t, dimension, dimension>& linear()
		{
			return m_linear;
		}
		const matrix<numeric_t, dimension, dimension>& linear() const
		{
			return m_linear;
		}

		vector<numeric_t, dimension>& translation()
		{
			return m_translation;
		}
		const vector<numeric_t, dimension>& translation() const
		{
			return m_translation;
		}

		vector<numeric_t, dimension>& transform(const vector<numeric_t, dimension>& point) const
		{
			return m_linear * point + m_translation;
		}

		constexpr affine(const matrix<numeric_t, dimension, dimension>& linear, const vector<numeric_t, dimension>& translation) : m_linear(linear), m_translation(translation)
		{
			
		}

		static affine<numeric_t, dimension> identity()
		{
			return { identity_matrix<numeric_t, 3,3>(), vector<numeric_t, 3>::zero() };
		}
	};

	template<typename numeric_t, dim_t dimension>
	matrix<numeric_t, dimension + 1, dimension + 1> to_matrix(const affine<numeric_t, dimension>& t)
	{
		matrix<numeric_t, dimension + 1, dimension + 1> out;

		for (size_t i = 0; i < dimension; i++)
		{
			out[i] = gtm::concat(t.linear()[i], t.translation()[i]);
		}

		for (size_t i = 0; i < dimension; i++)
		{
			out[dimension][i] = 0;
		}
		out[dimension][dimension] = 1.0;

		return out;
	}

	template<typename numeric_t, dim_t dim>
	affine<numeric_t, dim> transform(const affine<numeric_t, dim>& a, const affine<numeric_t, dim>& b)
	{
		return affine<numeric_t, dim>(matrix_mul(a.linear(), b.linear()), matrix_mul(a.linear(), b.translation()) + a.translation() );
	}

	template<typename numeric_t, dim_t dim, dim_t m, dim_t p>
	vector<numeric_t, dim> matrix_mul(const affine<numeric_t, dim>& transform, const matrix<numeric_t, m, p>& point)
	{
		matrix<numeric_t, n, p> out;

		for (dim_t dim_i = 0; dim_i < dim; dim_i++)
		{
			for (dim_t p_i = 0; p_i < p; p_i++)
			{
				for (dim_t m_i = 0; m_i < m; m_i++)
				{
					out[dim_i][p_i] += a[dim_i][m_i] * b[m_i, p_i];
				}
			}
		}
		for (dim_t p_i = 0; p_i < p; p_i++)
		{
			out[dim][p_i] += a[dim][m - 1] * b[m - 1, p_i];
		}

		return out;
	}

	using affine3d = affine<double, 3>;
	using affine3f = affine<float, 3>;
}