#pragma once
#include <string>
#include <array>

namespace gtm
{
	using dim_t = size_t;

	template<typename numeric_t, dim_t ...dimensions>
	struct tensor_proxy;

	template<typename numeric_t, dim_t dimension>
	struct tensor_proxy<numeric_t, dimension>
	{
		std::array<numeric_t&, dimension> m_refs;

		numeric_t& operator[](const dim_t& i)
		{
			return m_refs[i];
		}
	};

	template<typename numeric_t, dim_t dimension, dim_t ...other_dimensions>
	struct tensor_proxy<numeric_t, dimension, other_dimensions...>
	{
		std::array<tensor_proxy<numeric_t, other_dimensions...>, dimension> m_subproxys;

		numeric_t& operator[](const dim_t& i)
		{
			return m_subproxys[i];
		}
	};

	template<typename numeric_t, dim_t... dimensions>
	struct default_tensor_data;

	template<typename numeric_t, dim_t dimension>
	struct default_tensor_data<numeric_t, dimension>
	{
	private:
		std::array<numeric_t, dimension> m_elements;
	public:

		numeric_t& operator[](dim_t i)
		{
			return m_elements[i];
		}
		const numeric_t& operator[](dim_t i) const
		{
			return m_elements[i];
		}

		constexpr default_tensor_data(const std::array<numeric_t, dimension>& elements) : m_elements(elements)
		{

		}

		constexpr default_tensor_data()
		{

		}
	};

	template<typename numeric_t, dim_t dimension, dim_t ...other_dimensions>
	struct default_tensor_data<numeric_t, dimension, other_dimensions...>
	{
		static constexpr dim_t _size = default_tensor_data<numeric_t, other_dimensions...>::_size * dimension;
	private:
		union
		{
			std::array<default_tensor_data<numeric_t, other_dimensions...>, dimension> m_subtensors;
			std::array<numeric_t, default_tensor_data<numeric_t, other_dimensions...>::_size> m_all_elements;
		};
	public:

		tensor_proxy<numeric_t, other_dimensions...>& operator[](dim_t i)
		{
			return {}; m_subtensors[i];
		}
		const tensor_proxy<const numeric_t, other_dimensions...>& operator[](dim_t i) const
		{
			return {}; m_subtensors[i];
		}

		constexpr default_tensor_data(const std::array<numeric_t, _size>& elements) : m_all_elements(elements)
		{

		}
	};

#pragma region Array_Cast
	//this array cast snippet is from stack overflow
	template <std::size_t... Is>
	struct indices {};
	template <std::size_t N, std::size_t... Is>
	struct build_indices : build_indices<N - 1, N - 1, Is...> {};
	template <std::size_t... Is>
	struct build_indices<0, Is...> : indices<Is...> {};

	template<typename T, typename U, size_t i, size_t... Is>
	constexpr auto array_cast_helper(const std::array<U, i> &a, indices<Is...>)->std::array<T, i>
	{
		return { { static_cast<T>(std::get<Is>(a))... } };
	}

	template<typename T, typename U, size_t i>
	constexpr auto array_cast(const std::array<U, i> &a)->std::array<T, i>
	{
		// tag dispatch to helper with array indices
		return array_cast_helper<T>(a, build_indices<i>());
	}
#pragma endregion

	template<typename T>
	struct _carrier
	{
		T m_val;

		explicit constexpr _carrier(const T& val) : m_val(val)
		{

		}
	};

	template<typename numeric_t, dim_t... dimensions>
	struct tensor;

	template<typename _numeric_t, dim_t _dimension>
	struct tensor<_numeric_t, _dimension> final
	{
		static constexpr dim_t dimension = _dimension;

		using numeric_t = _numeric_t;

		static constexpr dim_t size()
		{
			return dimension;
		}


		std::array<numeric_t, dimension> m_elements;


		numeric_t & operator[](dim_t index)
		{
			return m_elements[index];
		}
		const numeric_t& operator[](dim_t index) const
		{
			return m_elements[index];
		}


		tensor<numeric_t, dimension> operator+(const tensor<numeric_t, dimension>& other) const
		{
			tensor<numeric_t, dimension> out;

			for (size_t i = 0; i < dimension; i++)
			{
				out[i] = (*this)[i] + other[i];
			}

			return out;
		}
		tensor<numeric_t, dimension> operator-(const tensor<numeric_t, dimension>& other) const
		{
			tensor<numeric_t, dimension> out;

			for (size_t i = 0; i < dimension; i++)
			{
				out[i] = (*this)[i] - other[i];
			}

			return out;
		}

		tensor<numeric_t, dimension>& operator+=(const tensor<numeric_t, dimension>& other)
		{
			for (size_t i = 0; i < dimension; i++)
			{
				(*this)[i] += other[i];
			}

			return *this;
		}
		tensor<numeric_t, dimension>& operator-=(const tensor<numeric_t, dimension>& other)
		{
			for (size_t i = 0; i < dimension; i++)
			{
				(*this)[i] -= other[i];
			}

			return *this;
		}


		tensor<numeric_t, dimension> operator*(const numeric_t& scalar) const
		{
			tensor<numeric_t, dimension> out;

			for (size_t i = 0; i < dimension; i++)
			{
				out[i] = (*this)[i] * scalar;
			}

			return out;
		}
		tensor<numeric_t, dimension> operator/(const numeric_t& scalar) const
		{
			tensor<numeric_t, dimension> out;

			for (size_t i = 0; i < dimension; i++)
			{
				out[i] = (*this)[i] / scalar;
			}

			return out;
		}
		
		tensor<numeric_t, dimension>& operator*=(const numeric_t& scalar)
		{
			for (size_t i = 0; i < dimension; i++)
			{
				m_elements[i] = (*this)[i] *= scalar;
			}

			return *this;
		}
		tensor<numeric_t, dimension>& operator/=(const numeric_t& scalar)
		{
			for (size_t i = 0; i < dimension; i++)
			{
				m_elements[i] = (*this)[i] /= scalar;
			}

			return *this;
		}


		bool operator==(const tensor<numeric_t, dimension>& other)
		{
			for (size_t i = 0; i < dimension; i++)
			{
				if ((*this)[i] != other[i])
				{
					return false;
				}
			}

			return true;
		}
		bool operator!=(const tensor<numeric_t, dimension>& other)
		{
			return !(*this == other);
		}


		static tensor<numeric_t, dimension> one()
		{
			tensor<numeric_t, dimension> out;
			for (size_t i = 0; i < dimension; i++)
			{
				out[i] = 1;
			}
			return out;
		}
		static tensor<numeric_t, dimension> zero()
		{
			tensor<numeric_t, dimension> out;
			for (size_t i = 0; i < dimension; i++)
			{
				out[i] = 0;
			}
			return out;
		}

		template<typename to_numeric_t>
		constexpr operator tensor<to_numeric_t, dimension>() const
		{
			return tensor<to_numeric_t, dimension>{ array_cast<to_numeric_t>(m_elements) };
		}
		//constexpr operator std::array<numeric_t, dimension>() const
		//{
		//	return m_elements;
		//}

		//constexpr tensor()
		//{

		//}
		//constexpr tensor(const tensor<numeric_t, dimension>& v) : m_elements(v.m_elements)
		//{

		//}
		//tensor<numeric_t, dimension>& operator=(const tensor<numeric_t, dimension>& v)
		//{
		//	m_elements = v.m_elements;
		//	return *this;
		//}

		//constexpr tensor(const _carrier<std::array<numeric_t, dimension>>& c) : m_elements(c.m_val)
		//{

		//}

		//template<typename First, typename Second, typename ...Args>
		//constexpr tensor(First, Second, Args...);
	};

	//template<typename _numeric_t>
	//struct tensor<_numeric_t, 3> final
	//{
	//	static constexpr dim_t dimension = 3;

	//	using numeric_t = _numeric_t;

	//	static constexpr dim_t size()
	//	{
	//		return dimension;
	//	}

	//	union
	//	{
	//		struct
	//		{
	//			numeric_t x;
	//			numeric_t y;
	//			numeric_t z;
	//		};
	//		std::array<numeric_t, dimension> m_elements;
	//	};


	//	numeric_t & operator[](dim_t index)
	//	{
	//		return m_elements[index];
	//	}
	//	const numeric_t& operator[](dim_t index) const
	//	{
	//		return m_elements[index];
	//	}


	//	tensor<numeric_t, dimension> operator+(const tensor<numeric_t, dimension>& other) const
	//	{
	//		tensor<numeric_t, dimension> out;

	//		for (size_t i = 0; i < dimension; i++)
	//		{
	//			out[i] = (*this)[i] + other[i];
	//		}

	//		return out;
	//	}
	//	tensor<numeric_t, dimension> operator-(const tensor<numeric_t, dimension>& other) const
	//	{
	//		tensor<numeric_t, dimension> out;

	//		for (size_t i = 0; i < dimension; i++)
	//		{
	//			out[i] = (*this)[i] - other[i];
	//		}

	//		return out;
	//	}

	//	tensor<numeric_t, dimension>& operator+=(const tensor<numeric_t, dimension>& other)
	//	{
	//		for (size_t i = 0; i < dimension; i++)
	//		{
	//			(*this)[i] += other[i];
	//		}

	//		return *this;
	//	}
	//	tensor<numeric_t, dimension>& operator-=(const tensor<numeric_t, dimension>& other)
	//	{
	//		for (size_t i = 0; i < dimension; i++)
	//		{
	//			(*this)[i] -= other[i];
	//		}

	//		return *this;
	//	}


	//	tensor<numeric_t, dimension> operator*(const numeric_t& scalar) const
	//	{
	//		tensor<numeric_t, dimension> out;

	//		for (size_t i = 0; i < dimension; i++)
	//		{
	//			out[i] = (*this)[i] * scalar;
	//		}

	//		return out;
	//	}
	//	tensor<numeric_t, dimension> operator/(const numeric_t& scalar) const
	//	{
	//		tensor<numeric_t, dimension> out;

	//		for (size_t i = 0; i < dimension; i++)
	//		{
	//			out[i] = (*this)[i] / scalar;
	//		}

	//		return out;
	//	}

	//	tensor<numeric_t, dimension>& operator*=(const numeric_t& scalar)
	//	{
	//		for (size_t i = 0; i < dimension; i++)
	//		{
	//			(*this)[i] = (*this)[i] *= scalar;
	//		}

	//		return *this;
	//	}
	//	tensor<numeric_t, dimension>& operator/=(const numeric_t& scalar)
	//	{
	//		for (size_t i = 0; i < dimension; i++)
	//		{
	//			(*this)[i] = (*this)[i] /= scalar;
	//		}

	//		return *this;
	//	}


	//	bool operator==(const tensor<numeric_t, dimension>& other)
	//	{
	//		for (size_t i = 0; i < dimension; i++)
	//		{
	//			if ((*this)[i] != other[i])
	//			{
	//				return false;
	//			}
	//		}

	//		return true;
	//	}
	//	bool operator!=(const tensor<numeric_t, dimension>& other)
	//	{
	//		return !(*this == other);
	//	}


	//	static tensor<numeric_t, dimension> one()
	//	{
	//		tensor<numeric_t, dimension> out;
	//		for (size_t i = 0; i < dimension; i++)
	//		{
	//			out[i] = 1;
	//		}
	//		return out;
	//	}
	//	static tensor<numeric_t, dimension> zero()
	//	{
	//		tensor<numeric_t, dimension> out;
	//		for (size_t i = 0; i < dimension; i++)
	//		{
	//			out[i] = 0;
	//		}
	//		return out;
	//	}

	//	template<typename to_numeric_t>
	//	constexpr operator tensor<to_numeric_t, dimension>() const
	//	{
	//		return tensor<to_numeric_t, dimension>{ _carrier<std::array<to_numeric_t, dimension>>(array_cast<to_numeric_t>(m_elements)) };
	//	}
	//	constexpr operator std::array<numeric_t, dimension>() const
	//	{
	//		return m_elements;
	//	}

	//	constexpr tensor()
	//	{

	//	}
	//	constexpr tensor(const tensor<numeric_t, dimension>& v) : m_elements(v.m_elements)
	//	{

	//	}
	//	tensor<numeric_t, dimension>& operator=(const tensor<numeric_t, dimension>& v)
	//	{
	//		m_elements = v.m_elements;
	//		return *this;
	//	}

	//	constexpr tensor(const _carrier<std::array<numeric_t, dimension>>& c) : m_elements(c.m_val)
	//	{

	//	}

	//	template<typename First, typename Second, typename ...Args>
	//	constexpr tensor(First, Second, Args...);
	//};

	//template<typename _numeric_t>
	//struct tensor<_numeric_t, 2> final
	//{
	//	static constexpr dim_t dimension = 2;

	//	using numeric_t = _numeric_t;

	//	static constexpr dim_t size()
	//	{
	//		return dimension;
	//	}

	//	union
	//	{
	//		struct
	//		{
	//			numeric_t x;
	//			numeric_t y;
	//		};
	//		std::array<numeric_t, dimension> m_elements;
	//	};


	//	numeric_t & operator[](dim_t index)
	//	{
	//		return m_elements[index];
	//	}
	//	const numeric_t& operator[](dim_t index) const
	//	{
	//		return m_elements[index];
	//	}


	//	tensor<numeric_t, dimension> operator+(const tensor<numeric_t, dimension>& other) const
	//	{
	//		tensor<numeric_t, dimension> out;

	//		for (size_t i = 0; i < dimension; i++)
	//		{
	//			out[i] = (*this)[i] + other[i];
	//		}

	//		return out;
	//	}
	//	tensor<numeric_t, dimension> operator-(const tensor<numeric_t, dimension>& other) const
	//	{
	//		tensor<numeric_t, dimension> out;

	//		for (size_t i = 0; i < dimension; i++)
	//		{
	//			out[i] = (*this)[i] - other[i];
	//		}

	//		return out;
	//	}

	//	tensor<numeric_t, dimension>& operator+=(const tensor<numeric_t, dimension>& other)
	//	{
	//		for (size_t i = 0; i < dimension; i++)
	//		{
	//			(*this)[i] += other[i];
	//		}

	//		return *this;
	//	}
	//	tensor<numeric_t, dimension>& operator-=(const tensor<numeric_t, dimension>& other)
	//	{
	//		for (size_t i = 0; i < dimension; i++)
	//		{
	//			(*this)[i] -= other[i];
	//		}

	//		return *this;
	//	}



	//	tensor<numeric_t, dimension> operator*(const numeric_t& scalar) const
	//	{
	//		tensor<numeric_t, dimension> out;

	//		for (size_t i = 0; i < dimension; i++)
	//		{
	//			out[i] = (*this)[i] * scalar;
	//		}

	//		return out;
	//	}
	//	tensor<numeric_t, dimension> operator/(const numeric_t& scalar) const
	//	{
	//		tensor<numeric_t, dimension> out;

	//		for (size_t i = 0; i < dimension; i++)
	//		{
	//			out[i] = (*this)[i] / scalar;
	//		}

	//		return out;
	//	}

	//	tensor<numeric_t, dimension>& operator*=(const numeric_t& scalar)
	//	{
	//		for (size_t i = 0; i < dimension; i++)
	//		{
	//			(*this)[i] = (*this)[i] *= scalar;
	//		}

	//		return *this;
	//	}
	//	tensor<numeric_t, dimension>& operator/=(const numeric_t& scalar)
	//	{
	//		for (size_t i = 0; i < dimension; i++)
	//		{
	//			(*this)[i] = (*this)[i] /= scalar;
	//		}

	//		return *this;
	//	}



	//	bool operator==(const tensor<numeric_t, dimension>& other)
	//	{
	//		for (size_t i = 0; i < dimension; i++)
	//		{
	//			if ((*this)[i] != other[i])
	//			{
	//				return false;
	//			}
	//		}

	//		return true;
	//	}
	//	bool operator!=(const tensor<numeric_t, dimension>& other)
	//	{
	//		return !(*this == other);
	//	}


	//	static tensor<numeric_t, dimension> one()
	//	{
	//		tensor<numeric_t, dimension> out;
	//		for (size_t i = 0; i < dimension; i++)
	//		{
	//			out[i] = 1;
	//		}
	//		return out;
	//	}
	//	static tensor<numeric_t, dimension> zero()
	//	{
	//		tensor<numeric_t, dimension> out;
	//		for (size_t i = 0; i < dimension; i++)
	//		{
	//			out[i] = 0;
	//		}
	//		return out;
	//	}

	//	template<typename to_numeric_t>
	//	constexpr operator tensor<to_numeric_t, dimension>() const
	//	{
	//		return tensor<to_numeric_t, dimension>{ _carrier<std::array<to_numeric_t, dimension>>(array_cast<to_numeric_t>(m_elements)) };
	//	}
	//	constexpr operator std::array<numeric_t, dimension>() const
	//	{
	//		return m_elements;
	//	}

	//	constexpr tensor()
	//	{

	//	}
	//	constexpr tensor(const tensor<numeric_t, dimension>& v) : m_elements(v.m_elements)
	//	{

	//	}
	//	tensor<numeric_t, dimension>& operator=(const tensor<numeric_t, dimension>& v)
	//	{
	//		m_elements = v.m_elements;
	//		return *this;
	//	}

	//	constexpr tensor(const _carrier<std::array<numeric_t, dimension>>& c) : m_elements(c.m_val)
	//	{

	//	}

	//	template<typename First, typename Second, typename ...Args>
	//	constexpr tensor(First, Second, Args...);
	//};

#pragma region black magic


	template<typename T, size_t array_size, size_t range_left, typename ...Args>
	struct _add_number_to_array_helper;
	template<typename T, size_t array_size, typename ...Args>
	struct _add_number_to_array_helper<T, array_size, 0, Args...>
	{
		std::array<T, array_size + 1> composed;

		constexpr _add_number_to_array_helper(const std::array<T, array_size>& array_ref, T to_add, Args... args) :
			composed{ args..., to_add }
		{

		}
	};
	template<typename T, size_t array_size, size_t array_size_minus_index, typename ...Args>
	struct _add_number_to_array_helper : public _add_number_to_array_helper<T, array_size, array_size_minus_index - 1, Args..., T>
	{
		static constexpr size_t index = array_size - array_size_minus_index;

		constexpr _add_number_to_array_helper(const std::array<T, array_size>& array_ref, T to_add, Args... args) :
			_add_number_to_array_helper<T, array_size, array_size_minus_index - 1, Args..., T>(array_ref, to_add, args..., array_ref[index])
		{

		}
	};
	template<typename T, size_t array_size>
	struct _add_number_to_array : public _add_number_to_array_helper<T, array_size, array_size>
	{
		constexpr _add_number_to_array(const std::array<T, array_size>& array_ref, T to_add) :
			_add_number_to_array_helper<T, array_size, array_size>(array_ref, to_add)
		{

		}
	};


	template<typename T, size_t first_size, typename other_T, size_t second_size, size_t second_size_minus_index>
	struct _combine_arrays_helper;
	template<typename T, size_t first_size, typename other_T, size_t second_size>
	struct _combine_arrays_helper<T, first_size, other_T, second_size, 0>
	{
		std::array<T, first_size> composed;

		constexpr _combine_arrays_helper(const std::array<T, first_size>& first, const std::array<other_T, second_size>& second) :
			composed(first)
		{

		}
	};
	template<typename T, size_t first_size, typename other_T, size_t second_size, size_t second_size_minus_index>
	struct _combine_arrays_helper : public _combine_arrays_helper<T, first_size + 1, other_T, second_size, second_size_minus_index - 1>
	{
		static constexpr size_t second_index = second_size - second_size_minus_index;

		constexpr _combine_arrays_helper(const std::array<T, first_size>& first, const std::array<other_T, second_size>& second) :
			_combine_arrays_helper<T, first_size + 1, other_T, second_size, second_size_minus_index - 1>(_add_number_to_array<T, first_size>(first, static_cast<T>(second[second_index])).composed, second)
		{

		}
	};
	template<typename T, size_t first_size, typename other_T, size_t second_size>
	struct _combine_arrays : public _combine_arrays_helper<T, first_size, other_T, second_size, second_size>
	{
		constexpr _combine_arrays(const std::array<T, first_size>& first, const std::array<other_T, second_size>& second) :
			_combine_arrays_helper<T, first_size, other_T, second_size, second_size>(first, second)
		{

		}
	};


	template<typename T, size_t stored_size, typename ...values>
	struct _compose_helper;
	template<typename T, size_t stored_size, typename other_T>
	struct _compose_helper<T, stored_size, other_T>
	{
		std::array<T, stored_size + 1> composed;

		constexpr _compose_helper(const std::array<T, stored_size>& stored_array_ref, other_T last) :
			composed(_add_number_to_array<T, stored_size>(stored_array_ref, static_cast<T>(last)).composed)
		{

		}
	};
	template<typename T, size_t stored_size, typename other_t, size_t vector_size>
	struct _compose_helper<T, stored_size, tensor<other_t, vector_size>>
	{
		std::array<T, stored_size + vector_size> composed;

		constexpr _compose_helper(const std::array<T, stored_size>& stored_array_ref, const tensor<other_t, vector_size>& last_ref) :
			composed(_combine_arrays<T, stored_size, other_t, vector_size>(stored_array_ref, last_ref).composed)
		{

		}
	};
	template<typename T, size_t stored_size, typename current_t, typename ...values>
	struct _compose_helper<T, stored_size, current_t, values...> : public _compose_helper<T, stored_size + 1, values...>
	{
		constexpr _compose_helper(const std::array<T, stored_size>& stored_array_ref, current_t current, values... tail_values) :
			_compose_helper<T, stored_size + 1, values...>(_add_number_to_array<T, stored_size>(stored_array_ref, static_cast<T>(current)).composed, tail_values...)
		{

		}
	};
	template<typename T, size_t stored_size, typename other_t, size_t vector_size, typename ...values>
	struct _compose_helper<T, stored_size, tensor<other_t, vector_size>, values...> : public _compose_helper<T, stored_size + vector_size, values...>
	{
		constexpr _compose_helper(const std::array<T, stored_size>& stored_array_ref, const tensor<other_t, vector_size>& current_ref, values... tail_values) :
			_compose_helper<T, stored_size + vector_size, values...>(_combine_arrays<T, stored_size, other_t, vector_size>(stored_array_ref, current_ref).composed, tail_values...)
		{

		}
	};
	template<typename T, typename ...Args>
	struct _compose : public _compose_helper<T, 0, Args...>
	{
		constexpr _compose(Args... args) :
			_compose_helper<T, 0, Args...>({}, args...)
		{

		}
	};


	template<typename t, typename ...args>
	struct _test_arg_validity;
	template<typename t>
	struct _test_arg_validity<t, t>
	{
		static constexpr bool value = true;
	};
	template<typename t, size_t vector_size>
	struct _test_arg_validity<t, tensor<t, vector_size>>
	{
		static constexpr bool value = true;
	};
	template<typename t, typename last>
	struct _test_arg_validity<t, last>
	{
		static constexpr bool value = false;
	};
	template<typename t, typename ...tail>
	struct _test_arg_validity<t, t, tail...> : public _test_arg_validity<t, tail...>
	{

	};
	template<typename t, size_t vector_size, typename ...tail>
	struct _test_arg_validity<t, tensor<t, vector_size>, tail...> : public _test_arg_validity<t, tail...>
	{

	};
	template<typename t, typename last, typename ...tail>
	struct _test_arg_validity<t, last, tail...>
	{
		static constexpr bool value = false;
	};


	template<typename T, typename ...Args>
	struct _count_numbers_helper;
	template<typename T>
	struct _count_numbers_helper<T, T>
	{
		size_t count;

		constexpr _count_numbers_helper(size_t counted) : count(counted + 1)
		{

		}
	};
	template<typename T, size_t vector_size>
	struct _count_numbers_helper<T, tensor<T, vector_size>>
	{
		size_t count;

		constexpr _count_numbers_helper(size_t counted) : count(counted + array_size)
		{

		}
	};
	template<typename T, typename ...Tail>
	struct _count_numbers_helper<T, T, Tail...> : public _count_numbers_helper<T, Tail...>
	{
		constexpr _count_numbers_helper(size_t counted) : _count_numbers_helper<T, Tail...>(counted + 1)
		{

		}
	};
	template<typename T, size_t vector_size, typename ...Tail>
	struct _count_numbers_helper<T, tensor<T, vector_size>, Tail...> : public _count_numbers_helper<T, Tail...>
	{
		constexpr _count_numbers_helper(size_t counted) : _count_numbers_helper<T, Tail...>(counted + vector_size)
		{

		}
	};
	template<typename T, typename ...Args>
	struct _count_numbers : public _count_numbers_helper<T, Args...>
	{
		constexpr _count_numbers() : _count_numbers_helper<T, Args...>(0)
		{

		}
	};


	template<typename T, size_t expected_size, typename ...Args>
	constexpr std::array<T, expected_size> _compose_no_errors(Args... args)
	{
		/*if constexpr (_test_arg_validity<T, Args...>::value)
		{
		if constexpr (_count_numbers<T, Args...>().count == expected_size)
		{*/
		return _compose<T, Args...>(args...).composed;
		/*	}
		else
		{
		return std::array<T, expected_size>{ };
		}
		}
		else
		{
		return std::array<T, expected_size>{ };
		}*/
	}


#pragma endregion

	//template<typename _numeric_t, dim_t dimension>
	//template<typename First, typename Second, typename ...Args>
	//inline constexpr tensor<_numeric_t, dimension>::tensor(First first, Second second, Args ...args) : m_elements(_compose_no_errors<_numeric_t, dimension, First, Second, Args...>(first, second, args...))
	//{
	//	/*if constexpr (_test_arg_validity<_numeric_t, Args...>::value)
	//	{
	//	static_assert(_count_numbers<_numeric_t, Args...>().count == dimension, "eng::tensor<numeric_t, dimension>::tensor(Args...): the arguments evaluated to an invalid number of elements");
	//	}
	//	else
	//	{
	//	static_assert(false, "eng::tensor<numeric_t, dimension>::tensor(Args...): invalid arguments");
	//	}*/
	//}

	//template<typename _numeric_t>
	//template<typename First, typename Second, typename ...Args>
	//inline constexpr tensor<_numeric_t, 3>::tensor(First first, Second second, Args ...args) : m_elements(_compose_no_errors<_numeric_t, 3, First, Second, Args...>(first, second, args...))
	//{
	//	/*if constexpr (_test_arg_validity<_numeric_t, Args...>::value)
	//	{
	//	static_assert(_count_numbers<_numeric_t, Args...>().count == dimension, "eng::tensor<numeric_t, dimension>::tensor(Args...): the arguments evaluated to an invalid number of elements");
	//	}
	//	else
	//	{
	//	static_assert(false, "eng::tensor<numeric_t, dimension>::tensor(Args...): invalid arguments");
	//	}*/
	//}

	//template<typename _numeric_t>
	//template<typename First, typename Second, typename ...Args>
	//inline constexpr tensor<_numeric_t, 2>::tensor(First first, Second second, Args ...args) : m_elements(_compose_no_errors<_numeric_t, 2, First, Second, Args...>(first, second, args...))
	//{
	//	/*if constexpr (_test_arg_validity<_numeric_t, Args...>::value)
	//	{
	//	static_assert(_count_numbers<_numeric_t, Args...>().count == dimension, "eng::tensor<numeric_t, dimension>::tensor(Args...): the arguments evaluated to an invalid number of elements");
	//	}
	//	else
	//	{
	//	static_assert(false, "eng::tensor<numeric_t, dimension>::tensor(Args...): invalid arguments");
	//	}*/
	//}


	template<typename _numeric_t, dim_t dimension, dim_t ...otherDimensions>
	struct tensor<_numeric_t, dimension, otherDimensions...> final
	{
		using numeric_t = _numeric_t;

		static constexpr dim_t size()
		{
			return dimension * tensor<numeric_t, otherDimensions...>::size();
		}

		std::array<tensor<numeric_t, otherDimensions...>, dimension> m_elements;

		tensor<numeric_t, otherDimensions...>& operator[](dim_t index)
		{
			return m_elements[index];
		}
		const tensor<numeric_t, otherDimensions...>& operator[](dim_t index) const
		{
			return m_elements[index];
		}


		tensor<numeric_t, dimension, otherDimensions...> operator+(const tensor<numeric_t, dimension, otherDimensions...>& other) const
		{
			tensor<numeric_t, dimension, otherDimensions...> out;

			for (size_t i = 0; i < dimension; i++)
			{
				out[i] = (*this)[i] + other[i];
			}

			return out;
		}
		tensor<numeric_t, dimension, otherDimensions...> operator-(const tensor<numeric_t, dimension, otherDimensions...>& other) const
		{
			tensor<numeric_t, dimension, otherDimensions...> out;

			for (size_t i = 0; i < dimension; i++)
			{
				out[i] = (*this)[i] - other[i];
			}

			return out;
		}

		tensor<numeric_t, dimension, otherDimensions...>& operator+=(const tensor<numeric_t, dimension, otherDimensions...>& other)
		{
			for (size_t i = 0; i < dimension; i++)
			{
				(*this)[i] += other[i];
			}

			return *this;
		}
		tensor<numeric_t, dimension, otherDimensions...>& operator-=(const tensor<numeric_t, dimension, otherDimensions...>& other)
		{
			for (size_t i = 0; i < dimension; i++)
			{
				(*this)[i] -= other[i];
			}

			return *this;
		}


		tensor<numeric_t, dimension, otherDimensions...> operator*(const numeric_t& scalar) const
		{
			tensor<numeric_t, dimension, otherDimensions...> out;

			for (size_t i = 0; i < dimension; i++)
			{
				out[i] = (*this)[i] * scalar;
			}

			return out;
		}
		tensor<numeric_t, dimension, otherDimensions...> operator/(const numeric_t& scalar) const
		{
			tensor<numeric_t, dimension, otherDimensions...> out;

			for (size_t i = 0; i < dimension; i++)
			{
				out[i] = (*this)[i] / scalar;
			}

			return out;
		}


		bool operator==(const tensor<numeric_t, dimension, otherDimensions...>& other)
		{
			for (size_t i = 0; i < dimension; i++)
			{
				if ((*this)[i] != other[i])
				{
					return false;
				}
			}

			return true;
		}
		bool operator!=(const tensor<numeric_t, dimension, otherDimensions...>& other)
		{
			return !(this == other);
		}

		static tensor<numeric_t, dimension, otherDimensions...> one()
		{
			tensor<numeric_t, dimension, otherDimensions...> out;
			for (size_t i = 0; i < dimension; i++)
			{
				out[i] = tensor<numeric_t, otherDimensions...>::one();
			}
			return out;
		}
		static tensor<numeric_t, dimension, otherDimensions...> zero()
		{
			tensor<numeric_t, dimension, otherDimensions...> out;
			for (size_t i = 0; i < dimension; i++)
			{
				out[i] = tensor<numeric_t, otherDimensions...>::zero();
			}
			return out;
		}

		template<typename to_numeric_t>
		operator tensor<to_numeric_t, dimension, otherDimensions...>() const
		{
			tensor<to_numeric_t, dimension, otherDimensions...> out;

			for (size_t i = 0; i < dimension; i++)
			{
				out[i] = static_cast<tensor<to_numeric_t, otherDimensions...>>((*this)[i]);
			}

			return out;
		}
	};

	template<typename numeric_t, dim_t... dimensions>
	tensor<numeric_t, dimensions...> operator*(const numeric_t& scalar, const tensor<numeric_t, dimensions...>& t)
	{
		return t * scalar;
	}

	template<typename numeric_t, dim_t dimension_b, dim_t ... dimensions>
	constexpr tensor<numeric_t, 1 + dimension_b, dimensions...> concat(const numeric_t& a, const tensor<numeric_t, dimension_b, dimensions...> b)
	{
		return { _combine_arrays<numeric_t, 1, numeric_t, dimension_b>({ a }, b.m_elements).composed };
	}

	template<typename numeric_t, dim_t dimension_a, dim_t ... dimensions>
	constexpr tensor<numeric_t, dimension_a + 1, dimensions...> concat(const tensor<numeric_t, dimension_a, dimensions...>& a, const numeric_t b)
	{
		return { _combine_arrays<numeric_t, dimension_a, numeric_t, 1>(a.m_elements, { b }).composed };
	}

	template<typename numeric_t, dim_t dimension_a, dim_t dimension_b, dim_t ... dimensions>
	constexpr tensor<numeric_t, dimension_a + dimension_b, dimensions...> concat(const tensor<numeric_t, dimension_a, dimensions...>& a, const tensor<numeric_t, dimension_b, dimensions...> b)
	{
		return { _combine_arrays<numeric_t, dimension_a, numeric_t, dimension_b>(a.m_elements, b.m_elements).composed };
	}

	template<typename numeric_t, dim_t dimension_a, typename ...tail, dim_t ... dimensions>
	constexpr auto concat(const tensor<numeric_t, dimension_a, dimensions...>& a, tail... _tail) -> tensor<numeric_t, dimension_a + decltype(concat(_tail...))::size(), dimensions...>
	{
		return concat(a, concat(_tail...));
	}

	template<typename numeric_t, dim_t rows, dim_t columns>
	using matrix = tensor<numeric_t, rows, columns>;

	template<typename numeric_t, dim_t dimension>
	using vector = tensor<numeric_t, dimension>;

	//floating poinypes
	using vector3d = vector<double, 3>;
	using vector3f = vector<float, 3>;

	//integer types
	using vector3i = vector<int, 3>;
	using vector3u = vector<unsigned int, 3>;



	//floating poinypes
	using vector2d = vector<double, 2>;
	using vector2f = vector<float, 2>;

	//integer types
	using vector2i = vector<int, 2>;
	using vector2u = vector<unsigned int, 2>;
};

namespace std
{
	template<typename numeric_t, size_t dimension>
	std::string to_string(const gtm::vector<numeric_t, dimension>& v)
	{
		std::string out = "";

		if (dimension > 0)
		{
			out.append(std::to_string(v[0]));

			for (size_t i = 1; i < dimension; i++)
			{
				out.append(" / ");
				out.append(std::to_string(v[i]));
			}
		}

		return out;
	}
	template<typename numeric_t, size_t dimension>
	std::wstring to_wstring(const gtm::vector<numeric_t, dimension>& v)
	{
		std::wstring out = L"";

		if (dimension > 0)
		{
			out.append(std::to_wstring(v[0]));

			for (size_t i = 1; i < dimension; i++)
			{
				out.append(L" / ");
				out.append(std::to_wstring(v[i]));
			}
		}

		return out;
	}
}
