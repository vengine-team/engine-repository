#pragma once
#include <gtm/transform.h>


namespace gtm
{
	template<typename numeric_t, dim_t r, dim_t c>
	struct _column_major_matrix_storage
	{
		std::array<std::array<numeric_t, c>, r> m_stored;

		_column_major_matrix_storage(const matrix<numeric_t, r, c>& m)
		{
			for (size_t r_i = 0; r_i < r; r_i++)
			{
				for (size_t c_i = 0; c_i < c; c_i++)
				{
					m_stored[r_i][c_i] = m[r_i][c_i];
				}
			}
		}
	};

	////
	//template<typename numeric_t>
	//inline numeric_t angle(const Eigen::Matrix<numeric_t, 3, 1>& a, const Eigen::Matrix<numeric_t, 3, 1>& b)
	//{
	//	return cos(a.dot(b) / a.norm() * b.norm());
	//}
	////

	template<typename numeric_t>
	inline vector<numeric_t, 3> cross(const vector<numeric_t, 3>& a, const vector<numeric_t, 3>& b)
	{
		vector<numeric_t, 3> out;
		out[0] = a[1] * b[2] - a[2] * b[1];
		out[1] = a[2] * b[0] - a[0] * b[2];
		out[2] = a[0] * b[1] - a[1] * b[0];
		return out;
	}

	template<typename numeric_t, dim_t dimension>
	inline numeric_t dot(const vector<numeric_t, dimension>& a, const vector<numeric_t, dimension>& b)
	{
		numeric_t out = 0;
		for (dim_t i = 0; i < dimension; i++)
		{
			out += a[i] * b[i];
		}
		return out;
	}

	template<typename numeric_t, dim_t dimension>
	inline numeric_t magnitude(const vector<numeric_t, dimension>& v)
	{
		numeric_t sum = 0;

		for (dim_t i = 0; i < dimension; i++)
		{
			sum += v[i] * v[i];
		}

		return sqrt(sum);
	}

	template<typename numeric_t, dim_t dimension>
	inline vector<numeric_t, dimension> normalize(const vector<numeric_t, dimension>& v)
	{
		return v / magnitude(v);
	}

	template<typename numeric_t>
	inline numeric_t angle(const vector<numeric_t, 3>& a, const vector<numeric_t, 3>& b)
	{
		return cos(dot(a, b) / magnitude(a) * magnitude(b));
	}

	template<typename numeric_t, dim_t rows, dim_t columns>
	inline matrix<numeric_t, rows, columns> identity_matrix()
	{
		matrix<numeric_t, rows, columns> out = matrix<numeric_t, rows, columns>::zero();
		
		if (rows < columns)
		{
			for (dim_t i = 0; i < columns; i++)
			{
				out[i][i] = 1;
			}
		}
		else
		{
			for (dim_t i = 0; i < rows; i++)
			{
				out[i][i] = 1;
			}
		}

		return out;
	}

	template<typename numeric_t, dim_t n, dim_t m, dim_t p>
	inline matrix<numeric_t, n, p> matrix_mul(const matrix<numeric_t, n, m>& a, const matrix<numeric_t, m, p>& b)
	{
		matrix<numeric_t, n, p> out;

		for (dim_t n_i = 0; n_i < n; n_i++)
		{
			for (dim_t p_i = 0; p_i < p; p_i++)
			{
				out[n_i][p_i] = 0;

				for (dim_t m_i = 0; m_i < m; m_i++)
				{
					out[n_i][p_i] += a[n_i][m_i] * b[m_i][p_i];
				}
			}
		}

		return out;
	}

	template<typename numeric_t, dim_t n, dim_t m>
	inline vector<numeric_t, n> matrix_mul(const matrix<numeric_t, n, m>& a, const vector<numeric_t, m>& b)
	{
		vector<numeric_t, n> out;

		for (dim_t n_i = 0; n_i < n; n_i++)
		{
			out[n_i] = 0;

			for (dim_t m_i = 0; m_i < m; m_i++)
			{
				out[n_i] += a[n_i][m_i] * b[m_i];
			}
		}

		return out;
	}


	template<typename numeric_t, dim_t n, dim_t m, dim_t p>
	inline matrix<numeric_t, n, p> transform(const matrix<numeric_t, n, m>& a, const matrix<numeric_t, m, p>& b)
	{
		return matrix_mul(a, b);
	}

	template<typename numeric_t, dim_t n, dim_t m>
	inline vector<numeric_t, n> transform(const matrix<numeric_t, n, m>& a, const vector<numeric_t, m>& b)
	{
		return matrix_mul(a, b);
	}


	template<typename _numeric_t, dim_t dimension>
	struct aligned_box;

	template<typename _numeric_t, dim_t dimension>
	struct aligned_box final
	{
		using numeric_t = _numeric_t;

		vector<numeric_t, dimension> lower;
		vector<numeric_t, dimension> upper;

		aligned_box()
		{

		}

		aligned_box(vector<numeric_t, dimension> _lower, vector<numeric_t, dimension> _upper)
		{
			lower = _lower;
			upper = _upper;
		}

		template<typename to_numeric_t>
		operator aligned_box<to_numeric_t, dimension>() const
		{
			return { static_cast<vector<to_numeric_t, dimension>>(lower), static_cast<vector<to_numeric_t, dimension>>(upper) };
		}
	};

	template<typename _numeric_t>
	struct aligned_box<_numeric_t, 2> final
	{
		using numeric_t = _numeric_t;
		union
		{
			struct
			{
				numeric_t left;
				numeric_t top;
				numeric_t right;
				numeric_t bottom;
			};

			struct
			{
				vector<numeric_t, 2> lower;
				vector<numeric_t, 2> upper;
			};
		};

		aligned_box()
		{

		}

		aligned_box(vector<numeric_t, 2> _lower, vector<numeric_t, 2> _upper)
		{
			lower = _lower;
			upper = _upper;
		}

		template<typename to_numeric_t>
		operator aligned_box<to_numeric_t, 2>() const
		{
			return { static_cast<vector<to_numeric_t, 2>>(lower), static_cast<vector<to_numeric_t, 2>>(upper) };
		}
	};

	template<typename numeric_t>
	using rectangle = aligned_box<numeric_t, 2>;
}